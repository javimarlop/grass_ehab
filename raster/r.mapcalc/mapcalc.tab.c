
/* A Bison parser, made by GNU Bison 2.4.1.  */

/* Skeleton implementation for Bison's Yacc-like parsers in C
   
      Copyright (C) 1984, 1989, 1990, 2000, 2001, 2002, 2003, 2004, 2005, 2006
   Free Software Foundation, Inc.
   
   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.  */

/* As a special exception, you may create a larger work that contains
   part or all of the Bison parser skeleton and distribute that work
   under terms of your choice, so long as that work isn't itself a
   parser generator using the skeleton or a modified version thereof
   as a parser skeleton.  Alternatively, if you modify or redistribute
   the parser skeleton itself, you may (at your option) remove this
   special exception, which will cause the skeleton and the resulting
   Bison output files to be licensed under the GNU General Public
   License without this special exception.
   
   This special exception was added by the Free Software Foundation in
   version 2.2 of Bison.  */

/* C LALR(1) parser skeleton written by Richard Stallman, by
   simplifying the original so-called "semantic" parser.  */

/* All symbols defined below should begin with yy or YY, to avoid
   infringing on user name space.  This should be done even for local
   variables, as they might otherwise be expanded by user macros.
   There are some unavoidable exceptions within include files to
   define necessary library symbols; they are noted "INFRINGES ON
   USER NAME SPACE" below.  */

/* Identify Bison output.  */
#define YYBISON 1

/* Bison version.  */
#define YYBISON_VERSION "2.4.1"

/* Skeleton name.  */
#define YYSKELETON_NAME "yacc.c"

/* Pure parsers.  */
#define YYPURE 0

/* Push parsers.  */
#define YYPUSH 0

/* Pull parsers.  */
#define YYPULL 1

/* Using locations.  */
#define YYLSP_NEEDED 0



/* Copy the first part of user declarations.  */

/* Line 189 of yacc.c  */
#line 2 "mapcalc.y"


#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdarg.h>

#include <unistd.h>

#include "mapcalc.h"

#define YYDEBUG 1
#define YYERROR_VERBOSE 1

static int syntax_error_occurred;



/* Line 189 of yacc.c  */
#line 92 "mapcalc.tab.c"

/* Enabling traces.  */
#ifndef YYDEBUG
# define YYDEBUG 0
#endif

/* Enabling verbose error messages.  */
#ifdef YYERROR_VERBOSE
# undef YYERROR_VERBOSE
# define YYERROR_VERBOSE 1
#else
# define YYERROR_VERBOSE 0
#endif

/* Enabling the token table.  */
#ifndef YYTOKEN_TABLE
# define YYTOKEN_TABLE 0
#endif


/* Tokens.  */
#ifndef YYTOKENTYPE
# define YYTOKENTYPE
   /* Put the tokens into the symbol table, so that GDB and other debuggers
      know about them.  */
   enum yytokentype {
     VARNAME = 258,
     NAME = 259,
     VARSTRING = 260,
     STRING = 261,
     INTEGER = 262,
     FLOAT = 263,
     DOUBLE = 264,
     GT = 265,
     GE = 266,
     LT = 267,
     LE = 268,
     EQ = 269,
     NE = 270,
     LOGAND = 271,
     LOGOR = 272,
     LOGAND2 = 273,
     LOGOR2 = 274,
     BITAND = 275,
     BITOR = 276,
     LSH = 277,
     RSH = 278,
     RSHU = 279
   };
#endif
/* Tokens.  */
#define VARNAME 258
#define NAME 259
#define VARSTRING 260
#define STRING 261
#define INTEGER 262
#define FLOAT 263
#define DOUBLE 264
#define GT 265
#define GE 266
#define LT 267
#define LE 268
#define EQ 269
#define NE 270
#define LOGAND 271
#define LOGOR 272
#define LOGAND2 273
#define LOGOR2 274
#define BITAND 275
#define BITOR 276
#define LSH 277
#define RSH 278
#define RSHU 279




#if ! defined YYSTYPE && ! defined YYSTYPE_IS_DECLARED
typedef union YYSTYPE
{

/* Line 214 of yacc.c  */
#line 20 "mapcalc.y"

	int ival;
	double fval;
	char *sval;
	expression *exp;
	expr_list *list;



/* Line 214 of yacc.c  */
#line 186 "mapcalc.tab.c"
} YYSTYPE;
# define YYSTYPE_IS_TRIVIAL 1
# define yystype YYSTYPE /* obsolescent; will be withdrawn */
# define YYSTYPE_IS_DECLARED 1
#endif


/* Copy the second part of user declarations.  */

/* Line 264 of yacc.c  */
#line 71 "mapcalc.y"


static expr_list *result;

extern int yylex(void);

int yyparse(void);
void yyerror(char *s);



/* Line 264 of yacc.c  */
#line 210 "mapcalc.tab.c"

#ifdef short
# undef short
#endif

#ifdef YYTYPE_UINT8
typedef YYTYPE_UINT8 yytype_uint8;
#else
typedef unsigned char yytype_uint8;
#endif

#ifdef YYTYPE_INT8
typedef YYTYPE_INT8 yytype_int8;
#elif (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
typedef signed char yytype_int8;
#else
typedef short int yytype_int8;
#endif

#ifdef YYTYPE_UINT16
typedef YYTYPE_UINT16 yytype_uint16;
#else
typedef unsigned short int yytype_uint16;
#endif

#ifdef YYTYPE_INT16
typedef YYTYPE_INT16 yytype_int16;
#else
typedef short int yytype_int16;
#endif

#ifndef YYSIZE_T
# ifdef __SIZE_TYPE__
#  define YYSIZE_T __SIZE_TYPE__
# elif defined size_t
#  define YYSIZE_T size_t
# elif ! defined YYSIZE_T && (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
#  include <stddef.h> /* INFRINGES ON USER NAME SPACE */
#  define YYSIZE_T size_t
# else
#  define YYSIZE_T unsigned int
# endif
#endif

#define YYSIZE_MAXIMUM ((YYSIZE_T) -1)

#ifndef YY_
# if YYENABLE_NLS
#  if ENABLE_NLS
#   include <libintl.h> /* INFRINGES ON USER NAME SPACE */
#   define YY_(msgid) dgettext ("bison-runtime", msgid)
#  endif
# endif
# ifndef YY_
#  define YY_(msgid) msgid
# endif
#endif

/* Suppress unused-variable warnings by "using" E.  */
#if ! defined lint || defined __GNUC__
# define YYUSE(e) ((void) (e))
#else
# define YYUSE(e) /* empty */
#endif

/* Identity function, used to suppress warnings about constant conditions.  */
#ifndef lint
# define YYID(n) (n)
#else
#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static int
YYID (int yyi)
#else
static int
YYID (yyi)
    int yyi;
#endif
{
  return yyi;
}
#endif

#if ! defined yyoverflow || YYERROR_VERBOSE

/* The parser invokes alloca or malloc; define the necessary symbols.  */

# ifdef YYSTACK_USE_ALLOCA
#  if YYSTACK_USE_ALLOCA
#   ifdef __GNUC__
#    define YYSTACK_ALLOC __builtin_alloca
#   elif defined __BUILTIN_VA_ARG_INCR
#    include <alloca.h> /* INFRINGES ON USER NAME SPACE */
#   elif defined _AIX
#    define YYSTACK_ALLOC __alloca
#   elif defined _MSC_VER
#    include <malloc.h> /* INFRINGES ON USER NAME SPACE */
#    define alloca _alloca
#   else
#    define YYSTACK_ALLOC alloca
#    if ! defined _ALLOCA_H && ! defined _STDLIB_H && (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
#     include <stdlib.h> /* INFRINGES ON USER NAME SPACE */
#     ifndef _STDLIB_H
#      define _STDLIB_H 1
#     endif
#    endif
#   endif
#  endif
# endif

# ifdef YYSTACK_ALLOC
   /* Pacify GCC's `empty if-body' warning.  */
#  define YYSTACK_FREE(Ptr) do { /* empty */; } while (YYID (0))
#  ifndef YYSTACK_ALLOC_MAXIMUM
    /* The OS might guarantee only one guard page at the bottom of the stack,
       and a page size can be as small as 4096 bytes.  So we cannot safely
       invoke alloca (N) if N exceeds 4096.  Use a slightly smaller number
       to allow for a few compiler-allocated temporary stack slots.  */
#   define YYSTACK_ALLOC_MAXIMUM 4032 /* reasonable circa 2006 */
#  endif
# else
#  define YYSTACK_ALLOC YYMALLOC
#  define YYSTACK_FREE YYFREE
#  ifndef YYSTACK_ALLOC_MAXIMUM
#   define YYSTACK_ALLOC_MAXIMUM YYSIZE_MAXIMUM
#  endif
#  if (defined __cplusplus && ! defined _STDLIB_H \
       && ! ((defined YYMALLOC || defined malloc) \
	     && (defined YYFREE || defined free)))
#   include <stdlib.h> /* INFRINGES ON USER NAME SPACE */
#   ifndef _STDLIB_H
#    define _STDLIB_H 1
#   endif
#  endif
#  ifndef YYMALLOC
#   define YYMALLOC malloc
#   if ! defined malloc && ! defined _STDLIB_H && (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
void *malloc (YYSIZE_T); /* INFRINGES ON USER NAME SPACE */
#   endif
#  endif
#  ifndef YYFREE
#   define YYFREE free
#   if ! defined free && ! defined _STDLIB_H && (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
void free (void *); /* INFRINGES ON USER NAME SPACE */
#   endif
#  endif
# endif
#endif /* ! defined yyoverflow || YYERROR_VERBOSE */


#if (! defined yyoverflow \
     && (! defined __cplusplus \
	 || (defined YYSTYPE_IS_TRIVIAL && YYSTYPE_IS_TRIVIAL)))

/* A type that is properly aligned for any stack member.  */
union yyalloc
{
  yytype_int16 yyss_alloc;
  YYSTYPE yyvs_alloc;
};

/* The size of the maximum gap between one aligned stack and the next.  */
# define YYSTACK_GAP_MAXIMUM (sizeof (union yyalloc) - 1)

/* The size of an array large to enough to hold all stacks, each with
   N elements.  */
# define YYSTACK_BYTES(N) \
     ((N) * (sizeof (yytype_int16) + sizeof (YYSTYPE)) \
      + YYSTACK_GAP_MAXIMUM)

/* Copy COUNT objects from FROM to TO.  The source and destination do
   not overlap.  */
# ifndef YYCOPY
#  if defined __GNUC__ && 1 < __GNUC__
#   define YYCOPY(To, From, Count) \
      __builtin_memcpy (To, From, (Count) * sizeof (*(From)))
#  else
#   define YYCOPY(To, From, Count)		\
      do					\
	{					\
	  YYSIZE_T yyi;				\
	  for (yyi = 0; yyi < (Count); yyi++)	\
	    (To)[yyi] = (From)[yyi];		\
	}					\
      while (YYID (0))
#  endif
# endif

/* Relocate STACK from its old location to the new one.  The
   local variables YYSIZE and YYSTACKSIZE give the old and new number of
   elements in the stack, and YYPTR gives the new location of the
   stack.  Advance YYPTR to a properly aligned location for the next
   stack.  */
# define YYSTACK_RELOCATE(Stack_alloc, Stack)				\
    do									\
      {									\
	YYSIZE_T yynewbytes;						\
	YYCOPY (&yyptr->Stack_alloc, Stack, yysize);			\
	Stack = &yyptr->Stack_alloc;					\
	yynewbytes = yystacksize * sizeof (*Stack) + YYSTACK_GAP_MAXIMUM; \
	yyptr += yynewbytes / sizeof (*yyptr);				\
      }									\
    while (YYID (0))

#endif

/* YYFINAL -- State number of the termination state.  */
#define YYFINAL  13
/* YYLAST -- Last index in YYTABLE.  */
#define YYLAST   177

/* YYNTOKENS -- Number of terminals.  */
#define YYNTOKENS  49
/* YYNNTS -- Number of nonterminals.  */
#define YYNNTS  27
/* YYNRULES -- Number of rules.  */
#define YYNRULES  84
/* YYNRULES -- Number of states.  */
#define YYNSTATES  144

/* YYTRANSLATE(YYLEX) -- Bison symbol number corresponding to YYLEX.  */
#define YYUNDEFTOK  2
#define YYMAXUTOK   279

#define YYTRANSLATE(YYX)						\
  ((unsigned int) (YYX) <= YYMAXUTOK ? yytranslate[YYX] : YYUNDEFTOK)

/* YYTRANSLATE[YYLEX] -- Bison symbol number corresponding to YYLEX.  */
static const yytype_uint8 yytranslate[] =
{
       0,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,    41,     2,    31,     2,    45,     2,     2,
      38,    39,    43,    46,    35,    34,     2,    44,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,    48,    25,
       2,    26,     2,    47,    27,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,    36,     2,    37,    42,     2,     2,     2,    30,     2,
       2,     2,     2,    29,     2,    33,     2,     2,     2,     2,
       2,     2,     2,     2,    28,     2,     2,     2,     2,     2,
       2,    32,     2,     2,     2,     2,    40,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     1,     2,     3,     4,
       5,     6,     7,     8,     9,    10,    11,    12,    13,    14,
      15,    16,    17,    18,    19,    20,    21,    22,    23,    24
};

#if YYDEBUG
/* YYPRHS[YYN] -- Index of the first RHS symbol of rule number YYN in
   YYRHS.  */
static const yytype_uint16 yyprhs[] =
{
       0,     0,     3,     5,     7,    10,    14,    18,    22,    26,
      28,    30,    32,    36,    38,    40,    42,    44,    46,    48,
      50,    52,    55,    57,    61,    63,    65,    70,    77,    86,
      88,    94,   102,   112,   115,   119,   124,   128,   130,   132,
     134,   136,   138,   140,   142,   145,   148,   151,   153,   157,
     159,   163,   167,   171,   173,   177,   181,   183,   187,   191,
     195,   197,   201,   205,   209,   213,   215,   219,   223,   225,
     229,   231,   235,   237,   241,   245,   247,   251,   255,   257,
     263,   265,   269,   271,   273
};

/* YYRHS -- A `-1'-separated list of the rules' RHS.  */
static const yytype_int8 yyrhs[] =
{
      50,     0,    -1,    51,    -1,    52,    -1,    52,    25,    -1,
      52,    25,    51,    -1,     1,    25,    51,    -1,     6,    26,
      74,    -1,     4,    26,    74,    -1,    59,    -1,     6,    -1,
       4,    -1,    75,    27,    75,    -1,    27,    -1,    28,    -1,
      29,    -1,    30,    -1,    31,    -1,    32,    -1,    33,    -1,
       7,    -1,    34,     7,    -1,    74,    -1,    74,    35,    56,
      -1,     5,    -1,     3,    -1,    53,    36,    55,    37,    -1,
      53,    36,    55,    35,    55,    37,    -1,    53,    36,    55,
      35,    55,    35,    55,    37,    -1,    53,    -1,    54,    53,
      36,    55,    37,    -1,    54,    53,    36,    55,    35,    55,
      37,    -1,    54,    53,    36,    55,    35,    55,    35,    55,
      37,    -1,    54,    53,    -1,    75,    38,    39,    -1,    75,
      38,    56,    39,    -1,    38,    74,    39,    -1,    57,    -1,
      58,    -1,    59,    -1,     7,    -1,     8,    -1,     9,    -1,
      60,    -1,    34,    60,    -1,    40,    60,    -1,    41,    60,
      -1,    61,    -1,    61,    42,    62,    -1,    62,    -1,    63,
      43,    62,    -1,    63,    44,    62,    -1,    63,    45,    62,
      -1,    63,    -1,    64,    46,    63,    -1,    64,    34,    63,
      -1,    64,    -1,    65,    22,    64,    -1,    65,    23,    64,
      -1,    65,    24,    64,    -1,    65,    -1,    66,    10,    65,
      -1,    66,    11,    65,    -1,    66,    12,    65,    -1,    66,
      13,    65,    -1,    66,    -1,    67,    14,    66,    -1,    67,
      15,    66,    -1,    67,    -1,    68,    20,    67,    -1,    68,
      -1,    69,    21,    68,    -1,    69,    -1,    70,    16,    69,
      -1,    70,    18,    69,    -1,    70,    -1,    71,    17,    70,
      -1,    71,    19,    70,    -1,    71,    -1,    71,    47,    72,
      48,    72,    -1,    72,    -1,    75,    26,    73,    -1,    73,
      -1,     4,    -1,     3,    -1
};

/* YYRLINE[YYN] -- source line where rule number YYN was defined.  */
static const yytype_uint8 yyrline[] =
{
       0,    84,    84,    87,    88,    89,    90,    93,    94,    95,
      98,    99,   100,   103,   104,   105,   106,   107,   108,   109,
     112,   113,   116,   117,   120,   121,   124,   125,   127,   129,
     130,   132,   134,   136,   139,   140,   144,   145,   146,   147,
     148,   149,   150,   153,   154,   155,   156,   159,   160,   163,
     164,   165,   166,   169,   170,   171,   174,   175,   176,   177,
     180,   181,   182,   183,   184,   186,   187,   188,   191,   192,
     195,   196,   199,   200,   201,   204,   205,   206,   209,   210,
     214,   215,   218,   221,   222
};
#endif

#if YYDEBUG || YYERROR_VERBOSE || YYTOKEN_TABLE
/* YYTNAME[SYMBOL-NUM] -- String name of the symbol SYMBOL-NUM.
   First, the terminals, then, starting at YYNTOKENS, nonterminals.  */
static const char *const yytname[] =
{
  "$end", "error", "$undefined", "VARNAME", "NAME", "VARSTRING", "STRING",
  "INTEGER", "FLOAT", "DOUBLE", "GT", "GE", "LT", "LE", "EQ", "NE",
  "LOGAND", "LOGOR", "LOGAND2", "LOGOR2", "BITAND", "BITOR", "LSH", "RSH",
  "RSHU", "';'", "'='", "'@'", "'r'", "'g'", "'b'", "'#'", "'y'", "'i'",
  "'-'", "','", "'['", "']'", "'('", "')'", "'~'", "'!'", "'^'", "'*'",
  "'/'", "'%'", "'+'", "'?'", "':'", "$accept", "program", "defs", "def",
  "map", "mapmod", "index", "expr_list", "atom_var", "atom_map",
  "atom_func", "exp_atom", "exp_pre", "exp_pow", "exp_mul", "exp_add",
  "exp_sh", "exp_cmp", "exp_eq", "exp_bitand", "exp_bitor", "exp_logand",
  "exp_logor", "exp_cond", "exp_let", "exp", "name", 0
};
#endif

# ifdef YYPRINT
/* YYTOKNUM[YYLEX-NUM] -- Internal token number corresponding to
   token YYLEX-NUM.  */
static const yytype_uint16 yytoknum[] =
{
       0,   256,   257,   258,   259,   260,   261,   262,   263,   264,
     265,   266,   267,   268,   269,   270,   271,   272,   273,   274,
     275,   276,   277,   278,   279,    59,    61,    64,   114,   103,
      98,    35,   121,   105,    45,    44,    91,    93,    40,    41,
     126,    33,    94,    42,    47,    37,    43,    63,    58
};
# endif

/* YYR1[YYN] -- Symbol number of symbol that rule YYN derives.  */
static const yytype_uint8 yyr1[] =
{
       0,    49,    50,    51,    51,    51,    51,    52,    52,    52,
      53,    53,    53,    54,    54,    54,    54,    54,    54,    54,
      55,    55,    56,    56,    57,    57,    58,    58,    58,    58,
      58,    58,    58,    58,    59,    59,    60,    60,    60,    60,
      60,    60,    60,    61,    61,    61,    61,    62,    62,    63,
      63,    63,    63,    64,    64,    64,    65,    65,    65,    65,
      66,    66,    66,    66,    66,    67,    67,    67,    68,    68,
      69,    69,    70,    70,    70,    71,    71,    71,    72,    72,
      73,    73,    74,    75,    75
};

/* YYR2[YYN] -- Number of symbols composing right hand side of rule YYN.  */
static const yytype_uint8 yyr2[] =
{
       0,     2,     1,     1,     2,     3,     3,     3,     3,     1,
       1,     1,     3,     1,     1,     1,     1,     1,     1,     1,
       1,     2,     1,     3,     1,     1,     4,     6,     8,     1,
       5,     7,     9,     2,     3,     4,     3,     1,     1,     1,
       1,     1,     1,     1,     2,     2,     2,     1,     3,     1,
       3,     3,     3,     1,     3,     3,     1,     3,     3,     3,
       1,     3,     3,     3,     3,     1,     3,     3,     1,     3,
       1,     3,     1,     3,     3,     1,     3,     3,     1,     5,
       1,     3,     1,     1,     1
};

/* YYDEFACT[STATE-NAME] -- Default rule to reduce with in state
   STATE-NUM when YYTABLE doesn't specify something else to do.  Zero
   means the default is an error.  */
static const yytype_uint8 yydefact[] =
{
       0,     0,    84,    83,     0,     0,     2,     3,     9,     0,
       0,     0,     0,     1,     0,     0,     6,    25,    11,    24,
      10,    40,    41,    42,    13,    14,    15,    16,    17,    18,
      19,     0,     0,     0,     0,    29,     0,    37,    38,    39,
      43,    47,    49,    53,    56,    60,    65,    68,    70,    72,
      75,    78,    80,    82,     8,     0,     7,     5,    34,     0,
      22,    44,     0,     0,    45,    46,     0,    33,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,    35,     0,    36,    20,     0,     0,     0,
      48,    50,    51,    52,    55,    54,    57,    58,    59,    61,
      62,    63,    64,    66,    67,    69,    71,    73,    74,    76,
      77,     0,    81,    83,    12,    23,    21,     0,    26,     0,
       0,     0,     0,    30,    79,     0,    27,     0,     0,     0,
      31,    28,     0,    32
};

/* YYDEFGOTO[NTERM-NUM].  */
static const yytype_int8 yydefgoto[] =
{
      -1,     5,     6,     7,    35,    36,    98,    59,    37,    38,
      39,    40,    41,    42,    43,    44,    45,    46,    47,    48,
      49,    50,    51,    52,    53,    60,    62
};

/* YYPACT[STATE-NUM] -- Index in YYTABLE of the portion describing
   STATE-NUM.  */
#define YYPACT_NINF -99
static const yytype_int16 yypact[] =
{
     116,   -17,   -99,     9,    12,    26,   -99,    65,   -99,    73,
     116,    55,    55,   -99,   106,    16,   -99,   -10,    13,   -99,
     -99,   -99,   -99,   -99,   -99,   -99,   -99,   -99,   -99,   -99,
     -99,    70,    55,    70,    70,    69,   132,   -99,   -99,   -99,
     -99,    79,   -99,    22,   -16,   117,   113,    38,   134,   141,
     129,   -14,   -99,   -99,   -99,    42,   -99,   -99,   -99,    98,
     130,   -99,    43,   127,   -99,   -99,    -3,   131,   142,    55,
      55,    55,    55,    55,    55,    55,    55,    55,    55,    55,
      55,    55,    55,    55,    55,    55,    55,    55,    55,    55,
      55,    55,    68,   -99,    55,   -99,   -99,   161,   111,    -3,
     -99,   -99,   -99,   -99,    22,    22,   -16,   -16,   -16,   117,
     117,   117,   117,   113,   113,    38,   134,   141,   141,   129,
     129,   122,   -99,   -99,   -99,   -99,   -99,    -3,   -99,   114,
      55,   115,    -3,   -99,   -99,    -3,   -99,   118,   135,    -3,
     -99,   -99,   136,   -99
};

/* YYPGOTO[NTERM-NUM].  */
static const yytype_int16 yypgoto[] =
{
     -99,   -99,    -1,   -99,   138,   -99,   -98,    77,   -99,   -99,
     104,    82,   -99,    58,    83,    67,    53,    76,    91,    92,
      74,    75,   -99,   -88,    85,    -5,     0
};

/* YYTABLE[YYPACT[STATE-NUM]].  What to do in state STATE-NUM.  If
   positive, shift that token.  If negative, reduce the rule which
   number is the opposite.  If zero, do what YYDEFACT says.
   If YYTABLE_NINF, syntax error.  */
#define YYTABLE_NINF -85
static const yytype_int16 yytable[] =
{
       9,   129,   121,    88,    96,    89,    54,    56,    10,    16,
       9,    55,    55,    57,     9,    55,   -84,   -84,    73,    17,
      18,    19,    20,    21,    22,    23,    13,    63,   -84,   131,
      74,    97,    55,    90,   137,    11,    68,   138,    12,   -83,
     -83,   142,   134,    24,    25,    26,    27,    28,    29,    30,
      31,   -83,    82,    83,    32,    58,    33,    34,    17,    18,
      19,    20,    21,    22,    23,    70,    71,    72,    91,    92,
      92,     2,   123,    17,    18,    19,    20,    21,    22,    23,
      15,    15,    24,    25,    26,    27,    28,    29,    30,    31,
      14,    55,   124,    32,    55,    33,    34,    24,    25,    26,
      27,    28,    29,    30,     8,    66,    -4,     1,    32,     2,
       3,    15,     4,    61,     8,    64,    65,     1,     8,     2,
       3,    69,     4,    78,    79,    80,    81,   100,   101,   102,
     103,   109,   110,   111,   112,     2,    18,    93,    20,    75,
      76,    77,   106,   107,   108,    86,   127,    87,   128,   132,
     135,   133,   136,   139,    84,   140,   104,   105,   113,   114,
     117,   118,    85,   119,   120,    94,    95,    99,   126,    92,
     130,   125,   141,   143,    67,   115,   122,   116
};

static const yytype_uint8 yycheck[] =
{
       0,    99,    90,    17,     7,    19,    11,    12,    25,    10,
      10,    11,    12,    14,    14,    15,    26,    27,    34,     3,
       4,     5,     6,     7,     8,     9,     0,    32,    38,   127,
      46,    34,    32,    47,   132,    26,    36,   135,    26,    26,
      27,   139,   130,    27,    28,    29,    30,    31,    32,    33,
      34,    38,    14,    15,    38,    39,    40,    41,     3,     4,
       5,     6,     7,     8,     9,    43,    44,    45,    26,    27,
      27,     3,     4,     3,     4,     5,     6,     7,     8,     9,
      38,    38,    27,    28,    29,    30,    31,    32,    33,    34,
      25,    91,    92,    38,    94,    40,    41,    27,    28,    29,
      30,    31,    32,    33,     0,    36,     0,     1,    38,     3,
       4,    38,     6,    31,    10,    33,    34,     1,    14,     3,
       4,    42,     6,    10,    11,    12,    13,    69,    70,    71,
      72,    78,    79,    80,    81,     3,     4,    39,     6,    22,
      23,    24,    75,    76,    77,    16,    35,    18,    37,    35,
      35,    37,    37,    35,    20,    37,    73,    74,    82,    83,
      86,    87,    21,    88,    89,    35,    39,    36,     7,    27,
      48,    94,    37,    37,    36,    84,    91,    85
};

/* YYSTOS[STATE-NUM] -- The (internal number of the) accessing
   symbol of state STATE-NUM.  */
static const yytype_uint8 yystos[] =
{
       0,     1,     3,     4,     6,    50,    51,    52,    59,    75,
      25,    26,    26,     0,    25,    38,    51,     3,     4,     5,
       6,     7,     8,     9,    27,    28,    29,    30,    31,    32,
      33,    34,    38,    40,    41,    53,    54,    57,    58,    59,
      60,    61,    62,    63,    64,    65,    66,    67,    68,    69,
      70,    71,    72,    73,    74,    75,    74,    51,    39,    56,
      74,    60,    75,    74,    60,    60,    36,    53,    75,    42,
      43,    44,    45,    34,    46,    22,    23,    24,    10,    11,
      12,    13,    14,    15,    20,    21,    16,    18,    17,    19,
      47,    26,    27,    39,    35,    39,     7,    34,    55,    36,
      62,    62,    62,    62,    63,    63,    64,    64,    64,    65,
      65,    65,    65,    66,    66,    67,    68,    69,    69,    70,
      70,    72,    73,     4,    75,    56,     7,    35,    37,    55,
      48,    55,    35,    37,    72,    35,    37,    55,    55,    35,
      37,    37,    55,    37
};

#define yyerrok		(yyerrstatus = 0)
#define yyclearin	(yychar = YYEMPTY)
#define YYEMPTY		(-2)
#define YYEOF		0

#define YYACCEPT	goto yyacceptlab
#define YYABORT		goto yyabortlab
#define YYERROR		goto yyerrorlab


/* Like YYERROR except do call yyerror.  This remains here temporarily
   to ease the transition to the new meaning of YYERROR, for GCC.
   Once GCC version 2 has supplanted version 1, this can go.  */

#define YYFAIL		goto yyerrlab

#define YYRECOVERING()  (!!yyerrstatus)

#define YYBACKUP(Token, Value)					\
do								\
  if (yychar == YYEMPTY && yylen == 1)				\
    {								\
      yychar = (Token);						\
      yylval = (Value);						\
      yytoken = YYTRANSLATE (yychar);				\
      YYPOPSTACK (1);						\
      goto yybackup;						\
    }								\
  else								\
    {								\
      yyerror (YY_("syntax error: cannot back up")); \
      YYERROR;							\
    }								\
while (YYID (0))


#define YYTERROR	1
#define YYERRCODE	256


/* YYLLOC_DEFAULT -- Set CURRENT to span from RHS[1] to RHS[N].
   If N is 0, then set CURRENT to the empty location which ends
   the previous symbol: RHS[0] (always defined).  */

#define YYRHSLOC(Rhs, K) ((Rhs)[K])
#ifndef YYLLOC_DEFAULT
# define YYLLOC_DEFAULT(Current, Rhs, N)				\
    do									\
      if (YYID (N))                                                    \
	{								\
	  (Current).first_line   = YYRHSLOC (Rhs, 1).first_line;	\
	  (Current).first_column = YYRHSLOC (Rhs, 1).first_column;	\
	  (Current).last_line    = YYRHSLOC (Rhs, N).last_line;		\
	  (Current).last_column  = YYRHSLOC (Rhs, N).last_column;	\
	}								\
      else								\
	{								\
	  (Current).first_line   = (Current).last_line   =		\
	    YYRHSLOC (Rhs, 0).last_line;				\
	  (Current).first_column = (Current).last_column =		\
	    YYRHSLOC (Rhs, 0).last_column;				\
	}								\
    while (YYID (0))
#endif


/* YY_LOCATION_PRINT -- Print the location on the stream.
   This macro was not mandated originally: define only if we know
   we won't break user code: when these are the locations we know.  */

#ifndef YY_LOCATION_PRINT
# if YYLTYPE_IS_TRIVIAL
#  define YY_LOCATION_PRINT(File, Loc)			\
     fprintf (File, "%d.%d-%d.%d",			\
	      (Loc).first_line, (Loc).first_column,	\
	      (Loc).last_line,  (Loc).last_column)
# else
#  define YY_LOCATION_PRINT(File, Loc) ((void) 0)
# endif
#endif


/* YYLEX -- calling `yylex' with the right arguments.  */

#ifdef YYLEX_PARAM
# define YYLEX yylex (YYLEX_PARAM)
#else
# define YYLEX yylex ()
#endif

/* Enable debugging if requested.  */
#if YYDEBUG

# ifndef YYFPRINTF
#  include <stdio.h> /* INFRINGES ON USER NAME SPACE */
#  define YYFPRINTF fprintf
# endif

# define YYDPRINTF(Args)			\
do {						\
  if (yydebug)					\
    YYFPRINTF Args;				\
} while (YYID (0))

# define YY_SYMBOL_PRINT(Title, Type, Value, Location)			  \
do {									  \
  if (yydebug)								  \
    {									  \
      YYFPRINTF (stderr, "%s ", Title);					  \
      yy_symbol_print (stderr,						  \
		  Type, Value); \
      YYFPRINTF (stderr, "\n");						  \
    }									  \
} while (YYID (0))


/*--------------------------------.
| Print this symbol on YYOUTPUT.  |
`--------------------------------*/

/*ARGSUSED*/
#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static void
yy_symbol_value_print (FILE *yyoutput, int yytype, YYSTYPE const * const yyvaluep)
#else
static void
yy_symbol_value_print (yyoutput, yytype, yyvaluep)
    FILE *yyoutput;
    int yytype;
    YYSTYPE const * const yyvaluep;
#endif
{
  if (!yyvaluep)
    return;
# ifdef YYPRINT
  if (yytype < YYNTOKENS)
    YYPRINT (yyoutput, yytoknum[yytype], *yyvaluep);
# else
  YYUSE (yyoutput);
# endif
  switch (yytype)
    {
      default:
	break;
    }
}


/*--------------------------------.
| Print this symbol on YYOUTPUT.  |
`--------------------------------*/

#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static void
yy_symbol_print (FILE *yyoutput, int yytype, YYSTYPE const * const yyvaluep)
#else
static void
yy_symbol_print (yyoutput, yytype, yyvaluep)
    FILE *yyoutput;
    int yytype;
    YYSTYPE const * const yyvaluep;
#endif
{
  if (yytype < YYNTOKENS)
    YYFPRINTF (yyoutput, "token %s (", yytname[yytype]);
  else
    YYFPRINTF (yyoutput, "nterm %s (", yytname[yytype]);

  yy_symbol_value_print (yyoutput, yytype, yyvaluep);
  YYFPRINTF (yyoutput, ")");
}

/*------------------------------------------------------------------.
| yy_stack_print -- Print the state stack from its BOTTOM up to its |
| TOP (included).                                                   |
`------------------------------------------------------------------*/

#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static void
yy_stack_print (yytype_int16 *yybottom, yytype_int16 *yytop)
#else
static void
yy_stack_print (yybottom, yytop)
    yytype_int16 *yybottom;
    yytype_int16 *yytop;
#endif
{
  YYFPRINTF (stderr, "Stack now");
  for (; yybottom <= yytop; yybottom++)
    {
      int yybot = *yybottom;
      YYFPRINTF (stderr, " %d", yybot);
    }
  YYFPRINTF (stderr, "\n");
}

# define YY_STACK_PRINT(Bottom, Top)				\
do {								\
  if (yydebug)							\
    yy_stack_print ((Bottom), (Top));				\
} while (YYID (0))


/*------------------------------------------------.
| Report that the YYRULE is going to be reduced.  |
`------------------------------------------------*/

#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static void
yy_reduce_print (YYSTYPE *yyvsp, int yyrule)
#else
static void
yy_reduce_print (yyvsp, yyrule)
    YYSTYPE *yyvsp;
    int yyrule;
#endif
{
  int yynrhs = yyr2[yyrule];
  int yyi;
  unsigned long int yylno = yyrline[yyrule];
  YYFPRINTF (stderr, "Reducing stack by rule %d (line %lu):\n",
	     yyrule - 1, yylno);
  /* The symbols being reduced.  */
  for (yyi = 0; yyi < yynrhs; yyi++)
    {
      YYFPRINTF (stderr, "   $%d = ", yyi + 1);
      yy_symbol_print (stderr, yyrhs[yyprhs[yyrule] + yyi],
		       &(yyvsp[(yyi + 1) - (yynrhs)])
		       		       );
      YYFPRINTF (stderr, "\n");
    }
}

# define YY_REDUCE_PRINT(Rule)		\
do {					\
  if (yydebug)				\
    yy_reduce_print (yyvsp, Rule); \
} while (YYID (0))

/* Nonzero means print parse trace.  It is left uninitialized so that
   multiple parsers can coexist.  */
int yydebug;
#else /* !YYDEBUG */
# define YYDPRINTF(Args)
# define YY_SYMBOL_PRINT(Title, Type, Value, Location)
# define YY_STACK_PRINT(Bottom, Top)
# define YY_REDUCE_PRINT(Rule)
#endif /* !YYDEBUG */


/* YYINITDEPTH -- initial size of the parser's stacks.  */
#ifndef	YYINITDEPTH
# define YYINITDEPTH 200
#endif

/* YYMAXDEPTH -- maximum size the stacks can grow to (effective only
   if the built-in stack extension method is used).

   Do not make this value too large; the results are undefined if
   YYSTACK_ALLOC_MAXIMUM < YYSTACK_BYTES (YYMAXDEPTH)
   evaluated with infinite-precision integer arithmetic.  */

#ifndef YYMAXDEPTH
# define YYMAXDEPTH 10000
#endif



#if YYERROR_VERBOSE

# ifndef yystrlen
#  if defined __GLIBC__ && defined _STRING_H
#   define yystrlen strlen
#  else
/* Return the length of YYSTR.  */
#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static YYSIZE_T
yystrlen (const char *yystr)
#else
static YYSIZE_T
yystrlen (yystr)
    const char *yystr;
#endif
{
  YYSIZE_T yylen;
  for (yylen = 0; yystr[yylen]; yylen++)
    continue;
  return yylen;
}
#  endif
# endif

# ifndef yystpcpy
#  if defined __GLIBC__ && defined _STRING_H && defined _GNU_SOURCE
#   define yystpcpy stpcpy
#  else
/* Copy YYSRC to YYDEST, returning the address of the terminating '\0' in
   YYDEST.  */
#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static char *
yystpcpy (char *yydest, const char *yysrc)
#else
static char *
yystpcpy (yydest, yysrc)
    char *yydest;
    const char *yysrc;
#endif
{
  char *yyd = yydest;
  const char *yys = yysrc;

  while ((*yyd++ = *yys++) != '\0')
    continue;

  return yyd - 1;
}
#  endif
# endif

# ifndef yytnamerr
/* Copy to YYRES the contents of YYSTR after stripping away unnecessary
   quotes and backslashes, so that it's suitable for yyerror.  The
   heuristic is that double-quoting is unnecessary unless the string
   contains an apostrophe, a comma, or backslash (other than
   backslash-backslash).  YYSTR is taken from yytname.  If YYRES is
   null, do not copy; instead, return the length of what the result
   would have been.  */
static YYSIZE_T
yytnamerr (char *yyres, const char *yystr)
{
  if (*yystr == '"')
    {
      YYSIZE_T yyn = 0;
      char const *yyp = yystr;

      for (;;)
	switch (*++yyp)
	  {
	  case '\'':
	  case ',':
	    goto do_not_strip_quotes;

	  case '\\':
	    if (*++yyp != '\\')
	      goto do_not_strip_quotes;
	    /* Fall through.  */
	  default:
	    if (yyres)
	      yyres[yyn] = *yyp;
	    yyn++;
	    break;

	  case '"':
	    if (yyres)
	      yyres[yyn] = '\0';
	    return yyn;
	  }
    do_not_strip_quotes: ;
    }

  if (! yyres)
    return yystrlen (yystr);

  return yystpcpy (yyres, yystr) - yyres;
}
# endif

/* Copy into YYRESULT an error message about the unexpected token
   YYCHAR while in state YYSTATE.  Return the number of bytes copied,
   including the terminating null byte.  If YYRESULT is null, do not
   copy anything; just return the number of bytes that would be
   copied.  As a special case, return 0 if an ordinary "syntax error"
   message will do.  Return YYSIZE_MAXIMUM if overflow occurs during
   size calculation.  */
static YYSIZE_T
yysyntax_error (char *yyresult, int yystate, int yychar)
{
  int yyn = yypact[yystate];

  if (! (YYPACT_NINF < yyn && yyn <= YYLAST))
    return 0;
  else
    {
      int yytype = YYTRANSLATE (yychar);
      YYSIZE_T yysize0 = yytnamerr (0, yytname[yytype]);
      YYSIZE_T yysize = yysize0;
      YYSIZE_T yysize1;
      int yysize_overflow = 0;
      enum { YYERROR_VERBOSE_ARGS_MAXIMUM = 5 };
      char const *yyarg[YYERROR_VERBOSE_ARGS_MAXIMUM];
      int yyx;

# if 0
      /* This is so xgettext sees the translatable formats that are
	 constructed on the fly.  */
      YY_("syntax error, unexpected %s");
      YY_("syntax error, unexpected %s, expecting %s");
      YY_("syntax error, unexpected %s, expecting %s or %s");
      YY_("syntax error, unexpected %s, expecting %s or %s or %s");
      YY_("syntax error, unexpected %s, expecting %s or %s or %s or %s");
# endif
      char *yyfmt;
      char const *yyf;
      static char const yyunexpected[] = "syntax error, unexpected %s";
      static char const yyexpecting[] = ", expecting %s";
      static char const yyor[] = " or %s";
      char yyformat[sizeof yyunexpected
		    + sizeof yyexpecting - 1
		    + ((YYERROR_VERBOSE_ARGS_MAXIMUM - 2)
		       * (sizeof yyor - 1))];
      char const *yyprefix = yyexpecting;

      /* Start YYX at -YYN if negative to avoid negative indexes in
	 YYCHECK.  */
      int yyxbegin = yyn < 0 ? -yyn : 0;

      /* Stay within bounds of both yycheck and yytname.  */
      int yychecklim = YYLAST - yyn + 1;
      int yyxend = yychecklim < YYNTOKENS ? yychecklim : YYNTOKENS;
      int yycount = 1;

      yyarg[0] = yytname[yytype];
      yyfmt = yystpcpy (yyformat, yyunexpected);

      for (yyx = yyxbegin; yyx < yyxend; ++yyx)
	if (yycheck[yyx + yyn] == yyx && yyx != YYTERROR)
	  {
	    if (yycount == YYERROR_VERBOSE_ARGS_MAXIMUM)
	      {
		yycount = 1;
		yysize = yysize0;
		yyformat[sizeof yyunexpected - 1] = '\0';
		break;
	      }
	    yyarg[yycount++] = yytname[yyx];
	    yysize1 = yysize + yytnamerr (0, yytname[yyx]);
	    yysize_overflow |= (yysize1 < yysize);
	    yysize = yysize1;
	    yyfmt = yystpcpy (yyfmt, yyprefix);
	    yyprefix = yyor;
	  }

      yyf = YY_(yyformat);
      yysize1 = yysize + yystrlen (yyf);
      yysize_overflow |= (yysize1 < yysize);
      yysize = yysize1;

      if (yysize_overflow)
	return YYSIZE_MAXIMUM;

      if (yyresult)
	{
	  /* Avoid sprintf, as that infringes on the user's name space.
	     Don't have undefined behavior even if the translation
	     produced a string with the wrong number of "%s"s.  */
	  char *yyp = yyresult;
	  int yyi = 0;
	  while ((*yyp = *yyf) != '\0')
	    {
	      if (*yyp == '%' && yyf[1] == 's' && yyi < yycount)
		{
		  yyp += yytnamerr (yyp, yyarg[yyi++]);
		  yyf += 2;
		}
	      else
		{
		  yyp++;
		  yyf++;
		}
	    }
	}
      return yysize;
    }
}
#endif /* YYERROR_VERBOSE */


/*-----------------------------------------------.
| Release the memory associated to this symbol.  |
`-----------------------------------------------*/

/*ARGSUSED*/
#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static void
yydestruct (const char *yymsg, int yytype, YYSTYPE *yyvaluep)
#else
static void
yydestruct (yymsg, yytype, yyvaluep)
    const char *yymsg;
    int yytype;
    YYSTYPE *yyvaluep;
#endif
{
  YYUSE (yyvaluep);

  if (!yymsg)
    yymsg = "Deleting";
  YY_SYMBOL_PRINT (yymsg, yytype, yyvaluep, yylocationp);

  switch (yytype)
    {

      default:
	break;
    }
}

/* Prevent warnings from -Wmissing-prototypes.  */
#ifdef YYPARSE_PARAM
#if defined __STDC__ || defined __cplusplus
int yyparse (void *YYPARSE_PARAM);
#else
int yyparse ();
#endif
#else /* ! YYPARSE_PARAM */
#if defined __STDC__ || defined __cplusplus
int yyparse (void);
#else
int yyparse ();
#endif
#endif /* ! YYPARSE_PARAM */


/* The lookahead symbol.  */
int yychar;

/* The semantic value of the lookahead symbol.  */
YYSTYPE yylval;

/* Number of syntax errors so far.  */
int yynerrs;



/*-------------------------.
| yyparse or yypush_parse.  |
`-------------------------*/

#ifdef YYPARSE_PARAM
#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
int
yyparse (void *YYPARSE_PARAM)
#else
int
yyparse (YYPARSE_PARAM)
    void *YYPARSE_PARAM;
#endif
#else /* ! YYPARSE_PARAM */
#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
int
yyparse (void)
#else
int
yyparse ()

#endif
#endif
{


    int yystate;
    /* Number of tokens to shift before error messages enabled.  */
    int yyerrstatus;

    /* The stacks and their tools:
       `yyss': related to states.
       `yyvs': related to semantic values.

       Refer to the stacks thru separate pointers, to allow yyoverflow
       to reallocate them elsewhere.  */

    /* The state stack.  */
    yytype_int16 yyssa[YYINITDEPTH];
    yytype_int16 *yyss;
    yytype_int16 *yyssp;

    /* The semantic value stack.  */
    YYSTYPE yyvsa[YYINITDEPTH];
    YYSTYPE *yyvs;
    YYSTYPE *yyvsp;

    YYSIZE_T yystacksize;

  int yyn;
  int yyresult;
  /* Lookahead token as an internal (translated) token number.  */
  int yytoken;
  /* The variables used to return semantic value and location from the
     action routines.  */
  YYSTYPE yyval;

#if YYERROR_VERBOSE
  /* Buffer for error messages, and its allocated size.  */
  char yymsgbuf[128];
  char *yymsg = yymsgbuf;
  YYSIZE_T yymsg_alloc = sizeof yymsgbuf;
#endif

#define YYPOPSTACK(N)   (yyvsp -= (N), yyssp -= (N))

  /* The number of symbols on the RHS of the reduced rule.
     Keep to zero when no symbol should be popped.  */
  int yylen = 0;

  yytoken = 0;
  yyss = yyssa;
  yyvs = yyvsa;
  yystacksize = YYINITDEPTH;

  YYDPRINTF ((stderr, "Starting parse\n"));

  yystate = 0;
  yyerrstatus = 0;
  yynerrs = 0;
  yychar = YYEMPTY; /* Cause a token to be read.  */

  /* Initialize stack pointers.
     Waste one element of value and location stack
     so that they stay on the same level as the state stack.
     The wasted elements are never initialized.  */
  yyssp = yyss;
  yyvsp = yyvs;

  goto yysetstate;

/*------------------------------------------------------------.
| yynewstate -- Push a new state, which is found in yystate.  |
`------------------------------------------------------------*/
 yynewstate:
  /* In all cases, when you get here, the value and location stacks
     have just been pushed.  So pushing a state here evens the stacks.  */
  yyssp++;

 yysetstate:
  *yyssp = yystate;

  if (yyss + yystacksize - 1 <= yyssp)
    {
      /* Get the current used size of the three stacks, in elements.  */
      YYSIZE_T yysize = yyssp - yyss + 1;

#ifdef yyoverflow
      {
	/* Give user a chance to reallocate the stack.  Use copies of
	   these so that the &'s don't force the real ones into
	   memory.  */
	YYSTYPE *yyvs1 = yyvs;
	yytype_int16 *yyss1 = yyss;

	/* Each stack pointer address is followed by the size of the
	   data in use in that stack, in bytes.  This used to be a
	   conditional around just the two extra args, but that might
	   be undefined if yyoverflow is a macro.  */
	yyoverflow (YY_("memory exhausted"),
		    &yyss1, yysize * sizeof (*yyssp),
		    &yyvs1, yysize * sizeof (*yyvsp),
		    &yystacksize);

	yyss = yyss1;
	yyvs = yyvs1;
      }
#else /* no yyoverflow */
# ifndef YYSTACK_RELOCATE
      goto yyexhaustedlab;
# else
      /* Extend the stack our own way.  */
      if (YYMAXDEPTH <= yystacksize)
	goto yyexhaustedlab;
      yystacksize *= 2;
      if (YYMAXDEPTH < yystacksize)
	yystacksize = YYMAXDEPTH;

      {
	yytype_int16 *yyss1 = yyss;
	union yyalloc *yyptr =
	  (union yyalloc *) YYSTACK_ALLOC (YYSTACK_BYTES (yystacksize));
	if (! yyptr)
	  goto yyexhaustedlab;
	YYSTACK_RELOCATE (yyss_alloc, yyss);
	YYSTACK_RELOCATE (yyvs_alloc, yyvs);
#  undef YYSTACK_RELOCATE
	if (yyss1 != yyssa)
	  YYSTACK_FREE (yyss1);
      }
# endif
#endif /* no yyoverflow */

      yyssp = yyss + yysize - 1;
      yyvsp = yyvs + yysize - 1;

      YYDPRINTF ((stderr, "Stack size increased to %lu\n",
		  (unsigned long int) yystacksize));

      if (yyss + yystacksize - 1 <= yyssp)
	YYABORT;
    }

  YYDPRINTF ((stderr, "Entering state %d\n", yystate));

  if (yystate == YYFINAL)
    YYACCEPT;

  goto yybackup;

/*-----------.
| yybackup.  |
`-----------*/
yybackup:

  /* Do appropriate processing given the current state.  Read a
     lookahead token if we need one and don't already have one.  */

  /* First try to decide what to do without reference to lookahead token.  */
  yyn = yypact[yystate];
  if (yyn == YYPACT_NINF)
    goto yydefault;

  /* Not known => get a lookahead token if don't already have one.  */

  /* YYCHAR is either YYEMPTY or YYEOF or a valid lookahead symbol.  */
  if (yychar == YYEMPTY)
    {
      YYDPRINTF ((stderr, "Reading a token: "));
      yychar = YYLEX;
    }

  if (yychar <= YYEOF)
    {
      yychar = yytoken = YYEOF;
      YYDPRINTF ((stderr, "Now at end of input.\n"));
    }
  else
    {
      yytoken = YYTRANSLATE (yychar);
      YY_SYMBOL_PRINT ("Next token is", yytoken, &yylval, &yylloc);
    }

  /* If the proper action on seeing token YYTOKEN is to reduce or to
     detect an error, take that action.  */
  yyn += yytoken;
  if (yyn < 0 || YYLAST < yyn || yycheck[yyn] != yytoken)
    goto yydefault;
  yyn = yytable[yyn];
  if (yyn <= 0)
    {
      if (yyn == 0 || yyn == YYTABLE_NINF)
	goto yyerrlab;
      yyn = -yyn;
      goto yyreduce;
    }

  /* Count tokens shifted since error; after three, turn off error
     status.  */
  if (yyerrstatus)
    yyerrstatus--;

  /* Shift the lookahead token.  */
  YY_SYMBOL_PRINT ("Shifting", yytoken, &yylval, &yylloc);

  /* Discard the shifted token.  */
  yychar = YYEMPTY;

  yystate = yyn;
  *++yyvsp = yylval;

  goto yynewstate;


/*-----------------------------------------------------------.
| yydefault -- do the default action for the current state.  |
`-----------------------------------------------------------*/
yydefault:
  yyn = yydefact[yystate];
  if (yyn == 0)
    goto yyerrlab;
  goto yyreduce;


/*-----------------------------.
| yyreduce -- Do a reduction.  |
`-----------------------------*/
yyreduce:
  /* yyn is the number of a rule to reduce with.  */
  yylen = yyr2[yyn];

  /* If YYLEN is nonzero, implement the default value of the action:
     `$$ = $1'.

     Otherwise, the following line sets YYVAL to garbage.
     This behavior is undocumented and Bison
     users should not rely upon it.  Assigning to YYVAL
     unconditionally makes the parser a bit smaller, and it avoids a
     GCC warning that YYVAL may be used uninitialized.  */
  yyval = yyvsp[1-yylen];


  YY_REDUCE_PRINT (yyn);
  switch (yyn)
    {
        case 2:

/* Line 1455 of yacc.c  */
#line 84 "mapcalc.y"
    { (yyval.list) = result = (yyvsp[(1) - (1)].list);		}
    break;

  case 3:

/* Line 1455 of yacc.c  */
#line 87 "mapcalc.y"
    { (yyval.list) = list((yyvsp[(1) - (1)].exp),NULL);		}
    break;

  case 4:

/* Line 1455 of yacc.c  */
#line 88 "mapcalc.y"
    { (yyval.list) = list((yyvsp[(1) - (2)].exp),NULL);		}
    break;

  case 5:

/* Line 1455 of yacc.c  */
#line 89 "mapcalc.y"
    { (yyval.list) = list((yyvsp[(1) - (3)].exp),(yyvsp[(3) - (3)].list));		}
    break;

  case 6:

/* Line 1455 of yacc.c  */
#line 90 "mapcalc.y"
    { (yyval.list) = (yyvsp[(3) - (3)].list);			}
    break;

  case 7:

/* Line 1455 of yacc.c  */
#line 93 "mapcalc.y"
    { (yyval.exp) = binding((yyvsp[(1) - (3)].sval),(yyvsp[(3) - (3)].exp)); define_variable((yyval.exp));	}
    break;

  case 8:

/* Line 1455 of yacc.c  */
#line 94 "mapcalc.y"
    { (yyval.exp) = binding((yyvsp[(1) - (3)].sval),(yyvsp[(3) - (3)].exp)); define_variable((yyval.exp));	}
    break;

  case 12:

/* Line 1455 of yacc.c  */
#line 100 "mapcalc.y"
    { (yyval.sval) = composite((yyvsp[(1) - (3)].sval),(yyvsp[(3) - (3)].sval));	}
    break;

  case 13:

/* Line 1455 of yacc.c  */
#line 103 "mapcalc.y"
    { (yyval.ival) = '@';			}
    break;

  case 14:

/* Line 1455 of yacc.c  */
#line 104 "mapcalc.y"
    { (yyval.ival) = 'r';			}
    break;

  case 15:

/* Line 1455 of yacc.c  */
#line 105 "mapcalc.y"
    { (yyval.ival) = 'g';			}
    break;

  case 16:

/* Line 1455 of yacc.c  */
#line 106 "mapcalc.y"
    { (yyval.ival) = 'b';			}
    break;

  case 17:

/* Line 1455 of yacc.c  */
#line 107 "mapcalc.y"
    { (yyval.ival) = '#';			}
    break;

  case 18:

/* Line 1455 of yacc.c  */
#line 108 "mapcalc.y"
    { (yyval.ival) = 'y';			}
    break;

  case 19:

/* Line 1455 of yacc.c  */
#line 109 "mapcalc.y"
    { (yyval.ival) = 'i';			}
    break;

  case 21:

/* Line 1455 of yacc.c  */
#line 113 "mapcalc.y"
    { (yyval.ival) = -(yyvsp[(2) - (2)].ival);			}
    break;

  case 22:

/* Line 1455 of yacc.c  */
#line 116 "mapcalc.y"
    { (yyval.list) = singleton((yyvsp[(1) - (1)].exp));		}
    break;

  case 23:

/* Line 1455 of yacc.c  */
#line 117 "mapcalc.y"
    { (yyval.list) = list((yyvsp[(1) - (3)].exp), (yyvsp[(3) - (3)].list));		}
    break;

  case 24:

/* Line 1455 of yacc.c  */
#line 120 "mapcalc.y"
    { (yyval.exp) = variable((yyvsp[(1) - (1)].sval));		}
    break;

  case 25:

/* Line 1455 of yacc.c  */
#line 121 "mapcalc.y"
    { (yyval.exp) = variable((yyvsp[(1) - (1)].sval));		}
    break;

  case 26:

/* Line 1455 of yacc.c  */
#line 124 "mapcalc.y"
    { (yyval.exp) = mapname((yyvsp[(1) - (4)].sval),'M',(yyvsp[(3) - (4)].ival),0,0);	}
    break;

  case 27:

/* Line 1455 of yacc.c  */
#line 126 "mapcalc.y"
    { (yyval.exp) = mapname((yyvsp[(1) - (6)].sval),'M',(yyvsp[(3) - (6)].ival),(yyvsp[(5) - (6)].ival),0);	}
    break;

  case 28:

/* Line 1455 of yacc.c  */
#line 128 "mapcalc.y"
    { (yyval.exp) = mapname((yyvsp[(1) - (8)].sval),'M',(yyvsp[(3) - (8)].ival),(yyvsp[(5) - (8)].ival),(yyvsp[(7) - (8)].ival));}
    break;

  case 29:

/* Line 1455 of yacc.c  */
#line 129 "mapcalc.y"
    { (yyval.exp) = mapname((yyvsp[(1) - (1)].sval),'M',0,0,0);	}
    break;

  case 30:

/* Line 1455 of yacc.c  */
#line 131 "mapcalc.y"
    { (yyval.exp) = mapname((yyvsp[(2) - (5)].sval),(yyvsp[(1) - (5)].ival),(yyvsp[(4) - (5)].ival),0,0);	}
    break;

  case 31:

/* Line 1455 of yacc.c  */
#line 133 "mapcalc.y"
    { (yyval.exp) = mapname((yyvsp[(2) - (7)].sval),(yyvsp[(1) - (7)].ival),(yyvsp[(4) - (7)].ival),(yyvsp[(6) - (7)].ival),0);	}
    break;

  case 32:

/* Line 1455 of yacc.c  */
#line 135 "mapcalc.y"
    { (yyval.exp) = mapname((yyvsp[(2) - (9)].sval),(yyvsp[(1) - (9)].ival),(yyvsp[(4) - (9)].ival),(yyvsp[(6) - (9)].ival),(yyvsp[(8) - (9)].ival));	}
    break;

  case 33:

/* Line 1455 of yacc.c  */
#line 136 "mapcalc.y"
    { (yyval.exp) = mapname((yyvsp[(2) - (2)].sval),(yyvsp[(1) - (2)].ival),0,0,0);	}
    break;

  case 34:

/* Line 1455 of yacc.c  */
#line 139 "mapcalc.y"
    { (yyval.exp) = function((yyvsp[(1) - (3)].sval), NULL);	}
    break;

  case 35:

/* Line 1455 of yacc.c  */
#line 141 "mapcalc.y"
    { (yyval.exp) = function((yyvsp[(1) - (4)].sval), (yyvsp[(3) - (4)].list));	}
    break;

  case 36:

/* Line 1455 of yacc.c  */
#line 144 "mapcalc.y"
    { (yyval.exp) = (yyvsp[(2) - (3)].exp);			}
    break;

  case 40:

/* Line 1455 of yacc.c  */
#line 148 "mapcalc.y"
    { (yyval.exp) = constant_int((yyvsp[(1) - (1)].ival));	}
    break;

  case 41:

/* Line 1455 of yacc.c  */
#line 149 "mapcalc.y"
    { (yyval.exp) = constant_float((yyvsp[(1) - (1)].fval));	}
    break;

  case 42:

/* Line 1455 of yacc.c  */
#line 150 "mapcalc.y"
    { (yyval.exp) = constant_double((yyvsp[(1) - (1)].fval));	}
    break;

  case 44:

/* Line 1455 of yacc.c  */
#line 154 "mapcalc.y"
    { (yyval.exp) = operator("neg","-",1,singleton((yyvsp[(2) - (2)].exp)));	}
    break;

  case 45:

/* Line 1455 of yacc.c  */
#line 155 "mapcalc.y"
    { (yyval.exp) = operator("bitnot","~",1,singleton((yyvsp[(2) - (2)].exp)));	}
    break;

  case 46:

/* Line 1455 of yacc.c  */
#line 156 "mapcalc.y"
    { (yyval.exp) = operator("not","!",1,singleton((yyvsp[(2) - (2)].exp)));	}
    break;

  case 48:

/* Line 1455 of yacc.c  */
#line 160 "mapcalc.y"
    { (yyval.exp) = operator("pow","^",2,pair((yyvsp[(1) - (3)].exp),(yyvsp[(3) - (3)].exp)));	}
    break;

  case 50:

/* Line 1455 of yacc.c  */
#line 164 "mapcalc.y"
    { (yyval.exp) = operator("mul","*",3,pair((yyvsp[(1) - (3)].exp),(yyvsp[(3) - (3)].exp)));	}
    break;

  case 51:

/* Line 1455 of yacc.c  */
#line 165 "mapcalc.y"
    { (yyval.exp) = operator("div","/",3,pair((yyvsp[(1) - (3)].exp),(yyvsp[(3) - (3)].exp)));	}
    break;

  case 52:

/* Line 1455 of yacc.c  */
#line 166 "mapcalc.y"
    { (yyval.exp) = operator("mod","%",3,pair((yyvsp[(1) - (3)].exp),(yyvsp[(3) - (3)].exp)));	}
    break;

  case 54:

/* Line 1455 of yacc.c  */
#line 170 "mapcalc.y"
    { (yyval.exp) = operator("add","+",4,pair((yyvsp[(1) - (3)].exp),(yyvsp[(3) - (3)].exp)));	}
    break;

  case 55:

/* Line 1455 of yacc.c  */
#line 171 "mapcalc.y"
    { (yyval.exp) = operator("sub","-",4,pair((yyvsp[(1) - (3)].exp),(yyvsp[(3) - (3)].exp)));	}
    break;

  case 57:

/* Line 1455 of yacc.c  */
#line 175 "mapcalc.y"
    { (yyval.exp) = operator("shiftl","<<",5,pair((yyvsp[(1) - (3)].exp),(yyvsp[(3) - (3)].exp)));	}
    break;

  case 58:

/* Line 1455 of yacc.c  */
#line 176 "mapcalc.y"
    { (yyval.exp) = operator("shiftr",">>",5,pair((yyvsp[(1) - (3)].exp),(yyvsp[(3) - (3)].exp)));	}
    break;

  case 59:

/* Line 1455 of yacc.c  */
#line 177 "mapcalc.y"
    { (yyval.exp) = operator("shiftru",">>>",5,pair((yyvsp[(1) - (3)].exp),(yyvsp[(3) - (3)].exp)));	}
    break;

  case 61:

/* Line 1455 of yacc.c  */
#line 181 "mapcalc.y"
    { (yyval.exp) = operator("gt",">", 6,pair((yyvsp[(1) - (3)].exp),(yyvsp[(3) - (3)].exp)));	}
    break;

  case 62:

/* Line 1455 of yacc.c  */
#line 182 "mapcalc.y"
    { (yyval.exp) = operator("ge",">=",6,pair((yyvsp[(1) - (3)].exp),(yyvsp[(3) - (3)].exp)));	}
    break;

  case 63:

/* Line 1455 of yacc.c  */
#line 183 "mapcalc.y"
    { (yyval.exp) = operator("lt","<", 6,pair((yyvsp[(1) - (3)].exp),(yyvsp[(3) - (3)].exp)));	}
    break;

  case 64:

/* Line 1455 of yacc.c  */
#line 184 "mapcalc.y"
    { (yyval.exp) = operator("le","<=",6,pair((yyvsp[(1) - (3)].exp),(yyvsp[(3) - (3)].exp)));	}
    break;

  case 66:

/* Line 1455 of yacc.c  */
#line 187 "mapcalc.y"
    { (yyval.exp) = operator("eq","==",7,pair((yyvsp[(1) - (3)].exp),(yyvsp[(3) - (3)].exp)));	}
    break;

  case 67:

/* Line 1455 of yacc.c  */
#line 188 "mapcalc.y"
    { (yyval.exp) = operator("ne","!=",7,pair((yyvsp[(1) - (3)].exp),(yyvsp[(3) - (3)].exp)));	}
    break;

  case 69:

/* Line 1455 of yacc.c  */
#line 192 "mapcalc.y"
    { (yyval.exp) = operator("bitand","&",8,pair((yyvsp[(1) - (3)].exp),(yyvsp[(3) - (3)].exp)));	}
    break;

  case 71:

/* Line 1455 of yacc.c  */
#line 196 "mapcalc.y"
    { (yyval.exp) = operator("bitor", "|",9,pair((yyvsp[(1) - (3)].exp),(yyvsp[(3) - (3)].exp)));	}
    break;

  case 73:

/* Line 1455 of yacc.c  */
#line 200 "mapcalc.y"
    { (yyval.exp) = operator("and","&&",10,pair((yyvsp[(1) - (3)].exp),(yyvsp[(3) - (3)].exp)));	}
    break;

  case 74:

/* Line 1455 of yacc.c  */
#line 201 "mapcalc.y"
    { (yyval.exp) = operator("and2","&&&",10,pair((yyvsp[(1) - (3)].exp),(yyvsp[(3) - (3)].exp)));	}
    break;

  case 76:

/* Line 1455 of yacc.c  */
#line 205 "mapcalc.y"
    { (yyval.exp) = operator("or", "||",11,pair((yyvsp[(1) - (3)].exp),(yyvsp[(3) - (3)].exp)));	}
    break;

  case 77:

/* Line 1455 of yacc.c  */
#line 206 "mapcalc.y"
    { (yyval.exp) = operator("or2", "|||",11,pair((yyvsp[(1) - (3)].exp),(yyvsp[(3) - (3)].exp)));	}
    break;

  case 79:

/* Line 1455 of yacc.c  */
#line 211 "mapcalc.y"
    { (yyval.exp) = operator("if","?:",12,triple((yyvsp[(1) - (5)].exp),(yyvsp[(3) - (5)].exp),(yyvsp[(5) - (5)].exp)));	}
    break;

  case 81:

/* Line 1455 of yacc.c  */
#line 215 "mapcalc.y"
    { (yyval.exp) = binding((yyvsp[(1) - (3)].sval),(yyvsp[(3) - (3)].exp)); define_variable((yyval.exp));	}
    break;

  case 82:

/* Line 1455 of yacc.c  */
#line 218 "mapcalc.y"
    { if (syntax_error_occurred) {syntax_error_occurred = 0; YYERROR; } else (yyval.exp) = (yyvsp[(1) - (1)].exp);	}
    break;



/* Line 1455 of yacc.c  */
#line 1967 "mapcalc.tab.c"
      default: break;
    }
  YY_SYMBOL_PRINT ("-> $$ =", yyr1[yyn], &yyval, &yyloc);

  YYPOPSTACK (yylen);
  yylen = 0;
  YY_STACK_PRINT (yyss, yyssp);

  *++yyvsp = yyval;

  /* Now `shift' the result of the reduction.  Determine what state
     that goes to, based on the state we popped back to and the rule
     number reduced by.  */

  yyn = yyr1[yyn];

  yystate = yypgoto[yyn - YYNTOKENS] + *yyssp;
  if (0 <= yystate && yystate <= YYLAST && yycheck[yystate] == *yyssp)
    yystate = yytable[yystate];
  else
    yystate = yydefgoto[yyn - YYNTOKENS];

  goto yynewstate;


/*------------------------------------.
| yyerrlab -- here on detecting error |
`------------------------------------*/
yyerrlab:
  /* If not already recovering from an error, report this error.  */
  if (!yyerrstatus)
    {
      ++yynerrs;
#if ! YYERROR_VERBOSE
      yyerror (YY_("syntax error"));
#else
      {
	YYSIZE_T yysize = yysyntax_error (0, yystate, yychar);
	if (yymsg_alloc < yysize && yymsg_alloc < YYSTACK_ALLOC_MAXIMUM)
	  {
	    YYSIZE_T yyalloc = 2 * yysize;
	    if (! (yysize <= yyalloc && yyalloc <= YYSTACK_ALLOC_MAXIMUM))
	      yyalloc = YYSTACK_ALLOC_MAXIMUM;
	    if (yymsg != yymsgbuf)
	      YYSTACK_FREE (yymsg);
	    yymsg = (char *) YYSTACK_ALLOC (yyalloc);
	    if (yymsg)
	      yymsg_alloc = yyalloc;
	    else
	      {
		yymsg = yymsgbuf;
		yymsg_alloc = sizeof yymsgbuf;
	      }
	  }

	if (0 < yysize && yysize <= yymsg_alloc)
	  {
	    (void) yysyntax_error (yymsg, yystate, yychar);
	    yyerror (yymsg);
	  }
	else
	  {
	    yyerror (YY_("syntax error"));
	    if (yysize != 0)
	      goto yyexhaustedlab;
	  }
      }
#endif
    }



  if (yyerrstatus == 3)
    {
      /* If just tried and failed to reuse lookahead token after an
	 error, discard it.  */

      if (yychar <= YYEOF)
	{
	  /* Return failure if at end of input.  */
	  if (yychar == YYEOF)
	    YYABORT;
	}
      else
	{
	  yydestruct ("Error: discarding",
		      yytoken, &yylval);
	  yychar = YYEMPTY;
	}
    }

  /* Else will try to reuse lookahead token after shifting the error
     token.  */
  goto yyerrlab1;


/*---------------------------------------------------.
| yyerrorlab -- error raised explicitly by YYERROR.  |
`---------------------------------------------------*/
yyerrorlab:

  /* Pacify compilers like GCC when the user code never invokes
     YYERROR and the label yyerrorlab therefore never appears in user
     code.  */
  if (/*CONSTCOND*/ 0)
     goto yyerrorlab;

  /* Do not reclaim the symbols of the rule which action triggered
     this YYERROR.  */
  YYPOPSTACK (yylen);
  yylen = 0;
  YY_STACK_PRINT (yyss, yyssp);
  yystate = *yyssp;
  goto yyerrlab1;


/*-------------------------------------------------------------.
| yyerrlab1 -- common code for both syntax error and YYERROR.  |
`-------------------------------------------------------------*/
yyerrlab1:
  yyerrstatus = 3;	/* Each real token shifted decrements this.  */

  for (;;)
    {
      yyn = yypact[yystate];
      if (yyn != YYPACT_NINF)
	{
	  yyn += YYTERROR;
	  if (0 <= yyn && yyn <= YYLAST && yycheck[yyn] == YYTERROR)
	    {
	      yyn = yytable[yyn];
	      if (0 < yyn)
		break;
	    }
	}

      /* Pop the current state because it cannot handle the error token.  */
      if (yyssp == yyss)
	YYABORT;


      yydestruct ("Error: popping",
		  yystos[yystate], yyvsp);
      YYPOPSTACK (1);
      yystate = *yyssp;
      YY_STACK_PRINT (yyss, yyssp);
    }

  *++yyvsp = yylval;


  /* Shift the error token.  */
  YY_SYMBOL_PRINT ("Shifting", yystos[yyn], yyvsp, yylsp);

  yystate = yyn;
  goto yynewstate;


/*-------------------------------------.
| yyacceptlab -- YYACCEPT comes here.  |
`-------------------------------------*/
yyacceptlab:
  yyresult = 0;
  goto yyreturn;

/*-----------------------------------.
| yyabortlab -- YYABORT comes here.  |
`-----------------------------------*/
yyabortlab:
  yyresult = 1;
  goto yyreturn;

#if !defined(yyoverflow) || YYERROR_VERBOSE
/*-------------------------------------------------.
| yyexhaustedlab -- memory exhaustion comes here.  |
`-------------------------------------------------*/
yyexhaustedlab:
  yyerror (YY_("memory exhausted"));
  yyresult = 2;
  /* Fall through.  */
#endif

yyreturn:
  if (yychar != YYEMPTY)
     yydestruct ("Cleanup: discarding lookahead",
		 yytoken, &yylval);
  /* Do not reclaim the symbols of the rule which action triggered
     this YYABORT or YYACCEPT.  */
  YYPOPSTACK (yylen);
  YY_STACK_PRINT (yyss, yyssp);
  while (yyssp != yyss)
    {
      yydestruct ("Cleanup: popping",
		  yystos[*yyssp], yyvsp);
      YYPOPSTACK (1);
    }
#ifndef yyoverflow
  if (yyss != yyssa)
    YYSTACK_FREE (yyss);
#endif
#if YYERROR_VERBOSE
  if (yymsg != yymsgbuf)
    YYSTACK_FREE (yymsg);
#endif
  /* Make sure YYID is used.  */
  return YYID (yyresult);
}



/* Line 1675 of yacc.c  */
#line 225 "mapcalc.y"


void syntax_error(const char *fmt, ...)
{
	va_list va;

	va_start(va, fmt);
	vfprintf(stderr, fmt, va);
	va_end(va);

	fprintf(stderr, "\n");

	syntax_error_occurred = 1;
}

void yyerror(char *s)
{
	fprintf(stderr, "%s\n", s);
	syntax_error_occurred = 0;
}

static expr_list *parse(void)
{
#if 0
	yydebug = 1;
#endif
	syntax_error_occurred = 0;

	if (yyparse() != 0)
	{
		fprintf(stderr, "Parse error\n");
		return NULL;
	}

	if (syntax_error_occurred)
	{
		fprintf(stderr, "Syntax error\n");
		return NULL;
	}

	return result;
}

expr_list *parse_string(const char *s)
{
	initialize_scanner_string(s);
	return parse();
}

expr_list *parse_stream(FILE *fp)
{
	expr_list *e;

	initialize_scanner_stream(fp);
	if (isatty(fileno(fp)))
		fputs("Enter expressions, \"end\" when done.\n", stderr);
	e = parse();
	if (isatty(fileno(fp)))
		fputs("\n", stderr);
	return e;
}


