#############################################################################
#
# MODULE:   	Grass Compilation
# AUTHOR(S):	Original author unknown - probably CERL
#		Markus Neteler - Germany/Italy - neteler@itc.it
#   	    	Justin Hickey - Thailand - jhickey@hpcc.nectec.or.th
#   	    	Huidae Cho - Korea - grass4u@gmail.com
#   	    	Eric G. Miller - egm2@jps.net
# PURPOSE:  	The source file for this Makefile is in src/CMD/head/head.in.
#		It is the top part of a file called make.rules which is used
#		for compiling all GRASS modules. This part of the file provides
#		make variables that are dependent on the results of the
#		configure script.
# COPYRIGHT:    (C) 2000 by the GRASS Development Team
#
#               This program is free software under the GNU General Public
#   	    	License (>=v2). Read the file COPYING that comes with GRASS
#   	    	for details.
#
#############################################################################

############################## Make Variables ###############################

CC                  = gcc
CXX                 = c++
LEX                 = flex
YACC                = bison -y
PERL                = /usr/bin/perl
AR                  = ar
RANLIB              = ranlib
MKDIR               = mkdir -p
CHMOD               = chmod
INSTALL             = /usr/bin/install -c 
INSTALL_DATA        = ${INSTALL} -m 644

prefix              = /home/majavie/hierba_hanks
exec_prefix         = ${prefix}
ARCH                = x86_64-unknown-linux-gnu
UNIX_BIN            = ${exec_prefix}/bin
INST_DIR            = ${prefix}/grass-7.1.svn

GRASS_HOME          = /home/majavie/grass_sc/grass7_trunk
RUN_GISBASE         = /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu

GRASS_VERSION_MAJOR = 7
GRASS_VERSION_MINOR = 1
GRASS_VERSION_RELEASE = svn
GRASS_VERSION_DATE  = 2014
GRASS_VERSION_SVN   = 61265

STRIPFLAG           = 
LD_SEARCH_FLAGS     = -Wl,-rpath-link,${LIB_RUNTIME_DIR}
LD_LIBRARY_PATH_VAR = LD_LIBRARY_PATH

#generate static (ST) or shared (SH)
GRASS_LIBRARY_TYPE  = shlib

#static libs:
STLIB_LD            = ${AR} cr
STLIB_PREFIX        = lib
STLIB_SUFFIX        = .a

#shared libs
SHLIB_PREFIX        = lib
SHLIB_LD            = gcc -shared
SHLIB_LDFLAGS       = 
SHLIB_CFLAGS        = -fPIC
SHLIB_SUFFIX        = .so
EXE                 = 

DEFAULT_DATABASE    =
DEFAULT_LOCATION    =

CPPFLAGS            = 
CFLAGS              = -Wall 
CXXFLAGS            = -g -O2
INCLUDE_DIRS        = 
LINK_FLAGS          =   -Wl,--export-dynamic

DLLIB               = -ldl
XCFLAGS             = 
XLIBPATH            =  -L/usr/lib64
XLIB                =  -lSM -lICE -lX11
XEXTRALIBS          = 
USE_X11             = 1

MATHLIB             = -lm 
ICONVLIB            = 
INTLLIB             = 
SOCKLIB             = 

#ZLIB:
ZLIB                =  -lz 
ZLIBINCPATH         = 
ZLIBLIBPATH         = 

DBMIEXTRALIB        = 

#readline
READLINEINCPATH     = 
READLINELIBPATH     = 
READLINELIB         = 
HISTORYLIB          = 

#PostgreSQL:
PQINCPATH           = 
PQLIBPATH           = 
PQLIB               =  -lpq 
USE_POSTGRES        = 1

#MySQL:
MYSQLINCPATH        = 
MYSQLLIBPATH        = 
MYSQLLIB            = 
MYSQLDLIB           = 

#SQLite:
SQLITEINCPATH       = 
SQLITELIBPATH       = 
SQLITELIB           =  -lsqlite3 

#FFMPEG
FFMPEGINCPATH       = 
FFMPEGLIBPATH       = 
FFMPEGLIB           = 

#ODBC:
ODBCINC             = 
ODBCLIB             = 

#Image formats:
PNGINC              = 
PNGLIB              =  -lpng  -lz  -lm
USE_PNG             = 1

TIFFINCPATH         = 
TIFFLIBPATH         = 
TIFFLIB             =  -ltiff 

#openGL files for NVIZ/r3.showdspf
OPENGLINC           =  -I/usr/include/GL/
OPENGLLIB           =   -lGL 
OPENGLULIB          =   -lGLU 
OPENGL_X11          = 1
OPENGL_AQUA         = 
OPENGL_WINDOWS      = 
USE_OPENGL          = 1

#FFTW:
FFTWINC             = 
FFTWLIB             =  -lfftw3 -lm

#LAPACK/BLAS stuff for gmath lib:
BLASLIB             = -lblas
BLASINC             = 
LAPACKLIB           =  -llapack 
LAPACKINC           = 

#GDAL/OGR
GDALLIBS            = -lgdal
GDALCFLAGS          = -I/usr/include/gdal
USE_GDAL            = 1
USE_OGR             = 1

#NetCDF
NETCDFLIBS          = 
NETCDFCFLAGS        =     
USE_NETCDF          = 

#LAS LiDAR through libLAS
LASLIBS             = 
LASCFLAGS           = 
LASINC              = 
USE_LIBLAS          = 

#GEOS
GEOSLIBS            = -L/usr/lib64 -lgeos -lgeos_c 
GEOSCFLAGS          = -I/usr/include
USE_GEOS            = 1

#FreeType:
FTINC               =  -I/usr/include/freetype2
FTLIB               =  -lfreetype 

#PROJ.4:
PROJINC             =  $(GDALCFLAGS)
PROJLIB             =  -lproj 
NAD2BIN             = /usr/bin/nad2bin
PROJSHARE           = /usr/share/proj/

#OPENDWG:
OPENDWGINCPATH      = 
OPENDWGLIBPATH      = 
OPENDWGLIB          = 
USE_OPENDWG         = 

#cairo
CAIROINC                  = -I/usr/include/cairo -I/usr/include/pixman-1 -I/usr/include/freetype2 -I/usr/include/libpng12  
CAIROLIB                  = -lfreetype -lz -lfontconfig -lXrender -lcairo -lX11    
USE_CAIRO                 = 1
CAIRO_HAS_XRENDER         = 1
CAIRO_HAS_XRENDER_SURFACE = 1

#Python
PYTHON              = python

#wxWidgets
WXVERSION           = 2.8.12
WXWIDGETSCXXFLAGS   = -I/usr/lib64/wx/include/gtk2-unicode-release-2.8 -I/usr/include/wx-2.8 -D_FILE_OFFSET_BITS=64 -D_LARGE_FILES -D__WXGTK__ -pthread
WXWIDGETSCPPFLAGS   = -I/usr/lib64/wx/include/gtk2-unicode-release-2.8 -I/usr/include/wx-2.8 -D_FILE_OFFSET_BITS=64 -D_LARGE_FILES -D__WXGTK__
WXWIDGETSLIB        = -pthread   -lwx_gtk2u_richtext-2.8 -lwx_gtk2u_aui-2.8 -lwx_gtk2u_xrc-2.8 -lwx_gtk2u_qa-2.8 -lwx_gtk2u_html-2.8 -lwx_gtk2u_adv-2.8 -lwx_gtk2u_core-2.8 -lwx_baseu_xml-2.8 -lwx_baseu_net-2.8 -lwx_baseu-2.8 
USE_WXWIDGETS       = 1
MACOSX_ARCHS_WXPYTHON = 

#regex
REGEXINCPATH        = 
REGEXLIBPATH        = 
REGEXLIB            =  
USE_REGEX           = 1

#pthreads
PTHREADINCPATH      = 
PTHREADLIBPATH      = 
PTHREADLIB          =  -lpthread 
USE_PTHREAD         = 1

#OpenMP
OMPINCPATH          = 
OMPLIBPATH          = 
OMPLIB              = 
OMPCFLAGS           = 
USE_OPENMP          = 

#OpenCL
OCLINCPATH          = 
OCLLIBPATH          = 
OCLLIB              = 
USE_OPENCL          = 

#i18N
HAVE_NLS            = 1

#Large File Support (LFS)
USE_LARGEFILES      = 1
LFS_CFLAGS          = 

#BSD sockets
HAVE_SOCKET         = 1

MINGW		    = 
MACOSX_APP	    = 
MACOSX_ARCHS        = 
MACOSX_SDK          = 

# Cross compilation
CROSS_COMPILING     =  
