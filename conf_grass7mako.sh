CFLAGS="-Wall" ./configure \
 --prefix=/home/majavie/hierba3 \
 --enable-largefile \
 --with-sqlite \
 --with-freetype=yes \
 --with-freetype-includes=/usr/include/freetype2 \
 --with-python=/home/javier/anaconda \
 --with-cxx \
 --with-tcltk-includes=/usr/include/tcl8.5 \
 --enable-64bit \
 --with-gdal=/usr/bin/gdal-config \
 --with-proj --with-proj-share=/usr/share/proj/ \
 --with-geos=/usr/bin/geos-config \
 --with-nls \
 --with-opengl-includes=/usr/include/GL/ \
 --with-x \
 --with-fftw \
 --with-wx=/usr/bin/wx-config \
 --with-wxwidgets=/usr/bin/wx-config \
 --with-cairo \
 --enable-shared \
 --with-pthread \
 --with-lapack \
 --with-blas

#  --with-odbc=yes \
# --with-postgres \
#  --enable-largefile \
 #--with-proj-share=/usr/share/proj \
 #--with-freetype \
 #--with-freetype-includes=/usr/include/freetype2 \
 #--with-sqlite \
# --with-python \ 
 #--with-wxwidgets \
 #--with-geos=yes \
 #--with-cairo=yes \
