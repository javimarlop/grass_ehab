.. wxGUI documentation master file, created by
   sphinx-quickstart on Tue Jun  3 09:20:51 2014.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

wxGUI documentation
=================================

.. toctree::
  :maxdepth: 2

  wxgui_libraries
  wxgui_tools


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

