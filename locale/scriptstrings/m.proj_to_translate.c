_("Converts coordinates from one projection to another (cs2cs frontend).")
_("miscellaneous")
_("projection")
_("Input coordinates to reproject")
_("Input coordinates")
_("Name of input coordinate file")
_("'-' to read from standard input")
_("Input coordinates")
_("Name for output coordinate file (omit to send to stdout)")
_("Output")
_("Field separator (format: input[,output])")
_("Input coordinates")
_("Input projection parameters (PROJ.4 style)")
_("Projections")
_("Output projection parameters (PROJ.4 style)")
_("Projections")
_("Use LL WGS84 as input and current location as output projection")
_("Projections")
_("Use current location as input and LL WGS84 as output projection")
_("Projections")
_("Output long/lat in decimal degrees, or other projections with many decimal places")
_("Output")
_("Include input coordinates in output file")
_("Output")
_("Include column names in output file")
_("Output")
