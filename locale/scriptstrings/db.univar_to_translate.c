_("Calculates univariate statistics on selected table column.")
_("database")
_("statistics")
_("attribute table")
_("Name of attribute column on which to calculate statistics (must be numeric)")
_("Percentile to calculate (requires extended statistics flag)")
_("Extended statistics (quartiles and 90th percentile)")
_("Print stats in shell script style")
