_("Creates/modifies the color table associated with each raster map of the space time raster dataset.")
_("temporal")
_("color table")
_("Name of color table (see r.color help)")
_("Define")
_("Raster map from which to copy color table")
_("Define")
_("3D raster map from which to copy color table")
_("Define")
_("Path to rules file")
_("Define")
_("Remove existing color tables")
_("Remove")
_("Only write new color table if it does not already exist")
_("List available rules then exit")
_("Print")
_("Invert colors")
_("Define")
_("Logarithmic scaling")
_("Define")
_("Logarithmic-absolute scaling")
_("Define")
_("Histogram equalization")
_("Define")
