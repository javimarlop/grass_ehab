_("Apply temporal and spatial operations on space time raster datasets using temporal raster algebra.")
_("temporal")
_("algebra")
_("r.mapcalc expression for temporal and spatial analysis of space time raster datasets")
_("Basename of the new generated output maps")
_("A numerical suffix separated by an underscore will be attached to create a unique identifier")
_("Number of r.mapcalc processes to run in parallel")
_("Activate spatial topology")
_("Register Null maps")
