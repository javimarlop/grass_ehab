/*! \page pygrasslib PyGRASS Library

by GRASS Development Team (http://grass.osgeo.org)

\section pygrassIntro Introduction

PyGRASS improves the integration between GRASS GIS and Python (see 
also \ref pythonlib), making the use of Python under GRASS more 
consistent with the language itself. Furthermore, it simplifies 
GRASS scripting and programming and more natural for the user.

\section pygrassPackages PyGRASS packages 

The following PyGRASS packages are provided:
 - \subpage pygrassGis
 - \subpage pygrassRaster
 - \subpage pygrassVector
 - \subpage pygrassModule
 - \subpage pygrassFunctions
 - \subpage pygrassMessages
 - \subpage pygrassErrors
 
 Below these packages are illustrated in greater detail.

\subsection pygrassGis GIS Package

 - python::pygrass::gis

Basic elements of the GRASS data-base:

  - python::pygrass::gis::Gisdbase
  - python::pygrass::gis::Location
  - python::pygrass::gis::Mapset
  - python::pygrass::gis::VisibleMapset

Computational region:

  - python::pygrass::gis::region::Region

Mapset handling:
     
  - python::pygrass::gis::set_current_mapset
  - python::pygrass::gis::make_mapset

\subsection pygrassRaster Raster Package

PyGRASS uses 4 different Raster classes, that respect the 4 
different approaches of GRASS-C API. The read access is row wise for 
RastRow and RasterRowIO and additionally cached in the RowIO class. 
Both classes write sequentially. RowIO is row cached, RastSegment 
and RasterNumpy are tile cached for reading and writing therefore a 
randomly access is possible. Hence RasterRow and RasterRowIO should 
be used in case for fast (cached) row read access and RasterRow for 
fast sequential writing. Segment and Numpy should be used for random 
access, but Numpy only for files not larger than 2GB.

 - python::pygrass::raster

Reading/writing raster maps:

  - python::pygrass::raster::RasterRow
  - python::pygrass::raster::RasterRowIO
  - python::pygrass::raster::RasterSegment
  - python::pygrass::raster::RasterNumpy

Random raster data:
     
  - python::pygrass::raster::random_map_only_columns
  - python::pygrass::raster::random_map

Raster categories:

  - python::pygrass::raster::category::Category

Raster metadata:

  - python::pygrass::raster::history::History

\subsection pygrassVector Vector Package

 - python::pygrass::vector

List of high-level classes:

  - python::pygrass::vector::Vector
  - python::pygrass::vector::VectorTopo

Basic data structures and functions:

 - python::pygrass::vector::basic
  - python::pygrass::vector::basic::Bbox
  - python::pygrass::vector::basic::BoxList
  - python::pygrass::vector::basic::Cats
  - python::pygrass::vector::basic::CatsList

Search for features:

 - python::pygrass::vector::find
  - python::pygrass::vector::find::PointFinder
  - python::pygrass::vector::find::BboxFinder
  - python::pygrass::vector::find::PolygonFinder

Geometry and topology elements handling:

 - python::pygrass::vector::geometry
  - python::pygrass::vector::geometry::Attrs
  - python::pygrass::vector::geometry::Geo
  - python::pygrass::vector::geometry::Point
  - python::pygrass::vector::geometry::Line
  - python::pygrass::vector::geometry::Node
  - python::pygrass::vector::geometry::Boundary
  - python::pygrass::vector::geometry::Centroid
  - python::pygrass::vector::geometry::Isle
  - python::pygrass::vector::geometry::Isles
  - python::pygrass::vector::geometry::Area

Vector attribute handling:

 - python::pygrass::vector::attributes

\subsection pygrassModule Interface to GRASS GIS modules

 - python::pygrass::modules

List of high-level classes:

 - python::pygrass::modules::interface::module::Module
 - python::pygrass::modules::interface::module::ParallelModuleQueue 

List of namespaces:

 - python::pygrass::modules::grid
 - python::pygrass::modules::interface
 - python::pygrass::modules::shortcuts

\subsection pygrassFunctions Functions Package

List of pyGRASS functions:

 - python::pygrass::functions

\subsection pygrassMessages Messages Package

Messages handling in PyGRASS:

 - python::pygrass::messages::FatalError
 - python::pygrass::messages::Messenger

\subsection pygrassErrors Errors Package

Defined errors classes:

 - python::pygrass::errors::ParameterError
 - python::pygrass::errors::FlagError
 - python::pygrass::errors::DBError
 - python::pygrass::errors::GrassError
 - python::pygrass::errors::OpenError

\section pygrassAuthors Authors

Pietro Zambelli (Google Summer of Code 2012)

\section References References


*  Zambelli, P., Gebbert, S., Ciolli, M., 2013: *PyGRASS: An Object Oriented Python Application Programming Interface (API) for Geographic Resources Analysis Support System (GRASS) Geographic Information System (GIS)* 
* . ISPRS International Journal of Geo-Information 2, 201–219. (
*  `DOI <http://dx.doi.org/10.3390/ijgi2010201>`
* |
*  `PDF <http://www.mdpi.com/2220-9964/2/1/201/pdf>`
* )

*/
