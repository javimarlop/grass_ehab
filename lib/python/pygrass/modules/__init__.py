# -*- coding: utf-8 -*-
"""
Created on Thu Jul 12 10:23:15 2012

@author: pietro

"""
from grass.pygrass.modules import interface
from grass.pygrass.modules.interface import Module, ParallelModuleQueue

from grass.pygrass.modules import grid
from grass.pygrass.modules.grid.grid import GridModule

from grass.pygrass.modules import shortcuts
