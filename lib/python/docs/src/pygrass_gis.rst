.. _GRASSdatabase-label:

GRASS database management
===============================

These classes are used to manage the infrastructure
of GRASS database: Gisdbase, Location and Mapset


.. _Region-label:

Region management
======================

The Region class it is useful to obtain information
about the computational region and to change it.

