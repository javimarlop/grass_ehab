PyGRASS message interface
=========================

The PyGRASS message interface is a fast and exit-safe
interface to the `GRASS C-library message functions <http://grass.osgeo.org/programming7/gis_2error_8c.html>`_.

