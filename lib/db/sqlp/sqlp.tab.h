
/* A Bison parser, made by GNU Bison 2.4.1.  */

/* Skeleton interface for Bison's Yacc-like parsers in C
   
      Copyright (C) 1984, 1989, 1990, 2000, 2001, 2002, 2003, 2004, 2005, 2006
   Free Software Foundation, Inc.
   
   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.  */

/* As a special exception, you may create a larger work that contains
   part or all of the Bison parser skeleton and distribute that work
   under terms of your choice, so long as that work isn't itself a
   parser generator using the skeleton or a modified version thereof
   as a parser skeleton.  Alternatively, if you modify or redistribute
   the parser skeleton itself, you may (at your option) remove this
   special exception, which will cause the skeleton and the resulting
   Bison output files to be licensed under the GNU General Public
   License without this special exception.
   
   This special exception was added by the Free Software Foundation in
   version 2.2 of Bison.  */


/* Tokens.  */
#ifndef YYTOKENTYPE
# define YYTOKENTYPE
   /* Put the tokens into the symbol table, so that GDB and other debuggers
      know about them.  */
   enum yytokentype {
     COMPARISON_OPERATOR = 258,
     NAME = 259,
     STRING = 260,
     INTNUM = 261,
     FLOATNUM = 262,
     ADD = 263,
     DROP = 264,
     COLUMN = 265,
     EQUAL = 266,
     SELECT = 267,
     FROM = 268,
     WHERE = 269,
     DELETE = 270,
     INSERT = 271,
     INTO = 272,
     VALUES = 273,
     UPDATE = 274,
     SET = 275,
     AND = 276,
     OR = 277,
     NOT = 278,
     ALTER = 279,
     TABLE = 280,
     CREATE = 281,
     NULL_VALUE = 282,
     VARCHAR = 283,
     INT = 284,
     INTEGER = 285,
     DOUBLE = 286,
     PRECISION = 287,
     DATE = 288,
     TIME = 289,
     ORDER = 290,
     BY = 291,
     IS = 292,
     ASC = 293,
     DESC = 294
   };
#endif
/* Tokens.  */
#define COMPARISON_OPERATOR 258
#define NAME 259
#define STRING 260
#define INTNUM 261
#define FLOATNUM 262
#define ADD 263
#define DROP 264
#define COLUMN 265
#define EQUAL 266
#define SELECT 267
#define FROM 268
#define WHERE 269
#define DELETE 270
#define INSERT 271
#define INTO 272
#define VALUES 273
#define UPDATE 274
#define SET 275
#define AND 276
#define OR 277
#define NOT 278
#define ALTER 279
#define TABLE 280
#define CREATE 281
#define NULL_VALUE 282
#define VARCHAR 283
#define INT 284
#define INTEGER 285
#define DOUBLE 286
#define PRECISION 287
#define DATE 288
#define TIME 289
#define ORDER 290
#define BY 291
#define IS 292
#define ASC 293
#define DESC 294




#if ! defined YYSTYPE && ! defined YYSTYPE_IS_DECLARED
typedef union YYSTYPE
{

/* Line 1676 of yacc.c  */
#line 34 "sqlp.y"

	int    intval;
	double floatval;
	char   *strval;
	int    subtok;
	SQLPNODE   *node;



/* Line 1676 of yacc.c  */
#line 140 "sqlp.tab.h"
} YYSTYPE;
# define YYSTYPE_IS_TRIVIAL 1
# define yystype YYSTYPE /* obsolescent; will be withdrawn */
# define YYSTYPE_IS_DECLARED 1
#endif

extern YYSTYPE yylval;


