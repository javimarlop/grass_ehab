
/* A Bison parser, made by GNU Bison 2.4.1.  */

/* Skeleton implementation for Bison's Yacc-like parsers in C
   
      Copyright (C) 1984, 1989, 1990, 2000, 2001, 2002, 2003, 2004, 2005, 2006
   Free Software Foundation, Inc.
   
   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.  */

/* As a special exception, you may create a larger work that contains
   part or all of the Bison parser skeleton and distribute that work
   under terms of your choice, so long as that work isn't itself a
   parser generator using the skeleton or a modified version thereof
   as a parser skeleton.  Alternatively, if you modify or redistribute
   the parser skeleton itself, you may (at your option) remove this
   special exception, which will cause the skeleton and the resulting
   Bison output files to be licensed under the GNU General Public
   License without this special exception.
   
   This special exception was added by the Free Software Foundation in
   version 2.2 of Bison.  */

/* C LALR(1) parser skeleton written by Richard Stallman, by
   simplifying the original so-called "semantic" parser.  */

/* All symbols defined below should begin with yy or YY, to avoid
   infringing on user name space.  This should be done even for local
   variables, as they might otherwise be expanded by user macros.
   There are some unavoidable exceptions within include files to
   define necessary library symbols; they are noted "INFRINGES ON
   USER NAME SPACE" below.  */

/* Identify Bison output.  */
#define YYBISON 1

/* Bison version.  */
#define YYBISON_VERSION "2.4.1"

/* Skeleton name.  */
#define YYSKELETON_NAME "yacc.c"

/* Pure parsers.  */
#define YYPURE 0

/* Push parsers.  */
#define YYPUSH 0

/* Pull parsers.  */
#define YYPULL 1

/* Using locations.  */
#define YYLSP_NEEDED 0



/* Copy the first part of user declarations.  */

/* Line 189 of yacc.c  */
#line 21 "sqlp.y"

#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <grass/sqlp.h>

#define YYDEBUG 1
#define YYERROR_VERBOSE 1



/* Line 189 of yacc.c  */
#line 85 "sqlp.tab.c"

/* Enabling traces.  */
#ifndef YYDEBUG
# define YYDEBUG 0
#endif

/* Enabling verbose error messages.  */
#ifdef YYERROR_VERBOSE
# undef YYERROR_VERBOSE
# define YYERROR_VERBOSE 1
#else
# define YYERROR_VERBOSE 0
#endif

/* Enabling the token table.  */
#ifndef YYTOKEN_TABLE
# define YYTOKEN_TABLE 0
#endif


/* Tokens.  */
#ifndef YYTOKENTYPE
# define YYTOKENTYPE
   /* Put the tokens into the symbol table, so that GDB and other debuggers
      know about them.  */
   enum yytokentype {
     COMPARISON_OPERATOR = 258,
     NAME = 259,
     STRING = 260,
     INTNUM = 261,
     FLOATNUM = 262,
     ADD = 263,
     DROP = 264,
     COLUMN = 265,
     EQUAL = 266,
     SELECT = 267,
     FROM = 268,
     WHERE = 269,
     DELETE = 270,
     INSERT = 271,
     INTO = 272,
     VALUES = 273,
     UPDATE = 274,
     SET = 275,
     AND = 276,
     OR = 277,
     NOT = 278,
     ALTER = 279,
     TABLE = 280,
     CREATE = 281,
     NULL_VALUE = 282,
     VARCHAR = 283,
     INT = 284,
     INTEGER = 285,
     DOUBLE = 286,
     PRECISION = 287,
     DATE = 288,
     TIME = 289,
     ORDER = 290,
     BY = 291,
     IS = 292,
     ASC = 293,
     DESC = 294
   };
#endif
/* Tokens.  */
#define COMPARISON_OPERATOR 258
#define NAME 259
#define STRING 260
#define INTNUM 261
#define FLOATNUM 262
#define ADD 263
#define DROP 264
#define COLUMN 265
#define EQUAL 266
#define SELECT 267
#define FROM 268
#define WHERE 269
#define DELETE 270
#define INSERT 271
#define INTO 272
#define VALUES 273
#define UPDATE 274
#define SET 275
#define AND 276
#define OR 277
#define NOT 278
#define ALTER 279
#define TABLE 280
#define CREATE 281
#define NULL_VALUE 282
#define VARCHAR 283
#define INT 284
#define INTEGER 285
#define DOUBLE 286
#define PRECISION 287
#define DATE 288
#define TIME 289
#define ORDER 290
#define BY 291
#define IS 292
#define ASC 293
#define DESC 294




#if ! defined YYSTYPE && ! defined YYSTYPE_IS_DECLARED
typedef union YYSTYPE
{

/* Line 214 of yacc.c  */
#line 34 "sqlp.y"

	int    intval;
	double floatval;
	char   *strval;
	int    subtok;
	SQLPNODE   *node;



/* Line 214 of yacc.c  */
#line 209 "sqlp.tab.c"
} YYSTYPE;
# define YYSTYPE_IS_TRIVIAL 1
# define yystype YYSTYPE /* obsolescent; will be withdrawn */
# define YYSTYPE_IS_DECLARED 1
#endif


/* Copy the second part of user declarations.  */

/* Line 264 of yacc.c  */
#line 88 "sqlp.y"

 
extern int yylex(void);



/* Line 264 of yacc.c  */
#line 228 "sqlp.tab.c"

#ifdef short
# undef short
#endif

#ifdef YYTYPE_UINT8
typedef YYTYPE_UINT8 yytype_uint8;
#else
typedef unsigned char yytype_uint8;
#endif

#ifdef YYTYPE_INT8
typedef YYTYPE_INT8 yytype_int8;
#elif (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
typedef signed char yytype_int8;
#else
typedef short int yytype_int8;
#endif

#ifdef YYTYPE_UINT16
typedef YYTYPE_UINT16 yytype_uint16;
#else
typedef unsigned short int yytype_uint16;
#endif

#ifdef YYTYPE_INT16
typedef YYTYPE_INT16 yytype_int16;
#else
typedef short int yytype_int16;
#endif

#ifndef YYSIZE_T
# ifdef __SIZE_TYPE__
#  define YYSIZE_T __SIZE_TYPE__
# elif defined size_t
#  define YYSIZE_T size_t
# elif ! defined YYSIZE_T && (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
#  include <stddef.h> /* INFRINGES ON USER NAME SPACE */
#  define YYSIZE_T size_t
# else
#  define YYSIZE_T unsigned int
# endif
#endif

#define YYSIZE_MAXIMUM ((YYSIZE_T) -1)

#ifndef YY_
# if YYENABLE_NLS
#  if ENABLE_NLS
#   include <libintl.h> /* INFRINGES ON USER NAME SPACE */
#   define YY_(msgid) dgettext ("bison-runtime", msgid)
#  endif
# endif
# ifndef YY_
#  define YY_(msgid) msgid
# endif
#endif

/* Suppress unused-variable warnings by "using" E.  */
#if ! defined lint || defined __GNUC__
# define YYUSE(e) ((void) (e))
#else
# define YYUSE(e) /* empty */
#endif

/* Identity function, used to suppress warnings about constant conditions.  */
#ifndef lint
# define YYID(n) (n)
#else
#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static int
YYID (int yyi)
#else
static int
YYID (yyi)
    int yyi;
#endif
{
  return yyi;
}
#endif

#if ! defined yyoverflow || YYERROR_VERBOSE

/* The parser invokes alloca or malloc; define the necessary symbols.  */

# ifdef YYSTACK_USE_ALLOCA
#  if YYSTACK_USE_ALLOCA
#   ifdef __GNUC__
#    define YYSTACK_ALLOC __builtin_alloca
#   elif defined __BUILTIN_VA_ARG_INCR
#    include <alloca.h> /* INFRINGES ON USER NAME SPACE */
#   elif defined _AIX
#    define YYSTACK_ALLOC __alloca
#   elif defined _MSC_VER
#    include <malloc.h> /* INFRINGES ON USER NAME SPACE */
#    define alloca _alloca
#   else
#    define YYSTACK_ALLOC alloca
#    if ! defined _ALLOCA_H && ! defined _STDLIB_H && (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
#     include <stdlib.h> /* INFRINGES ON USER NAME SPACE */
#     ifndef _STDLIB_H
#      define _STDLIB_H 1
#     endif
#    endif
#   endif
#  endif
# endif

# ifdef YYSTACK_ALLOC
   /* Pacify GCC's `empty if-body' warning.  */
#  define YYSTACK_FREE(Ptr) do { /* empty */; } while (YYID (0))
#  ifndef YYSTACK_ALLOC_MAXIMUM
    /* The OS might guarantee only one guard page at the bottom of the stack,
       and a page size can be as small as 4096 bytes.  So we cannot safely
       invoke alloca (N) if N exceeds 4096.  Use a slightly smaller number
       to allow for a few compiler-allocated temporary stack slots.  */
#   define YYSTACK_ALLOC_MAXIMUM 4032 /* reasonable circa 2006 */
#  endif
# else
#  define YYSTACK_ALLOC YYMALLOC
#  define YYSTACK_FREE YYFREE
#  ifndef YYSTACK_ALLOC_MAXIMUM
#   define YYSTACK_ALLOC_MAXIMUM YYSIZE_MAXIMUM
#  endif
#  if (defined __cplusplus && ! defined _STDLIB_H \
       && ! ((defined YYMALLOC || defined malloc) \
	     && (defined YYFREE || defined free)))
#   include <stdlib.h> /* INFRINGES ON USER NAME SPACE */
#   ifndef _STDLIB_H
#    define _STDLIB_H 1
#   endif
#  endif
#  ifndef YYMALLOC
#   define YYMALLOC malloc
#   if ! defined malloc && ! defined _STDLIB_H && (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
void *malloc (YYSIZE_T); /* INFRINGES ON USER NAME SPACE */
#   endif
#  endif
#  ifndef YYFREE
#   define YYFREE free
#   if ! defined free && ! defined _STDLIB_H && (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
void free (void *); /* INFRINGES ON USER NAME SPACE */
#   endif
#  endif
# endif
#endif /* ! defined yyoverflow || YYERROR_VERBOSE */


#if (! defined yyoverflow \
     && (! defined __cplusplus \
	 || (defined YYSTYPE_IS_TRIVIAL && YYSTYPE_IS_TRIVIAL)))

/* A type that is properly aligned for any stack member.  */
union yyalloc
{
  yytype_int16 yyss_alloc;
  YYSTYPE yyvs_alloc;
};

/* The size of the maximum gap between one aligned stack and the next.  */
# define YYSTACK_GAP_MAXIMUM (sizeof (union yyalloc) - 1)

/* The size of an array large to enough to hold all stacks, each with
   N elements.  */
# define YYSTACK_BYTES(N) \
     ((N) * (sizeof (yytype_int16) + sizeof (YYSTYPE)) \
      + YYSTACK_GAP_MAXIMUM)

/* Copy COUNT objects from FROM to TO.  The source and destination do
   not overlap.  */
# ifndef YYCOPY
#  if defined __GNUC__ && 1 < __GNUC__
#   define YYCOPY(To, From, Count) \
      __builtin_memcpy (To, From, (Count) * sizeof (*(From)))
#  else
#   define YYCOPY(To, From, Count)		\
      do					\
	{					\
	  YYSIZE_T yyi;				\
	  for (yyi = 0; yyi < (Count); yyi++)	\
	    (To)[yyi] = (From)[yyi];		\
	}					\
      while (YYID (0))
#  endif
# endif

/* Relocate STACK from its old location to the new one.  The
   local variables YYSIZE and YYSTACKSIZE give the old and new number of
   elements in the stack, and YYPTR gives the new location of the
   stack.  Advance YYPTR to a properly aligned location for the next
   stack.  */
# define YYSTACK_RELOCATE(Stack_alloc, Stack)				\
    do									\
      {									\
	YYSIZE_T yynewbytes;						\
	YYCOPY (&yyptr->Stack_alloc, Stack, yysize);			\
	Stack = &yyptr->Stack_alloc;					\
	yynewbytes = yystacksize * sizeof (*Stack) + YYSTACK_GAP_MAXIMUM; \
	yyptr += yynewbytes / sizeof (*yyptr);				\
      }									\
    while (YYID (0))

#endif

/* YYFINAL -- State number of the termination state.  */
#define YYFINAL  27
/* YYLAST -- Last index in YYTABLE.  */
#define YYLAST   156

/* YYNTOKENS -- Number of terminals.  */
#define YYNTOKENS  48
/* YYNNTS -- Number of nonterminals.  */
#define YYNNTS  32
/* YYNRULES -- Number of rules.  */
#define YYNRULES  87
/* YYNRULES -- Number of states.  */
#define YYNSTATES  157

/* YYTRANSLATE(YYLEX) -- Bison symbol number corresponding to YYLEX.  */
#define YYUNDEFTOK  2
#define YYMAXUTOK   294

#define YYTRANSLATE(YYX)						\
  ((unsigned int) (YYX) <= YYMAXUTOK ? yytranslate[YYX] : YYUNDEFTOK)

/* YYTRANSLATE[YYLEX] -- Bison symbol number corresponding to YYLEX.  */
static const yytype_uint8 yytranslate[] =
{
       0,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
      41,    42,    44,    46,    43,    45,     2,    47,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,    40,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     1,     2,     3,     4,
       5,     6,     7,     8,     9,    10,    11,    12,    13,    14,
      15,    16,    17,    18,    19,    20,    21,    22,    23,    24,
      25,    26,    27,    28,    29,    30,    31,    32,    33,    34,
      35,    36,    37,    38,    39
};

#if YYDEBUG
/* YYPRHS[YYN] -- Index of the first RHS symbol of rule number YYN in
   YYRHS.  */
static const yytype_uint16 yyprhs[] =
{
       0,     0,     3,     5,     7,     9,    11,    13,    15,    17,
      20,    27,    33,    40,    47,    51,    56,    63,    71,    81,
      85,    91,    96,   104,   109,   116,   118,   122,   128,   131,
     134,   137,   141,   144,   147,   149,   151,   153,   157,   159,
     164,   166,   168,   170,   173,   175,   178,   182,   186,   190,
     195,   199,   204,   206,   210,   214,   218,   220,   222,   226,
     228,   232,   234,   238,   241,   245,   249,   253,   257,   259,
     263,   267,   269,   273,   277,   279,   282,   284,   286,   290,
     292,   294,   296,   298,   300,   302,   304,   307
};

/* YYRHS -- A `-1'-separated list of the rules' RHS.  */
static const yytype_int8 yyrhs[] =
{
      49,     0,    -1,    50,    -1,    51,    -1,    52,    -1,    55,
      -1,    53,    -1,    56,    -1,    54,    -1,    49,    40,    -1,
      24,    25,    61,     8,    10,    58,    -1,    24,    25,    61,
       8,    58,    -1,    24,    25,    61,     9,    10,     4,    -1,
      26,    25,    61,    41,    57,    42,    -1,     9,    25,    61,
      -1,    12,    59,    13,    61,    -1,    12,    59,    13,    61,
      14,    66,    -1,    12,    59,    13,    61,    35,    36,    77,
      -1,    12,    59,    13,    61,    14,    66,    35,    36,    77,
      -1,    15,    13,    61,    -1,    15,    13,    61,    14,    66,
      -1,    16,    17,    61,    62,    -1,    16,    17,    61,    41,
      59,    42,    62,    -1,    19,    61,    20,    64,    -1,    19,
      61,    20,    64,    14,    66,    -1,    58,    -1,    57,    43,
      58,    -1,     4,    28,    41,     6,    42,    -1,     4,    29,
      -1,     4,    30,    -1,     4,    31,    -1,     4,    31,    32,
      -1,     4,    33,    -1,     4,    34,    -1,    44,    -1,    60,
      -1,     4,    -1,    60,    43,     4,    -1,     4,    -1,    18,
      41,    63,    42,    -1,    27,    -1,     5,    -1,     6,    -1,
      45,     6,    -1,     7,    -1,    45,     7,    -1,    63,    43,
      27,    -1,    63,    43,     5,    -1,    63,    43,     6,    -1,
      63,    43,    45,     6,    -1,    63,    43,     7,    -1,    63,
      43,    45,     7,    -1,    65,    -1,    64,    43,    65,    -1,
       4,    11,    27,    -1,     4,    11,    71,    -1,    67,    -1,
      68,    -1,    67,    22,    68,    -1,    69,    -1,    68,    21,
      69,    -1,    70,    -1,    41,    67,    42,    -1,    23,    69,
      -1,    71,    11,    71,    -1,    71,     3,    71,    -1,    71,
      37,    27,    -1,    71,    23,    27,    -1,    72,    -1,    71,
      46,    72,    -1,    71,    45,    72,    -1,    73,    -1,    72,
      44,    73,    -1,    72,    47,    73,    -1,    74,    -1,    45,
      73,    -1,    75,    -1,    76,    -1,    41,    71,    42,    -1,
       5,    -1,     6,    -1,     7,    -1,     4,    -1,    78,    -1,
      79,    -1,     4,    -1,     4,    38,    -1,     4,    39,    -1
};

/* YYRLINE[YYN] -- source line where rule number YYN was defined.  */
static const yytype_uint16 yyrline[] =
{
       0,    97,    97,    98,    99,   100,   101,   102,   103,   104,
     108,   109,   110,   114,   118,   122,   123,   124,   125,   129,
     130,   134,   135,   139,   140,   145,   146,   150,   151,   152,
     153,   154,   155,   156,   160,   161,   165,   166,   170,   174,
     178,   179,   180,   181,   182,   183,   184,   185,   186,   187,
     188,   189,   193,   194,   198,   202,   206,   213,   214,   218,
     219,   223,   224,   225,   230,   233,   236,   239,   246,   247,
     250,   256,   257,   260,   266,   267,   273,   274,   275,   280,
     281,   282,   287,   290,   290,   293,   294,   297
};
#endif

#if YYDEBUG || YYERROR_VERBOSE || YYTOKEN_TABLE
/* YYTNAME[SYMBOL-NUM] -- String name of the symbol SYMBOL-NUM.
   First, the terminals, then, starting at YYNTOKENS, nonterminals.  */
static const char *const yytname[] =
{
  "$end", "error", "$undefined", "COMPARISON_OPERATOR", "NAME", "STRING",
  "INTNUM", "FLOATNUM", "ADD", "DROP", "COLUMN", "EQUAL", "SELECT", "FROM",
  "WHERE", "DELETE", "INSERT", "INTO", "VALUES", "UPDATE", "SET", "AND",
  "OR", "NOT", "ALTER", "TABLE", "CREATE", "NULL_VALUE", "VARCHAR", "INT",
  "INTEGER", "DOUBLE", "PRECISION", "DATE", "TIME", "ORDER", "BY", "IS",
  "ASC", "DESC", "';'", "'('", "')'", "','", "'*'", "'-'", "'+'", "'/'",
  "$accept", "y_sql", "y_alter", "y_create", "y_drop", "y_select",
  "y_delete", "y_insert", "y_update", "y_columndefs", "y_columndef",
  "y_columns", "y_column_list", "y_table", "y_values", "y_value_list",
  "y_assignments", "y_assignment", "y_condition", "y_sub_condition",
  "y_sub_condition2", "y_boolean", "y_comparison", "y_expression",
  "y_product", "y_term", "y_atom", "y_value", "y_column", "y_order",
  "y_order_asc", "y_order_desc", 0
};
#endif

# ifdef YYPRINT
/* YYTOKNUM[YYLEX-NUM] -- Internal token number corresponding to
   token YYLEX-NUM.  */
static const yytype_uint16 yytoknum[] =
{
       0,   256,   257,   258,   259,   260,   261,   262,   263,   264,
     265,   266,   267,   268,   269,   270,   271,   272,   273,   274,
     275,   276,   277,   278,   279,   280,   281,   282,   283,   284,
     285,   286,   287,   288,   289,   290,   291,   292,   293,   294,
      59,    40,    41,    44,    42,    45,    43,    47
};
# endif

/* YYR1[YYN] -- Symbol number of symbol that rule YYN derives.  */
static const yytype_uint8 yyr1[] =
{
       0,    48,    49,    49,    49,    49,    49,    49,    49,    49,
      50,    50,    50,    51,    52,    53,    53,    53,    53,    54,
      54,    55,    55,    56,    56,    57,    57,    58,    58,    58,
      58,    58,    58,    58,    59,    59,    60,    60,    61,    62,
      63,    63,    63,    63,    63,    63,    63,    63,    63,    63,
      63,    63,    64,    64,    65,    65,    66,    67,    67,    68,
      68,    69,    69,    69,    70,    70,    70,    70,    71,    71,
      71,    72,    72,    72,    73,    73,    74,    74,    74,    75,
      75,    75,    76,    77,    77,    78,    78,    79
};

/* YYR2[YYN] -- Number of symbols composing right hand side of rule YYN.  */
static const yytype_uint8 yyr2[] =
{
       0,     2,     1,     1,     1,     1,     1,     1,     1,     2,
       6,     5,     6,     6,     3,     4,     6,     7,     9,     3,
       5,     4,     7,     4,     6,     1,     3,     5,     2,     2,
       2,     3,     2,     2,     1,     1,     1,     3,     1,     4,
       1,     1,     1,     2,     1,     2,     3,     3,     3,     4,
       3,     4,     1,     3,     3,     3,     1,     1,     3,     1,
       3,     1,     3,     2,     3,     3,     3,     3,     1,     3,
       3,     1,     3,     3,     1,     2,     1,     1,     3,     1,
       1,     1,     1,     1,     1,     1,     2,     2
};

/* YYDEFACT[STATE-NAME] -- Default rule to reduce with in state
   STATE-NUM when YYTABLE doesn't specify something else to do.  Zero
   means the default is an error.  */
static const yytype_uint8 yydefact[] =
{
       0,     0,     0,     0,     0,     0,     0,     0,     0,     2,
       3,     4,     6,     8,     5,     7,     0,    36,    34,     0,
      35,     0,     0,    38,     0,     0,     0,     1,     9,    14,
       0,     0,    19,     0,     0,     0,     0,    15,    37,     0,
       0,     0,    21,     0,    23,    52,     0,     0,     0,     0,
       0,    82,    79,    80,    81,     0,     0,     0,    20,    56,
      57,    59,    61,     0,    68,    71,    74,    76,    77,     0,
       0,     0,     0,     0,     0,     0,    11,     0,     0,    25,
      16,     0,    63,     0,     0,     0,    75,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,    41,    42,    44,
      40,     0,     0,     0,    54,    55,    24,    53,     0,    28,
      29,    30,    32,    33,    10,    12,    13,     0,     0,    85,
      17,    83,    84,    62,    78,     0,    58,    60,    65,    64,
      67,    66,    70,    69,    72,    73,    43,    45,    39,     0,
      22,     0,    31,    26,     0,    86,    87,    47,    48,    50,
      46,     0,     0,    18,    49,    51,    27
};

/* YYDEFGOTO[NTERM-NUM].  */
static const yytype_int8 yydefgoto[] =
{
      -1,     8,     9,    10,    11,    12,    13,    14,    15,    78,
      76,    19,    20,    24,    42,   102,    44,    45,    58,    59,
      60,    61,    62,    63,    64,    65,    66,    67,    68,   120,
     121,   122
};

/* YYPACT[STATE-NUM] -- Index in YYTABLE of the portion describing
   STATE-NUM.  */
#define YYPACT_NINF -52
static const yytype_int16 yypact[] =
{
      64,    -8,     3,    11,     5,    32,    49,    57,     2,   -52,
     -52,   -52,   -52,   -52,   -52,   -52,    32,   -52,   -52,    39,
      41,    32,    32,   -52,   102,    32,    32,   -52,   -52,   -52,
      32,    85,   109,     7,    95,    10,    83,    37,   -52,     8,
      84,     3,   -52,   115,   -10,   -52,     6,   117,   124,     8,
      93,   -52,   -52,   -52,   -52,     8,     8,    55,   -52,   108,
     110,   -52,   -52,     0,    31,   -52,   -52,   -52,   -52,    23,
      90,    50,     8,    95,    79,   124,   -52,   129,    24,   -52,
      99,   131,   -52,   -16,    -2,    55,   -52,     8,     8,    55,
      55,   111,   113,    55,    55,    55,    55,   -52,   -52,   -52,
     -52,    80,    51,   118,   -52,    59,   -52,   -52,    96,   -52,
     -52,   107,   -52,   -52,   -52,   -52,   -52,   124,   105,    78,
     -52,   -52,   -52,   -52,   -52,    69,   110,   -52,    59,    59,
     -52,   -52,    31,    31,   -52,   -52,   -52,   -52,   -52,    58,
     -52,   136,   -52,   -52,   131,   -52,   -52,   -52,   -52,   -52,
     -52,   112,   101,   -52,   -52,   -52,   -52
};

/* YYPGOTO[NTERM-NUM].  */
static const yytype_int8 yypgoto[] =
{
     -52,   -52,   -52,   -52,   -52,   -52,   -52,   -52,   -52,   -52,
     -48,   103,   -52,    76,    42,   -52,   -52,    73,     9,    91,
      61,   -47,   -52,   -51,    27,   -25,   -52,   -52,   -52,    12,
     -52,   -52
};

/* YYTABLE[YYPACT[STATE-NUM]].  What to do in state STATE-NUM.  If
   positive, shift that token.  If negative, reduce the rule which
   number is the opposite.  If zero, do what YYDEFACT says.
   If YYTABLE_NINF, syntax error.  */
#define YYTABLE_NINF -1
static const yytype_uint8 yytable[] =
{
      79,    89,    27,    89,    72,    84,    87,    17,    82,    90,
      74,    90,    51,    52,    53,    54,    75,    16,    46,    47,
     105,    91,    22,    91,    21,    40,   123,   114,    97,    98,
      99,    55,    86,    73,   125,    92,    23,    92,   128,   129,
     124,   127,    28,    93,    94,    93,    94,    18,    41,    56,
     100,    49,    30,    57,    51,    52,    53,    54,    80,    51,
      52,    53,    54,   147,   148,   149,   116,   117,   101,   143,
     134,   135,    50,     1,    25,    95,     2,   104,    96,     3,
       4,   106,    26,     5,    31,   150,   136,   137,     6,    38,
       7,    85,    29,   138,   139,    57,    85,    32,    33,    43,
      57,    35,    36,   151,    93,    94,    37,   108,   109,   110,
     111,   124,   112,   113,    93,    94,   145,   146,   154,   155,
     132,   133,    34,    39,    48,    69,    71,    77,    74,    81,
      87,    88,   103,   115,   118,   119,    40,   141,   130,   142,
     131,   144,   152,   156,    70,   140,   107,    83,   126,     0,
       0,     0,     0,     0,     0,     0,   153
};

static const yytype_int16 yycheck[] =
{
      48,     3,     0,     3,    14,    56,    22,     4,    55,    11,
       4,    11,     4,     5,     6,     7,    10,    25,     8,     9,
      71,    23,    17,    23,    13,    18,    42,    75,     5,     6,
       7,    23,    57,    43,    85,    37,     4,    37,    89,    90,
      42,    88,    40,    45,    46,    45,    46,    44,    41,    41,
      27,    14,    13,    45,     4,     5,     6,     7,    49,     4,
       5,     6,     7,     5,     6,     7,    42,    43,    45,   117,
      95,    96,    35,     9,    25,    44,    12,    27,    47,    15,
      16,    72,    25,    19,    43,    27,     6,     7,    24,     4,
      26,    41,    16,    42,    43,    45,    41,    21,    22,     4,
      45,    25,    26,    45,    45,    46,    30,    28,    29,    30,
      31,    42,    33,    34,    45,    46,    38,    39,     6,     7,
      93,    94,    20,    14,    41,    41,    11,    10,     4,    36,
      22,    21,    42,     4,    35,     4,    18,    41,    27,    32,
      27,    36,     6,    42,    41,   103,    73,    56,    87,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,   144
};

/* YYSTOS[STATE-NUM] -- The (internal number of the) accessing
   symbol of state STATE-NUM.  */
static const yytype_uint8 yystos[] =
{
       0,     9,    12,    15,    16,    19,    24,    26,    49,    50,
      51,    52,    53,    54,    55,    56,    25,     4,    44,    59,
      60,    13,    17,     4,    61,    25,    25,     0,    40,    61,
      13,    43,    61,    61,    20,    61,    61,    61,     4,    14,
      18,    41,    62,     4,    64,    65,     8,     9,    41,    14,
      35,     4,     5,     6,     7,    23,    41,    45,    66,    67,
      68,    69,    70,    71,    72,    73,    74,    75,    76,    41,
      59,    11,    14,    43,     4,    10,    58,    10,    57,    58,
      66,    36,    69,    67,    71,    41,    73,    22,    21,     3,
      11,    23,    37,    45,    46,    44,    47,     5,     6,     7,
      27,    45,    63,    42,    27,    71,    66,    65,    28,    29,
      30,    31,    33,    34,    58,     4,    42,    43,    35,     4,
      77,    78,    79,    42,    42,    71,    68,    69,    71,    71,
      27,    27,    72,    72,    73,    73,     6,     7,    42,    43,
      62,    41,    32,    58,    36,    38,    39,     5,     6,     7,
      27,    45,     6,    77,     6,     7,    42
};

#define yyerrok		(yyerrstatus = 0)
#define yyclearin	(yychar = YYEMPTY)
#define YYEMPTY		(-2)
#define YYEOF		0

#define YYACCEPT	goto yyacceptlab
#define YYABORT		goto yyabortlab
#define YYERROR		goto yyerrorlab


/* Like YYERROR except do call yyerror.  This remains here temporarily
   to ease the transition to the new meaning of YYERROR, for GCC.
   Once GCC version 2 has supplanted version 1, this can go.  */

#define YYFAIL		goto yyerrlab

#define YYRECOVERING()  (!!yyerrstatus)

#define YYBACKUP(Token, Value)					\
do								\
  if (yychar == YYEMPTY && yylen == 1)				\
    {								\
      yychar = (Token);						\
      yylval = (Value);						\
      yytoken = YYTRANSLATE (yychar);				\
      YYPOPSTACK (1);						\
      goto yybackup;						\
    }								\
  else								\
    {								\
      yyerror (YY_("syntax error: cannot back up")); \
      YYERROR;							\
    }								\
while (YYID (0))


#define YYTERROR	1
#define YYERRCODE	256


/* YYLLOC_DEFAULT -- Set CURRENT to span from RHS[1] to RHS[N].
   If N is 0, then set CURRENT to the empty location which ends
   the previous symbol: RHS[0] (always defined).  */

#define YYRHSLOC(Rhs, K) ((Rhs)[K])
#ifndef YYLLOC_DEFAULT
# define YYLLOC_DEFAULT(Current, Rhs, N)				\
    do									\
      if (YYID (N))                                                    \
	{								\
	  (Current).first_line   = YYRHSLOC (Rhs, 1).first_line;	\
	  (Current).first_column = YYRHSLOC (Rhs, 1).first_column;	\
	  (Current).last_line    = YYRHSLOC (Rhs, N).last_line;		\
	  (Current).last_column  = YYRHSLOC (Rhs, N).last_column;	\
	}								\
      else								\
	{								\
	  (Current).first_line   = (Current).last_line   =		\
	    YYRHSLOC (Rhs, 0).last_line;				\
	  (Current).first_column = (Current).last_column =		\
	    YYRHSLOC (Rhs, 0).last_column;				\
	}								\
    while (YYID (0))
#endif


/* YY_LOCATION_PRINT -- Print the location on the stream.
   This macro was not mandated originally: define only if we know
   we won't break user code: when these are the locations we know.  */

#ifndef YY_LOCATION_PRINT
# if YYLTYPE_IS_TRIVIAL
#  define YY_LOCATION_PRINT(File, Loc)			\
     fprintf (File, "%d.%d-%d.%d",			\
	      (Loc).first_line, (Loc).first_column,	\
	      (Loc).last_line,  (Loc).last_column)
# else
#  define YY_LOCATION_PRINT(File, Loc) ((void) 0)
# endif
#endif


/* YYLEX -- calling `yylex' with the right arguments.  */

#ifdef YYLEX_PARAM
# define YYLEX yylex (YYLEX_PARAM)
#else
# define YYLEX yylex ()
#endif

/* Enable debugging if requested.  */
#if YYDEBUG

# ifndef YYFPRINTF
#  include <stdio.h> /* INFRINGES ON USER NAME SPACE */
#  define YYFPRINTF fprintf
# endif

# define YYDPRINTF(Args)			\
do {						\
  if (yydebug)					\
    YYFPRINTF Args;				\
} while (YYID (0))

# define YY_SYMBOL_PRINT(Title, Type, Value, Location)			  \
do {									  \
  if (yydebug)								  \
    {									  \
      YYFPRINTF (stderr, "%s ", Title);					  \
      yy_symbol_print (stderr,						  \
		  Type, Value); \
      YYFPRINTF (stderr, "\n");						  \
    }									  \
} while (YYID (0))


/*--------------------------------.
| Print this symbol on YYOUTPUT.  |
`--------------------------------*/

/*ARGSUSED*/
#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static void
yy_symbol_value_print (FILE *yyoutput, int yytype, YYSTYPE const * const yyvaluep)
#else
static void
yy_symbol_value_print (yyoutput, yytype, yyvaluep)
    FILE *yyoutput;
    int yytype;
    YYSTYPE const * const yyvaluep;
#endif
{
  if (!yyvaluep)
    return;
# ifdef YYPRINT
  if (yytype < YYNTOKENS)
    YYPRINT (yyoutput, yytoknum[yytype], *yyvaluep);
# else
  YYUSE (yyoutput);
# endif
  switch (yytype)
    {
      default:
	break;
    }
}


/*--------------------------------.
| Print this symbol on YYOUTPUT.  |
`--------------------------------*/

#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static void
yy_symbol_print (FILE *yyoutput, int yytype, YYSTYPE const * const yyvaluep)
#else
static void
yy_symbol_print (yyoutput, yytype, yyvaluep)
    FILE *yyoutput;
    int yytype;
    YYSTYPE const * const yyvaluep;
#endif
{
  if (yytype < YYNTOKENS)
    YYFPRINTF (yyoutput, "token %s (", yytname[yytype]);
  else
    YYFPRINTF (yyoutput, "nterm %s (", yytname[yytype]);

  yy_symbol_value_print (yyoutput, yytype, yyvaluep);
  YYFPRINTF (yyoutput, ")");
}

/*------------------------------------------------------------------.
| yy_stack_print -- Print the state stack from its BOTTOM up to its |
| TOP (included).                                                   |
`------------------------------------------------------------------*/

#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static void
yy_stack_print (yytype_int16 *yybottom, yytype_int16 *yytop)
#else
static void
yy_stack_print (yybottom, yytop)
    yytype_int16 *yybottom;
    yytype_int16 *yytop;
#endif
{
  YYFPRINTF (stderr, "Stack now");
  for (; yybottom <= yytop; yybottom++)
    {
      int yybot = *yybottom;
      YYFPRINTF (stderr, " %d", yybot);
    }
  YYFPRINTF (stderr, "\n");
}

# define YY_STACK_PRINT(Bottom, Top)				\
do {								\
  if (yydebug)							\
    yy_stack_print ((Bottom), (Top));				\
} while (YYID (0))


/*------------------------------------------------.
| Report that the YYRULE is going to be reduced.  |
`------------------------------------------------*/

#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static void
yy_reduce_print (YYSTYPE *yyvsp, int yyrule)
#else
static void
yy_reduce_print (yyvsp, yyrule)
    YYSTYPE *yyvsp;
    int yyrule;
#endif
{
  int yynrhs = yyr2[yyrule];
  int yyi;
  unsigned long int yylno = yyrline[yyrule];
  YYFPRINTF (stderr, "Reducing stack by rule %d (line %lu):\n",
	     yyrule - 1, yylno);
  /* The symbols being reduced.  */
  for (yyi = 0; yyi < yynrhs; yyi++)
    {
      YYFPRINTF (stderr, "   $%d = ", yyi + 1);
      yy_symbol_print (stderr, yyrhs[yyprhs[yyrule] + yyi],
		       &(yyvsp[(yyi + 1) - (yynrhs)])
		       		       );
      YYFPRINTF (stderr, "\n");
    }
}

# define YY_REDUCE_PRINT(Rule)		\
do {					\
  if (yydebug)				\
    yy_reduce_print (yyvsp, Rule); \
} while (YYID (0))

/* Nonzero means print parse trace.  It is left uninitialized so that
   multiple parsers can coexist.  */
int yydebug;
#else /* !YYDEBUG */
# define YYDPRINTF(Args)
# define YY_SYMBOL_PRINT(Title, Type, Value, Location)
# define YY_STACK_PRINT(Bottom, Top)
# define YY_REDUCE_PRINT(Rule)
#endif /* !YYDEBUG */


/* YYINITDEPTH -- initial size of the parser's stacks.  */
#ifndef	YYINITDEPTH
# define YYINITDEPTH 200
#endif

/* YYMAXDEPTH -- maximum size the stacks can grow to (effective only
   if the built-in stack extension method is used).

   Do not make this value too large; the results are undefined if
   YYSTACK_ALLOC_MAXIMUM < YYSTACK_BYTES (YYMAXDEPTH)
   evaluated with infinite-precision integer arithmetic.  */

#ifndef YYMAXDEPTH
# define YYMAXDEPTH 10000
#endif



#if YYERROR_VERBOSE

# ifndef yystrlen
#  if defined __GLIBC__ && defined _STRING_H
#   define yystrlen strlen
#  else
/* Return the length of YYSTR.  */
#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static YYSIZE_T
yystrlen (const char *yystr)
#else
static YYSIZE_T
yystrlen (yystr)
    const char *yystr;
#endif
{
  YYSIZE_T yylen;
  for (yylen = 0; yystr[yylen]; yylen++)
    continue;
  return yylen;
}
#  endif
# endif

# ifndef yystpcpy
#  if defined __GLIBC__ && defined _STRING_H && defined _GNU_SOURCE
#   define yystpcpy stpcpy
#  else
/* Copy YYSRC to YYDEST, returning the address of the terminating '\0' in
   YYDEST.  */
#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static char *
yystpcpy (char *yydest, const char *yysrc)
#else
static char *
yystpcpy (yydest, yysrc)
    char *yydest;
    const char *yysrc;
#endif
{
  char *yyd = yydest;
  const char *yys = yysrc;

  while ((*yyd++ = *yys++) != '\0')
    continue;

  return yyd - 1;
}
#  endif
# endif

# ifndef yytnamerr
/* Copy to YYRES the contents of YYSTR after stripping away unnecessary
   quotes and backslashes, so that it's suitable for yyerror.  The
   heuristic is that double-quoting is unnecessary unless the string
   contains an apostrophe, a comma, or backslash (other than
   backslash-backslash).  YYSTR is taken from yytname.  If YYRES is
   null, do not copy; instead, return the length of what the result
   would have been.  */
static YYSIZE_T
yytnamerr (char *yyres, const char *yystr)
{
  if (*yystr == '"')
    {
      YYSIZE_T yyn = 0;
      char const *yyp = yystr;

      for (;;)
	switch (*++yyp)
	  {
	  case '\'':
	  case ',':
	    goto do_not_strip_quotes;

	  case '\\':
	    if (*++yyp != '\\')
	      goto do_not_strip_quotes;
	    /* Fall through.  */
	  default:
	    if (yyres)
	      yyres[yyn] = *yyp;
	    yyn++;
	    break;

	  case '"':
	    if (yyres)
	      yyres[yyn] = '\0';
	    return yyn;
	  }
    do_not_strip_quotes: ;
    }

  if (! yyres)
    return yystrlen (yystr);

  return yystpcpy (yyres, yystr) - yyres;
}
# endif

/* Copy into YYRESULT an error message about the unexpected token
   YYCHAR while in state YYSTATE.  Return the number of bytes copied,
   including the terminating null byte.  If YYRESULT is null, do not
   copy anything; just return the number of bytes that would be
   copied.  As a special case, return 0 if an ordinary "syntax error"
   message will do.  Return YYSIZE_MAXIMUM if overflow occurs during
   size calculation.  */
static YYSIZE_T
yysyntax_error (char *yyresult, int yystate, int yychar)
{
  int yyn = yypact[yystate];

  if (! (YYPACT_NINF < yyn && yyn <= YYLAST))
    return 0;
  else
    {
      int yytype = YYTRANSLATE (yychar);
      YYSIZE_T yysize0 = yytnamerr (0, yytname[yytype]);
      YYSIZE_T yysize = yysize0;
      YYSIZE_T yysize1;
      int yysize_overflow = 0;
      enum { YYERROR_VERBOSE_ARGS_MAXIMUM = 5 };
      char const *yyarg[YYERROR_VERBOSE_ARGS_MAXIMUM];
      int yyx;

# if 0
      /* This is so xgettext sees the translatable formats that are
	 constructed on the fly.  */
      YY_("syntax error, unexpected %s");
      YY_("syntax error, unexpected %s, expecting %s");
      YY_("syntax error, unexpected %s, expecting %s or %s");
      YY_("syntax error, unexpected %s, expecting %s or %s or %s");
      YY_("syntax error, unexpected %s, expecting %s or %s or %s or %s");
# endif
      char *yyfmt;
      char const *yyf;
      static char const yyunexpected[] = "syntax error, unexpected %s";
      static char const yyexpecting[] = ", expecting %s";
      static char const yyor[] = " or %s";
      char yyformat[sizeof yyunexpected
		    + sizeof yyexpecting - 1
		    + ((YYERROR_VERBOSE_ARGS_MAXIMUM - 2)
		       * (sizeof yyor - 1))];
      char const *yyprefix = yyexpecting;

      /* Start YYX at -YYN if negative to avoid negative indexes in
	 YYCHECK.  */
      int yyxbegin = yyn < 0 ? -yyn : 0;

      /* Stay within bounds of both yycheck and yytname.  */
      int yychecklim = YYLAST - yyn + 1;
      int yyxend = yychecklim < YYNTOKENS ? yychecklim : YYNTOKENS;
      int yycount = 1;

      yyarg[0] = yytname[yytype];
      yyfmt = yystpcpy (yyformat, yyunexpected);

      for (yyx = yyxbegin; yyx < yyxend; ++yyx)
	if (yycheck[yyx + yyn] == yyx && yyx != YYTERROR)
	  {
	    if (yycount == YYERROR_VERBOSE_ARGS_MAXIMUM)
	      {
		yycount = 1;
		yysize = yysize0;
		yyformat[sizeof yyunexpected - 1] = '\0';
		break;
	      }
	    yyarg[yycount++] = yytname[yyx];
	    yysize1 = yysize + yytnamerr (0, yytname[yyx]);
	    yysize_overflow |= (yysize1 < yysize);
	    yysize = yysize1;
	    yyfmt = yystpcpy (yyfmt, yyprefix);
	    yyprefix = yyor;
	  }

      yyf = YY_(yyformat);
      yysize1 = yysize + yystrlen (yyf);
      yysize_overflow |= (yysize1 < yysize);
      yysize = yysize1;

      if (yysize_overflow)
	return YYSIZE_MAXIMUM;

      if (yyresult)
	{
	  /* Avoid sprintf, as that infringes on the user's name space.
	     Don't have undefined behavior even if the translation
	     produced a string with the wrong number of "%s"s.  */
	  char *yyp = yyresult;
	  int yyi = 0;
	  while ((*yyp = *yyf) != '\0')
	    {
	      if (*yyp == '%' && yyf[1] == 's' && yyi < yycount)
		{
		  yyp += yytnamerr (yyp, yyarg[yyi++]);
		  yyf += 2;
		}
	      else
		{
		  yyp++;
		  yyf++;
		}
	    }
	}
      return yysize;
    }
}
#endif /* YYERROR_VERBOSE */


/*-----------------------------------------------.
| Release the memory associated to this symbol.  |
`-----------------------------------------------*/

/*ARGSUSED*/
#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static void
yydestruct (const char *yymsg, int yytype, YYSTYPE *yyvaluep)
#else
static void
yydestruct (yymsg, yytype, yyvaluep)
    const char *yymsg;
    int yytype;
    YYSTYPE *yyvaluep;
#endif
{
  YYUSE (yyvaluep);

  if (!yymsg)
    yymsg = "Deleting";
  YY_SYMBOL_PRINT (yymsg, yytype, yyvaluep, yylocationp);

  switch (yytype)
    {

      default:
	break;
    }
}

/* Prevent warnings from -Wmissing-prototypes.  */
#ifdef YYPARSE_PARAM
#if defined __STDC__ || defined __cplusplus
int yyparse (void *YYPARSE_PARAM);
#else
int yyparse ();
#endif
#else /* ! YYPARSE_PARAM */
#if defined __STDC__ || defined __cplusplus
int yyparse (void);
#else
int yyparse ();
#endif
#endif /* ! YYPARSE_PARAM */


/* The lookahead symbol.  */
int yychar;

/* The semantic value of the lookahead symbol.  */
YYSTYPE yylval;

/* Number of syntax errors so far.  */
int yynerrs;



/*-------------------------.
| yyparse or yypush_parse.  |
`-------------------------*/

#ifdef YYPARSE_PARAM
#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
int
yyparse (void *YYPARSE_PARAM)
#else
int
yyparse (YYPARSE_PARAM)
    void *YYPARSE_PARAM;
#endif
#else /* ! YYPARSE_PARAM */
#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
int
yyparse (void)
#else
int
yyparse ()

#endif
#endif
{


    int yystate;
    /* Number of tokens to shift before error messages enabled.  */
    int yyerrstatus;

    /* The stacks and their tools:
       `yyss': related to states.
       `yyvs': related to semantic values.

       Refer to the stacks thru separate pointers, to allow yyoverflow
       to reallocate them elsewhere.  */

    /* The state stack.  */
    yytype_int16 yyssa[YYINITDEPTH];
    yytype_int16 *yyss;
    yytype_int16 *yyssp;

    /* The semantic value stack.  */
    YYSTYPE yyvsa[YYINITDEPTH];
    YYSTYPE *yyvs;
    YYSTYPE *yyvsp;

    YYSIZE_T yystacksize;

  int yyn;
  int yyresult;
  /* Lookahead token as an internal (translated) token number.  */
  int yytoken;
  /* The variables used to return semantic value and location from the
     action routines.  */
  YYSTYPE yyval;

#if YYERROR_VERBOSE
  /* Buffer for error messages, and its allocated size.  */
  char yymsgbuf[128];
  char *yymsg = yymsgbuf;
  YYSIZE_T yymsg_alloc = sizeof yymsgbuf;
#endif

#define YYPOPSTACK(N)   (yyvsp -= (N), yyssp -= (N))

  /* The number of symbols on the RHS of the reduced rule.
     Keep to zero when no symbol should be popped.  */
  int yylen = 0;

  yytoken = 0;
  yyss = yyssa;
  yyvs = yyvsa;
  yystacksize = YYINITDEPTH;

  YYDPRINTF ((stderr, "Starting parse\n"));

  yystate = 0;
  yyerrstatus = 0;
  yynerrs = 0;
  yychar = YYEMPTY; /* Cause a token to be read.  */

  /* Initialize stack pointers.
     Waste one element of value and location stack
     so that they stay on the same level as the state stack.
     The wasted elements are never initialized.  */
  yyssp = yyss;
  yyvsp = yyvs;

  goto yysetstate;

/*------------------------------------------------------------.
| yynewstate -- Push a new state, which is found in yystate.  |
`------------------------------------------------------------*/
 yynewstate:
  /* In all cases, when you get here, the value and location stacks
     have just been pushed.  So pushing a state here evens the stacks.  */
  yyssp++;

 yysetstate:
  *yyssp = yystate;

  if (yyss + yystacksize - 1 <= yyssp)
    {
      /* Get the current used size of the three stacks, in elements.  */
      YYSIZE_T yysize = yyssp - yyss + 1;

#ifdef yyoverflow
      {
	/* Give user a chance to reallocate the stack.  Use copies of
	   these so that the &'s don't force the real ones into
	   memory.  */
	YYSTYPE *yyvs1 = yyvs;
	yytype_int16 *yyss1 = yyss;

	/* Each stack pointer address is followed by the size of the
	   data in use in that stack, in bytes.  This used to be a
	   conditional around just the two extra args, but that might
	   be undefined if yyoverflow is a macro.  */
	yyoverflow (YY_("memory exhausted"),
		    &yyss1, yysize * sizeof (*yyssp),
		    &yyvs1, yysize * sizeof (*yyvsp),
		    &yystacksize);

	yyss = yyss1;
	yyvs = yyvs1;
      }
#else /* no yyoverflow */
# ifndef YYSTACK_RELOCATE
      goto yyexhaustedlab;
# else
      /* Extend the stack our own way.  */
      if (YYMAXDEPTH <= yystacksize)
	goto yyexhaustedlab;
      yystacksize *= 2;
      if (YYMAXDEPTH < yystacksize)
	yystacksize = YYMAXDEPTH;

      {
	yytype_int16 *yyss1 = yyss;
	union yyalloc *yyptr =
	  (union yyalloc *) YYSTACK_ALLOC (YYSTACK_BYTES (yystacksize));
	if (! yyptr)
	  goto yyexhaustedlab;
	YYSTACK_RELOCATE (yyss_alloc, yyss);
	YYSTACK_RELOCATE (yyvs_alloc, yyvs);
#  undef YYSTACK_RELOCATE
	if (yyss1 != yyssa)
	  YYSTACK_FREE (yyss1);
      }
# endif
#endif /* no yyoverflow */

      yyssp = yyss + yysize - 1;
      yyvsp = yyvs + yysize - 1;

      YYDPRINTF ((stderr, "Stack size increased to %lu\n",
		  (unsigned long int) yystacksize));

      if (yyss + yystacksize - 1 <= yyssp)
	YYABORT;
    }

  YYDPRINTF ((stderr, "Entering state %d\n", yystate));

  if (yystate == YYFINAL)
    YYACCEPT;

  goto yybackup;

/*-----------.
| yybackup.  |
`-----------*/
yybackup:

  /* Do appropriate processing given the current state.  Read a
     lookahead token if we need one and don't already have one.  */

  /* First try to decide what to do without reference to lookahead token.  */
  yyn = yypact[yystate];
  if (yyn == YYPACT_NINF)
    goto yydefault;

  /* Not known => get a lookahead token if don't already have one.  */

  /* YYCHAR is either YYEMPTY or YYEOF or a valid lookahead symbol.  */
  if (yychar == YYEMPTY)
    {
      YYDPRINTF ((stderr, "Reading a token: "));
      yychar = YYLEX;
    }

  if (yychar <= YYEOF)
    {
      yychar = yytoken = YYEOF;
      YYDPRINTF ((stderr, "Now at end of input.\n"));
    }
  else
    {
      yytoken = YYTRANSLATE (yychar);
      YY_SYMBOL_PRINT ("Next token is", yytoken, &yylval, &yylloc);
    }

  /* If the proper action on seeing token YYTOKEN is to reduce or to
     detect an error, take that action.  */
  yyn += yytoken;
  if (yyn < 0 || YYLAST < yyn || yycheck[yyn] != yytoken)
    goto yydefault;
  yyn = yytable[yyn];
  if (yyn <= 0)
    {
      if (yyn == 0 || yyn == YYTABLE_NINF)
	goto yyerrlab;
      yyn = -yyn;
      goto yyreduce;
    }

  /* Count tokens shifted since error; after three, turn off error
     status.  */
  if (yyerrstatus)
    yyerrstatus--;

  /* Shift the lookahead token.  */
  YY_SYMBOL_PRINT ("Shifting", yytoken, &yylval, &yylloc);

  /* Discard the shifted token.  */
  yychar = YYEMPTY;

  yystate = yyn;
  *++yyvsp = yylval;

  goto yynewstate;


/*-----------------------------------------------------------.
| yydefault -- do the default action for the current state.  |
`-----------------------------------------------------------*/
yydefault:
  yyn = yydefact[yystate];
  if (yyn == 0)
    goto yyerrlab;
  goto yyreduce;


/*-----------------------------.
| yyreduce -- Do a reduction.  |
`-----------------------------*/
yyreduce:
  /* yyn is the number of a rule to reduce with.  */
  yylen = yyr2[yyn];

  /* If YYLEN is nonzero, implement the default value of the action:
     `$$ = $1'.

     Otherwise, the following line sets YYVAL to garbage.
     This behavior is undocumented and Bison
     users should not rely upon it.  Assigning to YYVAL
     unconditionally makes the parser a bit smaller, and it avoids a
     GCC warning that YYVAL may be used uninitialized.  */
  yyval = yyvsp[1-yylen];


  YY_REDUCE_PRINT (yyn);
  switch (yyn)
    {
        case 10:

/* Line 1455 of yacc.c  */
#line 108 "sqlp.y"
    { sqpCommand(SQLP_ADD_COLUMN); }
    break;

  case 11:

/* Line 1455 of yacc.c  */
#line 109 "sqlp.y"
    { sqpCommand(SQLP_ADD_COLUMN); }
    break;

  case 12:

/* Line 1455 of yacc.c  */
#line 110 "sqlp.y"
    { sqpCommand(SQLP_DROP_COLUMN); sqpColumn((yyvsp[(6) - (6)].strval));}
    break;

  case 13:

/* Line 1455 of yacc.c  */
#line 114 "sqlp.y"
    { sqpCommand(SQLP_CREATE); }
    break;

  case 14:

/* Line 1455 of yacc.c  */
#line 118 "sqlp.y"
    { sqpCommand(SQLP_DROP); }
    break;

  case 15:

/* Line 1455 of yacc.c  */
#line 122 "sqlp.y"
    { sqpCommand(SQLP_SELECT); }
    break;

  case 16:

/* Line 1455 of yacc.c  */
#line 123 "sqlp.y"
    { sqpCommand(SQLP_SELECT); }
    break;

  case 17:

/* Line 1455 of yacc.c  */
#line 124 "sqlp.y"
    { sqpCommand(SQLP_SELECT); }
    break;

  case 18:

/* Line 1455 of yacc.c  */
#line 125 "sqlp.y"
    { sqpCommand(SQLP_SELECT); }
    break;

  case 19:

/* Line 1455 of yacc.c  */
#line 129 "sqlp.y"
    { sqpCommand(SQLP_DELETE); }
    break;

  case 20:

/* Line 1455 of yacc.c  */
#line 130 "sqlp.y"
    { sqpCommand(SQLP_DELETE); }
    break;

  case 21:

/* Line 1455 of yacc.c  */
#line 134 "sqlp.y"
    { sqpCommand(SQLP_INSERT); }
    break;

  case 22:

/* Line 1455 of yacc.c  */
#line 135 "sqlp.y"
    { sqpCommand(SQLP_INSERT); }
    break;

  case 23:

/* Line 1455 of yacc.c  */
#line 139 "sqlp.y"
    { sqpCommand(SQLP_UPDATE); }
    break;

  case 24:

/* Line 1455 of yacc.c  */
#line 140 "sqlp.y"
    { sqpCommand(SQLP_UPDATE); }
    break;

  case 27:

/* Line 1455 of yacc.c  */
#line 150 "sqlp.y"
    { sqpColumnDef( (yyvsp[(1) - (5)].strval), SQLP_VARCHAR, (yyvsp[(4) - (5)].intval), 0 ); }
    break;

  case 28:

/* Line 1455 of yacc.c  */
#line 151 "sqlp.y"
    { sqpColumnDef( (yyvsp[(1) - (2)].strval), SQLP_INTEGER,  0, 0 ); }
    break;

  case 29:

/* Line 1455 of yacc.c  */
#line 152 "sqlp.y"
    { sqpColumnDef( (yyvsp[(1) - (2)].strval), SQLP_INTEGER,  0, 0 ); }
    break;

  case 30:

/* Line 1455 of yacc.c  */
#line 153 "sqlp.y"
    { sqpColumnDef( (yyvsp[(1) - (2)].strval), SQLP_DOUBLE,   0, 0 ); }
    break;

  case 31:

/* Line 1455 of yacc.c  */
#line 154 "sqlp.y"
    { sqpColumnDef( (yyvsp[(1) - (3)].strval), SQLP_DOUBLE,   0, 0 ); }
    break;

  case 32:

/* Line 1455 of yacc.c  */
#line 155 "sqlp.y"
    { sqpColumnDef( (yyvsp[(1) - (2)].strval), SQLP_DATE,     0, 0 ); }
    break;

  case 33:

/* Line 1455 of yacc.c  */
#line 156 "sqlp.y"
    { sqpColumnDef( (yyvsp[(1) - (2)].strval), SQLP_TIME,     0, 0 ); }
    break;

  case 36:

/* Line 1455 of yacc.c  */
#line 165 "sqlp.y"
    { sqpColumn( (yyvsp[(1) - (1)].strval) ); }
    break;

  case 37:

/* Line 1455 of yacc.c  */
#line 166 "sqlp.y"
    { sqpColumn( (yyvsp[(3) - (3)].strval) ); }
    break;

  case 38:

/* Line 1455 of yacc.c  */
#line 170 "sqlp.y"
    { sqpTable( (yyvsp[(1) - (1)].strval) ); }
    break;

  case 40:

/* Line 1455 of yacc.c  */
#line 178 "sqlp.y"
    { sqpValue( NULL,  0, 0.0, SQLP_NULL ); }
    break;

  case 41:

/* Line 1455 of yacc.c  */
#line 179 "sqlp.y"
    { sqpValue( (yyvsp[(1) - (1)].strval),    0, 0.0, SQLP_S ); }
    break;

  case 42:

/* Line 1455 of yacc.c  */
#line 180 "sqlp.y"
    { sqpValue( NULL, (yyvsp[(1) - (1)].intval), 0.0, SQLP_I ); }
    break;

  case 43:

/* Line 1455 of yacc.c  */
#line 181 "sqlp.y"
    { sqpValue( NULL, -(yyvsp[(2) - (2)].intval), 0.0, SQLP_I ); }
    break;

  case 44:

/* Line 1455 of yacc.c  */
#line 182 "sqlp.y"
    { sqpValue( NULL,  0,  (yyvsp[(1) - (1)].floatval), SQLP_D ); }
    break;

  case 45:

/* Line 1455 of yacc.c  */
#line 183 "sqlp.y"
    { sqpValue( NULL, 0, -(yyvsp[(2) - (2)].floatval), SQLP_D ); }
    break;

  case 46:

/* Line 1455 of yacc.c  */
#line 184 "sqlp.y"
    { sqpValue( NULL,  0, 0.0, SQLP_NULL ); }
    break;

  case 47:

/* Line 1455 of yacc.c  */
#line 185 "sqlp.y"
    { sqpValue( (yyvsp[(3) - (3)].strval),    0, 0.0, SQLP_S ); }
    break;

  case 48:

/* Line 1455 of yacc.c  */
#line 186 "sqlp.y"
    { sqpValue( NULL, (yyvsp[(3) - (3)].intval), 0.0, SQLP_I ); }
    break;

  case 49:

/* Line 1455 of yacc.c  */
#line 187 "sqlp.y"
    { sqpValue( NULL, -(yyvsp[(4) - (4)].intval), 0.0, SQLP_I ); }
    break;

  case 50:

/* Line 1455 of yacc.c  */
#line 188 "sqlp.y"
    { sqpValue( NULL,  0,  (yyvsp[(3) - (3)].floatval), SQLP_D ); }
    break;

  case 51:

/* Line 1455 of yacc.c  */
#line 189 "sqlp.y"
    { sqpValue( NULL, 0, -(yyvsp[(4) - (4)].floatval), SQLP_D ); }
    break;

  case 54:

/* Line 1455 of yacc.c  */
#line 198 "sqlp.y"
    { sqpAssignment( (yyvsp[(1) - (3)].strval), NULL,  0, 0.0, NULL, SQLP_NULL ); }
    break;

  case 55:

/* Line 1455 of yacc.c  */
#line 202 "sqlp.y"
    { sqpAssignment( (yyvsp[(1) - (3)].strval), NULL, 0, 0.0, (yyvsp[(3) - (3)].node), SQLP_EXPR ); }
    break;

  case 56:

/* Line 1455 of yacc.c  */
#line 206 "sqlp.y"
    { 
		    (yyval.node) = (yyvsp[(1) - (1)].node);
		    sqlpStmt->upperNodeptr = (yyval.node); 
		}
    break;

  case 57:

/* Line 1455 of yacc.c  */
#line 213 "sqlp.y"
    { (yyval.node) = (yyvsp[(1) - (1)].node); }
    break;

  case 58:

/* Line 1455 of yacc.c  */
#line 214 "sqlp.y"
    { (yyval.node) = sqpNewExpressionNode (SQLP_OR, (yyvsp[(1) - (3)].node), (yyvsp[(3) - (3)].node)); }
    break;

  case 59:

/* Line 1455 of yacc.c  */
#line 218 "sqlp.y"
    { (yyval.node) = (yyvsp[(1) - (1)].node); }
    break;

  case 60:

/* Line 1455 of yacc.c  */
#line 219 "sqlp.y"
    { (yyval.node) = sqpNewExpressionNode (SQLP_AND, (yyvsp[(1) - (3)].node), (yyvsp[(3) - (3)].node)); }
    break;

  case 61:

/* Line 1455 of yacc.c  */
#line 223 "sqlp.y"
    { (yyval.node) = (yyvsp[(1) - (1)].node); }
    break;

  case 62:

/* Line 1455 of yacc.c  */
#line 224 "sqlp.y"
    { (yyval.node) = (yyvsp[(2) - (3)].node); }
    break;

  case 63:

/* Line 1455 of yacc.c  */
#line 225 "sqlp.y"
    { (yyval.node) = sqpNewExpressionNode ( SQLP_NOT, NULL, (yyvsp[(2) - (2)].node)); }
    break;

  case 64:

/* Line 1455 of yacc.c  */
#line 230 "sqlp.y"
    {
		    (yyval.node) = sqpNewExpressionNode ( SQLP_EQ, (yyvsp[(1) - (3)].node), (yyvsp[(3) - (3)].node));
		}
    break;

  case 65:

/* Line 1455 of yacc.c  */
#line 233 "sqlp.y"
    {
		    (yyval.node) = sqpNewExpressionNode ( sqpOperatorCode((yyvsp[(2) - (3)].strval)), (yyvsp[(1) - (3)].node), (yyvsp[(3) - (3)].node));
		}
    break;

  case 66:

/* Line 1455 of yacc.c  */
#line 236 "sqlp.y"
    {
		    (yyval.node) = sqpNewExpressionNode ( SQLP_ISNULL, NULL, (yyvsp[(1) - (3)].node));
		}
    break;

  case 67:

/* Line 1455 of yacc.c  */
#line 239 "sqlp.y"
    {
		    (yyval.node) = sqpNewExpressionNode ( SQLP_NOTNULL, NULL, (yyvsp[(1) - (3)].node));
		}
    break;

  case 68:

/* Line 1455 of yacc.c  */
#line 246 "sqlp.y"
    { (yyval.node) = (yyvsp[(1) - (1)].node); }
    break;

  case 69:

/* Line 1455 of yacc.c  */
#line 247 "sqlp.y"
    {
		    (yyval.node) = sqpNewExpressionNode ( sqpOperatorCode("+"), (yyvsp[(1) - (3)].node), (yyvsp[(3) - (3)].node) );
		}
    break;

  case 70:

/* Line 1455 of yacc.c  */
#line 250 "sqlp.y"
    {
		    (yyval.node) = sqpNewExpressionNode ( sqpOperatorCode("-"), (yyvsp[(1) - (3)].node), (yyvsp[(3) - (3)].node) );
		}
    break;

  case 71:

/* Line 1455 of yacc.c  */
#line 256 "sqlp.y"
    { (yyval.node) = (yyvsp[(1) - (1)].node); }
    break;

  case 72:

/* Line 1455 of yacc.c  */
#line 257 "sqlp.y"
    {
		    (yyval.node) = sqpNewExpressionNode ( sqpOperatorCode("*"), (yyvsp[(1) - (3)].node), (yyvsp[(3) - (3)].node) );
		}
    break;

  case 73:

/* Line 1455 of yacc.c  */
#line 260 "sqlp.y"
    {
		    (yyval.node) = sqpNewExpressionNode ( sqpOperatorCode("/"), (yyvsp[(1) - (3)].node), (yyvsp[(3) - (3)].node) );
		}
    break;

  case 74:

/* Line 1455 of yacc.c  */
#line 266 "sqlp.y"
    { (yyval.node) = (yyvsp[(1) - (1)].node); }
    break;

  case 75:

/* Line 1455 of yacc.c  */
#line 267 "sqlp.y"
    {
		    (yyval.node) = sqpNewExpressionNode ( sqpOperatorCode("-"), sqpNewValueNode ( NULL, 0, 0.0,  SQLP_I ), (yyvsp[(2) - (2)].node) );
		}
    break;

  case 76:

/* Line 1455 of yacc.c  */
#line 273 "sqlp.y"
    { (yyval.node) = (yyvsp[(1) - (1)].node); }
    break;

  case 77:

/* Line 1455 of yacc.c  */
#line 274 "sqlp.y"
    { (yyval.node) = (yyvsp[(1) - (1)].node); }
    break;

  case 78:

/* Line 1455 of yacc.c  */
#line 275 "sqlp.y"
    { (yyval.node) = (yyvsp[(2) - (3)].node); }
    break;

  case 79:

/* Line 1455 of yacc.c  */
#line 280 "sqlp.y"
    { (yyval.node) = sqpNewValueNode (   (yyvsp[(1) - (1)].strval),  0, 0.0,  SQLP_S ); }
    break;

  case 80:

/* Line 1455 of yacc.c  */
#line 281 "sqlp.y"
    { (yyval.node) = sqpNewValueNode ( NULL, (yyvsp[(1) - (1)].intval), 0.0,  SQLP_I ); }
    break;

  case 81:

/* Line 1455 of yacc.c  */
#line 282 "sqlp.y"
    { (yyval.node) = sqpNewValueNode ( NULL,  0,  (yyvsp[(1) - (1)].floatval),  SQLP_D ); }
    break;

  case 82:

/* Line 1455 of yacc.c  */
#line 287 "sqlp.y"
    {(yyval.node) = sqpNewColumnNode (  (yyvsp[(1) - (1)].strval) );}
    break;

  case 85:

/* Line 1455 of yacc.c  */
#line 293 "sqlp.y"
    { sqpOrderColumn( (yyvsp[(1) - (1)].strval), SORT_ASC ); }
    break;

  case 86:

/* Line 1455 of yacc.c  */
#line 294 "sqlp.y"
    { sqpOrderColumn( (yyvsp[(1) - (2)].strval), SORT_ASC ); }
    break;

  case 87:

/* Line 1455 of yacc.c  */
#line 297 "sqlp.y"
    { sqpOrderColumn( (yyvsp[(1) - (2)].strval), SORT_DESC ); }
    break;



/* Line 1455 of yacc.c  */
#line 2071 "sqlp.tab.c"
      default: break;
    }
  YY_SYMBOL_PRINT ("-> $$ =", yyr1[yyn], &yyval, &yyloc);

  YYPOPSTACK (yylen);
  yylen = 0;
  YY_STACK_PRINT (yyss, yyssp);

  *++yyvsp = yyval;

  /* Now `shift' the result of the reduction.  Determine what state
     that goes to, based on the state we popped back to and the rule
     number reduced by.  */

  yyn = yyr1[yyn];

  yystate = yypgoto[yyn - YYNTOKENS] + *yyssp;
  if (0 <= yystate && yystate <= YYLAST && yycheck[yystate] == *yyssp)
    yystate = yytable[yystate];
  else
    yystate = yydefgoto[yyn - YYNTOKENS];

  goto yynewstate;


/*------------------------------------.
| yyerrlab -- here on detecting error |
`------------------------------------*/
yyerrlab:
  /* If not already recovering from an error, report this error.  */
  if (!yyerrstatus)
    {
      ++yynerrs;
#if ! YYERROR_VERBOSE
      yyerror (YY_("syntax error"));
#else
      {
	YYSIZE_T yysize = yysyntax_error (0, yystate, yychar);
	if (yymsg_alloc < yysize && yymsg_alloc < YYSTACK_ALLOC_MAXIMUM)
	  {
	    YYSIZE_T yyalloc = 2 * yysize;
	    if (! (yysize <= yyalloc && yyalloc <= YYSTACK_ALLOC_MAXIMUM))
	      yyalloc = YYSTACK_ALLOC_MAXIMUM;
	    if (yymsg != yymsgbuf)
	      YYSTACK_FREE (yymsg);
	    yymsg = (char *) YYSTACK_ALLOC (yyalloc);
	    if (yymsg)
	      yymsg_alloc = yyalloc;
	    else
	      {
		yymsg = yymsgbuf;
		yymsg_alloc = sizeof yymsgbuf;
	      }
	  }

	if (0 < yysize && yysize <= yymsg_alloc)
	  {
	    (void) yysyntax_error (yymsg, yystate, yychar);
	    yyerror (yymsg);
	  }
	else
	  {
	    yyerror (YY_("syntax error"));
	    if (yysize != 0)
	      goto yyexhaustedlab;
	  }
      }
#endif
    }



  if (yyerrstatus == 3)
    {
      /* If just tried and failed to reuse lookahead token after an
	 error, discard it.  */

      if (yychar <= YYEOF)
	{
	  /* Return failure if at end of input.  */
	  if (yychar == YYEOF)
	    YYABORT;
	}
      else
	{
	  yydestruct ("Error: discarding",
		      yytoken, &yylval);
	  yychar = YYEMPTY;
	}
    }

  /* Else will try to reuse lookahead token after shifting the error
     token.  */
  goto yyerrlab1;


/*---------------------------------------------------.
| yyerrorlab -- error raised explicitly by YYERROR.  |
`---------------------------------------------------*/
yyerrorlab:

  /* Pacify compilers like GCC when the user code never invokes
     YYERROR and the label yyerrorlab therefore never appears in user
     code.  */
  if (/*CONSTCOND*/ 0)
     goto yyerrorlab;

  /* Do not reclaim the symbols of the rule which action triggered
     this YYERROR.  */
  YYPOPSTACK (yylen);
  yylen = 0;
  YY_STACK_PRINT (yyss, yyssp);
  yystate = *yyssp;
  goto yyerrlab1;


/*-------------------------------------------------------------.
| yyerrlab1 -- common code for both syntax error and YYERROR.  |
`-------------------------------------------------------------*/
yyerrlab1:
  yyerrstatus = 3;	/* Each real token shifted decrements this.  */

  for (;;)
    {
      yyn = yypact[yystate];
      if (yyn != YYPACT_NINF)
	{
	  yyn += YYTERROR;
	  if (0 <= yyn && yyn <= YYLAST && yycheck[yyn] == YYTERROR)
	    {
	      yyn = yytable[yyn];
	      if (0 < yyn)
		break;
	    }
	}

      /* Pop the current state because it cannot handle the error token.  */
      if (yyssp == yyss)
	YYABORT;


      yydestruct ("Error: popping",
		  yystos[yystate], yyvsp);
      YYPOPSTACK (1);
      yystate = *yyssp;
      YY_STACK_PRINT (yyss, yyssp);
    }

  *++yyvsp = yylval;


  /* Shift the error token.  */
  YY_SYMBOL_PRINT ("Shifting", yystos[yyn], yyvsp, yylsp);

  yystate = yyn;
  goto yynewstate;


/*-------------------------------------.
| yyacceptlab -- YYACCEPT comes here.  |
`-------------------------------------*/
yyacceptlab:
  yyresult = 0;
  goto yyreturn;

/*-----------------------------------.
| yyabortlab -- YYABORT comes here.  |
`-----------------------------------*/
yyabortlab:
  yyresult = 1;
  goto yyreturn;

#if !defined(yyoverflow) || YYERROR_VERBOSE
/*-------------------------------------------------.
| yyexhaustedlab -- memory exhaustion comes here.  |
`-------------------------------------------------*/
yyexhaustedlab:
  yyerror (YY_("memory exhausted"));
  yyresult = 2;
  /* Fall through.  */
#endif

yyreturn:
  if (yychar != YYEMPTY)
     yydestruct ("Cleanup: discarding lookahead",
		 yytoken, &yylval);
  /* Do not reclaim the symbols of the rule which action triggered
     this YYABORT or YYACCEPT.  */
  YYPOPSTACK (yylen);
  YY_STACK_PRINT (yyss, yyssp);
  while (yyssp != yyss)
    {
      yydestruct ("Cleanup: popping",
		  yystos[*yyssp], yyvsp);
      YYPOPSTACK (1);
    }
#ifndef yyoverflow
  if (yyss != yyssa)
    YYSTACK_FREE (yyss);
#endif
#if YYERROR_VERBOSE
  if (yymsg != yymsgbuf)
    YYSTACK_FREE (yymsg);
#endif
  /* Make sure YYID is used.  */
  return YYID (yyresult);
}



/* Line 1675 of yacc.c  */
#line 299 "sqlp.y"




