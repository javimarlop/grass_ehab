'''Wrapper for arraystats.h

Generated with:
./ctypesgen.py --cpp gcc -E       -I/home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include -I/home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include -D__GLIBC_HAVE_LONG_LONG -lgrass_arraystats.7.1.svn /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/arraystats.h /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/arraystats.h -o OBJ.x86_64-unknown-linux-gnu/arraystats.py

Do not modify this file.
'''

__docformat__ =  'restructuredtext'


_libs = {}
_libdirs = []

from ctypes_preamble import *
from ctypes_preamble import _variadic_function
from ctypes_loader import *

add_library_search_dirs([])

# Begin libraries

_libs["grass_arraystats.7.1.svn"] = load_library("grass_arraystats.7.1.svn")

# 1 libraries
# End libraries

# No modules

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/arraystats.h: 11
class struct_GASTATS(Structure):
    pass

struct_GASTATS.__slots__ = [
    'count',
    'min',
    'max',
    'sum',
    'sumsq',
    'sumabs',
    'mean',
    'meanabs',
    'var',
    'stdev',
]
struct_GASTATS._fields_ = [
    ('count', c_double),
    ('min', c_double),
    ('max', c_double),
    ('sum', c_double),
    ('sumsq', c_double),
    ('sumabs', c_double),
    ('mean', c_double),
    ('meanabs', c_double),
    ('var', c_double),
    ('stdev', c_double),
]

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/arraystats.h: 4
if hasattr(_libs['grass_arraystats.7.1.svn'], 'class_apply_algorithm'):
    class_apply_algorithm = _libs['grass_arraystats.7.1.svn'].class_apply_algorithm
    class_apply_algorithm.restype = c_double
    class_apply_algorithm.argtypes = [String, POINTER(c_double), c_int, POINTER(c_int), POINTER(c_double)]

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/arraystats.h: 5
if hasattr(_libs['grass_arraystats.7.1.svn'], 'class_interval'):
    class_interval = _libs['grass_arraystats.7.1.svn'].class_interval
    class_interval.restype = c_int
    class_interval.argtypes = [POINTER(c_double), c_int, c_int, POINTER(c_double)]

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/arraystats.h: 6
if hasattr(_libs['grass_arraystats.7.1.svn'], 'class_quant'):
    class_quant = _libs['grass_arraystats.7.1.svn'].class_quant
    class_quant.restype = c_int
    class_quant.argtypes = [POINTER(c_double), c_int, c_int, POINTER(c_double)]

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/arraystats.h: 7
if hasattr(_libs['grass_arraystats.7.1.svn'], 'class_discont'):
    class_discont = _libs['grass_arraystats.7.1.svn'].class_discont
    class_discont.restype = c_double
    class_discont.argtypes = [POINTER(c_double), c_int, c_int, POINTER(c_double)]

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/arraystats.h: 8
if hasattr(_libs['grass_arraystats.7.1.svn'], 'class_stdev'):
    class_stdev = _libs['grass_arraystats.7.1.svn'].class_stdev
    class_stdev.restype = c_double
    class_stdev.argtypes = [POINTER(c_double), c_int, c_int, POINTER(c_double)]

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/arraystats.h: 9
if hasattr(_libs['grass_arraystats.7.1.svn'], 'class_equiprob'):
    class_equiprob = _libs['grass_arraystats.7.1.svn'].class_equiprob
    class_equiprob.restype = c_int
    class_equiprob.argtypes = [POINTER(c_double), c_int, POINTER(c_int), POINTER(c_double)]

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/arraystats.h: 11
if hasattr(_libs['grass_arraystats.7.1.svn'], 'class_frequencies'):
    class_frequencies = _libs['grass_arraystats.7.1.svn'].class_frequencies
    class_frequencies.restype = c_int
    class_frequencies.argtypes = [POINTER(c_double), c_int, c_int, POINTER(c_double), POINTER(c_int)]

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/arraystats.h: 13
if hasattr(_libs['grass_arraystats.7.1.svn'], 'eqdrt'):
    eqdrt = _libs['grass_arraystats.7.1.svn'].eqdrt
    eqdrt.restype = None
    eqdrt.argtypes = [POINTER(c_double), POINTER(c_double), c_int, c_int, POINTER(c_double)]

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/arraystats.h: 14
if hasattr(_libs['grass_arraystats.7.1.svn'], 'basic_stats'):
    basic_stats = _libs['grass_arraystats.7.1.svn'].basic_stats
    basic_stats.restype = None
    basic_stats.argtypes = [POINTER(c_double), c_int, POINTER(struct_GASTATS)]

GASTATS = struct_GASTATS # /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/arraystats.h: 11

# No inserted files

