'''Wrapper for gis.h

Generated with:
./ctypesgen.py --cpp gcc -E       -I/home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include -I/home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include -D__GLIBC_HAVE_LONG_LONG -lgrass_gis.7.1.svn /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/gis.h /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h -o OBJ.x86_64-unknown-linux-gnu/gis.py

Do not modify this file.
'''

__docformat__ =  'restructuredtext'


_libs = {}
_libdirs = []

from ctypes_preamble import *
from ctypes_preamble import _variadic_function
from ctypes_loader import *

add_library_search_dirs([])

# Begin libraries

_libs["grass_gis.7.1.svn"] = load_library("grass_gis.7.1.svn")

# 1 libraries
# End libraries

# No modules

__dev_t = c_ulong # /usr/include/bits/types.h: 134

__uid_t = c_uint # /usr/include/bits/types.h: 135

__gid_t = c_uint # /usr/include/bits/types.h: 136

__ino_t = c_ulong # /usr/include/bits/types.h: 137

__mode_t = c_uint # /usr/include/bits/types.h: 139

__nlink_t = c_ulong # /usr/include/bits/types.h: 140

__off_t = c_long # /usr/include/bits/types.h: 141

__off64_t = c_long # /usr/include/bits/types.h: 142

__time_t = c_long # /usr/include/bits/types.h: 149

__blksize_t = c_long # /usr/include/bits/types.h: 164

__blkcnt_t = c_long # /usr/include/bits/types.h: 169

# /usr/include/libio.h: 271
class struct__IO_FILE(Structure):
    pass

FILE = struct__IO_FILE # /usr/include/stdio.h: 49

_IO_lock_t = None # /usr/include/libio.h: 180

# /usr/include/libio.h: 186
class struct__IO_marker(Structure):
    pass

struct__IO_marker.__slots__ = [
    '_next',
    '_sbuf',
    '_pos',
]
struct__IO_marker._fields_ = [
    ('_next', POINTER(struct__IO_marker)),
    ('_sbuf', POINTER(struct__IO_FILE)),
    ('_pos', c_int),
]

struct__IO_FILE.__slots__ = [
    '_flags',
    '_IO_read_ptr',
    '_IO_read_end',
    '_IO_read_base',
    '_IO_write_base',
    '_IO_write_ptr',
    '_IO_write_end',
    '_IO_buf_base',
    '_IO_buf_end',
    '_IO_save_base',
    '_IO_backup_base',
    '_IO_save_end',
    '_markers',
    '_chain',
    '_fileno',
    '_flags2',
    '_old_offset',
    '_cur_column',
    '_vtable_offset',
    '_shortbuf',
    '_lock',
    '_offset',
    '__pad1',
    '__pad2',
    '__pad3',
    '__pad4',
    '__pad5',
    '_mode',
    '_unused2',
]
struct__IO_FILE._fields_ = [
    ('_flags', c_int),
    ('_IO_read_ptr', String),
    ('_IO_read_end', String),
    ('_IO_read_base', String),
    ('_IO_write_base', String),
    ('_IO_write_ptr', String),
    ('_IO_write_end', String),
    ('_IO_buf_base', String),
    ('_IO_buf_end', String),
    ('_IO_save_base', String),
    ('_IO_backup_base', String),
    ('_IO_save_end', String),
    ('_markers', POINTER(struct__IO_marker)),
    ('_chain', POINTER(struct__IO_FILE)),
    ('_fileno', c_int),
    ('_flags2', c_int),
    ('_old_offset', __off_t),
    ('_cur_column', c_ushort),
    ('_vtable_offset', c_char),
    ('_shortbuf', c_char * 1),
    ('_lock', POINTER(_IO_lock_t)),
    ('_offset', __off64_t),
    ('__pad1', POINTER(None)),
    ('__pad2', POINTER(None)),
    ('__pad3', POINTER(None)),
    ('__pad4', POINTER(None)),
    ('__pad5', c_size_t),
    ('_mode', c_int),
    ('_unused2', c_char * (((15 * sizeof(c_int)) - (4 * sizeof(POINTER(None)))) - sizeof(c_size_t))),
]

off_t = __off_t # /usr/include/stdio.h: 91

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/datetime.h: 27
class struct_DateTime(Structure):
    pass

struct_DateTime.__slots__ = [
    'mode',
    '_from',
    'to',
    'fracsec',
    'year',
    'month',
    'day',
    'hour',
    'minute',
    'second',
    'positive',
    'tz',
]
struct_DateTime._fields_ = [
    ('mode', c_int),
    ('_from', c_int),
    ('to', c_int),
    ('fracsec', c_int),
    ('year', c_int),
    ('month', c_int),
    ('day', c_int),
    ('hour', c_int),
    ('minute', c_int),
    ('second', c_double),
    ('positive', c_int),
    ('tz', c_int),
]

DateTime = struct_DateTime # /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/datetime.h: 27

enum_anon_6 = c_int # /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/gis.h: 287

G_OPT_UNDEFINED = 0 # /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/gis.h: 287

G_OPT_DB_SQL = (G_OPT_UNDEFINED + 1) # /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/gis.h: 287

G_OPT_DB_WHERE = (G_OPT_DB_SQL + 1) # /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/gis.h: 287

G_OPT_DB_TABLE = (G_OPT_DB_WHERE + 1) # /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/gis.h: 287

G_OPT_DB_DRIVER = (G_OPT_DB_TABLE + 1) # /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/gis.h: 287

G_OPT_DB_DATABASE = (G_OPT_DB_DRIVER + 1) # /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/gis.h: 287

G_OPT_DB_SCHEMA = (G_OPT_DB_DATABASE + 1) # /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/gis.h: 287

G_OPT_DB_COLUMN = (G_OPT_DB_SCHEMA + 1) # /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/gis.h: 287

G_OPT_DB_COLUMNS = (G_OPT_DB_COLUMN + 1) # /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/gis.h: 287

G_OPT_DB_KEYCOLUMN = (G_OPT_DB_COLUMNS + 1) # /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/gis.h: 287

G_OPT_I_GROUP = (G_OPT_DB_KEYCOLUMN + 1) # /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/gis.h: 287

G_OPT_I_SUBGROUP = (G_OPT_I_GROUP + 1) # /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/gis.h: 287

G_OPT_R_INPUT = (G_OPT_I_SUBGROUP + 1) # /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/gis.h: 287

G_OPT_R_INPUTS = (G_OPT_R_INPUT + 1) # /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/gis.h: 287

G_OPT_R_OUTPUT = (G_OPT_R_INPUTS + 1) # /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/gis.h: 287

G_OPT_R_OUTPUTS = (G_OPT_R_OUTPUT + 1) # /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/gis.h: 287

G_OPT_R_MAP = (G_OPT_R_OUTPUTS + 1) # /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/gis.h: 287

G_OPT_R_MAPS = (G_OPT_R_MAP + 1) # /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/gis.h: 287

G_OPT_R_BASE = (G_OPT_R_MAPS + 1) # /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/gis.h: 287

G_OPT_R_COVER = (G_OPT_R_BASE + 1) # /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/gis.h: 287

G_OPT_R_ELEV = (G_OPT_R_COVER + 1) # /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/gis.h: 287

G_OPT_R_ELEVS = (G_OPT_R_ELEV + 1) # /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/gis.h: 287

G_OPT_R_INTERP_TYPE = (G_OPT_R_ELEVS + 1) # /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/gis.h: 287

G_OPT_R_BASENAME_INPUT = (G_OPT_R_INTERP_TYPE + 1) # /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/gis.h: 287

G_OPT_R_BASENAME_OUTPUT = (G_OPT_R_BASENAME_INPUT + 1) # /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/gis.h: 287

G_OPT_R3_INPUT = (G_OPT_R_BASENAME_OUTPUT + 1) # /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/gis.h: 287

G_OPT_R3_INPUTS = (G_OPT_R3_INPUT + 1) # /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/gis.h: 287

G_OPT_R3_OUTPUT = (G_OPT_R3_INPUTS + 1) # /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/gis.h: 287

G_OPT_R3_MAP = (G_OPT_R3_OUTPUT + 1) # /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/gis.h: 287

G_OPT_R3_MAPS = (G_OPT_R3_MAP + 1) # /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/gis.h: 287

G_OPT_R3_TYPE = (G_OPT_R3_MAPS + 1) # /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/gis.h: 287

G_OPT_R3_PRECISION = (G_OPT_R3_TYPE + 1) # /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/gis.h: 287

G_OPT_R3_TILE_DIMENSION = (G_OPT_R3_PRECISION + 1) # /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/gis.h: 287

G_OPT_R3_COMPRESSION = (G_OPT_R3_TILE_DIMENSION + 1) # /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/gis.h: 287

G_OPT_V_INPUT = (G_OPT_R3_COMPRESSION + 1) # /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/gis.h: 287

G_OPT_V_INPUTS = (G_OPT_V_INPUT + 1) # /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/gis.h: 287

G_OPT_V_OUTPUT = (G_OPT_V_INPUTS + 1) # /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/gis.h: 287

G_OPT_V_MAP = (G_OPT_V_OUTPUT + 1) # /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/gis.h: 287

G_OPT_V_MAPS = (G_OPT_V_MAP + 1) # /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/gis.h: 287

G_OPT_V_TYPE = (G_OPT_V_MAPS + 1) # /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/gis.h: 287

G_OPT_V3_TYPE = (G_OPT_V_TYPE + 1) # /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/gis.h: 287

G_OPT_V_FIELD = (G_OPT_V3_TYPE + 1) # /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/gis.h: 287

G_OPT_V_FIELD_ALL = (G_OPT_V_FIELD + 1) # /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/gis.h: 287

G_OPT_V_CAT = (G_OPT_V_FIELD_ALL + 1) # /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/gis.h: 287

G_OPT_V_CATS = (G_OPT_V_CAT + 1) # /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/gis.h: 287

G_OPT_V_ID = (G_OPT_V_CATS + 1) # /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/gis.h: 287

G_OPT_V_IDS = (G_OPT_V_ID + 1) # /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/gis.h: 287

G_OPT_F_INPUT = (G_OPT_V_IDS + 1) # /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/gis.h: 287

G_OPT_F_OUTPUT = (G_OPT_F_INPUT + 1) # /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/gis.h: 287

G_OPT_F_SEP = (G_OPT_F_OUTPUT + 1) # /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/gis.h: 287

G_OPT_C_FG = (G_OPT_F_SEP + 1) # /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/gis.h: 287

G_OPT_C_BG = (G_OPT_C_FG + 1) # /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/gis.h: 287

G_OPT_M_UNITS = (G_OPT_C_BG + 1) # /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/gis.h: 287

G_OPT_M_DATATYPE = (G_OPT_M_UNITS + 1) # /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/gis.h: 287

G_OPT_M_MAPSET = (G_OPT_M_DATATYPE + 1) # /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/gis.h: 287

G_OPT_M_COORDS = (G_OPT_M_MAPSET + 1) # /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/gis.h: 287

G_OPT_M_COLR = (G_OPT_M_COORDS + 1) # /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/gis.h: 287

G_OPT_M_DIR = (G_OPT_M_COLR + 1) # /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/gis.h: 287

G_OPT_M_REGION = (G_OPT_M_DIR + 1) # /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/gis.h: 287

G_OPT_STDS_INPUT = (G_OPT_M_REGION + 1) # /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/gis.h: 287

G_OPT_STDS_INPUTS = (G_OPT_STDS_INPUT + 1) # /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/gis.h: 287

G_OPT_STDS_OUTPUT = (G_OPT_STDS_INPUTS + 1) # /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/gis.h: 287

G_OPT_STRDS_INPUT = (G_OPT_STDS_OUTPUT + 1) # /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/gis.h: 287

G_OPT_STRDS_INPUTS = (G_OPT_STRDS_INPUT + 1) # /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/gis.h: 287

G_OPT_STRDS_OUTPUT = (G_OPT_STRDS_INPUTS + 1) # /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/gis.h: 287

G_OPT_STR3DS_INPUT = (G_OPT_STRDS_OUTPUT + 1) # /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/gis.h: 287

G_OPT_STR3DS_INPUTS = (G_OPT_STR3DS_INPUT + 1) # /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/gis.h: 287

G_OPT_STR3DS_OUTPUT = (G_OPT_STR3DS_INPUTS + 1) # /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/gis.h: 287

G_OPT_STVDS_INPUT = (G_OPT_STR3DS_OUTPUT + 1) # /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/gis.h: 287

G_OPT_STVDS_INPUTS = (G_OPT_STVDS_INPUT + 1) # /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/gis.h: 287

G_OPT_STVDS_OUTPUT = (G_OPT_STVDS_INPUTS + 1) # /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/gis.h: 287

G_OPT_MAP_INPUT = (G_OPT_STVDS_OUTPUT + 1) # /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/gis.h: 287

G_OPT_MAP_INPUTS = (G_OPT_MAP_INPUT + 1) # /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/gis.h: 287

G_OPT_STDS_TYPE = (G_OPT_MAP_INPUTS + 1) # /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/gis.h: 287

G_OPT_MAP_TYPE = (G_OPT_STDS_TYPE + 1) # /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/gis.h: 287

G_OPT_T_TYPE = (G_OPT_MAP_TYPE + 1) # /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/gis.h: 287

G_OPT_T_WHERE = (G_OPT_T_TYPE + 1) # /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/gis.h: 287

G_OPT_T_SAMPLE = (G_OPT_T_WHERE + 1) # /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/gis.h: 287

STD_OPT = enum_anon_6 # /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/gis.h: 287

enum_anon_7 = c_int # /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/gis.h: 303

G_FLG_UNDEFINED = 0 # /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/gis.h: 303

G_FLG_V_TABLE = (G_FLG_UNDEFINED + 1) # /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/gis.h: 303

G_FLG_V_TOPO = (G_FLG_V_TABLE + 1) # /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/gis.h: 303

STD_FLG = enum_anon_7 # /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/gis.h: 303

enum_anon_8 = c_int # /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/gis.h: 342

G_ELEMENT_RASTER = 1 # /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/gis.h: 342

G_ELEMENT_RASTER3D = 2 # /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/gis.h: 342

G_ELEMENT_VECTOR = 3 # /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/gis.h: 342

G_ELEMENT_OLDVECTOR = 4 # /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/gis.h: 342

G_ELEMENT_ASCIIVECTOR = 5 # /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/gis.h: 342

G_ELEMENT_ICON = 6 # /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/gis.h: 342

G_ELEMENT_LABEL = 7 # /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/gis.h: 342

G_ELEMENT_SITE = 8 # /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/gis.h: 342

G_ELEMENT_REGION = 9 # /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/gis.h: 342

G_ELEMENT_REGION3D = 10 # /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/gis.h: 342

G_ELEMENT_GROUP = 11 # /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/gis.h: 342

G_ELEMENT_3DVIEW = 12 # /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/gis.h: 342

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/gis.h: 363
class struct_Cell_head(Structure):
    pass

struct_Cell_head.__slots__ = [
    'format',
    'compressed',
    'rows',
    'rows3',
    'cols',
    'cols3',
    'depths',
    'proj',
    'zone',
    'ew_res',
    'ew_res3',
    'ns_res',
    'ns_res3',
    'tb_res',
    'north',
    'south',
    'east',
    'west',
    'top',
    'bottom',
]
struct_Cell_head._fields_ = [
    ('format', c_int),
    ('compressed', c_int),
    ('rows', c_int),
    ('rows3', c_int),
    ('cols', c_int),
    ('cols3', c_int),
    ('depths', c_int),
    ('proj', c_int),
    ('zone', c_int),
    ('ew_res', c_double),
    ('ew_res3', c_double),
    ('ns_res', c_double),
    ('ns_res3', c_double),
    ('tb_res', c_double),
    ('north', c_double),
    ('south', c_double),
    ('east', c_double),
    ('west', c_double),
    ('top', c_double),
    ('bottom', c_double),
]

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/gis.h: 425
class struct_G_3dview(Structure):
    pass

struct_G_3dview.__slots__ = [
    'pgm_id',
    'from_to',
    'fov',
    'twist',
    'exag',
    'mesh_freq',
    'poly_freq',
    'display_type',
    'lightson',
    'dozero',
    'colorgrid',
    'shading',
    'fringe',
    'surfonly',
    'doavg',
    'grid_col',
    'bg_col',
    'other_col',
    'lightpos',
    'lightcol',
    'ambient',
    'shine',
    'vwin',
]
struct_G_3dview._fields_ = [
    ('pgm_id', c_char * 40),
    ('from_to', (c_float * 3) * 2),
    ('fov', c_float),
    ('twist', c_float),
    ('exag', c_float),
    ('mesh_freq', c_int),
    ('poly_freq', c_int),
    ('display_type', c_int),
    ('lightson', c_int),
    ('dozero', c_int),
    ('colorgrid', c_int),
    ('shading', c_int),
    ('fringe', c_int),
    ('surfonly', c_int),
    ('doavg', c_int),
    ('grid_col', c_char * 40),
    ('bg_col', c_char * 40),
    ('other_col', c_char * 40),
    ('lightpos', c_float * 4),
    ('lightcol', c_float * 3),
    ('ambient', c_float),
    ('shine', c_float),
    ('vwin', struct_Cell_head),
]

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/gis.h: 452
class struct_Key_Value(Structure):
    pass

struct_Key_Value.__slots__ = [
    'nitems',
    'nalloc',
    'key',
    'value',
]
struct_Key_Value._fields_ = [
    ('nitems', c_int),
    ('nalloc', c_int),
    ('key', POINTER(POINTER(c_char))),
    ('value', POINTER(POINTER(c_char))),
]

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/gis.h: 482
class struct_Option(Structure):
    pass

struct_Option.__slots__ = [
    'key',
    'type',
    'required',
    'multiple',
    'options',
    'opts',
    'key_desc',
    'label',
    'description',
    'descriptions',
    'descs',
    'answer',
    '_def',
    'answers',
    'next_opt',
    'gisprompt',
    'guisection',
    'guidependency',
    'checker',
    'count',
]
struct_Option._fields_ = [
    ('key', String),
    ('type', c_int),
    ('required', c_int),
    ('multiple', c_int),
    ('options', String),
    ('opts', POINTER(POINTER(c_char))),
    ('key_desc', String),
    ('label', String),
    ('description', String),
    ('descriptions', String),
    ('descs', POINTER(POINTER(c_char))),
    ('answer', String),
    ('_def', String),
    ('answers', POINTER(POINTER(c_char))),
    ('next_opt', POINTER(struct_Option)),
    ('gisprompt', String),
    ('guisection', String),
    ('guidependency', String),
    ('checker', CFUNCTYPE(UNCHECKED(c_int), String)),
    ('count', c_int),
]

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/gis.h: 511
class struct_Flag(Structure):
    pass

struct_Flag.__slots__ = [
    'key',
    'answer',
    'suppress_required',
    'label',
    'description',
    'guisection',
    'next_flag',
]
struct_Flag._fields_ = [
    ('key', c_char),
    ('answer', c_char),
    ('suppress_required', c_char),
    ('label', String),
    ('description', String),
    ('guisection', String),
    ('next_flag', POINTER(struct_Flag)),
]

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/gis.h: 527
class struct_GModule(Structure):
    pass

struct_GModule.__slots__ = [
    'label',
    'description',
    'keywords',
    'overwrite',
    'verbose',
]
struct_GModule._fields_ = [
    ('label', String),
    ('description', String),
    ('keywords', POINTER(POINTER(c_char))),
    ('overwrite', c_int),
    ('verbose', c_int),
]

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/gis.h: 537
class struct_TimeStamp(Structure):
    pass

struct_TimeStamp.__slots__ = [
    'dt',
    'count',
]
struct_TimeStamp._fields_ = [
    ('dt', DateTime * 2),
    ('count', c_int),
]

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/gis.h: 543
class struct_Counter(Structure):
    pass

struct_Counter.__slots__ = [
    'value',
]
struct_Counter._fields_ = [
    ('value', c_int),
]

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/gis.h: 547
class struct_Popen(Structure):
    pass

struct_Popen.__slots__ = [
    'fp',
    'pid',
]
struct_Popen._fields_ = [
    ('fp', POINTER(FILE)),
    ('pid', c_int),
]

CELL = c_int # /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/gis.h: 552

DCELL = c_double # /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/gis.h: 553

FCELL = c_float # /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/gis.h: 554

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/gis.h: 556
class struct__Color_Value_(Structure):
    pass

struct__Color_Value_.__slots__ = [
    'value',
    'red',
    'grn',
    'blu',
]
struct__Color_Value_._fields_ = [
    ('value', DCELL),
    ('red', c_ubyte),
    ('grn', c_ubyte),
    ('blu', c_ubyte),
]

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/gis.h: 564
class struct__Color_Rule_(Structure):
    pass

struct__Color_Rule_.__slots__ = [
    'low',
    'high',
    'next',
    'prev',
]
struct__Color_Rule_._fields_ = [
    ('low', struct__Color_Value_),
    ('high', struct__Color_Value_),
    ('next', POINTER(struct__Color_Rule_)),
    ('prev', POINTER(struct__Color_Rule_)),
]

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/gis.h: 576
class struct_anon_9(Structure):
    pass

struct_anon_9.__slots__ = [
    'red',
    'grn',
    'blu',
    'set',
    'nalloc',
    'active',
]
struct_anon_9._fields_ = [
    ('red', POINTER(c_ubyte)),
    ('grn', POINTER(c_ubyte)),
    ('blu', POINTER(c_ubyte)),
    ('set', POINTER(c_ubyte)),
    ('nalloc', c_int),
    ('active', c_int),
]

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/gis.h: 586
class struct_anon_10(Structure):
    pass

struct_anon_10.__slots__ = [
    'vals',
    'rules',
    'nalloc',
    'active',
]
struct_anon_10._fields_ = [
    ('vals', POINTER(DCELL)),
    ('rules', POINTER(POINTER(struct__Color_Rule_))),
    ('nalloc', c_int),
    ('active', c_int),
]

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/gis.h: 571
class struct__Color_Info_(Structure):
    pass

struct__Color_Info_.__slots__ = [
    'rules',
    'n_rules',
    'lookup',
    'fp_lookup',
    'min',
    'max',
]
struct__Color_Info_._fields_ = [
    ('rules', POINTER(struct__Color_Rule_)),
    ('n_rules', c_int),
    ('lookup', struct_anon_9),
    ('fp_lookup', struct_anon_10),
    ('min', DCELL),
    ('max', DCELL),
]

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/gis.h: 598
class struct_Colors(Structure):
    pass

struct_Colors.__slots__ = [
    'version',
    'shift',
    'invert',
    'is_float',
    'null_set',
    'null_red',
    'null_grn',
    'null_blu',
    'undef_set',
    'undef_red',
    'undef_grn',
    'undef_blu',
    'fixed',
    'modular',
    'cmin',
    'cmax',
    'organizing',
]
struct_Colors._fields_ = [
    ('version', c_int),
    ('shift', DCELL),
    ('invert', c_int),
    ('is_float', c_int),
    ('null_set', c_int),
    ('null_red', c_ubyte),
    ('null_grn', c_ubyte),
    ('null_blu', c_ubyte),
    ('undef_set', c_int),
    ('undef_red', c_ubyte),
    ('undef_grn', c_ubyte),
    ('undef_blu', c_ubyte),
    ('fixed', struct__Color_Info_),
    ('modular', struct__Color_Info_),
    ('cmin', DCELL),
    ('cmax', DCELL),
    ('organizing', c_int),
]

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/gis.h: 622
class struct_ilist(Structure):
    pass

struct_ilist.__slots__ = [
    'value',
    'n_values',
    'alloc_values',
]
struct_ilist._fields_ = [
    ('value', POINTER(c_int)),
    ('n_values', c_int),
    ('alloc_values', c_int),
]

__jmp_buf = c_long * 8 # /usr/include/bits/setjmp.h: 32

# /usr/include/bits/sigset.h: 32
class struct_anon_11(Structure):
    pass

struct_anon_11.__slots__ = [
    '__val',
]
struct_anon_11._fields_ = [
    ('__val', c_ulong * (1024 / (8 * sizeof(c_ulong)))),
]

__sigset_t = struct_anon_11 # /usr/include/bits/sigset.h: 32

# /usr/include/setjmp.h: 35
class struct___jmp_buf_tag(Structure):
    pass

struct___jmp_buf_tag.__slots__ = [
    '__jmpbuf',
    '__mask_was_saved',
    '__saved_mask',
]
struct___jmp_buf_tag._fields_ = [
    ('__jmpbuf', __jmp_buf),
    ('__mask_was_saved', c_int),
    ('__saved_mask', __sigset_t),
]

jmp_buf = struct___jmp_buf_tag * 1 # /usr/include/setjmp.h: 49

# /usr/include/time.h: 120
class struct_timespec(Structure):
    pass

struct_timespec.__slots__ = [
    'tv_sec',
    'tv_nsec',
]
struct_timespec._fields_ = [
    ('tv_sec', __time_t),
    ('tv_nsec', c_long),
]

# /usr/include/bits/stat.h: 46
class struct_stat(Structure):
    pass

struct_stat.__slots__ = [
    'st_dev',
    'st_ino',
    'st_nlink',
    'st_mode',
    'st_uid',
    'st_gid',
    '__pad0',
    'st_rdev',
    'st_size',
    'st_blksize',
    'st_blocks',
    'st_atim',
    'st_mtim',
    'st_ctim',
    '__unused',
]
struct_stat._fields_ = [
    ('st_dev', __dev_t),
    ('st_ino', __ino_t),
    ('st_nlink', __nlink_t),
    ('st_mode', __mode_t),
    ('st_uid', __uid_t),
    ('st_gid', __gid_t),
    ('__pad0', c_int),
    ('st_rdev', __dev_t),
    ('st_size', __off_t),
    ('st_blksize', __blksize_t),
    ('st_blocks', __blkcnt_t),
    ('st_atim', struct_timespec),
    ('st_mtim', struct_timespec),
    ('st_ctim', struct_timespec),
    ('__unused', c_long * 3),
]

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 68
if hasattr(_libs['grass_gis.7.1.svn'], 'G_adjust_Cell_head'):
    G_adjust_Cell_head = _libs['grass_gis.7.1.svn'].G_adjust_Cell_head
    G_adjust_Cell_head.restype = None
    G_adjust_Cell_head.argtypes = [POINTER(struct_Cell_head), c_int, c_int]

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 69
if hasattr(_libs['grass_gis.7.1.svn'], 'G_adjust_Cell_head3'):
    G_adjust_Cell_head3 = _libs['grass_gis.7.1.svn'].G_adjust_Cell_head3
    G_adjust_Cell_head3.restype = None
    G_adjust_Cell_head3.argtypes = [POINTER(struct_Cell_head), c_int, c_int, c_int]

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 75
if hasattr(_libs['grass_gis.7.1.svn'], 'G__malloc'):
    G__malloc = _libs['grass_gis.7.1.svn'].G__malloc
    G__malloc.restype = POINTER(None)
    G__malloc.argtypes = [String, c_int, c_size_t]

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 76
if hasattr(_libs['grass_gis.7.1.svn'], 'G__calloc'):
    G__calloc = _libs['grass_gis.7.1.svn'].G__calloc
    G__calloc.restype = POINTER(None)
    G__calloc.argtypes = [String, c_int, c_size_t, c_size_t]

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 77
if hasattr(_libs['grass_gis.7.1.svn'], 'G__realloc'):
    G__realloc = _libs['grass_gis.7.1.svn'].G__realloc
    G__realloc.restype = POINTER(None)
    G__realloc.argtypes = [String, c_int, POINTER(None), c_size_t]

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 78
if hasattr(_libs['grass_gis.7.1.svn'], 'G_free'):
    G_free = _libs['grass_gis.7.1.svn'].G_free
    G_free.restype = None
    G_free.argtypes = [POINTER(None)]

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 94
if hasattr(_libs['grass_gis.7.1.svn'], 'G_begin_cell_area_calculations'):
    G_begin_cell_area_calculations = _libs['grass_gis.7.1.svn'].G_begin_cell_area_calculations
    G_begin_cell_area_calculations.restype = c_int
    G_begin_cell_area_calculations.argtypes = []

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 95
if hasattr(_libs['grass_gis.7.1.svn'], 'G_area_of_cell_at_row'):
    G_area_of_cell_at_row = _libs['grass_gis.7.1.svn'].G_area_of_cell_at_row
    G_area_of_cell_at_row.restype = c_double
    G_area_of_cell_at_row.argtypes = [c_int]

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 96
if hasattr(_libs['grass_gis.7.1.svn'], 'G_begin_polygon_area_calculations'):
    G_begin_polygon_area_calculations = _libs['grass_gis.7.1.svn'].G_begin_polygon_area_calculations
    G_begin_polygon_area_calculations.restype = c_int
    G_begin_polygon_area_calculations.argtypes = []

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 97
if hasattr(_libs['grass_gis.7.1.svn'], 'G_area_of_polygon'):
    G_area_of_polygon = _libs['grass_gis.7.1.svn'].G_area_of_polygon
    G_area_of_polygon.restype = c_double
    G_area_of_polygon.argtypes = [POINTER(c_double), POINTER(c_double), c_int]

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 100
if hasattr(_libs['grass_gis.7.1.svn'], 'G_begin_zone_area_on_ellipsoid'):
    G_begin_zone_area_on_ellipsoid = _libs['grass_gis.7.1.svn'].G_begin_zone_area_on_ellipsoid
    G_begin_zone_area_on_ellipsoid.restype = None
    G_begin_zone_area_on_ellipsoid.argtypes = [c_double, c_double, c_double]

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 101
if hasattr(_libs['grass_gis.7.1.svn'], 'G_darea0_on_ellipsoid'):
    G_darea0_on_ellipsoid = _libs['grass_gis.7.1.svn'].G_darea0_on_ellipsoid
    G_darea0_on_ellipsoid.restype = c_double
    G_darea0_on_ellipsoid.argtypes = [c_double]

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 102
if hasattr(_libs['grass_gis.7.1.svn'], 'G_area_for_zone_on_ellipsoid'):
    G_area_for_zone_on_ellipsoid = _libs['grass_gis.7.1.svn'].G_area_for_zone_on_ellipsoid
    G_area_for_zone_on_ellipsoid.restype = c_double
    G_area_for_zone_on_ellipsoid.argtypes = [c_double, c_double]

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 105
if hasattr(_libs['grass_gis.7.1.svn'], 'G_begin_ellipsoid_polygon_area'):
    G_begin_ellipsoid_polygon_area = _libs['grass_gis.7.1.svn'].G_begin_ellipsoid_polygon_area
    G_begin_ellipsoid_polygon_area.restype = None
    G_begin_ellipsoid_polygon_area.argtypes = [c_double, c_double]

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 106
if hasattr(_libs['grass_gis.7.1.svn'], 'G_ellipsoid_polygon_area'):
    G_ellipsoid_polygon_area = _libs['grass_gis.7.1.svn'].G_ellipsoid_polygon_area
    G_ellipsoid_polygon_area.restype = c_double
    G_ellipsoid_polygon_area.argtypes = [POINTER(c_double), POINTER(c_double), c_int]

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 109
if hasattr(_libs['grass_gis.7.1.svn'], 'G_planimetric_polygon_area'):
    G_planimetric_polygon_area = _libs['grass_gis.7.1.svn'].G_planimetric_polygon_area
    G_planimetric_polygon_area.restype = c_double
    G_planimetric_polygon_area.argtypes = [POINTER(c_double), POINTER(c_double), c_int]

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 112
if hasattr(_libs['grass_gis.7.1.svn'], 'G_begin_zone_area_on_sphere'):
    G_begin_zone_area_on_sphere = _libs['grass_gis.7.1.svn'].G_begin_zone_area_on_sphere
    G_begin_zone_area_on_sphere.restype = None
    G_begin_zone_area_on_sphere.argtypes = [c_double, c_double]

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 113
if hasattr(_libs['grass_gis.7.1.svn'], 'G_darea0_on_sphere'):
    G_darea0_on_sphere = _libs['grass_gis.7.1.svn'].G_darea0_on_sphere
    G_darea0_on_sphere.restype = c_double
    G_darea0_on_sphere.argtypes = [c_double]

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 114
if hasattr(_libs['grass_gis.7.1.svn'], 'G_area_for_zone_on_sphere'):
    G_area_for_zone_on_sphere = _libs['grass_gis.7.1.svn'].G_area_for_zone_on_sphere
    G_area_for_zone_on_sphere.restype = c_double
    G_area_for_zone_on_sphere.argtypes = [c_double, c_double]

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 117
if hasattr(_libs['grass_gis.7.1.svn'], 'G_ascii_check'):
    G_ascii_check = _libs['grass_gis.7.1.svn'].G_ascii_check
    G_ascii_check.restype = None
    G_ascii_check.argtypes = [String]

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 129
if hasattr(_libs['grass_gis.7.1.svn'], 'G_vasprintf'):
    G_vasprintf = _libs['grass_gis.7.1.svn'].G_vasprintf
    G_vasprintf.restype = c_int
    G_vasprintf.argtypes = [POINTER(POINTER(c_char)), String, c_void_p]

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 130
if hasattr(_libs['grass_gis.7.1.svn'], 'G_asprintf'):
    _func = _libs['grass_gis.7.1.svn'].G_asprintf
    _restype = c_int
    _argtypes = [POINTER(POINTER(c_char)), String]
    G_asprintf = _variadic_function(_func,_restype,_argtypes)

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 133
if hasattr(_libs['grass_gis.7.1.svn'], 'G_rasprintf'):
    _func = _libs['grass_gis.7.1.svn'].G_rasprintf
    _restype = c_int
    _argtypes = [POINTER(POINTER(c_char)), POINTER(c_size_t), String]
    G_rasprintf = _variadic_function(_func,_restype,_argtypes)

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 137
if hasattr(_libs['grass_gis.7.1.svn'], 'G_basename'):
    G_basename = _libs['grass_gis.7.1.svn'].G_basename
    G_basename.restype = ReturnString
    G_basename.argtypes = [String, String]

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 138
if hasattr(_libs['grass_gis.7.1.svn'], 'G_get_num_decimals'):
    G_get_num_decimals = _libs['grass_gis.7.1.svn'].G_get_num_decimals
    G_get_num_decimals.restype = c_size_t
    G_get_num_decimals.argtypes = [String]

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 139
if hasattr(_libs['grass_gis.7.1.svn'], 'G_double_to_basename_format'):
    G_double_to_basename_format = _libs['grass_gis.7.1.svn'].G_double_to_basename_format
    G_double_to_basename_format.restype = ReturnString
    G_double_to_basename_format.argtypes = [c_double, c_size_t, c_size_t]

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 140
if hasattr(_libs['grass_gis.7.1.svn'], 'G_get_basename_separator'):
    G_get_basename_separator = _libs['grass_gis.7.1.svn'].G_get_basename_separator
    G_get_basename_separator.restype = ReturnString
    G_get_basename_separator.argtypes = []

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 141
if hasattr(_libs['grass_gis.7.1.svn'], 'G_join_basename_strings'):
    G_join_basename_strings = _libs['grass_gis.7.1.svn'].G_join_basename_strings
    G_join_basename_strings.restype = ReturnString
    G_join_basename_strings.argtypes = [POINTER(POINTER(c_char)), c_size_t]

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 142
if hasattr(_libs['grass_gis.7.1.svn'], 'G_generate_basename'):
    G_generate_basename = _libs['grass_gis.7.1.svn'].G_generate_basename
    G_generate_basename.restype = ReturnString
    G_generate_basename.argtypes = [String, c_double, c_size_t, c_size_t]

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 145
if hasattr(_libs['grass_gis.7.1.svn'], 'G_bresenham_line'):
    G_bresenham_line = _libs['grass_gis.7.1.svn'].G_bresenham_line
    G_bresenham_line.restype = None
    G_bresenham_line.argtypes = [c_int, c_int, c_int, c_int, CFUNCTYPE(UNCHECKED(c_int), c_int, c_int)]

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 148
if hasattr(_libs['grass_gis.7.1.svn'], 'G_clicker'):
    G_clicker = _libs['grass_gis.7.1.svn'].G_clicker
    G_clicker.restype = None
    G_clicker.argtypes = []

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 151
if hasattr(_libs['grass_gis.7.1.svn'], 'G_color_rules_options'):
    G_color_rules_options = _libs['grass_gis.7.1.svn'].G_color_rules_options
    G_color_rules_options.restype = ReturnString
    G_color_rules_options.argtypes = []

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 152
if hasattr(_libs['grass_gis.7.1.svn'], 'G_color_rules_descriptions'):
    G_color_rules_descriptions = _libs['grass_gis.7.1.svn'].G_color_rules_descriptions
    G_color_rules_descriptions.restype = ReturnString
    G_color_rules_descriptions.argtypes = []

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 153
if hasattr(_libs['grass_gis.7.1.svn'], 'G_list_color_rules'):
    G_list_color_rules = _libs['grass_gis.7.1.svn'].G_list_color_rules
    G_list_color_rules.restype = None
    G_list_color_rules.argtypes = [POINTER(FILE)]

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 154
if hasattr(_libs['grass_gis.7.1.svn'], 'G_find_color_rule'):
    G_find_color_rule = _libs['grass_gis.7.1.svn'].G_find_color_rule
    G_find_color_rule.restype = c_int
    G_find_color_rule.argtypes = [String]

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 157
if hasattr(_libs['grass_gis.7.1.svn'], 'G_num_standard_colors'):
    G_num_standard_colors = _libs['grass_gis.7.1.svn'].G_num_standard_colors
    G_num_standard_colors.restype = c_int
    G_num_standard_colors.argtypes = []

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 160
if hasattr(_libs['grass_gis.7.1.svn'], 'G_insert_commas'):
    G_insert_commas = _libs['grass_gis.7.1.svn'].G_insert_commas
    G_insert_commas.restype = c_int
    G_insert_commas.argtypes = [String]

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 161
if hasattr(_libs['grass_gis.7.1.svn'], 'G_remove_commas'):
    G_remove_commas = _libs['grass_gis.7.1.svn'].G_remove_commas
    G_remove_commas.restype = None
    G_remove_commas.argtypes = [String]

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 164
if hasattr(_libs['grass_gis.7.1.svn'], 'G_recursive_copy'):
    G_recursive_copy = _libs['grass_gis.7.1.svn'].G_recursive_copy
    G_recursive_copy.restype = c_int
    G_recursive_copy.argtypes = [String, String]

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 167
if hasattr(_libs['grass_gis.7.1.svn'], 'G_copy_file'):
    G_copy_file = _libs['grass_gis.7.1.svn'].G_copy_file
    G_copy_file.restype = c_int
    G_copy_file.argtypes = [String, String]

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 170
if hasattr(_libs['grass_gis.7.1.svn'], 'G_is_initialized'):
    G_is_initialized = _libs['grass_gis.7.1.svn'].G_is_initialized
    G_is_initialized.restype = c_int
    G_is_initialized.argtypes = [POINTER(c_int)]

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 171
if hasattr(_libs['grass_gis.7.1.svn'], 'G_initialize_done'):
    G_initialize_done = _libs['grass_gis.7.1.svn'].G_initialize_done
    G_initialize_done.restype = None
    G_initialize_done.argtypes = [POINTER(c_int)]

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 172
if hasattr(_libs['grass_gis.7.1.svn'], 'G_init_counter'):
    G_init_counter = _libs['grass_gis.7.1.svn'].G_init_counter
    G_init_counter.restype = None
    G_init_counter.argtypes = [POINTER(struct_Counter), c_int]

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 173
if hasattr(_libs['grass_gis.7.1.svn'], 'G_counter_next'):
    G_counter_next = _libs['grass_gis.7.1.svn'].G_counter_next
    G_counter_next.restype = c_int
    G_counter_next.argtypes = [POINTER(struct_Counter)]

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 176
if hasattr(_libs['grass_gis.7.1.svn'], 'G_date'):
    G_date = _libs['grass_gis.7.1.svn'].G_date
    G_date.restype = ReturnString
    G_date.argtypes = []

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 179
if hasattr(_libs['grass_gis.7.1.svn'], 'G_get_datum_by_name'):
    G_get_datum_by_name = _libs['grass_gis.7.1.svn'].G_get_datum_by_name
    G_get_datum_by_name.restype = c_int
    G_get_datum_by_name.argtypes = [String]

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 180
if hasattr(_libs['grass_gis.7.1.svn'], 'G_datum_name'):
    G_datum_name = _libs['grass_gis.7.1.svn'].G_datum_name
    G_datum_name.restype = ReturnString
    G_datum_name.argtypes = [c_int]

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 181
if hasattr(_libs['grass_gis.7.1.svn'], 'G_datum_description'):
    G_datum_description = _libs['grass_gis.7.1.svn'].G_datum_description
    G_datum_description.restype = ReturnString
    G_datum_description.argtypes = [c_int]

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 182
if hasattr(_libs['grass_gis.7.1.svn'], 'G_datum_ellipsoid'):
    G_datum_ellipsoid = _libs['grass_gis.7.1.svn'].G_datum_ellipsoid
    G_datum_ellipsoid.restype = ReturnString
    G_datum_ellipsoid.argtypes = [c_int]

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 183
if hasattr(_libs['grass_gis.7.1.svn'], 'G_get_datumparams_from_projinfo'):
    G_get_datumparams_from_projinfo = _libs['grass_gis.7.1.svn'].G_get_datumparams_from_projinfo
    G_get_datumparams_from_projinfo.restype = c_int
    G_get_datumparams_from_projinfo.argtypes = [POINTER(struct_Key_Value), String, String]

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 184
if hasattr(_libs['grass_gis.7.1.svn'], 'G_read_datum_table'):
    G_read_datum_table = _libs['grass_gis.7.1.svn'].G_read_datum_table
    G_read_datum_table.restype = None
    G_read_datum_table.argtypes = []

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 188
if hasattr(_libs['grass_gis.7.1.svn'], 'G_init_debug'):
    G_init_debug = _libs['grass_gis.7.1.svn'].G_init_debug
    G_init_debug.restype = None
    G_init_debug.argtypes = []

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 189
if hasattr(_libs['grass_gis.7.1.svn'], 'G_debug'):
    _func = _libs['grass_gis.7.1.svn'].G_debug
    _restype = c_int
    _argtypes = [c_int, String]
    G_debug = _variadic_function(_func,_restype,_argtypes)

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 192
if hasattr(_libs['grass_gis.7.1.svn'], 'G_begin_distance_calculations'):
    G_begin_distance_calculations = _libs['grass_gis.7.1.svn'].G_begin_distance_calculations
    G_begin_distance_calculations.restype = c_int
    G_begin_distance_calculations.argtypes = []

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 193
if hasattr(_libs['grass_gis.7.1.svn'], 'G_distance'):
    G_distance = _libs['grass_gis.7.1.svn'].G_distance
    G_distance.restype = c_double
    G_distance.argtypes = [c_double, c_double, c_double, c_double]

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 194
if hasattr(_libs['grass_gis.7.1.svn'], 'G_distance_between_line_segments'):
    G_distance_between_line_segments = _libs['grass_gis.7.1.svn'].G_distance_between_line_segments
    G_distance_between_line_segments.restype = c_double
    G_distance_between_line_segments.argtypes = [c_double, c_double, c_double, c_double, c_double, c_double, c_double, c_double]

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 196
if hasattr(_libs['grass_gis.7.1.svn'], 'G_distance_point_to_line_segment'):
    G_distance_point_to_line_segment = _libs['grass_gis.7.1.svn'].G_distance_point_to_line_segment
    G_distance_point_to_line_segment.restype = c_double
    G_distance_point_to_line_segment.argtypes = [c_double, c_double, c_double, c_double, c_double, c_double]

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 200
if hasattr(_libs['grass_gis.7.1.svn'], 'G_done_msg'):
    _func = _libs['grass_gis.7.1.svn'].G_done_msg
    _restype = None
    _argtypes = [String]
    G_done_msg = _variadic_function(_func,_restype,_argtypes)

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 203
if hasattr(_libs['grass_gis.7.1.svn'], 'G_is_little_endian'):
    G_is_little_endian = _libs['grass_gis.7.1.svn'].G_is_little_endian
    G_is_little_endian.restype = c_int
    G_is_little_endian.argtypes = []

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 206
if hasattr(_libs['grass_gis.7.1.svn'], 'G_init_env'):
    G_init_env = _libs['grass_gis.7.1.svn'].G_init_env
    G_init_env.restype = None
    G_init_env.argtypes = []

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 207
if hasattr(_libs['grass_gis.7.1.svn'], 'G_getenv'):
    G_getenv = _libs['grass_gis.7.1.svn'].G_getenv
    G_getenv.restype = ReturnString
    G_getenv.argtypes = [String]

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 208
if hasattr(_libs['grass_gis.7.1.svn'], 'G_getenv2'):
    G_getenv2 = _libs['grass_gis.7.1.svn'].G_getenv2
    G_getenv2.restype = ReturnString
    G_getenv2.argtypes = [String, c_int]

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 209
if hasattr(_libs['grass_gis.7.1.svn'], 'G__getenv'):
    G__getenv = _libs['grass_gis.7.1.svn'].G__getenv
    G__getenv.restype = ReturnString
    G__getenv.argtypes = [String]

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 210
if hasattr(_libs['grass_gis.7.1.svn'], 'G__getenv2'):
    G__getenv2 = _libs['grass_gis.7.1.svn'].G__getenv2
    G__getenv2.restype = ReturnString
    G__getenv2.argtypes = [String, c_int]

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 211
if hasattr(_libs['grass_gis.7.1.svn'], 'G_setenv'):
    G_setenv = _libs['grass_gis.7.1.svn'].G_setenv
    G_setenv.restype = None
    G_setenv.argtypes = [String, String]

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 212
if hasattr(_libs['grass_gis.7.1.svn'], 'G_setenv2'):
    G_setenv2 = _libs['grass_gis.7.1.svn'].G_setenv2
    G_setenv2.restype = None
    G_setenv2.argtypes = [String, String, c_int]

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 213
if hasattr(_libs['grass_gis.7.1.svn'], 'G__setenv'):
    G__setenv = _libs['grass_gis.7.1.svn'].G__setenv
    G__setenv.restype = None
    G__setenv.argtypes = [String, String]

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 214
if hasattr(_libs['grass_gis.7.1.svn'], 'G__setenv2'):
    G__setenv2 = _libs['grass_gis.7.1.svn'].G__setenv2
    G__setenv2.restype = None
    G__setenv2.argtypes = [String, String, c_int]

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 215
if hasattr(_libs['grass_gis.7.1.svn'], 'G_unsetenv'):
    G_unsetenv = _libs['grass_gis.7.1.svn'].G_unsetenv
    G_unsetenv.restype = None
    G_unsetenv.argtypes = [String]

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 216
if hasattr(_libs['grass_gis.7.1.svn'], 'G_unsetenv2'):
    G_unsetenv2 = _libs['grass_gis.7.1.svn'].G_unsetenv2
    G_unsetenv2.restype = None
    G_unsetenv2.argtypes = [String, c_int]

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 217
if hasattr(_libs['grass_gis.7.1.svn'], 'G__write_env'):
    G__write_env = _libs['grass_gis.7.1.svn'].G__write_env
    G__write_env.restype = None
    G__write_env.argtypes = []

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 218
if hasattr(_libs['grass_gis.7.1.svn'], 'G_get_env_name'):
    G_get_env_name = _libs['grass_gis.7.1.svn'].G_get_env_name
    G_get_env_name.restype = ReturnString
    G_get_env_name.argtypes = [c_int]

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 219
if hasattr(_libs['grass_gis.7.1.svn'], 'G__read_env'):
    G__read_env = _libs['grass_gis.7.1.svn'].G__read_env
    G__read_env.restype = None
    G__read_env.argtypes = []

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 220
if hasattr(_libs['grass_gis.7.1.svn'], 'G_set_gisrc_mode'):
    G_set_gisrc_mode = _libs['grass_gis.7.1.svn'].G_set_gisrc_mode
    G_set_gisrc_mode.restype = None
    G_set_gisrc_mode.argtypes = [c_int]

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 221
if hasattr(_libs['grass_gis.7.1.svn'], 'G_get_gisrc_mode'):
    G_get_gisrc_mode = _libs['grass_gis.7.1.svn'].G_get_gisrc_mode
    G_get_gisrc_mode.restype = c_int
    G_get_gisrc_mode.argtypes = []

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 222
if hasattr(_libs['grass_gis.7.1.svn'], 'G_create_alt_env'):
    G_create_alt_env = _libs['grass_gis.7.1.svn'].G_create_alt_env
    G_create_alt_env.restype = None
    G_create_alt_env.argtypes = []

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 223
if hasattr(_libs['grass_gis.7.1.svn'], 'G_switch_env'):
    G_switch_env = _libs['grass_gis.7.1.svn'].G_switch_env
    G_switch_env.restype = None
    G_switch_env.argtypes = []

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 226
if hasattr(_libs['grass_gis.7.1.svn'], 'G_fatal_longjmp'):
    G_fatal_longjmp = _libs['grass_gis.7.1.svn'].G_fatal_longjmp
    G_fatal_longjmp.restype = POINTER(jmp_buf)
    G_fatal_longjmp.argtypes = [c_int]

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 227
if hasattr(_libs['grass_gis.7.1.svn'], 'G_info_format'):
    G_info_format = _libs['grass_gis.7.1.svn'].G_info_format
    G_info_format.restype = c_int
    G_info_format.argtypes = []

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 228
if hasattr(_libs['grass_gis.7.1.svn'], 'G_message'):
    _func = _libs['grass_gis.7.1.svn'].G_message
    _restype = None
    _argtypes = [String]
    G_message = _variadic_function(_func,_restype,_argtypes)

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 229
if hasattr(_libs['grass_gis.7.1.svn'], 'G_verbose_message'):
    _func = _libs['grass_gis.7.1.svn'].G_verbose_message
    _restype = None
    _argtypes = [String]
    G_verbose_message = _variadic_function(_func,_restype,_argtypes)

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 231
if hasattr(_libs['grass_gis.7.1.svn'], 'G_important_message'):
    _func = _libs['grass_gis.7.1.svn'].G_important_message
    _restype = None
    _argtypes = [String]
    G_important_message = _variadic_function(_func,_restype,_argtypes)

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 233
if hasattr(_libs['grass_gis.7.1.svn'], 'G_fatal_error'):
    _func = _libs['grass_gis.7.1.svn'].G_fatal_error
    _restype = None
    _argtypes = [String]
    G_fatal_error = _variadic_function(_func,_restype,_argtypes)

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 235
if hasattr(_libs['grass_gis.7.1.svn'], 'G_warning'):
    _func = _libs['grass_gis.7.1.svn'].G_warning
    _restype = None
    _argtypes = [String]
    G_warning = _variadic_function(_func,_restype,_argtypes)

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 236
if hasattr(_libs['grass_gis.7.1.svn'], 'G_suppress_warnings'):
    G_suppress_warnings = _libs['grass_gis.7.1.svn'].G_suppress_warnings
    G_suppress_warnings.restype = c_int
    G_suppress_warnings.argtypes = [c_int]

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 237
if hasattr(_libs['grass_gis.7.1.svn'], 'G_sleep_on_error'):
    G_sleep_on_error = _libs['grass_gis.7.1.svn'].G_sleep_on_error
    G_sleep_on_error.restype = c_int
    G_sleep_on_error.argtypes = [c_int]

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 238
if hasattr(_libs['grass_gis.7.1.svn'], 'G_set_error_routine'):
    G_set_error_routine = _libs['grass_gis.7.1.svn'].G_set_error_routine
    G_set_error_routine.restype = None
    G_set_error_routine.argtypes = [CFUNCTYPE(UNCHECKED(c_int), String, c_int)]

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 239
if hasattr(_libs['grass_gis.7.1.svn'], 'G_unset_error_routine'):
    G_unset_error_routine = _libs['grass_gis.7.1.svn'].G_unset_error_routine
    G_unset_error_routine.restype = None
    G_unset_error_routine.argtypes = []

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 240
if hasattr(_libs['grass_gis.7.1.svn'], 'G_init_logging'):
    G_init_logging = _libs['grass_gis.7.1.svn'].G_init_logging
    G_init_logging.restype = None
    G_init_logging.argtypes = []

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 243
if hasattr(_libs['grass_gis.7.1.svn'], 'G_file_name'):
    G_file_name = _libs['grass_gis.7.1.svn'].G_file_name
    G_file_name.restype = ReturnString
    G_file_name.argtypes = [String, String, String, String]

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 244
if hasattr(_libs['grass_gis.7.1.svn'], 'G_file_name_misc'):
    G_file_name_misc = _libs['grass_gis.7.1.svn'].G_file_name_misc
    G_file_name_misc.restype = ReturnString
    G_file_name_misc.argtypes = [String, String, String, String, String]

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 248
if hasattr(_libs['grass_gis.7.1.svn'], 'G_find_file'):
    G_find_file = _libs['grass_gis.7.1.svn'].G_find_file
    G_find_file.restype = ReturnString
    G_find_file.argtypes = [String, String, String]

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 249
if hasattr(_libs['grass_gis.7.1.svn'], 'G_find_file2'):
    G_find_file2 = _libs['grass_gis.7.1.svn'].G_find_file2
    G_find_file2.restype = ReturnString
    G_find_file2.argtypes = [String, String, String]

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 250
if hasattr(_libs['grass_gis.7.1.svn'], 'G_find_file_misc'):
    G_find_file_misc = _libs['grass_gis.7.1.svn'].G_find_file_misc
    G_find_file_misc.restype = ReturnString
    G_find_file_misc.argtypes = [String, String, String, String]

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 251
if hasattr(_libs['grass_gis.7.1.svn'], 'G_find_file2_misc'):
    G_find_file2_misc = _libs['grass_gis.7.1.svn'].G_find_file2_misc
    G_find_file2_misc.restype = ReturnString
    G_find_file2_misc.argtypes = [String, String, String, String]

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 255
if hasattr(_libs['grass_gis.7.1.svn'], 'G_find_etc'):
    G_find_etc = _libs['grass_gis.7.1.svn'].G_find_etc
    G_find_etc.restype = ReturnString
    G_find_etc.argtypes = [String]

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 258
if hasattr(_libs['grass_gis.7.1.svn'], 'G_find_raster'):
    G_find_raster = _libs['grass_gis.7.1.svn'].G_find_raster
    G_find_raster.restype = ReturnString
    G_find_raster.argtypes = [String, String]

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 259
if hasattr(_libs['grass_gis.7.1.svn'], 'G_find_raster2'):
    G_find_raster2 = _libs['grass_gis.7.1.svn'].G_find_raster2
    G_find_raster2.restype = ReturnString
    G_find_raster2.argtypes = [String, String]

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 262
if hasattr(_libs['grass_gis.7.1.svn'], 'G_find_raster3d'):
    G_find_raster3d = _libs['grass_gis.7.1.svn'].G_find_raster3d
    G_find_raster3d.restype = ReturnString
    G_find_raster3d.argtypes = [String, String]

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 265
if hasattr(_libs['grass_gis.7.1.svn'], 'G_find_vector'):
    G_find_vector = _libs['grass_gis.7.1.svn'].G_find_vector
    G_find_vector.restype = ReturnString
    G_find_vector.argtypes = [String, String]

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 266
if hasattr(_libs['grass_gis.7.1.svn'], 'G_find_vector2'):
    G_find_vector2 = _libs['grass_gis.7.1.svn'].G_find_vector2
    G_find_vector2.restype = ReturnString
    G_find_vector2.argtypes = [String, String]

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 269
if hasattr(_libs['grass_gis.7.1.svn'], 'G_zlib_compress'):
    G_zlib_compress = _libs['grass_gis.7.1.svn'].G_zlib_compress
    G_zlib_compress.restype = c_int
    G_zlib_compress.argtypes = [POINTER(c_ubyte), c_int, POINTER(c_ubyte), c_int]

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 270
if hasattr(_libs['grass_gis.7.1.svn'], 'G_zlib_expand'):
    G_zlib_expand = _libs['grass_gis.7.1.svn'].G_zlib_expand
    G_zlib_expand.restype = c_int
    G_zlib_expand.argtypes = [POINTER(c_ubyte), c_int, POINTER(c_ubyte), c_int]

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 271
if hasattr(_libs['grass_gis.7.1.svn'], 'G_zlib_write'):
    G_zlib_write = _libs['grass_gis.7.1.svn'].G_zlib_write
    G_zlib_write.restype = c_int
    G_zlib_write.argtypes = [c_int, POINTER(c_ubyte), c_int]

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 272
if hasattr(_libs['grass_gis.7.1.svn'], 'G_zlib_read'):
    G_zlib_read = _libs['grass_gis.7.1.svn'].G_zlib_read
    G_zlib_read.restype = c_int
    G_zlib_read.argtypes = [c_int, c_int, POINTER(c_ubyte), c_int]

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 273
if hasattr(_libs['grass_gis.7.1.svn'], 'G_zlib_write_noCompress'):
    G_zlib_write_noCompress = _libs['grass_gis.7.1.svn'].G_zlib_write_noCompress
    G_zlib_write_noCompress.restype = c_int
    G_zlib_write_noCompress.argtypes = [c_int, POINTER(c_ubyte), c_int]

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 276
if hasattr(_libs['grass_gis.7.1.svn'], 'G_begin_geodesic_equation'):
    G_begin_geodesic_equation = _libs['grass_gis.7.1.svn'].G_begin_geodesic_equation
    G_begin_geodesic_equation.restype = c_int
    G_begin_geodesic_equation.argtypes = [c_double, c_double, c_double, c_double]

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 277
if hasattr(_libs['grass_gis.7.1.svn'], 'G_geodesic_lat_from_lon'):
    G_geodesic_lat_from_lon = _libs['grass_gis.7.1.svn'].G_geodesic_lat_from_lon
    G_geodesic_lat_from_lon.restype = c_double
    G_geodesic_lat_from_lon.argtypes = [c_double]

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 280
if hasattr(_libs['grass_gis.7.1.svn'], 'G_begin_geodesic_distance'):
    G_begin_geodesic_distance = _libs['grass_gis.7.1.svn'].G_begin_geodesic_distance
    G_begin_geodesic_distance.restype = None
    G_begin_geodesic_distance.argtypes = [c_double, c_double]

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 281
if hasattr(_libs['grass_gis.7.1.svn'], 'G_set_geodesic_distance_lat1'):
    G_set_geodesic_distance_lat1 = _libs['grass_gis.7.1.svn'].G_set_geodesic_distance_lat1
    G_set_geodesic_distance_lat1.restype = None
    G_set_geodesic_distance_lat1.argtypes = [c_double]

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 282
if hasattr(_libs['grass_gis.7.1.svn'], 'G_set_geodesic_distance_lat2'):
    G_set_geodesic_distance_lat2 = _libs['grass_gis.7.1.svn'].G_set_geodesic_distance_lat2
    G_set_geodesic_distance_lat2.restype = None
    G_set_geodesic_distance_lat2.argtypes = [c_double]

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 283
if hasattr(_libs['grass_gis.7.1.svn'], 'G_geodesic_distance_lon_to_lon'):
    G_geodesic_distance_lon_to_lon = _libs['grass_gis.7.1.svn'].G_geodesic_distance_lon_to_lon
    G_geodesic_distance_lon_to_lon.restype = c_double
    G_geodesic_distance_lon_to_lon.argtypes = [c_double, c_double]

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 284
if hasattr(_libs['grass_gis.7.1.svn'], 'G_geodesic_distance'):
    G_geodesic_distance = _libs['grass_gis.7.1.svn'].G_geodesic_distance
    G_geodesic_distance.restype = c_double
    G_geodesic_distance.argtypes = [c_double, c_double, c_double, c_double]

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 287
if hasattr(_libs['grass_gis.7.1.svn'], 'G_get_ellipsoid_parameters'):
    G_get_ellipsoid_parameters = _libs['grass_gis.7.1.svn'].G_get_ellipsoid_parameters
    G_get_ellipsoid_parameters.restype = c_int
    G_get_ellipsoid_parameters.argtypes = [POINTER(c_double), POINTER(c_double)]

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 288
if hasattr(_libs['grass_gis.7.1.svn'], 'G_get_spheroid_by_name'):
    G_get_spheroid_by_name = _libs['grass_gis.7.1.svn'].G_get_spheroid_by_name
    G_get_spheroid_by_name.restype = c_int
    G_get_spheroid_by_name.argtypes = [String, POINTER(c_double), POINTER(c_double), POINTER(c_double)]

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 289
if hasattr(_libs['grass_gis.7.1.svn'], 'G_get_ellipsoid_by_name'):
    G_get_ellipsoid_by_name = _libs['grass_gis.7.1.svn'].G_get_ellipsoid_by_name
    G_get_ellipsoid_by_name.restype = c_int
    G_get_ellipsoid_by_name.argtypes = [String, POINTER(c_double), POINTER(c_double)]

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 290
if hasattr(_libs['grass_gis.7.1.svn'], 'G_ellipsoid_name'):
    G_ellipsoid_name = _libs['grass_gis.7.1.svn'].G_ellipsoid_name
    G_ellipsoid_name.restype = ReturnString
    G_ellipsoid_name.argtypes = [c_int]

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 291
if hasattr(_libs['grass_gis.7.1.svn'], 'G_ellipsoid_description'):
    G_ellipsoid_description = _libs['grass_gis.7.1.svn'].G_ellipsoid_description
    G_ellipsoid_description.restype = ReturnString
    G_ellipsoid_description.argtypes = [c_int]

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 292
if hasattr(_libs['grass_gis.7.1.svn'], 'G_read_ellipsoid_table'):
    G_read_ellipsoid_table = _libs['grass_gis.7.1.svn'].G_read_ellipsoid_table
    G_read_ellipsoid_table.restype = c_int
    G_read_ellipsoid_table.argtypes = [c_int]

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 295
if hasattr(_libs['grass_gis.7.1.svn'], 'G_get_projunits'):
    G_get_projunits = _libs['grass_gis.7.1.svn'].G_get_projunits
    G_get_projunits.restype = POINTER(struct_Key_Value)
    G_get_projunits.argtypes = []

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 296
if hasattr(_libs['grass_gis.7.1.svn'], 'G_get_projinfo'):
    G_get_projinfo = _libs['grass_gis.7.1.svn'].G_get_projinfo
    G_get_projinfo.restype = POINTER(struct_Key_Value)
    G_get_projinfo.argtypes = []

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 297
if hasattr(_libs['grass_gis.7.1.svn'], 'G_get_projepsg'):
    G_get_projepsg = _libs['grass_gis.7.1.svn'].G_get_projepsg
    G_get_projepsg.restype = POINTER(struct_Key_Value)
    G_get_projepsg.argtypes = []

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 300
if hasattr(_libs['grass_gis.7.1.svn'], 'G_get_window'):
    G_get_window = _libs['grass_gis.7.1.svn'].G_get_window
    G_get_window.restype = None
    G_get_window.argtypes = [POINTER(struct_Cell_head)]

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 301
if hasattr(_libs['grass_gis.7.1.svn'], 'G_get_default_window'):
    G_get_default_window = _libs['grass_gis.7.1.svn'].G_get_default_window
    G_get_default_window.restype = None
    G_get_default_window.argtypes = [POINTER(struct_Cell_head)]

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 302
if hasattr(_libs['grass_gis.7.1.svn'], 'G__get_window'):
    G__get_window = _libs['grass_gis.7.1.svn'].G__get_window
    G__get_window.restype = None
    G__get_window.argtypes = [POINTER(struct_Cell_head), String, String, String]

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 306
if hasattr(_libs['grass_gis.7.1.svn'], 'G_getl'):
    G_getl = _libs['grass_gis.7.1.svn'].G_getl
    G_getl.restype = c_int
    G_getl.argtypes = [String, c_int, POINTER(FILE)]

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 307
if hasattr(_libs['grass_gis.7.1.svn'], 'G_getl2'):
    G_getl2 = _libs['grass_gis.7.1.svn'].G_getl2
    G_getl2.restype = c_int
    G_getl2.argtypes = [String, c_int, POINTER(FILE)]

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 310
if hasattr(_libs['grass_gis.7.1.svn'], 'G_gisbase'):
    G_gisbase = _libs['grass_gis.7.1.svn'].G_gisbase
    G_gisbase.restype = ReturnString
    G_gisbase.argtypes = []

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 313
if hasattr(_libs['grass_gis.7.1.svn'], 'G_gisdbase'):
    G_gisdbase = _libs['grass_gis.7.1.svn'].G_gisdbase
    G_gisdbase.restype = ReturnString
    G_gisdbase.argtypes = []

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 316
if hasattr(_libs['grass_gis.7.1.svn'], 'G__gisinit'):
    G__gisinit = _libs['grass_gis.7.1.svn'].G__gisinit
    G__gisinit.restype = None
    G__gisinit.argtypes = [String, String]

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 317
if hasattr(_libs['grass_gis.7.1.svn'], 'G__no_gisinit'):
    G__no_gisinit = _libs['grass_gis.7.1.svn'].G__no_gisinit
    G__no_gisinit.restype = None
    G__no_gisinit.argtypes = [String]

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 318
if hasattr(_libs['grass_gis.7.1.svn'], 'G__check_gisinit'):
    G__check_gisinit = _libs['grass_gis.7.1.svn'].G__check_gisinit
    G__check_gisinit.restype = None
    G__check_gisinit.argtypes = []

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 319
if hasattr(_libs['grass_gis.7.1.svn'], 'G_init_all'):
    G_init_all = _libs['grass_gis.7.1.svn'].G_init_all
    G_init_all.restype = None
    G_init_all.argtypes = []

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 322
if hasattr(_libs['grass_gis.7.1.svn'], 'G_add_error_handler'):
    G_add_error_handler = _libs['grass_gis.7.1.svn'].G_add_error_handler
    G_add_error_handler.restype = None
    G_add_error_handler.argtypes = [CFUNCTYPE(UNCHECKED(None), POINTER(None)), POINTER(None)]

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 323
if hasattr(_libs['grass_gis.7.1.svn'], 'G_remove_error_handler'):
    G_remove_error_handler = _libs['grass_gis.7.1.svn'].G_remove_error_handler
    G_remove_error_handler.restype = None
    G_remove_error_handler.argtypes = [CFUNCTYPE(UNCHECKED(None), POINTER(None)), POINTER(None)]

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 324
if hasattr(_libs['grass_gis.7.1.svn'], 'G__call_error_handlers'):
    G__call_error_handlers = _libs['grass_gis.7.1.svn'].G__call_error_handlers
    G__call_error_handlers.restype = None
    G__call_error_handlers.argtypes = []

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 327
if hasattr(_libs['grass_gis.7.1.svn'], 'G_home'):
    G_home = _libs['grass_gis.7.1.svn'].G_home
    G_home.restype = ReturnString
    G_home.argtypes = []

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 328
if hasattr(_libs['grass_gis.7.1.svn'], 'G__home'):
    G__home = _libs['grass_gis.7.1.svn'].G__home
    G__home.restype = ReturnString
    G__home.argtypes = []

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 329
if hasattr(_libs['grass_gis.7.1.svn'], 'G_config_path'):
    G_config_path = _libs['grass_gis.7.1.svn'].G_config_path
    G_config_path.restype = ReturnString
    G_config_path.argtypes = []

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 332
if hasattr(_libs['grass_gis.7.1.svn'], 'G_init_ilist'):
    G_init_ilist = _libs['grass_gis.7.1.svn'].G_init_ilist
    G_init_ilist.restype = None
    G_init_ilist.argtypes = [POINTER(struct_ilist)]

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 333
if hasattr(_libs['grass_gis.7.1.svn'], 'G_free_ilist'):
    G_free_ilist = _libs['grass_gis.7.1.svn'].G_free_ilist
    G_free_ilist.restype = None
    G_free_ilist.argtypes = [POINTER(struct_ilist)]

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 334
if hasattr(_libs['grass_gis.7.1.svn'], 'G_new_ilist'):
    G_new_ilist = _libs['grass_gis.7.1.svn'].G_new_ilist
    G_new_ilist.restype = POINTER(struct_ilist)
    G_new_ilist.argtypes = []

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 335
if hasattr(_libs['grass_gis.7.1.svn'], 'G_ilist_add'):
    G_ilist_add = _libs['grass_gis.7.1.svn'].G_ilist_add
    G_ilist_add.restype = None
    G_ilist_add.argtypes = [POINTER(struct_ilist), c_int]

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 338
if hasattr(_libs['grass_gis.7.1.svn'], 'G_intersect_line_segments'):
    G_intersect_line_segments = _libs['grass_gis.7.1.svn'].G_intersect_line_segments
    G_intersect_line_segments.restype = c_int
    G_intersect_line_segments.argtypes = [c_double, c_double, c_double, c_double, c_double, c_double, c_double, c_double, POINTER(c_double), POINTER(c_double), POINTER(c_double), POINTER(c_double)]

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 343
if hasattr(_libs['grass_gis.7.1.svn'], 'G_is_gisbase'):
    G_is_gisbase = _libs['grass_gis.7.1.svn'].G_is_gisbase
    G_is_gisbase.restype = c_int
    G_is_gisbase.argtypes = [String]

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 344
if hasattr(_libs['grass_gis.7.1.svn'], 'G_is_location'):
    G_is_location = _libs['grass_gis.7.1.svn'].G_is_location
    G_is_location.restype = c_int
    G_is_location.argtypes = [String]

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 345
if hasattr(_libs['grass_gis.7.1.svn'], 'G_is_mapset'):
    G_is_mapset = _libs['grass_gis.7.1.svn'].G_is_mapset
    G_is_mapset.restype = c_int
    G_is_mapset.argtypes = [String]

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 348
if hasattr(_libs['grass_gis.7.1.svn'], 'G_create_key_value'):
    G_create_key_value = _libs['grass_gis.7.1.svn'].G_create_key_value
    G_create_key_value.restype = POINTER(struct_Key_Value)
    G_create_key_value.argtypes = []

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 349
if hasattr(_libs['grass_gis.7.1.svn'], 'G_set_key_value'):
    G_set_key_value = _libs['grass_gis.7.1.svn'].G_set_key_value
    G_set_key_value.restype = None
    G_set_key_value.argtypes = [String, String, POINTER(struct_Key_Value)]

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 350
if hasattr(_libs['grass_gis.7.1.svn'], 'G_find_key_value'):
    G_find_key_value = _libs['grass_gis.7.1.svn'].G_find_key_value
    G_find_key_value.restype = ReturnString
    G_find_key_value.argtypes = [String, POINTER(struct_Key_Value)]

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 351
if hasattr(_libs['grass_gis.7.1.svn'], 'G_free_key_value'):
    G_free_key_value = _libs['grass_gis.7.1.svn'].G_free_key_value
    G_free_key_value.restype = None
    G_free_key_value.argtypes = [POINTER(struct_Key_Value)]

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 354
if hasattr(_libs['grass_gis.7.1.svn'], 'G_fwrite_key_value'):
    G_fwrite_key_value = _libs['grass_gis.7.1.svn'].G_fwrite_key_value
    G_fwrite_key_value.restype = c_int
    G_fwrite_key_value.argtypes = [POINTER(FILE), POINTER(struct_Key_Value)]

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 355
if hasattr(_libs['grass_gis.7.1.svn'], 'G_fread_key_value'):
    G_fread_key_value = _libs['grass_gis.7.1.svn'].G_fread_key_value
    G_fread_key_value.restype = POINTER(struct_Key_Value)
    G_fread_key_value.argtypes = [POINTER(FILE)]

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 358
if hasattr(_libs['grass_gis.7.1.svn'], 'G_write_key_value_file'):
    G_write_key_value_file = _libs['grass_gis.7.1.svn'].G_write_key_value_file
    G_write_key_value_file.restype = None
    G_write_key_value_file.argtypes = [String, POINTER(struct_Key_Value)]

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 359
if hasattr(_libs['grass_gis.7.1.svn'], 'G_read_key_value_file'):
    G_read_key_value_file = _libs['grass_gis.7.1.svn'].G_read_key_value_file
    G_read_key_value_file.restype = POINTER(struct_Key_Value)
    G_read_key_value_file.argtypes = [String]

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 362
if hasattr(_libs['grass_gis.7.1.svn'], 'G_update_key_value_file'):
    G_update_key_value_file = _libs['grass_gis.7.1.svn'].G_update_key_value_file
    G_update_key_value_file.restype = None
    G_update_key_value_file.argtypes = [String, String, String]

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 363
if hasattr(_libs['grass_gis.7.1.svn'], 'G_lookup_key_value_from_file'):
    G_lookup_key_value_from_file = _libs['grass_gis.7.1.svn'].G_lookup_key_value_from_file
    G_lookup_key_value_from_file.restype = c_int
    G_lookup_key_value_from_file.argtypes = [String, String, POINTER(c_char), c_int]

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 366
if hasattr(_libs['grass_gis.7.1.svn'], 'G_legal_filename'):
    G_legal_filename = _libs['grass_gis.7.1.svn'].G_legal_filename
    G_legal_filename.restype = c_int
    G_legal_filename.argtypes = [String]

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 367
if hasattr(_libs['grass_gis.7.1.svn'], 'G_check_input_output_name'):
    G_check_input_output_name = _libs['grass_gis.7.1.svn'].G_check_input_output_name
    G_check_input_output_name.restype = c_int
    G_check_input_output_name.argtypes = [String, String, c_int]

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 370
if hasattr(_libs['grass_gis.7.1.svn'], 'G_set_distance_to_line_tolerance'):
    G_set_distance_to_line_tolerance = _libs['grass_gis.7.1.svn'].G_set_distance_to_line_tolerance
    G_set_distance_to_line_tolerance.restype = None
    G_set_distance_to_line_tolerance.argtypes = [c_double]

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 371
if hasattr(_libs['grass_gis.7.1.svn'], 'G_distance2_point_to_line'):
    G_distance2_point_to_line = _libs['grass_gis.7.1.svn'].G_distance2_point_to_line
    G_distance2_point_to_line.restype = c_double
    G_distance2_point_to_line.argtypes = [c_double, c_double, c_double, c_double, c_double, c_double]

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 375
if hasattr(_libs['grass_gis.7.1.svn'], 'G_list_element'):
    G_list_element = _libs['grass_gis.7.1.svn'].G_list_element
    G_list_element.restype = None
    G_list_element.argtypes = [String, String, String, CFUNCTYPE(UNCHECKED(c_int), String, String, String)]

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 377
if hasattr(_libs['grass_gis.7.1.svn'], 'G_list'):
    G_list = _libs['grass_gis.7.1.svn'].G_list
    G_list.restype = POINTER(POINTER(c_char))
    G_list.argtypes = [c_int, String, String, String]

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 378
if hasattr(_libs['grass_gis.7.1.svn'], 'G_free_list'):
    G_free_list = _libs['grass_gis.7.1.svn'].G_free_list
    G_free_list.restype = None
    G_free_list.argtypes = [POINTER(POINTER(c_char))]

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 381
if hasattr(_libs['grass_gis.7.1.svn'], 'G_lat_format'):
    G_lat_format = _libs['grass_gis.7.1.svn'].G_lat_format
    G_lat_format.restype = None
    G_lat_format.argtypes = [c_double, String]

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 382
if hasattr(_libs['grass_gis.7.1.svn'], 'G_lat_format_string'):
    G_lat_format_string = _libs['grass_gis.7.1.svn'].G_lat_format_string
    G_lat_format_string.restype = ReturnString
    G_lat_format_string.argtypes = []

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 383
if hasattr(_libs['grass_gis.7.1.svn'], 'G_lon_format'):
    G_lon_format = _libs['grass_gis.7.1.svn'].G_lon_format
    G_lon_format.restype = None
    G_lon_format.argtypes = [c_double, String]

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 384
if hasattr(_libs['grass_gis.7.1.svn'], 'G_lon_format_string'):
    G_lon_format_string = _libs['grass_gis.7.1.svn'].G_lon_format_string
    G_lon_format_string.restype = ReturnString
    G_lon_format_string.argtypes = []

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 385
if hasattr(_libs['grass_gis.7.1.svn'], 'G_llres_format'):
    G_llres_format = _libs['grass_gis.7.1.svn'].G_llres_format
    G_llres_format.restype = None
    G_llres_format.argtypes = [c_double, String]

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 386
if hasattr(_libs['grass_gis.7.1.svn'], 'G_llres_format_string'):
    G_llres_format_string = _libs['grass_gis.7.1.svn'].G_llres_format_string
    G_llres_format_string.restype = ReturnString
    G_llres_format_string.argtypes = []

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 387
if hasattr(_libs['grass_gis.7.1.svn'], 'G_lat_parts'):
    G_lat_parts = _libs['grass_gis.7.1.svn'].G_lat_parts
    G_lat_parts.restype = None
    G_lat_parts.argtypes = [c_double, POINTER(c_int), POINTER(c_int), POINTER(c_double), String]

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 388
if hasattr(_libs['grass_gis.7.1.svn'], 'G_lon_parts'):
    G_lon_parts = _libs['grass_gis.7.1.svn'].G_lon_parts
    G_lon_parts.restype = None
    G_lon_parts.argtypes = [c_double, POINTER(c_int), POINTER(c_int), POINTER(c_double), String]

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 391
if hasattr(_libs['grass_gis.7.1.svn'], 'G_lat_scan'):
    G_lat_scan = _libs['grass_gis.7.1.svn'].G_lat_scan
    G_lat_scan.restype = c_int
    G_lat_scan.argtypes = [String, POINTER(c_double)]

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 392
if hasattr(_libs['grass_gis.7.1.svn'], 'G_lon_scan'):
    G_lon_scan = _libs['grass_gis.7.1.svn'].G_lon_scan
    G_lon_scan.restype = c_int
    G_lon_scan.argtypes = [String, POINTER(c_double)]

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 393
if hasattr(_libs['grass_gis.7.1.svn'], 'G_llres_scan'):
    G_llres_scan = _libs['grass_gis.7.1.svn'].G_llres_scan
    G_llres_scan.restype = c_int
    G_llres_scan.argtypes = [String, POINTER(c_double)]

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 396
if hasattr(_libs['grass_gis.7.1.svn'], 'G_location'):
    G_location = _libs['grass_gis.7.1.svn'].G_location
    G_location.restype = ReturnString
    G_location.argtypes = []

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 397
if hasattr(_libs['grass_gis.7.1.svn'], 'G_location_path'):
    G_location_path = _libs['grass_gis.7.1.svn'].G_location_path
    G_location_path.restype = ReturnString
    G_location_path.argtypes = []

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 400
if hasattr(_libs['grass_gis.7.1.svn'], 'G_set_ls_filter'):
    G_set_ls_filter = _libs['grass_gis.7.1.svn'].G_set_ls_filter
    G_set_ls_filter.restype = None
    G_set_ls_filter.argtypes = [CFUNCTYPE(UNCHECKED(c_int), String, POINTER(None)), POINTER(None)]

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 401
if hasattr(_libs['grass_gis.7.1.svn'], 'G_set_ls_exclude_filter'):
    G_set_ls_exclude_filter = _libs['grass_gis.7.1.svn'].G_set_ls_exclude_filter
    G_set_ls_exclude_filter.restype = None
    G_set_ls_exclude_filter.argtypes = [CFUNCTYPE(UNCHECKED(c_int), String, POINTER(None)), POINTER(None)]

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 402
if hasattr(_libs['grass_gis.7.1.svn'], 'G__ls'):
    G__ls = _libs['grass_gis.7.1.svn'].G__ls
    G__ls.restype = POINTER(POINTER(c_char))
    G__ls.argtypes = [String, POINTER(c_int)]

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 403
if hasattr(_libs['grass_gis.7.1.svn'], 'G_ls'):
    G_ls = _libs['grass_gis.7.1.svn'].G_ls
    G_ls.restype = None
    G_ls.argtypes = [String, POINTER(FILE)]

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 404
if hasattr(_libs['grass_gis.7.1.svn'], 'G_ls_format'):
    G_ls_format = _libs['grass_gis.7.1.svn'].G_ls_format
    G_ls_format.restype = None
    G_ls_format.argtypes = [POINTER(POINTER(c_char)), c_int, c_int, POINTER(FILE)]

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 408
if hasattr(_libs['grass_gis.7.1.svn'], 'G_ls_regex_filter'):
    G_ls_regex_filter = _libs['grass_gis.7.1.svn'].G_ls_regex_filter
    G_ls_regex_filter.restype = POINTER(None)
    G_ls_regex_filter.argtypes = [String, c_int, c_int]

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 409
if hasattr(_libs['grass_gis.7.1.svn'], 'G_ls_glob_filter'):
    G_ls_glob_filter = _libs['grass_gis.7.1.svn'].G_ls_glob_filter
    G_ls_glob_filter.restype = POINTER(None)
    G_ls_glob_filter.argtypes = [String, c_int]

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 410
if hasattr(_libs['grass_gis.7.1.svn'], 'G_free_ls_filter'):
    G_free_ls_filter = _libs['grass_gis.7.1.svn'].G_free_ls_filter
    G_free_ls_filter.restype = None
    G_free_ls_filter.argtypes = [POINTER(None)]

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 414
if hasattr(_libs['grass_gis.7.1.svn'], 'G__machine_name'):
    G__machine_name = _libs['grass_gis.7.1.svn'].G__machine_name
    G__machine_name.restype = ReturnString
    G__machine_name.argtypes = []

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 417
if hasattr(_libs['grass_gis.7.1.svn'], 'G_make_location'):
    G_make_location = _libs['grass_gis.7.1.svn'].G_make_location
    G_make_location.restype = c_int
    G_make_location.argtypes = [String, POINTER(struct_Cell_head), POINTER(struct_Key_Value), POINTER(struct_Key_Value)]

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 419
if hasattr(_libs['grass_gis.7.1.svn'], 'G_compare_projections'):
    G_compare_projections = _libs['grass_gis.7.1.svn'].G_compare_projections
    G_compare_projections.restype = c_int
    G_compare_projections.argtypes = [POINTER(struct_Key_Value), POINTER(struct_Key_Value), POINTER(struct_Key_Value), POINTER(struct_Key_Value)]

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 423
if hasattr(_libs['grass_gis.7.1.svn'], 'G_make_mapset'):
    G_make_mapset = _libs['grass_gis.7.1.svn'].G_make_mapset
    G_make_mapset.restype = c_int
    G_make_mapset.argtypes = [String, String, String]

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 426
if hasattr(_libs['grass_gis.7.1.svn'], 'G_tolcase'):
    G_tolcase = _libs['grass_gis.7.1.svn'].G_tolcase
    G_tolcase.restype = ReturnString
    G_tolcase.argtypes = [String]

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 427
if hasattr(_libs['grass_gis.7.1.svn'], 'G_toucase'):
    G_toucase = _libs['grass_gis.7.1.svn'].G_toucase
    G_toucase.restype = ReturnString
    G_toucase.argtypes = [String]

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 430
if hasattr(_libs['grass_gis.7.1.svn'], 'G_mapset'):
    G_mapset = _libs['grass_gis.7.1.svn'].G_mapset
    G_mapset.restype = ReturnString
    G_mapset.argtypes = []

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 431
if hasattr(_libs['grass_gis.7.1.svn'], 'G_mapset_path'):
    G_mapset_path = _libs['grass_gis.7.1.svn'].G_mapset_path
    G_mapset_path.restype = ReturnString
    G_mapset_path.argtypes = []

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 434
if hasattr(_libs['grass_gis.7.1.svn'], 'G__make_mapset_element'):
    G__make_mapset_element = _libs['grass_gis.7.1.svn'].G__make_mapset_element
    G__make_mapset_element.restype = c_int
    G__make_mapset_element.argtypes = [String]

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 435
if hasattr(_libs['grass_gis.7.1.svn'], 'G__make_mapset_element_misc'):
    G__make_mapset_element_misc = _libs['grass_gis.7.1.svn'].G__make_mapset_element_misc
    G__make_mapset_element_misc.restype = c_int
    G__make_mapset_element_misc.argtypes = [String, String]

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 436
if hasattr(_libs['grass_gis.7.1.svn'], 'G__mapset_permissions'):
    G__mapset_permissions = _libs['grass_gis.7.1.svn'].G__mapset_permissions
    G__mapset_permissions.restype = c_int
    G__mapset_permissions.argtypes = [String]

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 437
if hasattr(_libs['grass_gis.7.1.svn'], 'G__mapset_permissions2'):
    G__mapset_permissions2 = _libs['grass_gis.7.1.svn'].G__mapset_permissions2
    G__mapset_permissions2.restype = c_int
    G__mapset_permissions2.argtypes = [String, String, String]

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 440
if hasattr(_libs['grass_gis.7.1.svn'], 'G_get_mapset_name'):
    G_get_mapset_name = _libs['grass_gis.7.1.svn'].G_get_mapset_name
    G_get_mapset_name.restype = ReturnString
    G_get_mapset_name.argtypes = [c_int]

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 441
if hasattr(_libs['grass_gis.7.1.svn'], 'G__get_list_of_mapsets'):
    G__get_list_of_mapsets = _libs['grass_gis.7.1.svn'].G__get_list_of_mapsets
    G__get_list_of_mapsets.restype = None
    G__get_list_of_mapsets.argtypes = []

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 442
if hasattr(_libs['grass_gis.7.1.svn'], 'G_create_alt_search_path'):
    G_create_alt_search_path = _libs['grass_gis.7.1.svn'].G_create_alt_search_path
    G_create_alt_search_path.restype = None
    G_create_alt_search_path.argtypes = []

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 443
if hasattr(_libs['grass_gis.7.1.svn'], 'G_switch_search_path'):
    G_switch_search_path = _libs['grass_gis.7.1.svn'].G_switch_search_path
    G_switch_search_path.restype = None
    G_switch_search_path.argtypes = []

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 444
if hasattr(_libs['grass_gis.7.1.svn'], 'G_reset_mapsets'):
    G_reset_mapsets = _libs['grass_gis.7.1.svn'].G_reset_mapsets
    G_reset_mapsets.restype = None
    G_reset_mapsets.argtypes = []

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 445
if hasattr(_libs['grass_gis.7.1.svn'], 'G_get_available_mapsets'):
    G_get_available_mapsets = _libs['grass_gis.7.1.svn'].G_get_available_mapsets
    G_get_available_mapsets.restype = POINTER(POINTER(c_char))
    G_get_available_mapsets.argtypes = []

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 446
if hasattr(_libs['grass_gis.7.1.svn'], 'G_add_mapset_to_search_path'):
    G_add_mapset_to_search_path = _libs['grass_gis.7.1.svn'].G_add_mapset_to_search_path
    G_add_mapset_to_search_path.restype = None
    G_add_mapset_to_search_path.argtypes = [String]

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 447
if hasattr(_libs['grass_gis.7.1.svn'], 'G_is_mapset_in_search_path'):
    G_is_mapset_in_search_path = _libs['grass_gis.7.1.svn'].G_is_mapset_in_search_path
    G_is_mapset_in_search_path.restype = c_int
    G_is_mapset_in_search_path.argtypes = [String]

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 450
if hasattr(_libs['grass_gis.7.1.svn'], 'G_myname'):
    G_myname = _libs['grass_gis.7.1.svn'].G_myname
    G_myname.restype = ReturnString
    G_myname.argtypes = []

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 453
if hasattr(_libs['grass_gis.7.1.svn'], 'G_color_values'):
    G_color_values = _libs['grass_gis.7.1.svn'].G_color_values
    G_color_values.restype = c_int
    G_color_values.argtypes = [String, POINTER(c_float), POINTER(c_float), POINTER(c_float)]

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 454
if hasattr(_libs['grass_gis.7.1.svn'], 'G_color_name'):
    G_color_name = _libs['grass_gis.7.1.svn'].G_color_name
    G_color_name.restype = ReturnString
    G_color_name.argtypes = [c_int]

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 457
if hasattr(_libs['grass_gis.7.1.svn'], 'G_newlines_to_spaces'):
    G_newlines_to_spaces = _libs['grass_gis.7.1.svn'].G_newlines_to_spaces
    G_newlines_to_spaces.restype = None
    G_newlines_to_spaces.argtypes = [String]

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 460
if hasattr(_libs['grass_gis.7.1.svn'], 'G_name_is_fully_qualified'):
    G_name_is_fully_qualified = _libs['grass_gis.7.1.svn'].G_name_is_fully_qualified
    G_name_is_fully_qualified.restype = c_int
    G_name_is_fully_qualified.argtypes = [String, String, String]

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 461
if hasattr(_libs['grass_gis.7.1.svn'], 'G_fully_qualified_name'):
    G_fully_qualified_name = _libs['grass_gis.7.1.svn'].G_fully_qualified_name
    G_fully_qualified_name.restype = ReturnString
    G_fully_qualified_name.argtypes = [String, String]

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 462
if hasattr(_libs['grass_gis.7.1.svn'], 'G_unqualified_name'):
    G_unqualified_name = _libs['grass_gis.7.1.svn'].G_unqualified_name
    G_unqualified_name.restype = c_int
    G_unqualified_name.argtypes = [String, String, String, String]

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 465
if hasattr(_libs['grass_gis.7.1.svn'], 'G_open_new'):
    G_open_new = _libs['grass_gis.7.1.svn'].G_open_new
    G_open_new.restype = c_int
    G_open_new.argtypes = [String, String]

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 466
if hasattr(_libs['grass_gis.7.1.svn'], 'G_open_old'):
    G_open_old = _libs['grass_gis.7.1.svn'].G_open_old
    G_open_old.restype = c_int
    G_open_old.argtypes = [String, String, String]

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 467
if hasattr(_libs['grass_gis.7.1.svn'], 'G_open_update'):
    G_open_update = _libs['grass_gis.7.1.svn'].G_open_update
    G_open_update.restype = c_int
    G_open_update.argtypes = [String, String]

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 468
if hasattr(_libs['grass_gis.7.1.svn'], 'G_fopen_new'):
    G_fopen_new = _libs['grass_gis.7.1.svn'].G_fopen_new
    G_fopen_new.restype = POINTER(FILE)
    G_fopen_new.argtypes = [String, String]

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 469
if hasattr(_libs['grass_gis.7.1.svn'], 'G_fopen_old'):
    G_fopen_old = _libs['grass_gis.7.1.svn'].G_fopen_old
    G_fopen_old.restype = POINTER(FILE)
    G_fopen_old.argtypes = [String, String, String]

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 470
if hasattr(_libs['grass_gis.7.1.svn'], 'G_fopen_append'):
    G_fopen_append = _libs['grass_gis.7.1.svn'].G_fopen_append
    G_fopen_append.restype = POINTER(FILE)
    G_fopen_append.argtypes = [String, String]

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 471
if hasattr(_libs['grass_gis.7.1.svn'], 'G_fopen_modify'):
    G_fopen_modify = _libs['grass_gis.7.1.svn'].G_fopen_modify
    G_fopen_modify.restype = POINTER(FILE)
    G_fopen_modify.argtypes = [String, String]

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 474
if hasattr(_libs['grass_gis.7.1.svn'], 'G_open_new_misc'):
    G_open_new_misc = _libs['grass_gis.7.1.svn'].G_open_new_misc
    G_open_new_misc.restype = c_int
    G_open_new_misc.argtypes = [String, String, String]

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 475
if hasattr(_libs['grass_gis.7.1.svn'], 'G_open_old_misc'):
    G_open_old_misc = _libs['grass_gis.7.1.svn'].G_open_old_misc
    G_open_old_misc.restype = c_int
    G_open_old_misc.argtypes = [String, String, String, String]

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 476
if hasattr(_libs['grass_gis.7.1.svn'], 'G_open_update_misc'):
    G_open_update_misc = _libs['grass_gis.7.1.svn'].G_open_update_misc
    G_open_update_misc.restype = c_int
    G_open_update_misc.argtypes = [String, String, String]

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 477
if hasattr(_libs['grass_gis.7.1.svn'], 'G_fopen_new_misc'):
    G_fopen_new_misc = _libs['grass_gis.7.1.svn'].G_fopen_new_misc
    G_fopen_new_misc.restype = POINTER(FILE)
    G_fopen_new_misc.argtypes = [String, String, String]

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 478
if hasattr(_libs['grass_gis.7.1.svn'], 'G_fopen_old_misc'):
    G_fopen_old_misc = _libs['grass_gis.7.1.svn'].G_fopen_old_misc
    G_fopen_old_misc.restype = POINTER(FILE)
    G_fopen_old_misc.argtypes = [String, String, String, String]

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 480
if hasattr(_libs['grass_gis.7.1.svn'], 'G_fopen_append_misc'):
    G_fopen_append_misc = _libs['grass_gis.7.1.svn'].G_fopen_append_misc
    G_fopen_append_misc.restype = POINTER(FILE)
    G_fopen_append_misc.argtypes = [String, String, String]

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 481
if hasattr(_libs['grass_gis.7.1.svn'], 'G_fopen_modify_misc'):
    G_fopen_modify_misc = _libs['grass_gis.7.1.svn'].G_fopen_modify_misc
    G_fopen_modify_misc.restype = POINTER(FILE)
    G_fopen_modify_misc.argtypes = [String, String, String]

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 484
if hasattr(_libs['grass_gis.7.1.svn'], 'G_check_overwrite'):
    G_check_overwrite = _libs['grass_gis.7.1.svn'].G_check_overwrite
    G_check_overwrite.restype = c_int
    G_check_overwrite.argtypes = [c_int, POINTER(POINTER(c_char))]

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 487
if hasattr(_libs['grass_gis.7.1.svn'], 'G_open_pager'):
    G_open_pager = _libs['grass_gis.7.1.svn'].G_open_pager
    G_open_pager.restype = POINTER(FILE)
    G_open_pager.argtypes = [POINTER(struct_Popen)]

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 488
if hasattr(_libs['grass_gis.7.1.svn'], 'G_close_pager'):
    G_close_pager = _libs['grass_gis.7.1.svn'].G_close_pager
    G_close_pager.restype = None
    G_close_pager.argtypes = [POINTER(struct_Popen)]

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 489
if hasattr(_libs['grass_gis.7.1.svn'], 'G_open_mail'):
    G_open_mail = _libs['grass_gis.7.1.svn'].G_open_mail
    G_open_mail.restype = POINTER(FILE)
    G_open_mail.argtypes = [POINTER(struct_Popen)]

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 490
if hasattr(_libs['grass_gis.7.1.svn'], 'G_close_mail'):
    G_close_mail = _libs['grass_gis.7.1.svn'].G_close_mail
    G_close_mail.restype = None
    G_close_mail.argtypes = [POINTER(struct_Popen)]

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 493
if hasattr(_libs['grass_gis.7.1.svn'], 'G_disable_interactive'):
    G_disable_interactive = _libs['grass_gis.7.1.svn'].G_disable_interactive
    G_disable_interactive.restype = None
    G_disable_interactive.argtypes = []

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 494
if hasattr(_libs['grass_gis.7.1.svn'], 'G_define_module'):
    G_define_module = _libs['grass_gis.7.1.svn'].G_define_module
    G_define_module.restype = POINTER(struct_GModule)
    G_define_module.argtypes = []

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 495
if hasattr(_libs['grass_gis.7.1.svn'], 'G_define_flag'):
    G_define_flag = _libs['grass_gis.7.1.svn'].G_define_flag
    G_define_flag.restype = POINTER(struct_Flag)
    G_define_flag.argtypes = []

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 496
if hasattr(_libs['grass_gis.7.1.svn'], 'G_define_option'):
    G_define_option = _libs['grass_gis.7.1.svn'].G_define_option
    G_define_option.restype = POINTER(struct_Option)
    G_define_option.argtypes = []

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 497
if hasattr(_libs['grass_gis.7.1.svn'], 'G_define_standard_option'):
    G_define_standard_option = _libs['grass_gis.7.1.svn'].G_define_standard_option
    G_define_standard_option.restype = POINTER(struct_Option)
    G_define_standard_option.argtypes = [c_int]

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 498
if hasattr(_libs['grass_gis.7.1.svn'], 'G_define_standard_flag'):
    G_define_standard_flag = _libs['grass_gis.7.1.svn'].G_define_standard_flag
    G_define_standard_flag.restype = POINTER(struct_Flag)
    G_define_standard_flag.argtypes = [c_int]

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 499
if hasattr(_libs['grass_gis.7.1.svn'], 'G_parser'):
    G_parser = _libs['grass_gis.7.1.svn'].G_parser
    G_parser.restype = c_int
    G_parser.argtypes = [c_int, POINTER(POINTER(c_char))]

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 500
if hasattr(_libs['grass_gis.7.1.svn'], 'G_usage'):
    G_usage = _libs['grass_gis.7.1.svn'].G_usage
    G_usage.restype = None
    G_usage.argtypes = []

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 501
if hasattr(_libs['grass_gis.7.1.svn'], 'G_recreate_command'):
    G_recreate_command = _libs['grass_gis.7.1.svn'].G_recreate_command
    G_recreate_command.restype = ReturnString
    G_recreate_command.argtypes = []

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 502
if hasattr(_libs['grass_gis.7.1.svn'], 'G_add_keyword'):
    G_add_keyword = _libs['grass_gis.7.1.svn'].G_add_keyword
    G_add_keyword.restype = None
    G_add_keyword.argtypes = [String]

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 503
if hasattr(_libs['grass_gis.7.1.svn'], 'G_set_keywords'):
    G_set_keywords = _libs['grass_gis.7.1.svn'].G_set_keywords
    G_set_keywords.restype = None
    G_set_keywords.argtypes = [String]

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 504
if hasattr(_libs['grass_gis.7.1.svn'], 'G_get_overwrite'):
    G_get_overwrite = _libs['grass_gis.7.1.svn'].G_get_overwrite
    G_get_overwrite.restype = c_int
    G_get_overwrite.argtypes = []

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 505
if hasattr(_libs['grass_gis.7.1.svn'], 'G_option_to_separator'):
    G_option_to_separator = _libs['grass_gis.7.1.svn'].G_option_to_separator
    G_option_to_separator.restype = ReturnString
    G_option_to_separator.argtypes = [POINTER(struct_Option)]

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 506
if hasattr(_libs['grass_gis.7.1.svn'], 'G_open_option_file'):
    G_open_option_file = _libs['grass_gis.7.1.svn'].G_open_option_file
    G_open_option_file.restype = POINTER(FILE)
    G_open_option_file.argtypes = [POINTER(struct_Option)]

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 507
if hasattr(_libs['grass_gis.7.1.svn'], 'G_close_option_file'):
    G_close_option_file = _libs['grass_gis.7.1.svn'].G_close_option_file
    G_close_option_file.restype = None
    G_close_option_file.argtypes = [POINTER(FILE)]

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 510
if hasattr(_libs['grass_gis.7.1.svn'], 'G_option_exclusive'):
    _func = _libs['grass_gis.7.1.svn'].G_option_exclusive
    _restype = None
    _argtypes = [POINTER(None)]
    G_option_exclusive = _variadic_function(_func,_restype,_argtypes)

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 511
if hasattr(_libs['grass_gis.7.1.svn'], 'G_option_required'):
    _func = _libs['grass_gis.7.1.svn'].G_option_required
    _restype = None
    _argtypes = [POINTER(None)]
    G_option_required = _variadic_function(_func,_restype,_argtypes)

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 512
if hasattr(_libs['grass_gis.7.1.svn'], 'G_option_requires'):
    _func = _libs['grass_gis.7.1.svn'].G_option_requires
    _restype = None
    _argtypes = [POINTER(None)]
    G_option_requires = _variadic_function(_func,_restype,_argtypes)

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 513
if hasattr(_libs['grass_gis.7.1.svn'], 'G_option_requires_all'):
    _func = _libs['grass_gis.7.1.svn'].G_option_requires_all
    _restype = None
    _argtypes = [POINTER(None)]
    G_option_requires_all = _variadic_function(_func,_restype,_argtypes)

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 514
if hasattr(_libs['grass_gis.7.1.svn'], 'G_option_collective'):
    _func = _libs['grass_gis.7.1.svn'].G_option_collective
    _restype = None
    _argtypes = [POINTER(None)]
    G_option_collective = _variadic_function(_func,_restype,_argtypes)

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 517
if hasattr(_libs['grass_gis.7.1.svn'], 'G_mkdir'):
    G_mkdir = _libs['grass_gis.7.1.svn'].G_mkdir
    G_mkdir.restype = c_int
    G_mkdir.argtypes = [String]

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 518
if hasattr(_libs['grass_gis.7.1.svn'], 'G_is_dirsep'):
    G_is_dirsep = _libs['grass_gis.7.1.svn'].G_is_dirsep
    G_is_dirsep.restype = c_int
    G_is_dirsep.argtypes = [c_char]

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 519
if hasattr(_libs['grass_gis.7.1.svn'], 'G_is_absolute_path'):
    G_is_absolute_path = _libs['grass_gis.7.1.svn'].G_is_absolute_path
    G_is_absolute_path.restype = c_int
    G_is_absolute_path.argtypes = [String]

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 520
if hasattr(_libs['grass_gis.7.1.svn'], 'G_convert_dirseps_to_host'):
    G_convert_dirseps_to_host = _libs['grass_gis.7.1.svn'].G_convert_dirseps_to_host
    G_convert_dirseps_to_host.restype = ReturnString
    G_convert_dirseps_to_host.argtypes = [String]

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 521
if hasattr(_libs['grass_gis.7.1.svn'], 'G_convert_dirseps_from_host'):
    G_convert_dirseps_from_host = _libs['grass_gis.7.1.svn'].G_convert_dirseps_from_host
    G_convert_dirseps_from_host.restype = ReturnString
    G_convert_dirseps_from_host.argtypes = [String]

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 522
if hasattr(_libs['grass_gis.7.1.svn'], 'G_lstat'):
    G_lstat = _libs['grass_gis.7.1.svn'].G_lstat
    G_lstat.restype = c_int
    G_lstat.argtypes = [String, POINTER(struct_stat)]

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 523
if hasattr(_libs['grass_gis.7.1.svn'], 'G_stat'):
    G_stat = _libs['grass_gis.7.1.svn'].G_stat
    G_stat.restype = c_int
    G_stat.argtypes = [String, POINTER(struct_stat)]

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 524
if hasattr(_libs['grass_gis.7.1.svn'], 'G_owner'):
    G_owner = _libs['grass_gis.7.1.svn'].G_owner
    G_owner.restype = c_int
    G_owner.argtypes = [String]

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 527
if hasattr(_libs['grass_gis.7.1.svn'], 'G_percent'):
    G_percent = _libs['grass_gis.7.1.svn'].G_percent
    G_percent.restype = None
    G_percent.argtypes = [c_long, c_long, c_int]

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 528
if hasattr(_libs['grass_gis.7.1.svn'], 'G_percent_reset'):
    G_percent_reset = _libs['grass_gis.7.1.svn'].G_percent_reset
    G_percent_reset.restype = None
    G_percent_reset.argtypes = []

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 529
if hasattr(_libs['grass_gis.7.1.svn'], 'G_progress'):
    G_progress = _libs['grass_gis.7.1.svn'].G_progress
    G_progress.restype = None
    G_progress.argtypes = [c_long, c_int]

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 530
if hasattr(_libs['grass_gis.7.1.svn'], 'G_set_percent_routine'):
    G_set_percent_routine = _libs['grass_gis.7.1.svn'].G_set_percent_routine
    G_set_percent_routine.restype = None
    G_set_percent_routine.argtypes = [CFUNCTYPE(UNCHECKED(c_int), c_int)]

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 531
if hasattr(_libs['grass_gis.7.1.svn'], 'G_unset_percent_routine'):
    G_unset_percent_routine = _libs['grass_gis.7.1.svn'].G_unset_percent_routine
    G_unset_percent_routine.restype = None
    G_unset_percent_routine.argtypes = []

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 534
if hasattr(_libs['grass_gis.7.1.svn'], 'G_popen_clear'):
    G_popen_clear = _libs['grass_gis.7.1.svn'].G_popen_clear
    G_popen_clear.restype = None
    G_popen_clear.argtypes = [POINTER(struct_Popen)]

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 535
if hasattr(_libs['grass_gis.7.1.svn'], 'G_popen_write'):
    G_popen_write = _libs['grass_gis.7.1.svn'].G_popen_write
    G_popen_write.restype = POINTER(FILE)
    G_popen_write.argtypes = [POINTER(struct_Popen), String, POINTER(POINTER(c_char))]

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 536
if hasattr(_libs['grass_gis.7.1.svn'], 'G_popen_read'):
    G_popen_read = _libs['grass_gis.7.1.svn'].G_popen_read
    G_popen_read.restype = POINTER(FILE)
    G_popen_read.argtypes = [POINTER(struct_Popen), String, POINTER(POINTER(c_char))]

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 537
if hasattr(_libs['grass_gis.7.1.svn'], 'G_popen_close'):
    G_popen_close = _libs['grass_gis.7.1.svn'].G_popen_close
    G_popen_close.restype = None
    G_popen_close.argtypes = [POINTER(struct_Popen)]

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 540
if hasattr(_libs['grass_gis.7.1.svn'], 'G_setup_plot'):
    G_setup_plot = _libs['grass_gis.7.1.svn'].G_setup_plot
    G_setup_plot.restype = None
    G_setup_plot.argtypes = [c_double, c_double, c_double, c_double, CFUNCTYPE(UNCHECKED(c_int), c_int, c_int), CFUNCTYPE(UNCHECKED(c_int), c_int, c_int)]

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 542
if hasattr(_libs['grass_gis.7.1.svn'], 'G_setup_fill'):
    G_setup_fill = _libs['grass_gis.7.1.svn'].G_setup_fill
    G_setup_fill.restype = None
    G_setup_fill.argtypes = [c_int]

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 543
if hasattr(_libs['grass_gis.7.1.svn'], 'G_plot_where_xy'):
    G_plot_where_xy = _libs['grass_gis.7.1.svn'].G_plot_where_xy
    G_plot_where_xy.restype = None
    G_plot_where_xy.argtypes = [c_double, c_double, POINTER(c_int), POINTER(c_int)]

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 544
if hasattr(_libs['grass_gis.7.1.svn'], 'G_plot_where_en'):
    G_plot_where_en = _libs['grass_gis.7.1.svn'].G_plot_where_en
    G_plot_where_en.restype = None
    G_plot_where_en.argtypes = [c_int, c_int, POINTER(c_double), POINTER(c_double)]

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 545
if hasattr(_libs['grass_gis.7.1.svn'], 'G_plot_point'):
    G_plot_point = _libs['grass_gis.7.1.svn'].G_plot_point
    G_plot_point.restype = None
    G_plot_point.argtypes = [c_double, c_double]

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 546
if hasattr(_libs['grass_gis.7.1.svn'], 'G_plot_line'):
    G_plot_line = _libs['grass_gis.7.1.svn'].G_plot_line
    G_plot_line.restype = None
    G_plot_line.argtypes = [c_double, c_double, c_double, c_double]

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 547
if hasattr(_libs['grass_gis.7.1.svn'], 'G_plot_line2'):
    G_plot_line2 = _libs['grass_gis.7.1.svn'].G_plot_line2
    G_plot_line2.restype = None
    G_plot_line2.argtypes = [c_double, c_double, c_double, c_double]

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 548
if hasattr(_libs['grass_gis.7.1.svn'], 'G_plot_polygon'):
    G_plot_polygon = _libs['grass_gis.7.1.svn'].G_plot_polygon
    G_plot_polygon.restype = c_int
    G_plot_polygon.argtypes = [POINTER(c_double), POINTER(c_double), c_int]

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 549
if hasattr(_libs['grass_gis.7.1.svn'], 'G_plot_area'):
    G_plot_area = _libs['grass_gis.7.1.svn'].G_plot_area
    G_plot_area.restype = c_int
    G_plot_area.argtypes = [POINTER(POINTER(c_double)), POINTER(POINTER(c_double)), POINTER(c_int), c_int]

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 550
if hasattr(_libs['grass_gis.7.1.svn'], 'G_plot_fx'):
    G_plot_fx = _libs['grass_gis.7.1.svn'].G_plot_fx
    G_plot_fx.restype = None
    G_plot_fx.argtypes = [CFUNCTYPE(UNCHECKED(c_double), c_double), c_double, c_double]

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 553
if hasattr(_libs['grass_gis.7.1.svn'], 'G_pole_in_polygon'):
    G_pole_in_polygon = _libs['grass_gis.7.1.svn'].G_pole_in_polygon
    G_pole_in_polygon.restype = c_int
    G_pole_in_polygon.argtypes = [POINTER(c_double), POINTER(c_double), c_int]

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 556
if hasattr(_libs['grass_gis.7.1.svn'], 'G_program_name'):
    G_program_name = _libs['grass_gis.7.1.svn'].G_program_name
    G_program_name.restype = ReturnString
    G_program_name.argtypes = []

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 557
if hasattr(_libs['grass_gis.7.1.svn'], 'G_set_program_name'):
    G_set_program_name = _libs['grass_gis.7.1.svn'].G_set_program_name
    G_set_program_name.restype = None
    G_set_program_name.argtypes = [String]

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 560
if hasattr(_libs['grass_gis.7.1.svn'], 'G_projection'):
    G_projection = _libs['grass_gis.7.1.svn'].G_projection
    G_projection.restype = c_int
    G_projection.argtypes = []

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 563
if hasattr(_libs['grass_gis.7.1.svn'], 'G__projection_units'):
    G__projection_units = _libs['grass_gis.7.1.svn'].G__projection_units
    G__projection_units.restype = c_int
    G__projection_units.argtypes = [c_int]

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 564
if hasattr(_libs['grass_gis.7.1.svn'], 'G__projection_name'):
    G__projection_name = _libs['grass_gis.7.1.svn'].G__projection_name
    G__projection_name.restype = ReturnString
    G__projection_name.argtypes = [c_int]

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 567
if hasattr(_libs['grass_gis.7.1.svn'], 'G_database_unit_name'):
    G_database_unit_name = _libs['grass_gis.7.1.svn'].G_database_unit_name
    G_database_unit_name.restype = ReturnString
    G_database_unit_name.argtypes = [c_int]

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 568
if hasattr(_libs['grass_gis.7.1.svn'], 'G_database_projection_name'):
    G_database_projection_name = _libs['grass_gis.7.1.svn'].G_database_projection_name
    G_database_projection_name.restype = ReturnString
    G_database_projection_name.argtypes = []

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 569
if hasattr(_libs['grass_gis.7.1.svn'], 'G_database_datum_name'):
    G_database_datum_name = _libs['grass_gis.7.1.svn'].G_database_datum_name
    G_database_datum_name.restype = ReturnString
    G_database_datum_name.argtypes = []

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 570
if hasattr(_libs['grass_gis.7.1.svn'], 'G_database_ellipse_name'):
    G_database_ellipse_name = _libs['grass_gis.7.1.svn'].G_database_ellipse_name
    G_database_ellipse_name.restype = ReturnString
    G_database_ellipse_name.argtypes = []

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 571
if hasattr(_libs['grass_gis.7.1.svn'], 'G_database_units_to_meters_factor'):
    G_database_units_to_meters_factor = _libs['grass_gis.7.1.svn'].G_database_units_to_meters_factor
    G_database_units_to_meters_factor.restype = c_double
    G_database_units_to_meters_factor.argtypes = []

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 572
if hasattr(_libs['grass_gis.7.1.svn'], 'G_database_epsg_code'):
    G_database_epsg_code = _libs['grass_gis.7.1.svn'].G_database_epsg_code
    G_database_epsg_code.restype = ReturnString
    G_database_epsg_code.argtypes = []

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 575
if hasattr(_libs['grass_gis.7.1.svn'], 'G_put_window'):
    G_put_window = _libs['grass_gis.7.1.svn'].G_put_window
    G_put_window.restype = c_int
    G_put_window.argtypes = [POINTER(struct_Cell_head)]

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 576
if hasattr(_libs['grass_gis.7.1.svn'], 'G__put_window'):
    G__put_window = _libs['grass_gis.7.1.svn'].G__put_window
    G__put_window.restype = c_int
    G__put_window.argtypes = [POINTER(struct_Cell_head), String, String]

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 579
if hasattr(_libs['grass_gis.7.1.svn'], 'G_putenv'):
    G_putenv = _libs['grass_gis.7.1.svn'].G_putenv
    G_putenv.restype = None
    G_putenv.argtypes = [String, String]

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 582
if hasattr(_libs['grass_gis.7.1.svn'], 'G_meridional_radius_of_curvature'):
    G_meridional_radius_of_curvature = _libs['grass_gis.7.1.svn'].G_meridional_radius_of_curvature
    G_meridional_radius_of_curvature.restype = c_double
    G_meridional_radius_of_curvature.argtypes = [c_double, c_double, c_double]

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 583
if hasattr(_libs['grass_gis.7.1.svn'], 'G_transverse_radius_of_curvature'):
    G_transverse_radius_of_curvature = _libs['grass_gis.7.1.svn'].G_transverse_radius_of_curvature
    G_transverse_radius_of_curvature.restype = c_double
    G_transverse_radius_of_curvature.argtypes = [c_double, c_double, c_double]

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 584
if hasattr(_libs['grass_gis.7.1.svn'], 'G_radius_of_conformal_tangent_sphere'):
    G_radius_of_conformal_tangent_sphere = _libs['grass_gis.7.1.svn'].G_radius_of_conformal_tangent_sphere
    G_radius_of_conformal_tangent_sphere.restype = c_double
    G_radius_of_conformal_tangent_sphere.argtypes = [c_double, c_double, c_double]

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 587
if hasattr(_libs['grass_gis.7.1.svn'], 'G__read_Cell_head'):
    G__read_Cell_head = _libs['grass_gis.7.1.svn'].G__read_Cell_head
    G__read_Cell_head.restype = None
    G__read_Cell_head.argtypes = [POINTER(FILE), POINTER(struct_Cell_head), c_int]

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 588
if hasattr(_libs['grass_gis.7.1.svn'], 'G__read_Cell_head_array'):
    G__read_Cell_head_array = _libs['grass_gis.7.1.svn'].G__read_Cell_head_array
    G__read_Cell_head_array.restype = None
    G__read_Cell_head_array.argtypes = [POINTER(POINTER(c_char)), POINTER(struct_Cell_head), c_int]

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 591
if hasattr(_libs['grass_gis.7.1.svn'], 'G_remove'):
    G_remove = _libs['grass_gis.7.1.svn'].G_remove
    G_remove.restype = c_int
    G_remove.argtypes = [String, String]

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 592
if hasattr(_libs['grass_gis.7.1.svn'], 'G_remove_misc'):
    G_remove_misc = _libs['grass_gis.7.1.svn'].G_remove_misc
    G_remove_misc.restype = c_int
    G_remove_misc.argtypes = [String, String, String]

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 595
if hasattr(_libs['grass_gis.7.1.svn'], 'G_rename_file'):
    G_rename_file = _libs['grass_gis.7.1.svn'].G_rename_file
    G_rename_file.restype = c_int
    G_rename_file.argtypes = [String, String]

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 596
if hasattr(_libs['grass_gis.7.1.svn'], 'G_rename'):
    G_rename = _libs['grass_gis.7.1.svn'].G_rename
    G_rename.restype = c_int
    G_rename.argtypes = [String, String, String]

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 599
if hasattr(_libs['grass_gis.7.1.svn'], 'G_begin_rhumbline_equation'):
    G_begin_rhumbline_equation = _libs['grass_gis.7.1.svn'].G_begin_rhumbline_equation
    G_begin_rhumbline_equation.restype = c_int
    G_begin_rhumbline_equation.argtypes = [c_double, c_double, c_double, c_double]

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 600
if hasattr(_libs['grass_gis.7.1.svn'], 'G_rhumbline_lat_from_lon'):
    G_rhumbline_lat_from_lon = _libs['grass_gis.7.1.svn'].G_rhumbline_lat_from_lon
    G_rhumbline_lat_from_lon.restype = c_double
    G_rhumbline_lat_from_lon.argtypes = [c_double]

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 603
if hasattr(_libs['grass_gis.7.1.svn'], 'G_rotate_around_point'):
    G_rotate_around_point = _libs['grass_gis.7.1.svn'].G_rotate_around_point
    G_rotate_around_point.restype = None
    G_rotate_around_point.argtypes = [c_double, c_double, POINTER(c_double), POINTER(c_double), c_double]

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 604
if hasattr(_libs['grass_gis.7.1.svn'], 'G_rotate_around_point_int'):
    G_rotate_around_point_int = _libs['grass_gis.7.1.svn'].G_rotate_around_point_int
    G_rotate_around_point_int.restype = None
    G_rotate_around_point_int.argtypes = [c_int, c_int, POINTER(c_int), POINTER(c_int), c_double]

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 607
if hasattr(_libs['grass_gis.7.1.svn'], 'G_ftell'):
    G_ftell = _libs['grass_gis.7.1.svn'].G_ftell
    G_ftell.restype = off_t
    G_ftell.argtypes = [POINTER(FILE)]

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 608
if hasattr(_libs['grass_gis.7.1.svn'], 'G_fseek'):
    G_fseek = _libs['grass_gis.7.1.svn'].G_fseek
    G_fseek.restype = None
    G_fseek.argtypes = [POINTER(FILE), off_t, c_int]

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 611
if hasattr(_libs['grass_gis.7.1.svn'], 'G_get_set_window'):
    G_get_set_window = _libs['grass_gis.7.1.svn'].G_get_set_window
    G_get_set_window.restype = None
    G_get_set_window.argtypes = [POINTER(struct_Cell_head)]

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 612
if hasattr(_libs['grass_gis.7.1.svn'], 'G_set_window'):
    G_set_window = _libs['grass_gis.7.1.svn'].G_set_window
    G_set_window.restype = None
    G_set_window.argtypes = [POINTER(struct_Cell_head)]

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 613
if hasattr(_libs['grass_gis.7.1.svn'], 'G_unset_window'):
    G_unset_window = _libs['grass_gis.7.1.svn'].G_unset_window
    G_unset_window.restype = None
    G_unset_window.argtypes = []

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 616
if hasattr(_libs['grass_gis.7.1.svn'], 'G_shortest_way'):
    G_shortest_way = _libs['grass_gis.7.1.svn'].G_shortest_way
    G_shortest_way.restype = None
    G_shortest_way.argtypes = [POINTER(c_double), POINTER(c_double)]

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 619
if hasattr(_libs['grass_gis.7.1.svn'], 'G_sleep'):
    G_sleep = _libs['grass_gis.7.1.svn'].G_sleep
    G_sleep.restype = None
    G_sleep.argtypes = [c_uint]

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 622
if hasattr(_libs['grass_gis.7.1.svn'], 'G_snprintf'):
    _func = _libs['grass_gis.7.1.svn'].G_snprintf
    _restype = c_int
    _argtypes = [String, c_size_t, String]
    G_snprintf = _variadic_function(_func,_restype,_argtypes)

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 626
if hasattr(_libs['grass_gis.7.1.svn'], 'G_strcasecmp'):
    G_strcasecmp = _libs['grass_gis.7.1.svn'].G_strcasecmp
    G_strcasecmp.restype = c_int
    G_strcasecmp.argtypes = [String, String]

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 627
if hasattr(_libs['grass_gis.7.1.svn'], 'G_strncasecmp'):
    G_strncasecmp = _libs['grass_gis.7.1.svn'].G_strncasecmp
    G_strncasecmp.restype = c_int
    G_strncasecmp.argtypes = [String, String, c_int]

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 628
if hasattr(_libs['grass_gis.7.1.svn'], 'G_store'):
    G_store = _libs['grass_gis.7.1.svn'].G_store
    G_store.restype = ReturnString
    G_store.argtypes = [String]

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 629
if hasattr(_libs['grass_gis.7.1.svn'], 'G_store_upper'):
    G_store_upper = _libs['grass_gis.7.1.svn'].G_store_upper
    G_store_upper.restype = ReturnString
    G_store_upper.argtypes = [String]

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 630
if hasattr(_libs['grass_gis.7.1.svn'], 'G_store_lower'):
    G_store_lower = _libs['grass_gis.7.1.svn'].G_store_lower
    G_store_lower.restype = ReturnString
    G_store_lower.argtypes = [String]

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 631
if hasattr(_libs['grass_gis.7.1.svn'], 'G_strchg'):
    G_strchg = _libs['grass_gis.7.1.svn'].G_strchg
    G_strchg.restype = ReturnString
    G_strchg.argtypes = [String, c_char, c_char]

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 632
if hasattr(_libs['grass_gis.7.1.svn'], 'G_str_replace'):
    G_str_replace = _libs['grass_gis.7.1.svn'].G_str_replace
    G_str_replace.restype = ReturnString
    G_str_replace.argtypes = [String, String, String]

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 633
if hasattr(_libs['grass_gis.7.1.svn'], 'G_strip'):
    G_strip = _libs['grass_gis.7.1.svn'].G_strip
    G_strip.restype = None
    G_strip.argtypes = [String]

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 634
if hasattr(_libs['grass_gis.7.1.svn'], 'G_chop'):
    G_chop = _libs['grass_gis.7.1.svn'].G_chop
    G_chop.restype = ReturnString
    G_chop.argtypes = [String]

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 635
if hasattr(_libs['grass_gis.7.1.svn'], 'G_str_to_upper'):
    G_str_to_upper = _libs['grass_gis.7.1.svn'].G_str_to_upper
    G_str_to_upper.restype = None
    G_str_to_upper.argtypes = [String]

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 636
if hasattr(_libs['grass_gis.7.1.svn'], 'G_str_to_lower'):
    G_str_to_lower = _libs['grass_gis.7.1.svn'].G_str_to_lower
    G_str_to_lower.restype = None
    G_str_to_lower.argtypes = [String]

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 637
if hasattr(_libs['grass_gis.7.1.svn'], 'G_str_to_sql'):
    G_str_to_sql = _libs['grass_gis.7.1.svn'].G_str_to_sql
    G_str_to_sql.restype = c_int
    G_str_to_sql.argtypes = [String]

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 638
if hasattr(_libs['grass_gis.7.1.svn'], 'G_squeeze'):
    G_squeeze = _libs['grass_gis.7.1.svn'].G_squeeze
    G_squeeze.restype = None
    G_squeeze.argtypes = [String]

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 639
if hasattr(_libs['grass_gis.7.1.svn'], 'G_strcasestr'):
    G_strcasestr = _libs['grass_gis.7.1.svn'].G_strcasestr
    G_strcasestr.restype = ReturnString
    G_strcasestr.argtypes = [String, String]

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 642
if hasattr(_libs['grass_gis.7.1.svn'], 'G_init_tempfile'):
    G_init_tempfile = _libs['grass_gis.7.1.svn'].G_init_tempfile
    G_init_tempfile.restype = None
    G_init_tempfile.argtypes = []

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 643
if hasattr(_libs['grass_gis.7.1.svn'], 'G_tempfile'):
    G_tempfile = _libs['grass_gis.7.1.svn'].G_tempfile
    G_tempfile.restype = ReturnString
    G_tempfile.argtypes = []

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 644
if hasattr(_libs['grass_gis.7.1.svn'], 'G__tempfile'):
    G__tempfile = _libs['grass_gis.7.1.svn'].G__tempfile
    G__tempfile.restype = ReturnString
    G__tempfile.argtypes = [c_int]

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 645
if hasattr(_libs['grass_gis.7.1.svn'], 'G__temp_element'):
    G__temp_element = _libs['grass_gis.7.1.svn'].G__temp_element
    G__temp_element.restype = None
    G__temp_element.argtypes = [String]

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 648
if hasattr(_libs['grass_gis.7.1.svn'], 'G_mktemp'):
    G_mktemp = _libs['grass_gis.7.1.svn'].G_mktemp
    G_mktemp.restype = ReturnString
    G_mktemp.argtypes = [String]

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 649
if hasattr(_libs['grass_gis.7.1.svn'], 'G_mkstemp'):
    G_mkstemp = _libs['grass_gis.7.1.svn'].G_mkstemp
    G_mkstemp.restype = c_int
    G_mkstemp.argtypes = [String, c_int, c_int]

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 650
if hasattr(_libs['grass_gis.7.1.svn'], 'G_mkstemp_fp'):
    G_mkstemp_fp = _libs['grass_gis.7.1.svn'].G_mkstemp_fp
    G_mkstemp_fp.restype = POINTER(FILE)
    G_mkstemp_fp.argtypes = [String, c_int, c_int]

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 653
if hasattr(_libs['grass_gis.7.1.svn'], 'G_init_timestamp'):
    G_init_timestamp = _libs['grass_gis.7.1.svn'].G_init_timestamp
    G_init_timestamp.restype = None
    G_init_timestamp.argtypes = [POINTER(struct_TimeStamp)]

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 654
if hasattr(_libs['grass_gis.7.1.svn'], 'G_set_timestamp'):
    G_set_timestamp = _libs['grass_gis.7.1.svn'].G_set_timestamp
    G_set_timestamp.restype = None
    G_set_timestamp.argtypes = [POINTER(struct_TimeStamp), POINTER(struct_DateTime)]

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 655
if hasattr(_libs['grass_gis.7.1.svn'], 'G_set_timestamp_range'):
    G_set_timestamp_range = _libs['grass_gis.7.1.svn'].G_set_timestamp_range
    G_set_timestamp_range.restype = None
    G_set_timestamp_range.argtypes = [POINTER(struct_TimeStamp), POINTER(struct_DateTime), POINTER(struct_DateTime)]

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 657
if hasattr(_libs['grass_gis.7.1.svn'], 'G__read_timestamp'):
    G__read_timestamp = _libs['grass_gis.7.1.svn'].G__read_timestamp
    G__read_timestamp.restype = c_int
    G__read_timestamp.argtypes = [POINTER(FILE), POINTER(struct_TimeStamp)]

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 658
if hasattr(_libs['grass_gis.7.1.svn'], 'G__write_timestamp'):
    G__write_timestamp = _libs['grass_gis.7.1.svn'].G__write_timestamp
    G__write_timestamp.restype = c_int
    G__write_timestamp.argtypes = [POINTER(FILE), POINTER(struct_TimeStamp)]

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 659
if hasattr(_libs['grass_gis.7.1.svn'], 'G_get_timestamps'):
    G_get_timestamps = _libs['grass_gis.7.1.svn'].G_get_timestamps
    G_get_timestamps.restype = None
    G_get_timestamps.argtypes = [POINTER(struct_TimeStamp), POINTER(struct_DateTime), POINTER(struct_DateTime), POINTER(c_int)]

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 660
if hasattr(_libs['grass_gis.7.1.svn'], 'G_format_timestamp'):
    G_format_timestamp = _libs['grass_gis.7.1.svn'].G_format_timestamp
    G_format_timestamp.restype = c_int
    G_format_timestamp.argtypes = [POINTER(struct_TimeStamp), String]

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 661
if hasattr(_libs['grass_gis.7.1.svn'], 'G_scan_timestamp'):
    G_scan_timestamp = _libs['grass_gis.7.1.svn'].G_scan_timestamp
    G_scan_timestamp.restype = c_int
    G_scan_timestamp.argtypes = [POINTER(struct_TimeStamp), String]

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 662
if hasattr(_libs['grass_gis.7.1.svn'], 'G_has_raster_timestamp'):
    G_has_raster_timestamp = _libs['grass_gis.7.1.svn'].G_has_raster_timestamp
    G_has_raster_timestamp.restype = c_int
    G_has_raster_timestamp.argtypes = [String, String]

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 663
if hasattr(_libs['grass_gis.7.1.svn'], 'G_read_raster_timestamp'):
    G_read_raster_timestamp = _libs['grass_gis.7.1.svn'].G_read_raster_timestamp
    G_read_raster_timestamp.restype = c_int
    G_read_raster_timestamp.argtypes = [String, String, POINTER(struct_TimeStamp)]

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 664
if hasattr(_libs['grass_gis.7.1.svn'], 'G_write_raster_timestamp'):
    G_write_raster_timestamp = _libs['grass_gis.7.1.svn'].G_write_raster_timestamp
    G_write_raster_timestamp.restype = c_int
    G_write_raster_timestamp.argtypes = [String, POINTER(struct_TimeStamp)]

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 665
if hasattr(_libs['grass_gis.7.1.svn'], 'G_remove_raster_timestamp'):
    G_remove_raster_timestamp = _libs['grass_gis.7.1.svn'].G_remove_raster_timestamp
    G_remove_raster_timestamp.restype = c_int
    G_remove_raster_timestamp.argtypes = [String]

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 666
if hasattr(_libs['grass_gis.7.1.svn'], 'G_has_vector_timestamp'):
    G_has_vector_timestamp = _libs['grass_gis.7.1.svn'].G_has_vector_timestamp
    G_has_vector_timestamp.restype = c_int
    G_has_vector_timestamp.argtypes = [String, String, String]

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 667
if hasattr(_libs['grass_gis.7.1.svn'], 'G_read_vector_timestamp'):
    G_read_vector_timestamp = _libs['grass_gis.7.1.svn'].G_read_vector_timestamp
    G_read_vector_timestamp.restype = c_int
    G_read_vector_timestamp.argtypes = [String, String, String, POINTER(struct_TimeStamp)]

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 668
if hasattr(_libs['grass_gis.7.1.svn'], 'G_write_vector_timestamp'):
    G_write_vector_timestamp = _libs['grass_gis.7.1.svn'].G_write_vector_timestamp
    G_write_vector_timestamp.restype = c_int
    G_write_vector_timestamp.argtypes = [String, String, POINTER(struct_TimeStamp)]

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 669
if hasattr(_libs['grass_gis.7.1.svn'], 'G_remove_vector_timestamp'):
    G_remove_vector_timestamp = _libs['grass_gis.7.1.svn'].G_remove_vector_timestamp
    G_remove_vector_timestamp.restype = c_int
    G_remove_vector_timestamp.argtypes = [String, String]

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 670
if hasattr(_libs['grass_gis.7.1.svn'], 'G_has_raster3d_timestamp'):
    G_has_raster3d_timestamp = _libs['grass_gis.7.1.svn'].G_has_raster3d_timestamp
    G_has_raster3d_timestamp.restype = c_int
    G_has_raster3d_timestamp.argtypes = [String, String]

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 671
if hasattr(_libs['grass_gis.7.1.svn'], 'G_read_raster3d_timestamp'):
    G_read_raster3d_timestamp = _libs['grass_gis.7.1.svn'].G_read_raster3d_timestamp
    G_read_raster3d_timestamp.restype = c_int
    G_read_raster3d_timestamp.argtypes = [String, String, POINTER(struct_TimeStamp)]

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 672
if hasattr(_libs['grass_gis.7.1.svn'], 'G_remove_raster3d_timestamp'):
    G_remove_raster3d_timestamp = _libs['grass_gis.7.1.svn'].G_remove_raster3d_timestamp
    G_remove_raster3d_timestamp.restype = c_int
    G_remove_raster3d_timestamp.argtypes = [String]

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 673
if hasattr(_libs['grass_gis.7.1.svn'], 'G_write_raster3d_timestamp'):
    G_write_raster3d_timestamp = _libs['grass_gis.7.1.svn'].G_write_raster3d_timestamp
    G_write_raster3d_timestamp.restype = c_int
    G_write_raster3d_timestamp.argtypes = [String, POINTER(struct_TimeStamp)]

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 676
if hasattr(_libs['grass_gis.7.1.svn'], 'G_tokenize'):
    G_tokenize = _libs['grass_gis.7.1.svn'].G_tokenize
    G_tokenize.restype = POINTER(POINTER(c_char))
    G_tokenize.argtypes = [String, String]

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 677
if hasattr(_libs['grass_gis.7.1.svn'], 'G_tokenize2'):
    G_tokenize2 = _libs['grass_gis.7.1.svn'].G_tokenize2
    G_tokenize2.restype = POINTER(POINTER(c_char))
    G_tokenize2.argtypes = [String, String, String]

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 678
if hasattr(_libs['grass_gis.7.1.svn'], 'G_number_of_tokens'):
    G_number_of_tokens = _libs['grass_gis.7.1.svn'].G_number_of_tokens
    G_number_of_tokens.restype = c_int
    G_number_of_tokens.argtypes = [POINTER(POINTER(c_char))]

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 679
if hasattr(_libs['grass_gis.7.1.svn'], 'G_free_tokens'):
    G_free_tokens = _libs['grass_gis.7.1.svn'].G_free_tokens
    G_free_tokens.restype = None
    G_free_tokens.argtypes = [POINTER(POINTER(c_char))]

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 682
if hasattr(_libs['grass_gis.7.1.svn'], 'G_trim_decimal'):
    G_trim_decimal = _libs['grass_gis.7.1.svn'].G_trim_decimal
    G_trim_decimal.restype = None
    G_trim_decimal.argtypes = [String]

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 685
if hasattr(_libs['grass_gis.7.1.svn'], 'G_meters_to_units_factor'):
    G_meters_to_units_factor = _libs['grass_gis.7.1.svn'].G_meters_to_units_factor
    G_meters_to_units_factor.restype = c_double
    G_meters_to_units_factor.argtypes = [c_int]

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 686
if hasattr(_libs['grass_gis.7.1.svn'], 'G_meters_to_units_factor_sq'):
    G_meters_to_units_factor_sq = _libs['grass_gis.7.1.svn'].G_meters_to_units_factor_sq
    G_meters_to_units_factor_sq.restype = c_double
    G_meters_to_units_factor_sq.argtypes = [c_int]

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 687
if hasattr(_libs['grass_gis.7.1.svn'], 'G_get_units_name'):
    G_get_units_name = _libs['grass_gis.7.1.svn'].G_get_units_name
    G_get_units_name.restype = ReturnString
    G_get_units_name.argtypes = [c_int, c_int, c_int]

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 688
if hasattr(_libs['grass_gis.7.1.svn'], 'G_units'):
    G_units = _libs['grass_gis.7.1.svn'].G_units
    G_units.restype = c_int
    G_units.argtypes = [String]

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 689
if hasattr(_libs['grass_gis.7.1.svn'], 'G_is_units_type_spatial'):
    G_is_units_type_spatial = _libs['grass_gis.7.1.svn'].G_is_units_type_spatial
    G_is_units_type_spatial.restype = c_int
    G_is_units_type_spatial.argtypes = [c_int]

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 690
if hasattr(_libs['grass_gis.7.1.svn'], 'G_is_units_type_temporal'):
    G_is_units_type_temporal = _libs['grass_gis.7.1.svn'].G_is_units_type_temporal
    G_is_units_type_temporal.restype = c_int
    G_is_units_type_temporal.argtypes = [c_int]

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 694
if hasattr(_libs['grass_gis.7.1.svn'], 'G_rc_path'):
    G_rc_path = _libs['grass_gis.7.1.svn'].G_rc_path
    G_rc_path.restype = ReturnString
    G_rc_path.argtypes = [String, String]

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 698
if hasattr(_libs['grass_gis.7.1.svn'], 'G_verbose'):
    G_verbose = _libs['grass_gis.7.1.svn'].G_verbose
    G_verbose.restype = c_int
    G_verbose.argtypes = []

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 699
if hasattr(_libs['grass_gis.7.1.svn'], 'G_verbose_min'):
    G_verbose_min = _libs['grass_gis.7.1.svn'].G_verbose_min
    G_verbose_min.restype = c_int
    G_verbose_min.argtypes = []

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 700
if hasattr(_libs['grass_gis.7.1.svn'], 'G_verbose_std'):
    G_verbose_std = _libs['grass_gis.7.1.svn'].G_verbose_std
    G_verbose_std.restype = c_int
    G_verbose_std.argtypes = []

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 701
if hasattr(_libs['grass_gis.7.1.svn'], 'G_verbose_max'):
    G_verbose_max = _libs['grass_gis.7.1.svn'].G_verbose_max
    G_verbose_max.restype = c_int
    G_verbose_max.argtypes = []

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 702
if hasattr(_libs['grass_gis.7.1.svn'], 'G_set_verbose'):
    G_set_verbose = _libs['grass_gis.7.1.svn'].G_set_verbose
    G_set_verbose.restype = c_int
    G_set_verbose.argtypes = [c_int]

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 705
if hasattr(_libs['grass_gis.7.1.svn'], 'G_3dview_warning'):
    G_3dview_warning = _libs['grass_gis.7.1.svn'].G_3dview_warning
    G_3dview_warning.restype = None
    G_3dview_warning.argtypes = [c_int]

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 706
if hasattr(_libs['grass_gis.7.1.svn'], 'G_get_3dview_defaults'):
    G_get_3dview_defaults = _libs['grass_gis.7.1.svn'].G_get_3dview_defaults
    G_get_3dview_defaults.restype = c_int
    G_get_3dview_defaults.argtypes = [POINTER(struct_G_3dview), POINTER(struct_Cell_head)]

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 707
if hasattr(_libs['grass_gis.7.1.svn'], 'G_put_3dview'):
    G_put_3dview = _libs['grass_gis.7.1.svn'].G_put_3dview
    G_put_3dview.restype = c_int
    G_put_3dview.argtypes = [String, String, POINTER(struct_G_3dview), POINTER(struct_Cell_head)]

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 709
if hasattr(_libs['grass_gis.7.1.svn'], 'G_get_3dview'):
    G_get_3dview = _libs['grass_gis.7.1.svn'].G_get_3dview
    G_get_3dview.restype = c_int
    G_get_3dview.argtypes = [String, String, POINTER(struct_G_3dview)]

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 712
if hasattr(_libs['grass_gis.7.1.svn'], 'G_whoami'):
    G_whoami = _libs['grass_gis.7.1.svn'].G_whoami
    G_whoami.restype = ReturnString
    G_whoami.argtypes = []

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 715
if hasattr(_libs['grass_gis.7.1.svn'], 'G_adjust_window_to_box'):
    G_adjust_window_to_box = _libs['grass_gis.7.1.svn'].G_adjust_window_to_box
    G_adjust_window_to_box.restype = None
    G_adjust_window_to_box.argtypes = [POINTER(struct_Cell_head), POINTER(struct_Cell_head), c_int, c_int]

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 719
if hasattr(_libs['grass_gis.7.1.svn'], 'G_format_northing'):
    G_format_northing = _libs['grass_gis.7.1.svn'].G_format_northing
    G_format_northing.restype = None
    G_format_northing.argtypes = [c_double, String, c_int]

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 720
if hasattr(_libs['grass_gis.7.1.svn'], 'G_format_easting'):
    G_format_easting = _libs['grass_gis.7.1.svn'].G_format_easting
    G_format_easting.restype = None
    G_format_easting.argtypes = [c_double, String, c_int]

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 721
if hasattr(_libs['grass_gis.7.1.svn'], 'G_format_resolution'):
    G_format_resolution = _libs['grass_gis.7.1.svn'].G_format_resolution
    G_format_resolution.restype = None
    G_format_resolution.argtypes = [c_double, String, c_int]

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 724
if hasattr(_libs['grass_gis.7.1.svn'], 'G_point_in_region'):
    G_point_in_region = _libs['grass_gis.7.1.svn'].G_point_in_region
    G_point_in_region.restype = c_int
    G_point_in_region.argtypes = [c_double, c_double]

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 725
if hasattr(_libs['grass_gis.7.1.svn'], 'G_point_in_window'):
    G_point_in_window = _libs['grass_gis.7.1.svn'].G_point_in_window
    G_point_in_window.restype = c_int
    G_point_in_window.argtypes = [c_double, c_double, POINTER(struct_Cell_head)]

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 728
if hasattr(_libs['grass_gis.7.1.svn'], 'G_limit_east'):
    G_limit_east = _libs['grass_gis.7.1.svn'].G_limit_east
    G_limit_east.restype = c_int
    G_limit_east.argtypes = [POINTER(c_double), c_int]

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 729
if hasattr(_libs['grass_gis.7.1.svn'], 'G_limit_west'):
    G_limit_west = _libs['grass_gis.7.1.svn'].G_limit_west
    G_limit_west.restype = c_int
    G_limit_west.argtypes = [POINTER(c_double), c_int]

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 730
if hasattr(_libs['grass_gis.7.1.svn'], 'G_limit_north'):
    G_limit_north = _libs['grass_gis.7.1.svn'].G_limit_north
    G_limit_north.restype = c_int
    G_limit_north.argtypes = [POINTER(c_double), c_int]

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 731
if hasattr(_libs['grass_gis.7.1.svn'], 'G_limit_south'):
    G_limit_south = _libs['grass_gis.7.1.svn'].G_limit_south
    G_limit_south.restype = c_int
    G_limit_south.argtypes = [POINTER(c_double), c_int]

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 734
if hasattr(_libs['grass_gis.7.1.svn'], 'G_window_overlap'):
    G_window_overlap = _libs['grass_gis.7.1.svn'].G_window_overlap
    G_window_overlap.restype = c_int
    G_window_overlap.argtypes = [POINTER(struct_Cell_head), c_double, c_double, c_double, c_double]

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 736
if hasattr(_libs['grass_gis.7.1.svn'], 'G_window_percentage_overlap'):
    G_window_percentage_overlap = _libs['grass_gis.7.1.svn'].G_window_percentage_overlap
    G_window_percentage_overlap.restype = c_double
    G_window_percentage_overlap.argtypes = [POINTER(struct_Cell_head), c_double, c_double, c_double, c_double]

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 740
if hasattr(_libs['grass_gis.7.1.svn'], 'G_scan_northing'):
    G_scan_northing = _libs['grass_gis.7.1.svn'].G_scan_northing
    G_scan_northing.restype = c_int
    G_scan_northing.argtypes = [String, POINTER(c_double), c_int]

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 741
if hasattr(_libs['grass_gis.7.1.svn'], 'G_scan_easting'):
    G_scan_easting = _libs['grass_gis.7.1.svn'].G_scan_easting
    G_scan_easting.restype = c_int
    G_scan_easting.argtypes = [String, POINTER(c_double), c_int]

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 742
if hasattr(_libs['grass_gis.7.1.svn'], 'G_scan_resolution'):
    G_scan_resolution = _libs['grass_gis.7.1.svn'].G_scan_resolution
    G_scan_resolution.restype = c_int
    G_scan_resolution.argtypes = [String, POINTER(c_double), c_int]

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 745
if hasattr(_libs['grass_gis.7.1.svn'], 'G_adjust_east_longitude'):
    G_adjust_east_longitude = _libs['grass_gis.7.1.svn'].G_adjust_east_longitude
    G_adjust_east_longitude.restype = c_double
    G_adjust_east_longitude.argtypes = [c_double, c_double]

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 746
if hasattr(_libs['grass_gis.7.1.svn'], 'G_adjust_easting'):
    G_adjust_easting = _libs['grass_gis.7.1.svn'].G_adjust_easting
    G_adjust_easting.restype = c_double
    G_adjust_easting.argtypes = [c_double, POINTER(struct_Cell_head)]

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 747
if hasattr(_libs['grass_gis.7.1.svn'], 'G__init_window'):
    G__init_window = _libs['grass_gis.7.1.svn'].G__init_window
    G__init_window.restype = None
    G__init_window.argtypes = []

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 750
if hasattr(_libs['grass_gis.7.1.svn'], 'G_begin_execute'):
    G_begin_execute = _libs['grass_gis.7.1.svn'].G_begin_execute
    G_begin_execute.restype = None
    G_begin_execute.argtypes = [CFUNCTYPE(UNCHECKED(None), POINTER(None)), POINTER(None), POINTER(POINTER(None)), c_int]

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 751
if hasattr(_libs['grass_gis.7.1.svn'], 'G_end_execute'):
    G_end_execute = _libs['grass_gis.7.1.svn'].G_end_execute
    G_end_execute.restype = None
    G_end_execute.argtypes = [POINTER(POINTER(None))]

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 752
if hasattr(_libs['grass_gis.7.1.svn'], 'G_init_workers'):
    G_init_workers = _libs['grass_gis.7.1.svn'].G_init_workers
    G_init_workers.restype = None
    G_init_workers.argtypes = []

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 753
if hasattr(_libs['grass_gis.7.1.svn'], 'G_finish_workers'):
    G_finish_workers = _libs['grass_gis.7.1.svn'].G_finish_workers
    G_finish_workers.restype = None
    G_finish_workers.argtypes = []

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 756
if hasattr(_libs['grass_gis.7.1.svn'], 'G__write_Cell_head'):
    G__write_Cell_head = _libs['grass_gis.7.1.svn'].G__write_Cell_head
    G__write_Cell_head.restype = None
    G__write_Cell_head.argtypes = [POINTER(FILE), POINTER(struct_Cell_head), c_int]

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 757
if hasattr(_libs['grass_gis.7.1.svn'], 'G__write_Cell_head3'):
    G__write_Cell_head3 = _libs['grass_gis.7.1.svn'].G__write_Cell_head3
    G__write_Cell_head3.restype = None
    G__write_Cell_head3.argtypes = [POINTER(FILE), POINTER(struct_Cell_head), c_int]

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 760
if hasattr(_libs['grass_gis.7.1.svn'], 'G_write_zeros'):
    G_write_zeros = _libs['grass_gis.7.1.svn'].G_write_zeros
    G_write_zeros.restype = None
    G_write_zeros.argtypes = [c_int, c_size_t]

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 763
if hasattr(_libs['grass_gis.7.1.svn'], 'G_xdr_get_int'):
    G_xdr_get_int = _libs['grass_gis.7.1.svn'].G_xdr_get_int
    G_xdr_get_int.restype = None
    G_xdr_get_int.argtypes = [POINTER(c_int), POINTER(None)]

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 764
if hasattr(_libs['grass_gis.7.1.svn'], 'G_xdr_put_int'):
    G_xdr_put_int = _libs['grass_gis.7.1.svn'].G_xdr_put_int
    G_xdr_put_int.restype = None
    G_xdr_put_int.argtypes = [POINTER(None), POINTER(c_int)]

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 765
if hasattr(_libs['grass_gis.7.1.svn'], 'G_xdr_get_float'):
    G_xdr_get_float = _libs['grass_gis.7.1.svn'].G_xdr_get_float
    G_xdr_get_float.restype = None
    G_xdr_get_float.argtypes = [POINTER(c_float), POINTER(None)]

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 766
if hasattr(_libs['grass_gis.7.1.svn'], 'G_xdr_put_float'):
    G_xdr_put_float = _libs['grass_gis.7.1.svn'].G_xdr_put_float
    G_xdr_put_float.restype = None
    G_xdr_put_float.argtypes = [POINTER(None), POINTER(c_float)]

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 767
if hasattr(_libs['grass_gis.7.1.svn'], 'G_xdr_get_double'):
    G_xdr_get_double = _libs['grass_gis.7.1.svn'].G_xdr_get_double
    G_xdr_get_double.restype = None
    G_xdr_get_double.argtypes = [POINTER(c_double), POINTER(None)]

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 768
if hasattr(_libs['grass_gis.7.1.svn'], 'G_xdr_put_double'):
    G_xdr_put_double = _libs['grass_gis.7.1.svn'].G_xdr_put_double
    G_xdr_put_double.restype = None
    G_xdr_put_double.argtypes = [POINTER(None), POINTER(c_double)]

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 771
if hasattr(_libs['grass_gis.7.1.svn'], 'G_zero'):
    G_zero = _libs['grass_gis.7.1.svn'].G_zero
    G_zero.restype = None
    G_zero.argtypes = [POINTER(None), c_int]

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 774
if hasattr(_libs['grass_gis.7.1.svn'], 'G_zone'):
    G_zone = _libs['grass_gis.7.1.svn'].G_zone
    G_zone.restype = c_int
    G_zone.argtypes = []

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/datetime.h: 7
try:
    DATETIME_YEAR = 101
except:
    pass

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/datetime.h: 7
try:
    DATETIME_MONTH = 102
except:
    pass

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/datetime.h: 7
try:
    DATETIME_DAY = 103
except:
    pass

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/datetime.h: 7
try:
    DATETIME_HOUR = 104
except:
    pass

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/datetime.h: 7
try:
    DATETIME_MINUTE = 105
except:
    pass

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/datetime.h: 7
try:
    DATETIME_SECOND = 106
except:
    pass

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/gis.h: 40
try:
    GIS_H_VERSION = '$Revision: 61095 $'
except:
    pass

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/gis.h: 40
try:
    GIS_H_DATE = '$Date: 2014-07-01 11:17:40 +0200 (Tue, 01 Jul 2014) $'
except:
    pass

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/gis.h: 41
def G_gisinit(pgm):
    return (G__gisinit (GIS_H_VERSION, pgm))

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/gis.h: 41
try:
    G_no_gisinit = (G__no_gisinit (GIS_H_VERSION))
except:
    pass

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/gis.h: 44
try:
    TRUE = 1
except:
    pass

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/gis.h: 47
try:
    FALSE = 0
except:
    pass

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/gis.h: 52
try:
    PRI_OFF_T = 'ld'
except:
    pass

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/gis.h: 54
try:
    NEWLINE = '\\n'
except:
    pass

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/gis.h: 58
try:
    U_UNDEFINED = (-1)
except:
    pass

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/gis.h: 58
try:
    U_UNKNOWN = 0
except:
    pass

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/gis.h: 58
try:
    U_ACRES = 1
except:
    pass

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/gis.h: 58
try:
    U_HECTARES = 2
except:
    pass

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/gis.h: 58
try:
    U_KILOMETERS = 3
except:
    pass

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/gis.h: 58
try:
    U_METERS = 4
except:
    pass

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/gis.h: 58
try:
    U_MILES = 5
except:
    pass

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/gis.h: 58
try:
    U_FEET = 6
except:
    pass

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/gis.h: 58
try:
    U_RADIANS = 7
except:
    pass

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/gis.h: 58
try:
    U_DEGREES = 8
except:
    pass

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/gis.h: 59
try:
    U_YEARS = DATETIME_YEAR
except:
    pass

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/gis.h: 59
try:
    U_MONTHS = DATETIME_MONTH
except:
    pass

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/gis.h: 59
try:
    U_DAYS = DATETIME_DAY
except:
    pass

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/gis.h: 59
try:
    U_HOURS = DATETIME_HOUR
except:
    pass

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/gis.h: 59
try:
    U_MINUTES = DATETIME_MINUTE
except:
    pass

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/gis.h: 59
try:
    U_SECONDS = DATETIME_SECOND
except:
    pass

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/gis.h: 61
try:
    PROJECTION_XY = 0
except:
    pass

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/gis.h: 62
try:
    PROJECTION_UTM = 1
except:
    pass

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/gis.h: 63
try:
    PROJECTION_SP = 2
except:
    pass

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/gis.h: 64
try:
    PROJECTION_LL = 3
except:
    pass

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/gis.h: 65
try:
    PROJECTION_OTHER = 99
except:
    pass

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/gis.h: 66
try:
    PROJECTION_FILE = 'PROJ_INFO'
except:
    pass

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/gis.h: 66
try:
    UNIT_FILE = 'PROJ_UNITS'
except:
    pass

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/gis.h: 66
try:
    EPSG_FILE = 'PROJ_EPSG'
except:
    pass

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/gis.h: 70
try:
    CONFIG_DIR = '.grass7'
except:
    pass

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/gis.h: 74
try:
    M_PI = 3.1415926535897931
except:
    pass

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/gis.h: 76
try:
    M_PI_2 = 1.5707963267948966
except:
    pass

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/gis.h: 78
try:
    M_PI_4 = 0.78539816339744828
except:
    pass

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/gis.h: 80
try:
    GRASS_EPSILON = 1.0000000000000001e-15
except:
    pass

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/gis.h: 82
try:
    G_VAR_GISRC = 0
except:
    pass

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/gis.h: 82
try:
    G_VAR_MAPSET = 1
except:
    pass

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/gis.h: 84
try:
    G_GISRC_MODE_FILE = 0
except:
    pass

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/gis.h: 84
try:
    G_GISRC_MODE_MEMORY = 1
except:
    pass

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/gis.h: 86
try:
    TYPE_INTEGER = 1
except:
    pass

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/gis.h: 86
try:
    TYPE_DOUBLE = 2
except:
    pass

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/gis.h: 86
try:
    TYPE_STRING = 3
except:
    pass

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/gis.h: 86
try:
    YES = 1
except:
    pass

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/gis.h: 86
try:
    NO = 0
except:
    pass

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/gis.h: 88
try:
    GNAME_MAX = 256
except:
    pass

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/gis.h: 88
try:
    GMAPSET_MAX = 256
except:
    pass

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/gis.h: 89
try:
    GPATH_MAX = 4096
except:
    pass

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/gis.h: 91
try:
    GBASENAME_SEP = '__'
except:
    pass

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/gis.h: 104
def deserialize_int32_le(buf):
    return (((((buf [0]) << 0) | ((buf [1]) << 8)) | ((buf [2]) << 16)) | ((buf [3]) << 24))

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/gis.h: 116
def deserialize_int32_be(buf):
    return (((((buf [0]) << 24) | ((buf [1]) << 16)) | ((buf [2]) << 8)) | ((buf [3]) << 0))

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/gis.h: 121
try:
    GRASS_DIRSEP = '/'
except:
    pass

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/gis.h: 125
try:
    HOST_DIRSEP = '/'
except:
    pass

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/gis.h: 125
try:
    G_DEV_NULL = '/dev/null'
except:
    pass

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/gis.h: 306
try:
    G_INFO_FORMAT_STANDARD = 0
except:
    pass

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/gis.h: 306
try:
    G_INFO_FORMAT_GUI = 1
except:
    pass

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/gis.h: 306
try:
    G_INFO_FORMAT_SILENT = 2
except:
    pass

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/gis.h: 306
try:
    G_INFO_FORMAT_PLAIN = 3
except:
    pass

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/gis.h: 308
try:
    G_ICON_CROSS = 0
except:
    pass

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/gis.h: 308
try:
    G_ICON_BOX = 1
except:
    pass

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/gis.h: 308
try:
    G_ICON_ARROW = 2
except:
    pass

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/gis.h: 310
try:
    DEFAULT_FG_COLOR = 'black'
except:
    pass

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/gis.h: 310
try:
    DEFAULT_BG_COLOR = 'white'
except:
    pass

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/gis.h: 312
try:
    G_FATAL_EXIT = 0
except:
    pass

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/gis.h: 312
try:
    G_FATAL_PRINT = 1
except:
    pass

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/gis.h: 312
try:
    G_FATAL_RETURN = 2
except:
    pass

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/gis.h: 314
try:
    ENDIAN_LITTLE = 0
except:
    pass

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/gis.h: 314
try:
    ENDIAN_BIG = 1
except:
    pass

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/gis.h: 314
try:
    ENDIAN_OTHER = 2
except:
    pass

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/gis.h: 319
try:
    GV_KEY_COLUMN = 'cat'
except:
    pass

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 57
def G__alloca(n):
    return (G_malloc (n))

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 57
def G__freea(p):
    return (G_free (p))

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 72
def G_incr_void_ptr(ptr, size):
    return (ptr + size)

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 88
def G_malloc(n):
    return (G__malloc ('<ctypesgen>', 0, n))

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 88
def G_calloc(m, n):
    return (G__calloc ('<ctypesgen>', 0, m, n))

# /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/defs/gis.h: 88
def G_realloc(p, n):
    return (G__realloc ('<ctypesgen>', 0, p, n))

Cell_head = struct_Cell_head # /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/gis.h: 363

G_3dview = struct_G_3dview # /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/gis.h: 425

Key_Value = struct_Key_Value # /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/gis.h: 452

Option = struct_Option # /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/gis.h: 482

Flag = struct_Flag # /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/gis.h: 511

GModule = struct_GModule # /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/gis.h: 527

TimeStamp = struct_TimeStamp # /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/gis.h: 537

Counter = struct_Counter # /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/gis.h: 543

Popen = struct_Popen # /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/gis.h: 547

_Color_Value_ = struct__Color_Value_ # /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/gis.h: 556

_Color_Rule_ = struct__Color_Rule_ # /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/gis.h: 564

_Color_Info_ = struct__Color_Info_ # /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/gis.h: 571

Colors = struct_Colors # /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/gis.h: 598

ilist = struct_ilist # /home/majavie/grass_sc/grass7_trunk/dist.x86_64-unknown-linux-gnu/include/grass/gis.h: 622

# No inserted files

