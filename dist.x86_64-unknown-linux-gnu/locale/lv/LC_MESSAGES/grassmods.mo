��    �      d    �      �     �  )   �           8  '   G     o     �     �     �     �     �     �  @   �  <   ;     x  :   �     �  0   �                1     P     a     }     �     �     �  0   �  +        /     A  -   U  #   �     �     �     �  #   �               0  (   J     s     �     �     �     �     �          *  1   C  "   u     �     �     �     �  	   �  
   �  ,   	  )   6  ,   `     �  "   �  6   �  !     	   (  "   2     U  E   a  5   �  4   �          1     K     e     y  
   �     �     �     �     �  $   �          1     M     e     x     �     �  &   �  )   �  	     �     %   �     �     �     �          )     <     [      w  &   �  '   �     �  (     )   /  '   Y  &   �     �     �  E   �          3     P     _     }  ?   �     �  -   �  -        9  $   S     x     �     �     �  .   �     �  B      /   O   )         �   #   �      �       !     !  %   !  3   >!     r!     �!      �!     �!  0   �!     �!     "     "  ?   ,"  =   l"  
   �"  !   �"     �"     �"  <   �"     5#  2   G#  )   z#     �#     �#  P   �#  '   &$     N$  %   l$  
   �$     �$  *   �$  "   �$     	%  "   (%     K%     ^%     q%  6   �%     �%     �%  -   �%     &     #&     ?&     [&      v&     �&     �&  F   �&     �&       '  (   !'  !   J'     l'     �'     �'  S   �'  T   (  X   ](  Y   �(     )  1   ,)  
   ^)  %   i)     �)  #   �)  �  �)  	   a+  1   k+  )   �+     �+  /   �+  !   ,     *,     :,     T,     c,      �,     �,  @   �,  D   �,  !   *-  N   L-     �-  7   �-     �-     �-     .     0.     <.     X.     d.     k.      �.  2   �.  1   �.     /     #/  E   8/  *   ~/     �/     �/     �/  (   �/     %0     80     U0  %   r0     �0     �0     �0  /   �0  "   1  ,   ;1  -   h1     �1  .   �1  $   �1     2     #2     A2     T2     m2     y2  +   �2  +   �2  ?   �2  )   3  &   H3  :   o3     �3     �3  B   �3     4  P   4  K   n4  :   �4  "   �4     5     55     R5     n5     �5     �5     �5     �5     �5  (   �5     6     56     T6  !   q6     �6  "   �6     �6      �6  1   �6     ,7  �   <7  +   �7  	   "8     ,8  #   I8     m8     �8  &   �8  %   �8  6   �8  9    9  ;   Z9     �9  .   �9  /   �9  /   :  2   B:     u:     �:  N   �:  !   �:  "   ;     B;  '   V;     ~;  O   �;     �;  7   �;  7   .<     f<  (   <     �<     �<     �<     �<  _   =     v=  Q   �=  I   �=  =   +>  *   i>  ,   �>  
   �>     �>     �>  .   �>  ?   ?     X?     r?  %   x?     �?  /   �?  ,   �?      @     #@  S   /@  N   �@     �@  0   �@     A     !A  =   :A     xA  N   �A  >   �A     B  &   9B  C   `B  1   �B  "   �B  -   �B     'C     5C  2   NC  $   �C     �C  (   �C     �C     �C     D  A   'D     iD     �D  2   �D     �D     �D     E     &E  $   CE     hE     pE  N   �E     �E  %   �E  *   
F  &   5F     \F     oF     �F  Y   �F  [   �F  c   UG  j   �G     $H  <   =H  
   zH  "   �H     �H  .   �H     &   X   �   $   �       �   R           D       f                d   �   /          w   �   P          ~       @      p   %   �   �       �           ?   �   2   "   K   T                      >   �   v       !   S       x       �   j   5       .   o          �   �   �   �   (       �   �       �   O           <   #      U   �   A       �       6   ^   �                         l       �   �   a   V   �   1      �   �       �          �       �       �   r   �   3       ;   �              c   H   �   �   �         �   )      �       �   `   �   b   �   ,   C       z       �   �   g           �   �   *   �   |   I   	   }   G   �          m   q      
       �   �           �   B           t   Z   �   �         �   M   �       �   h       y   �       �   0   �   �      8   �   �       9   -   �       �   �   u   �   �   [              +   W      �       s      Y   k       :   �   ]   �   '   {   _   F              �           n   L   �               Q   �   �      E   �          7       �   i   J      =   \           e   4   N    

Buttons:
 
Click mouse button on desired location

 
ERROR: vector map - can't read
 
Your choice:             Acres: %.3f		Sq Miles: %.4f
     Line: %5d  Angle: %.8f
   Isle[%d]: %d
  Left:  what's here
  Right: quit
 %d raster%s, %d vector%s
 %d records selected from table %s 'password' in database definition is not supported, use db.login 'user' in database definition is not supported, use db.login 2 layers must be specified 3D grid resolution (north-south, east-west and top-bottom) 3dview file <%s> not found Allow only horizontal and vertical flow of water Area
 Area height: %f
 Area: %d  Number of isles: %d
 Background color Background color (for null) Border color Buttons
 Calculating shadows from DEM... Cannot calculate area centroid Cannot combine 'from_table' and 'select' options Cannot combine 'select' and 'where' options Cannot copy table Cannot create index Cannot get db link info -> cannot copy table. Cannot grant privileges on table %s Cannot insert new row: %s Cannot read vector Character encoding Character to represent no data cell Cleaning tool Color for drawing text Column type not supported Compresses and decompresses raster maps. Coordinates for query Copy table failed Costs on the network = %f
 Curl labels along lines Database connection not defined Desired east-west resolution Desired north-south resolution Destination unreachable
 Detects the object's edges from a LIDAR data set. Display information for null cells Do not show category labels Do not show category numbers Elevation bias Error in pj_do_proj Error: %s Error: %s
 Exports a GRASS raster to a binary MAT-File. Exports a GRASS raster to a binary array. Extend location extents based on new dataset Failed opening input dig file. Failed opening input dig_att file. Failed to attach an attribute (category %d) to a line. Field separator (terse mode only) Font name Full menu (zoom + pan) & Quit menu Full output Generates a raster map layer with contiguous areas grown by one cell. Generates random cell values with spatial dependence. Generates random surface(s) with spatial dependence. Ignore SQL errors and continue Illegal x coordinate <%s> Illegal y coordinate <%s> Input database name Input driver name Input file Invalid number of steps: %s Island: %d  In area: %d
 Island: %d In area: %d
 Isle[%d]: %d
 L: add  M: remove  R: quit and save
 Lake area %f square meters Lake volume %f cubic meters Layer: %d
category: %d
 Left:   Zoom menu
 Length: %f
 Line height min: %f max: %f
 Line height: %f
 Line-of-sight raster analysis program. Line: %d  Type: %s  Left: %d  Right: %d   Look ok?  Makes each cell category value a function of the category values assigned to the cells around it, and stores new cell values in an output raster map layer. Maximum number of points in a segment Metric Name for ASCII output file Name for new location to create Name of a database file Name of an element Name of existing raster map(s) Name of existing vector map Name of layer to be used for HUE Name of layer to be used for INTENSITY Name of layer to be used for SATURATION Name of new location to create Name of output layer to be used for BLUE Name of output layer to be used for GREEN Name of output layer to be used for RED Name of raster map to be used for <%s> Neighborhood operation Neighborhood size No attribute table found -> using only category numbers as attributes No graphics device selected No graphics device selected! No map created No record for line (cat = %d) Node %d: %f %f
 Note: run 'd.erase' for the new region to affect the graphics.
 Nothing Found.
 Nothing to calculate. Please verify settings. Nothing to draw! (no categories with labels?) Number of copies to print Number of levels to be used for <%s> Original line color Output database name Output driver name Output field separator Output lake map or overwrite flag must be set! Output table name Outputs the raster map layer values lying on user-defined line(s). Override projection (use location's projection) Overwrite seed map with result (lake) map Page length (default: %d lines) Page width (default: %d characters) Percent complete:  Point height: %f
 Print Print the stats in shell script style Print the sun position output in shell script style Projection files missing Quiet Radius of buffer in raster cells Raster Raster map for which histogram will be displayed Raster map to be profiled Read error in vector map Reading %s ... Removes data base element files from the user's current mapset. Renames data base element files in the user's current mapset. Resolution Respect NULL values while drawing Right:  Quit
 Right:  Quit menu
 SQLite driver: column '%s', SQLite type %d  is not supported Scientific format Seed coordinates and output map lake= must be set! Seed map or seed coordinates must be set! Seed point coordinates Select type: line or area Selects the font in which text will be displayed on the user's graphics monitor. Size - Sq Meters: %.3f		Hectares: %.3f
 Skip categories with no label Sorry, you must choose an output map. Text color The file '%s' already exists. This region now saved as current region.

 Title for the resulting raster map Title of the output raster map Too many layers for this operation Tool doesn't exist Uncompress the map Unknown method <%s> Unknown option in database definition for PostgreSQL:  Unknown type Unknown type: %c Use negative depth values for lake raster map Value for the eastern edge Value for the northern edge Value for the southern edge Value for the western edge Value to write for "grown" cells Vector Vertical scale Volume is correct only if lake depth (terrain raster map) is in meters Water level You are clicking outside the map You must provide at least four points %d You must select at least one tool [%s] already compressed [%s] already uncompressed color can be one of:
 column '%s' : type bool (boolean) is stored as char(1), values: 0 (false), 1 (true) column '%s' : type character varying is stored as varchar(250) some data may be lost column '%s' : type int8 (bigint) is stored as integer (4 bytes) some data may be damaged column '%s', type 'string': unknown width -> stored as varchar(250) some data may be lost dig_att file doesn't exist. driver: %s
database: %s
table: %s
key column: %s
 length %f
 or an R:G:B triplet, e.g.: 0:127:255
 vector warning: %s - vector map not found
 Project-Id-Version: grassmods_lv
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2014-06-04 21:04+0200
PO-Revision-Date: 2012-01-27 14:24+0200
Last-Translator: Maris Nartiss <maris.kde@gmail.com>
Language-Team: Latvian
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: lv
X-Generator: KBabel 1.11.4
Plural-Forms:  nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n != 0 ? 1 : 2);
 

Pogas:
 
Noklikšķini peles pogu  izvēlētajā vietā

 
KĻŪDA: vektora failu - nevar nolasīt
 
Jūsu izvēle:             Akros: %.3f		Kvadrātjūdzēs: %.4f
     Līnija: %5d  Leņķis: %.8f
   Sala[%d]: %d
  Kreisais:  kas ir šeit
  Labā: beigt
 %d rastrs %s, %d vektors %s
 %d ieraksti atlasīti no tabulas %s 'parole' datubāzes definīcijā nav atbalstīta, lieto db.login 'lietotājs' datubāzes definīcijā nav atbalstīts, lieto db.login 2 līmeņiem jābūt norādītiem 3D izšķirtspēja (abas ziemļi-dienvidi, austrumi-rietumi un augša-apakša) 3d skata fails <%s> nav atrasts Atļaut tikai horizontālu un vertikālu ūdens plūsmu Laukums
 Laukuma augstums: %f
 Laukums: %d  Salu skaits: %d
 Fona krāsa Fona krāsa(priekš nulles) Malu krāsa Pogas
 Kalkulējam ēnas no DEM... Nevar noteikt laukuma centroīdu Nevar savietot 'no_tabula' un 'select' īpašības Nevar sakombinēt 'select' un 'where' īpašības Nevar nokopēt tabulu Nevar izveidot index Nevar iegūt datubāzes saites informāciju -> nevar nokopēt tabulu. Nevar piešķirt privilēģijas tabulai %s Nevar izveidot jaunu rindu: %s Nevar nolasīt vektoru failu Rakstuzīmju kodējums Simbols, ar ko apzīmēt neesošus datus Tīrīšanas rīks Krāsa zīmējamajam tekstam Kolonas tips nav atbalstīts Arhivēt un atarhivēt rastra failus. Vaicājuma koordinātes Tabulas kopēšana neizdevās Pašizmaksa tīklā = %f
 Izveidot punktus visā garumā ilīniju garumā Datubāzes konekcija nav definēta Izvēlētā austrumi-rietumi izšķirtspēja Izvēlētā ziemeļi-dienvidi izšķirtspēja Galapunkts nesasniedzams
 Detektē objektu robežas no LiDAR datu kopas. Parādīt nulles šūnu informāciju Nerādīt kategoriju uzrakstus Nerādīt kategoriju nummurus Pacēluma slīpums Kļūda iekš pj_do_proj Kļūda: %s Kļūda: %s
 Eksportē GRASS rastru uz bināro MAT-Failu Eksportēt GRASS rastru uz bināro masīvu. Paplašināt novietojuma apjomu pamatojoties uz jauno datu kopu Neveiksme atverot ieejas dig ieejas failu Neveiksme atverot ieejas dig_att failu Neveiksme pieveinojot atribūtu (kategorijai %d) līnijai. Lauku atdalītājs Fonta nosaukums Pilna izvēlne (palielināšana + pārvietošana)& Pamest izvēlni Pilns izvads Ģenerē rastra karti, kur vienlaidus apgabali ir palielināti par vienu šūnu. Ģenerēt nejau;sas ceļļu(šūnu) vērtības ar telpiskajām atkarībām. Ģenerēt nejaušu virsmu(as) ar telpiskajām atkarībām. Ignorēt SQL kļūdas un turpināt Nederīga x koordināta <%s> Nederīga y koordināta <%s> Ieejas datubāzes nosaukums Ieejas draivera nosaukums Ieejas fails Nederīgs soļu skaits: %s Sala: %d  Laukumā: %d
 Sala: %d Laukumā: %d
 Sala[%d]: %d
 L: un  M: atņemt R: iziet un saglabāt
 Ezera laukums %f kvadrātmetri Ezera tilpums ir %f kubikmetri Līmenis: %d
kategorija: %d
 Kreisais: Tuvināšanas izvēlne
 Garums: %f
 Līnijas augstums min: %f max: %f
 Līnijas augstums: %f
 Redzamības analīzes programma. Līnija: %d  Tips: %s  Kreisais: %d  Labais: %d   Izskatās labi? Izveido katras šūnas kategorijas vētību kā funkciju, kas atsaucas uz apkārt esošo šūnu kategoriju vērtībām un noglabā jauno šūnu vērtības izejas rastra kartes līmenī. Maksimālais punktu skaits vienā segmentā Metriskā Izvades ACII faila nosaukums Janveidojamā novietojuma nosaukums Datu bāzes faila nosaukums Elementa nosaukums Esksistējošu rastra failu nosaukumi. Eksistējošas vektorkartes nosaukums Līmeņa nosaukums kuru izmantos  HUE(krāsu niansēm) Līmeņa nosaukums kuru izmantos INTENSITY(intensitātei) Līmeņa nosaukums kuru izmantos SATURATION(koncentrācjai) Jaunā novietojuma nosaukums Līmeņa nosaukums kuru izmantos BLUE(zielais) Līmeņa nosaukums kuru izmantos GREEN(zaļais) Līmeņa nosaukums kuru izmantos RED(sarkanais) Izmantojamās rastra kartes nosaukums priekš <%s> Operācijas ar blausšūnām Apkārtnes izmērs šūnās Atribūtu tabula nav atrasta -> tiek izmantoti kategoriju numuri kā atribūti Nav izvēlēta grafiskā iekārta Nav izvēlēta grafiskā iekārta! Karte nav izveidota Nav ieraksta priekš līnijas (cat= %d) Piezīme %d: %f %f
 Piezīme: darbini 'd.erase' jaunajam reģionam ietekmējot gafisko rezultātu.
 Nekas nav atrasts.
 Nav nekā ko kalkulēt. Lūdzu pārbaudi iestatījumus. Nav ko zīmēt! (nav kategorijas ar urakstiem/birkām?) Drukājamo kopiju sakits Izmantojamo līmeņu skaits priekš <%s> Oriģinālā līnijas krāsa Izejas datubāzes nosaukums Izejas draivera nosaukums Izvades lauku atdalītājs Izvades kartei lake ir jābūt uzstādītai vai jābūt uzstādītam pārrakstīšanas karogam! Izejas tabulas nosaukums Izvada rastra kartes vērtības, kas atrodas uz lietotāja norādītās līnijas. Neņemt vērā projekciju (izmantot pašreizējā novietojuma projekciju) Pārrakstīt sākuma punkta karti ar rezultāta (ezera) karti Lapas garums (pēc noklusējuma %d rindas) Lapas platums (pēc noklusējuma %d simboli) Pabeigts:  Punkta augstums: %f
 Drukāt Drukāt statistiku komandrindas skripta stilā Drukāt saules pozīcijas apraskstu komandrindas skripta stilā Trūkst projekcijas failu Klusi Bufera zonas rādiuss rastra šūnās Rastrs Rastra karte, kuras histogramma tiks parādīta Rastra karte no kuras tiks nolasīts profils Kļūda nolasot vektordatu failu Lasa %s ... Noņemt datubāzes elemtu failus no lietotāja tekošās karšu biblotēkas/mapset. Pārsauc datubāzes elemtu failus lietotāja pašreizējā karšu biblotēkā. Izšķirtspēja Ņemt vērā NULL vērtības zīmēšanas laikā Labā: Iziet
 Labais: Pamest izvēlni
 SQLite draiveris: kolona '%s', SQLite tips %d  nav atbastīts Zinātniskais formāts Sākuma punkta koordinātēm un izvades kartei lake= jābūt uz uzstādītiem! Sākuma punkta kartei vai koordinātām jābūt uzstādītām! Sākuma punkta koordinātes Izvēlēties tipu: līnija vai laukums Izvēlies fontu kurš tiks parādīts lietotāja grafiskajā vidē. Izmērs - Kavadrātmetros: %.3f		Hektāros: %.3f
 Izlaist kategorijas bez uzrakstiem Piedodiet, jums ir jāizvēlas izvades karte. Teksta krāsa Fails '%s' jau eksistē. Šis reģions pašlaik saglabāts kā tekošais.

 rezultējošās rastra kartes tituls Izejas rastra kartes nosaukums Pārāk dayudz līmeņu šai operācijai Rīks neeksistē Atarhivēt kartis Nezināma metode <%s> Nezināms nosacījums datubāzes definīcijā priekš PostgreSQL: Nezināms/nepazīstms tips Nenosakāms tips: %c Izmantot negatīvas vērtības ezera rastra kartei Austrumu šākutnes vērtība Ziemeļu šķautnes vērtība Dienvidu šķautnes vērtība Rietumu šķautnes vērtība Vērtība "pieaudzētajām" šūnām Vektors Vertikālais mērogs Uzmanību. Tilpums ir korekts tikai tad, ja dziļums (reljefa karte) ir metros Ūdens līmenis Jūs noklikšķinājāt ārpus kartes Ir jāievada kā minimums četri punkti %d Jums ir jāizvēlas vismaz viens rīks [%s] jau arhivēts [%s] jau atarhivēts krāsa var būt viena no:
 kolona '%s' :tips bool (boolean) ir noteikts kā char(1), vērtības: 0 (false), 1 (true) kolona '%s' : tipa raksturojums ir sagalbāts kā varchar(250) daži dati var tikt zaudēti kolona '%s' : tips int8 (bigint) ir saglabāts kā integer (4 bytes) iespējas dažu datu bojājums Kolona '%s', tips 'string':nenosakāms platums -> saglabāts kā varchar(250) daži dati var tikt zaudēti dig_att fails neeksistē draiveris: %s
datubāze: %s
tabula: %s
atslēgas kolona: %s
 garums %f
 vai RGD triplets, piem. 0:126:255
 vektors brīdinājums: %s - vektora fails nav atrasts
 