<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title>GRASS GIS manual: r.his</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="grassdocs.css" type="text/css">
</head>
<body bgcolor="white">

<a href="index.html"><img src="grass_logo.png" alt="GRASS logo"></a>
<hr class="header">

<h2>NAME</h2>
<em><b>r.his</b></em>  - Generates red, green and blue raster map layers combining hue, intensity and saturation (HIS) values from user-specified input raster map layers.
<h2>KEYWORDS</h2>
raster, color transformation, RGB, HIS, IHS
<h2>SYNOPSIS</h2>
<div id="name"><b>r.his</b><br></div>
<b>r.his help</b><br>
<div id="synopsis"><b>r.his</b> [-<b>n</b>] <b>h_map</b>=<em>string</em>  [<b>i_map</b>=<em>string</em>]   [<b>s_map</b>=<em>string</em>]  <b>r_map</b>=<em>string</em> <b>g_map</b>=<em>string</em> <b>b_map</b>=<em>string</em>  [--<b>overwrite</b>]  [--<b>help</b>]  [--<b>verbose</b>]  [--<b>quiet</b>] 
</div>

<div id="flags">
<h3>Flags:</h3>
<dl>
<dt><b>-n</b></dt>
<dd>Respect NULL values while drawing</dd>

<dt><b>--overwrite</b></dt>
<dd>Allow output files to overwrite existing files</dd>
<dt><b>--help</b></dt>
<dd>Print usage summary</dd>
<dt><b>--verbose</b></dt>
<dd>Verbose module output</dd>
<dt><b>--quiet</b></dt>
<dd>Quiet module output</dd>
</dl>
</div>

<div id="parameters">
<h3>Parameters:</h3>
<dl>
<dt><b>h_map</b>=<em>string</em>&nbsp;<b>[required]</b></dt>
<dd>Name of layer to be used for HUE</dd>

<dt><b>i_map</b>=<em>string</em></dt>
<dd>Name of layer to be used for INTENSITY</dd>

<dt><b>s_map</b>=<em>string</em></dt>
<dd>Name of layer to be used for SATURATION</dd>

<dt><b>r_map</b>=<em>string</em>&nbsp;<b>[required]</b></dt>
<dd>Name of output layer to be used for RED</dd>

<dt><b>g_map</b>=<em>string</em>&nbsp;<b>[required]</b></dt>
<dd>Name of output layer to be used for GREEN</dd>

<dt><b>b_map</b>=<em>string</em>&nbsp;<b>[required]</b></dt>
<dd>Name of output layer to be used for BLUE</dd>

</dl>
</div>
<div class="toc">
<ul class="toc">
    <li class="toc"><a href="#description" class="toc">DESCRIPTION</a>
    <ul class="toc">
        <li class="toc"><a href="#the-process" class="toc">The Process</a></li>
    </ul></li>
    <li class="toc"><a href="#notes" class="toc">NOTES</a></li>
    <li class="toc"><a href="#examples" class="toc">EXAMPLES</a></li>
    <li class="toc"><a href="#see-also" class="toc">SEE ALSO</a></li>
    <li class="toc"><a href="#author" class="toc">AUTHOR</a></li>
</ul>
</div>
<h2><a name="description">DESCRIPTION</a></h2>

<i>HIS</i> stands for hue, intensity, and saturation. 
This program produces red, green and blue raster map layers
providing a visually pleasing combination of hue,
intensity, and saturation values from two or three
user-specified raster map layers.

<p>
The human brain automatically interprets the vast amount of
visual information available according to basic rules. 
Color, or <i>hue</i>, is used to categorize objects. 
Shading, or <i>intensity</i>, is interpreted as
three-dimensional texturing. Finally, the degree of
haziness, or <i>saturation</i>, is associated with
distance or depth. This program allows data from up to
three raster map layers to be combined into a color image
(in the form of separate red, green and blue raster map
layers) which retains the original information in terms of
<i>hue</i>, <i>intensity</i>, and <i>saturation</i>.

<p>
While any raster map layer can be used to represent the hue
information, map layers with a few very distinct colors
work best.  Only raster map layers representing
continuously varying data like elevation, aspect, weights,
intensities, or amounts can suitably be used to provide
intensity and saturation information.

<p>
For example, a visually pleasing image can be made by using
a watershed map for the <i>hue</i> factor, an aspect map
for the <i>intensity</i> factor, and an elevation map for
<i>saturation</i>. (The user may wish to leave out the
elevation information for a first try.) Ideally, the
resulting image should resemble the view from an aircraft
looking at a terrain on a sunny day with a bit of haze in
the valleys.

<h3><a name="the-process">The Process</a></h3>

Each map cell is processed individually. First, the working
color is set to the color of the corresponding cell in the
map layer chosen to represent <i>HUE</i>.  Second, this
color is multiplied by the <i>red</i> intensity of that
cell in the <i>INTENSITY</i> map layer.  This map layer
should have an appropriate gray-scale color table
associated with it. You can ensure this by using the color
manipulation capabilities of
<em><a href="d.colors.html">d.colors</a></em> or
<em><a href="r.colors.html">r.colors</a></em>.
Finally, the color is made somewhat gray-based on the
<i>red</i> intensity of that cell in the
<i>SATURATION</i> map layer.  Again, this map layer
should have a gray-scale color table associated with it.

<h2><a name="notes">NOTES</a></h2>

The name is misleading. The actual conversion used is

<pre>
  <u>H</u>.i.s + <u>G</u>.(1-s)

where

  <u>H</u>   is the R,G,B color from the hue map
  i   is the red value from the intensity map
  s   is the red value from the saturation map
  <u>G</u>   is 50% gray (R = G = B = 0.5)

</pre>

<p>
Either (but not both) of the intensity or the saturation
map layers may be omitted. This means that it is possible
to produce output images that represent combinations of
<i>his, hi,</i> or <i>hs</i>.

The separate <i>red</i>, <i>green</i> and <i>blue</i>
maps can be displayed on the graphics monitor using
<em><a href="d.rgb.html">d.rgb</a></em>, or combined into
a composite RGB layer using
<em><a href="r.composite.html">r.composite</a></em>.

Users wishing to simply display an <i>his</i> composite
image without actually generating any layers should use the
program <em><a href="d.his.html">d.his</a></em>.


<h2><a name="examples">EXAMPLES</a></h2>

Recreate the following <em>d.his</em> command using <em>r.his</em>:
<div class="code"><pre>
g.regiopn rast=elevation
r.shaded.relief map=elevation shad=elev.shad_relf
d.his h=elevation i=elev.shad_relf brighten=50
</pre></div>

<div class="code"><pre>
r.mapcalc "elev.shad_relf_bright50 = #elev.shad_relf * 1.5"
r.colors elev.shad_relf_bright50 color=grey255
r.his h_map=elevation i_map=elev.shad_relf_bright50 r_map=esr.r g_map=esr.g bmap=esr.b
d.rgb red=esr.r green=esr.g blue=esr.b
</pre></div>


<h2><a name="see-also">SEE ALSO</a></h2>

<em>
<a href="d.his.html">d.his</a>,
<a href="d.colors.html">d.colors</a>,
<a href="d.colortable.html">d.colortable</a>,
<a href="d.rgb.html">d.rgb</a>,
<a href="r.blend.html">r.blend</a>,
<a href="r.colors.html">r.colors</a>,
<a href="r.composite.html">r.composite</a>,
<a href="r.mapcalc.html">r.mapcalc</a>,
<a href="i.his.rgb.html">i.his.rgb</a>,
<a href="i.rgb.his.html">i.rgb.his</a>
</em>

<h2><a name="author">AUTHOR</a></h2>

Glynn Clements (based upon <em><a href="d.his.html">d.his</a></em>)

<p><i>Last changed: $Date: 2012-11-25 11:59:42 +0100 (Sun, 25 Nov 2012) $</i><hr class="header">
<p><a href="index.html">Main index</a> | <a href="raster.html">Raster index</a> | <a href="topics.html">Topics index</a> | <a href="keywords.html">Keywords Index</a> | <a href="full_index.html">Full index</a></p>
<p>&copy; 2003-2014 <a href="http://grass.osgeo.org">GRASS Development Team</a>, GRASS GIS 7.1.svn Reference Manual</p>
</body>
</html>
