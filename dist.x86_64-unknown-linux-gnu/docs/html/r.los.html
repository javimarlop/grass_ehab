<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title>GRASS GIS manual: r.los</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="grassdocs.css" type="text/css">
</head>
<body bgcolor="white">

<a href="index.html"><img src="grass_logo.png" alt="GRASS logo"></a>
<hr class="header">

<h2>NAME</h2>
<em><b>r.los</b></em>  - Line-of-sight raster analysis program.
<h2>KEYWORDS</h2>
raster, viewshed, line of sight
<h2>SYNOPSIS</h2>
<div id="name"><b>r.los</b><br></div>
<b>r.los help</b><br>
<div id="synopsis"><b>r.los</b> [-<b>c</b>] <b>input</b>=<em>name</em> <b>output</b>=<em>name</em> <b>coordinates</b>=<em>east,north</em>  [<b>patt_map</b>=<em>name</em>]   [<b>obs_elev</b>=<em>float</em>]   [<b>max_dist</b>=<em>float</em>]   [--<b>overwrite</b>]  [--<b>help</b>]  [--<b>verbose</b>]  [--<b>quiet</b>] 
</div>

<div id="flags">
<h3>Flags:</h3>
<dl>
<dt><b>-c</b></dt>
<dd>Consider earth curvature (current ellipsoid)</dd>

<dt><b>--overwrite</b></dt>
<dd>Allow output files to overwrite existing files</dd>
<dt><b>--help</b></dt>
<dd>Print usage summary</dd>
<dt><b>--verbose</b></dt>
<dd>Verbose module output</dd>
<dt><b>--quiet</b></dt>
<dd>Quiet module output</dd>
</dl>
</div>

<div id="parameters">
<h3>Parameters:</h3>
<dl>
<dt><b>input</b>=<em>name</em>&nbsp;<b>[required]</b></dt>
<dd>Name of input elevation raster map</dd>

<dt><b>output</b>=<em>name</em>&nbsp;<b>[required]</b></dt>
<dd>Name for output raster map</dd>

<dt><b>coordinates</b>=<em>east,north</em>&nbsp;<b>[required]</b></dt>
<dd>Coordinates of the viewing position</dd>

<dt><b>patt_map</b>=<em>name</em></dt>
<dd>Binary (1/0) raster map to use as a mask</dd>

<dt><b>obs_elev</b>=<em>float</em></dt>
<dd>Viewing position height above the ground</dd>
<dd>Default: <em>1.75</em></dd>

<dt><b>max_dist</b>=<em>float</em></dt>
<dd>Maximum distance from the viewing point (meters)</dd>
<dd>Options: <em>0-5000000</em></dd>
<dd>Default: <em>10000</em></dd>

</dl>
</div>
<div class="toc">
<ul class="toc">
    <li class="toc"><a href="#description" class="toc">DESCRIPTION</a></li>
    <li class="toc"><a href="#notes" class="toc">NOTES</a></li>
    <li class="toc"><a href="#example" class="toc">EXAMPLE</a></li>
    <li class="toc"><a href="#todo" class="toc">TODO</a></li>
    <li class="toc"><a href="#see-also" class="toc">SEE ALSO</a></li>
    <li class="toc"><a href="#author" class="toc">AUTHOR</a></li>
</ul>
</div>
<h2><a name="description">DESCRIPTION</a></h2>

<em>r.los</em> generates a raster output map in which the cells that are
visible from a user-specified observer position are marked with the
vertical angle (in degrees) required to see those cells (viewshed).
A value of 0 is directly below the specified viewing position,
90 is due horizontal, and 180 is directly above the observer.
The angle to the cell containing the viewing position is undefined
and set to 180.

<p>To run <em>r.los</em>, the user must specify at least 
an <b>input</b> map name, <b>output</b> map name, and the geographic 
<b>coordinate</b>s of the user's viewing location; 
any remaining parameters whose values are unspecified 
will be set to their default values (see below). 

<p>The <b>patt_map</b> is the name of a binary (1/0) raster map layer in which
cells within the areas of interest are assigned the category value '1', and
all other cells are assigned the category value '0' or NULL. If this parameter is
omitted, the analysis will be performed for the whole area within a certain
distance of the viewing point inside the geographic region boundaries.
<br>
Default: assign all cells that are within the <b>max_dist</b> and within
the user's current geographic region boundaries a value of 1.

<p>The <b>obs_elev</b> parameter defines the height of the observer (in
meters) above the viewing point's elevation.
<p>
The <b>max_dist</b> parameter is the maximum distance (in meters) from the
viewing point inside of which the line of sight analysis will be performed.
The cells outside this distance range are assigned a NULL value.


<h2><a name="notes">NOTES</a></h2>

For accurate results, the program must be run with the resolution of the 
geographic region set equal to the resolution of the data 
(see <em><a href="g.region.html">g.region</a></em>).

<p>The time to complete the calculation increases dramatically with the region size.
Try to keep the columns and rows under 1000.

<p>It is advisable to use a 'pattern layer' which identifies
the areas of interest in which the line of sight analysis
is required.  Such a measure will reduce the time taken by
the program to run.

<p>The curvature of the Earth is not taken into account for these calculations.
However, for interest's sake, a handy calculation for distance to the true horizon
is approximated by <i>d = sqrt(13*h)</i> where <i>h</i> is the height of the observer
in meters (above sea level) and <i>d</i> is the distance to the horizon in km.
This may be useful for setting the <b>max_dist</b> value.


<h2><a name="example">EXAMPLE</a></h2>

Spearfish example - calculation of viewshed from 50m tower
on top of a mountain:

<div class="code"><pre>
g.region rast=elevation.dem -p
r.los elevation.dem out=los coord=598869,4916642 obs_elev=50 max_dist=10000
r.colors -e los color=bgyr
d.shadedmap relief=aspect drape=los bright=10
echo "symbol extra/target 25 598869 4916642 red" | d.graph -m
</pre></div>

<h2><a name="todo">TODO</a></h2>

a) Rewrite using ideas from <em>r.cva</em> and a method which scales better
to large regions.<br>A suggested method is detailed in:<br>
Izraelevitz, David (USACE).<br>
'A Fast Algorithm for Approximate Viewshed Computation'<br>
<i>Photogrammetric Engineering & Remote Sensing</i>, July 2003
<!-- http://article.gmane.org/gmane.comp.gis.grass.devel/1781
  Post by Paul Kelly 2003-08-13 to grass-dev, 
  "Re: [bug #2061] (grass) r.los needs FP update" -->

b) or fix r.viewshed in Addons

<h2><a name="see-also">SEE ALSO</a></h2>

<em><a href="g.region.html">g.region</a></em>

<h2><a name="author">AUTHOR</a></h2>

Kewan Q. Khawaja, Intelligent Engineering Systems Laboratory, M.I.T.

<p><i>Last changed: $Date: 2011-11-08 22:24:20 +0100 (Tue, 08 Nov 2011) $</i><hr class="header">
<p><a href="index.html">Main index</a> | <a href="raster.html">Raster index</a> | <a href="topics.html">Topics index</a> | <a href="keywords.html">Keywords Index</a> | <a href="full_index.html">Full index</a></p>
<p>&copy; 2003-2014 <a href="http://grass.osgeo.org">GRASS Development Team</a>, GRASS GIS 7.1.svn Reference Manual</p>
</body>
</html>
