<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title>GRASS GIS Manual: 3D raster data (voxel) processing in GRASS GIS</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="grassdocs.css" type="text/css">
</head>
<body bgcolor="white">
<a href="index.html"><img src="grass_logo.png" alt="GRASS logo"></a>
<hr class="header">
<h2>3D raster data (voxel) processing in GRASS GIS</h2>
<div class="toc">
<ul class="toc">
    <li class="toc"><a href="#3d-raster-maps-(voxels)-in-general" class="toc">3D Raster maps (voxels) in general</a></li>
    <li class="toc"><a href="#3d-raster-(voxel)-import" class="toc">3D Raster (voxel) import</a></li>
    <li class="toc"><a href="#3d-region-settings-and-3d-mask" class="toc">3D region settings and 3D MASK</a></li>
    <li class="toc"><a href="#volume-operations" class="toc">Volume operations</a></li>
    <li class="toc"><a href="#3d-raster-conversion-to-vector-or-2d-raster-maps" class="toc">3D raster conversion to vector or 2D raster maps</a></li>
    <li class="toc"><a href="#3d-raster-statistics" class="toc">3D raster statistics</a></li>
    <li class="toc"><a href="#3d-raster-interpolation" class="toc">3D raster interpolation</a></li>
    <li class="toc"><a href="#3d-raster-export" class="toc">3D raster export</a></li>
    <li class="toc"><a href="#see-also" class="toc">See also</a></li>
</ul>
</div>
<!-- meta page description: 3D raster data (voxel) processing in GRASS GIS -->
<!-- meta page index: raster3D -->
<h3><a name="3d-raster-maps-(voxels)-in-general">3D Raster maps (voxels) in general</a></h3>

GRASS GIS is one of the few GIS software packages with volume data support.
Here data are stored as a 3D raster with a unit volume called a voxel
(volume pixel). Voxels are designed to support representations of
trivariate continuous fields. The vertical dimension supports spatial and temporal units.
Hence space time voxel cubes with different temporal resolutions can be created and processed.
<p>

GRASS GIS 3D raster maps use the same coordinate system as
2D raster maps (row count from north to south) with an additional z dimension (depth)
counting from bottom to top. The upper left corner (NW) is the origin of the voxel. 
Volumes are stored using a tile cache based approach. This allows abritrary read 
and write operations in the created volume. The size of the tiles can be specified at import time
with <a href="r3.in.ascii.html">r3.in.ascii</a> or the data can be retiled using
<a href="r3.retile.html">r3.retile</a> after import or creation.

<div align="center" style="margin: 10px">
<img src="r3_volume_layout.png" border=0><br>
<i>The volume coordinate system and tile layout of the RASTER3D library</i>
</div>

<h3><a name="3d-raster-(voxel)-import">3D Raster (voxel) import</a></h3>

The modules <a href="r3.in.ascii.html">r3.in.ascii</a> and
 <a href="r3.in.bin.html">r3.in.bin</a> supports
generic x,y,z ASCII and binary array import. Alternatively, volumes can be generated
from 3D point vector data (<a href="v.to.rast3.html">v.to.rast3</a>). 
Always the full map is imported.
Volumes can also be created based on 2D elevation map(s) and value raster map(s)
(<a href="r.to.rast3elev.html">r.to.rast3elev</a>). Alternatively,
a volume can be composed of several 2D raster map slices which are
merged into one 3D raster (voxel) map (<a href="r.to.rast3.html">r.to.rast3</a>).

<h3><a name="3d-region-settings-and-3d-mask">3D region settings and 3D MASK</a></h3>

GRASS 3D raster map processing is always performed in the current 3D region
settings (see <a href="g.region.html">g.region</a>, <em>-p3</em> flags), i.e.
the current region extent, vertical extent and current 3D resolution are used.
If the 3D resolution differs from that of the input raster map(s),
on-the-fly resampling is performed (nearest neighbor resampling).
If this is not desired, the input map(s) has/have to be reinterpolated
beforehand with one of the dedicated modules.
Masks can be set (<a href="r3.mask.html">r3.mask</a>).

<h3><a name="volume-operations">Volume operations</a></h3>

Powerful 3D map algebra is implemented in <a href="r3.mapcalc.html">r3.mapcalc</a>.

A 3D groundwater flow model is implemented in <a href="r3.gwflow.html">r3.gwflow</a>.

<h3><a name="3d-raster-conversion-to-vector-or-2d-raster-maps">3D raster conversion to vector or 2D raster maps</a></h3>

3D vector point data can be converted to a GRASS 3D raster map
(<a href="v.to.rast3.html">v.to.rast3</a>).
Layers from a 3D raster map can be converted to a 2D raster map
(<a href="r3.to.rast.html">r3.to.rast</a>).
Cross sectional 2D raster map can be extracted from 3D raster map based
on a 2D elevation map (<a href="r3.cross.rast.html">r3.cross.rast</a>).

<h3><a name="3d-raster-statistics">3D raster statistics</a></h3>

Volume statistics can be calculated with <a href="r3.stats.html">r3.stats</a>
and <a href="r3.univar.html">r3.univar</a>.

<h3><a name="3d-raster-interpolation">3D raster interpolation</a></h3>

From 3D vector points, GRASS 3D raster maps can be interpolated
(<a href="v.vol.rst.html">v.vol.rst</a>). Results are volumes,
or 2D raster maps can be also extracted.

<h3><a name="3d-raster-export">3D raster export</a></h3>

The modules <a href="r3.out.ascii.html">r3.out.ascii</a> and
 <a href="r3.out.bin.html">r3.out.bin</a> support the export of
 3D raster maps as ASCII or binary files. The output of these modules
 can be imported with the coresponding import modules noted above.
<p>
NetCDF export of 3D raster maps can be performed using the module 
<a href="r3.out.netcdf.html">r3.out.netcdf</a>. It supports 3D raster maps
with spatial and temporal vertical dimension (Space time voxel cubes).
<p>
GRASS 3D raster maps can be exported to VTK (<a href="r3.out.vtk.html">r3.out.vtk</a>).
VTK files can be visualized with the 
<em><a href="http://www.vtk.org">VTK Toolkit</a></em>, 
<em><a href="http://www.paraview.org">Paraview</a></em> and 
<em><a href="http://mayavi.sourceforge.net">MayaVi</a></em>.
GRASS 2D raster maps can be exported to VTK with <a href="r.out.vtk.html">r.out.vtk</a>,
GRASS vector maps can be exported to VTK with <a href="v.out.vtk.html">v.out.vtk</a>.
<p>
Alternatively, GRASS 3D raster maps can be imported and exported from/to Vis5D
(<a href="r3.in.v5d.html">r3.in.v5d</a>, <a href="r3.out.v5d.html">r3.out.v5d</a>).
Note that Vis5D is limited in the number of supported volumes.

<h3><a name="see-also">See also</a></h3>

<ul>
  <li><a href="rasterintro.html">Introduction into raster data processing</a></li>
  <li><a href="vectorintro.html">Introduction into vector data processing</a></li>
  <li><a href="imageryintro.html">Introduction into image processing</a></li>
  <li><a href="databaseintro.html">Database management</a></li>
  <li><a href="projectionintro.html">Projections and spatial transformations</a></li>
</ul><hr class="header">
<p><a href="index.html">Main index</a> | <a href="raster3D.html">raster3D index</a> | <a href="topics.html">Topics index</a> | <a href="keywords.html">Keywords Index</a> | <a href="full_index.html">Full index</a></p>
<p>&copy; 2003-2014 <a href="http://grass.osgeo.org">GRASS Development Team</a>, GRASS GIS 7.1.svn Reference Manual</p>
</body>
</html>
