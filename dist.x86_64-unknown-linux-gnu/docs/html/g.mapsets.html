<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title>GRASS GIS manual: g.mapsets</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="grassdocs.css" type="text/css">
</head>
<body bgcolor="white">

<a href="index.html"><img src="grass_logo.png" alt="GRASS logo"></a>
<hr class="header">

<h2>NAME</h2>
<em><b>g.mapsets</b></em>  - Modifies/prints the user's current mapset search path.<BR>
Affects the user's access to data existing under the other mapsets in the current location.
<h2>KEYWORDS</h2>
general, settings, search path
<h2>SYNOPSIS</h2>
<div id="name"><b>g.mapsets</b><br></div>
<b>g.mapsets help</b><br>
<div id="synopsis"><b>g.mapsets</b> [-<b>lps</b>] <b>mapset</b>=<em>name</em>[,<i>name</i>,...] <b>operation</b>=<em>string</em>  [<b>separator</b>=<em>character</em>]   [--<b>help</b>]  [--<b>verbose</b>]  [--<b>quiet</b>] 
</div>

<div id="flags">
<h3>Flags:</h3>
<dl>
<dt><b>-l</b></dt>
<dd>List all available mapsets in alphabetical order</dd>

<dt><b>-p</b></dt>
<dd>Print mapsets in current search path</dd>

<dt><b>-s</b></dt>
<dd>Launch mapset selection GUI dialog</dd>

<dt><b>--help</b></dt>
<dd>Print usage summary</dd>
<dt><b>--verbose</b></dt>
<dd>Verbose module output</dd>
<dt><b>--quiet</b></dt>
<dd>Quiet module output</dd>
</dl>
</div>

<div id="parameters">
<h3>Parameters:</h3>
<dl>
<dt><b>mapset</b>=<em>name[,<i>name</i>,...]</em>&nbsp;<b>[required]</b></dt>
<dd>Name of mapset (default: current search path)</dd>
<dd>Name(s) of existing mapset(s) to add/remove or set</dd>

<dt><b>operation</b>=<em>string</em>&nbsp;<b>[required]</b></dt>
<dd>Operation to perform</dd>
<dd>Options: <em>set, add, remove</em></dd>
<dd>Default: <em>add</em></dd>

<dt><b>separator</b>=<em>character</em></dt>
<dd>Field separator for printing (-l and -p flags)</dd>
<dd>Special characters: pipe, comma, space, tab, newline</dd>
<dd>Default: <em>space</em></dd>

</dl>
</div>
<div class="toc">
<ul class="toc">
    <li class="toc"><a href="#description" class="toc">DESCRIPTION</a></li>
    <li class="toc"><a href="#notes" class="toc">NOTES</a></li>
    <li class="toc"><a href="#examples" class="toc">EXAMPLES</a></li>
    <li class="toc"><a href="#see-also" class="toc">SEE ALSO</a></li>
    <li class="toc"><a href="#author" class="toc">AUTHOR</a></li>
</ul>
</div>
<h2><a name="description">DESCRIPTION</a></h2>

For basic information about GRASS <em>mapset</em>, <em>location</em>
and <em>data base</em> refer to <a href="helptext.html">GRASS
Quickstart</a>.

<p>
A <em>mapset</em> holds a distinct set of data layers, each relevant
to the same (or a subset of the same) geographic region, and each
drawn in the same map coordinate system.  At the outset of every GRASS
session, the user identifies a GRASS data base, location, and mapset
that are to be the user's <em>current data base</em>, <em>current
location</em>, and <em>current mapset</em> for the duration of the
session; any maps created by the user during the session will be
stored under the <em>current mapset</em> set at the session's outset
(see <em><a href="g.mapset.html">g.mapset</a></em> [without an "s"]
and <em><a href="g.gisenv.html">g.gisenv</a></em> for changing the
mapset with a session).

<p>
The user can add, modify, and delete data layers that exist under
his <em>current mapset</em>. Although the user can
also <em>access</em> (i.e., use) data that are stored under
<em>other</em> mapsets in the same GRASS location using the
<tt>mapname@mapsetname</tt> notation or mapset search path, the user
can only make permanent changes (create or modify data)
located in the <em>current mapset</em>.  The user's
<em>mapset search path</em> lists the order in which other mapsets in
the same GRASS location can be searched and their data accessed by the
user. The user can modify the listing and order in which these mapsets
are accessed by modifying the mapset search path; this can be done
using the <em>g.mapsets</em> command. This program allows the user to
use other's relevant map data without altering the original data
layer, and without taking up disk space with a copy of the original
map. The <tt>mapname@mapsetname</tt> notation may be used irrespective
of the mapset search path, i.e., any map found in another mapset with
sufficient <em><a href="g.access.html">g.access</a></em> privileges
may be called in such a manner.

<p>
<em>g.mapsets</em> shows the user available mapsets under the current
GRASS location, lists mapsets to which the user currently has access,
and lists the order in which accessible mapsets will be accessed by
GRASS programs searching for data files.  The user is then given the
opportunity to add or delete mapset names from his search path, or
modify the order in which mapsets will be accessed.

<p>
When the user specifies the name of a data base element file (e.g., a
particular vector map, raster map, <a href="i.group.html">imagery</a>
group file, etc.) to a GRASS program, the program searches for the
named file under each of the mapsets listed in the user's mapset
search path in the order listed there until the program finds a file
of the given name. Users can also specify a file by its mapset, to
make explicit the mapset from which the file is to be drawn; e.g., the
command:

<div class="code"><pre>
g.copy rast=soils.file@PERMANENT,my.soils
</pre></div>

ensures that a new file named <tt>my.soils</tt> is to be a copy of
the file <tt>soils.file</tt> from the mapset PERMANENT.

<p>
It is common for a user to have the special mapset
<b>PERMANENT</b> included in his mapset search path, as this mapset
typically contains finished base maps relevant to many
applications. Often, other mapsets which contain sets of interpreted
maps will be likewise included in the user's mapset search path.
Suppose, for example, that the mapset <em>Soil_Maps</em> contains
interpreted soils map layers to which the user wants access. The
mapset <em>Soil_Maps</em> should then be included in the user's
<em>search path</em> variable.

<p>
The <em>mapset search path</em> is saved as part of the current
mapset. When the user works with that mapset in subsequent GRASS
sessions, the previously saved mapset search path will be used (and
will continue to be used until it is modified by the user
with <em>g.mapsets</em>).

<h2><a name="notes">NOTES</a></h2>

By default <em>g.mapsets</em> adds to the current <em>mapset search
path</em> mapsets named by <b>mapset</b> option. Alternatively mapsets
can be removed (<b>operation=remove</b>) from the search path or
defined by <b>operation=set</b>.

<p>
Users can restrict others' access to their mapset files through use
of <em><a href="g.access.html">g.access</a></em>. Mapsets to which
access is restricted can still be listed in another's mapset search
path; however, access to these mapsets will remain restricted.

<h2><a name="examples">EXAMPLES</a></h2>

All available mapsets in the current location can be printed out by

<div class="code"><pre>
g.mapsets -l

Available mapsets:
PERMANENT user1 user2
</pre></div>

Add mapset 'user2' to the current mapset search path

<div class="code"><pre>
g.mapsets mapset=user2 operation=add
</pre></div>

The current mapset search path is changed accordingly

<div class="code"><pre>
g.mapsets -p

Accessible mapsets:
user1 user2
</pre></div>

Overwrite current search path

<div class="code"><pre>
g.mapsets mapset=user1,PERMANENT operation=set
</pre></div>

Alternatively the current mapset can be defined by a shortcut &quot;.&quot;

<div class="code"><pre>
g.mapsets mapset=.,PERMANENT operation=set
</pre></div>

<i>Note:</i> The current mapset will be always included in the search
path on the first position even if you change its position or omit the
current mapset from the <b>mapset</b> option.

<div class="code"><pre>
g.mapsets -p

Accessible mapsets:
user1 PERMANENT
</pre></div>

<h2><a name="see-also">SEE ALSO</a></h2>

<em>
  <a href="g.access.html">g.access</a>,
  <a href="g.copy.html">g.copy</a>,
  <a href="g.gisenv.html">g.gisenv</a>,
  <a href="g.list.html">g.list</a>,
  <a href="g.mapset.html">g.mapset</a>
</em>

<h2><a name="author">AUTHOR</a></h2>

Michael Shapiro, U.S.Army Construction Engineering Research Laboratory<br>
Greg Koerper, ManTech Environmental Technology, Inc.<br>
Updated to GRASS 7 by Martin Landa, Czech Technical University in Prague, Czech Republic

<p>
<i>Last changed: $Date: 2013-02-17 11:33:10 +0100 (Sun, 17 Feb 2013) $</i><hr class="header">
<p><a href="index.html">Main index</a> | <a href="general.html">General index</a> | <a href="topics.html">Topics index</a> | <a href="keywords.html">Keywords Index</a> | <a href="full_index.html">Full index</a></p>
<p>&copy; 2003-2014 <a href="http://grass.osgeo.org">GRASS Development Team</a>, GRASS GIS 7.1.svn Reference Manual</p>
</body>
</html>
