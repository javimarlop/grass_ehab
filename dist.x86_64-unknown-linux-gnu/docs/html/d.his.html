<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title>GRASS GIS manual: d.his</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="grassdocs.css" type="text/css">
</head>
<body bgcolor="white">

<a href="index.html"><img src="grass_logo.png" alt="GRASS logo"></a>
<hr class="header">

<h2>NAME</h2>
<em><b>d.his</b></em>  - Displays the result obtained by combining hue, intensity, and saturation (his) values from user-specified input raster map layers.
<h2>KEYWORDS</h2>
display, graphics, color transformation, RGB, HIS, IHS
<h2>SYNOPSIS</h2>
<div id="name"><b>d.his</b><br></div>
<b>d.his help</b><br>
<div id="synopsis"><b>d.his</b> [-<b>n</b>] <b>h_map</b>=<em>string</em>  [<b>i_map</b>=<em>string</em>]   [<b>s_map</b>=<em>string</em>]   [<b>brighten</b>=<em>integer</em>]   [--<b>help</b>]  [--<b>verbose</b>]  [--<b>quiet</b>] 
</div>

<div id="flags">
<h3>Flags:</h3>
<dl>
<dt><b>-n</b></dt>
<dd>Respect NULL values while drawing</dd>

<dt><b>--help</b></dt>
<dd>Print usage summary</dd>
<dt><b>--verbose</b></dt>
<dd>Verbose module output</dd>
<dt><b>--quiet</b></dt>
<dd>Quiet module output</dd>
</dl>
</div>

<div id="parameters">
<h3>Parameters:</h3>
<dl>
<dt><b>h_map</b>=<em>string</em>&nbsp;<b>[required]</b></dt>
<dd>Name of layer to be used for HUE</dd>

<dt><b>i_map</b>=<em>string</em></dt>
<dd>Name of layer to be used for INTENSITY</dd>

<dt><b>s_map</b>=<em>string</em></dt>
<dd>Name of layer to be used for SATURATION</dd>

<dt><b>brighten</b>=<em>integer</em></dt>
<dd>Percent to brighten intensity channel</dd>
<dd>Options: <em>-99-99</em></dd>
<dd>Default: <em>0</em></dd>

</dl>
</div>
<div class="toc">
<ul class="toc">
    <li class="toc"><a href="#description" class="toc">DESCRIPTION</a></li>
    <li class="toc"><a href="#options" class="toc">OPTIONS</a></li>
    <li class="toc"><a href="#the-process" class="toc">THE PROCESS</a></li>
    <li class="toc"><a href="#notes" class="toc">NOTES</a></li>
    <li class="toc"><a href="#example" class="toc">EXAMPLE</a>
    <ul class="toc">
        <li class="toc"><a href="#spearfish-dataset" class="toc">Spearfish dataset</a></li>
    </ul></li>
    <li class="toc"><a href="#see-also" class="toc">SEE ALSO</a></li>
    <li class="toc"><a href="#author" class="toc">AUTHOR</a></li>
</ul>
</div>
<h2><a name="description">DESCRIPTION</a></h2>

<em>HIS</em> stands for hue, intensity, and saturation.
This program produces a raster map layer providing a
visually pleasing combination of hue, intensity, and
saturation values from two or three user-specified raster
map layers.

<p>
The human brain automatically interprets the vast amount of
visual information available according to basic rules. 
Color, or <em>hue</em>, is used to categorize objects. 
Shading, or <em>intensity</em>, is interpreted as
three-dimensional texturing. Finally, the degree of
haziness, or <em>saturation</em>, is associated with
distance or depth. This program allows data from up to
three raster map layers to be combined into an image which
retains the original information in terms of <em>hue</em>,
<em>intensity</em>, and <em>saturation</em>.

<h2><a name="options">OPTIONS</a></h2>

This program can be run non-interactively or
interactively.  It will run non-interactively if the user
specifies on the command line the name of a map containing
hue values (<b>h_map</b>), and the name(s) of map(s)
containing intensity values (<b>i_map</b>) and/or
saturation values (<b>s_map</b>).  The resulting image will
be displayed in the active display frame on the graphics
monitor.

<p>
Alternately, the user can run the program interactively by
typing <b>d.his</b> without naming parameter values on the
command line.  In this case, the program will prompt the
user for parameter values using the standard GRASS 
GUI interface.

<p>
While any raster map layer can be used to represent the hue
information, map layers with a few very distinct colors
work best.  Only raster map layers representing
continuously varying data like elevation, aspect, weights,
intensities, or amounts can suitably be used to provide
intensity and saturation information.

<p>
For example, a visually pleasing image can be
made by using a watershed map for the <em>hue</em> factor,
an aspect map for the <em>intensity</em> factor, and an
elevation map for <em>saturation</em>.  (The user may wish
to leave out the elevation information for a first try.)
Ideally, the resulting image should resemble the view from
an aircraft looking at a terrain on a sunny day with a bit
of haze in the valleys.

<p>The <b>brighten</b> option does not truly represent a percentage,
but calling it that makes the option easy to understand, and it
sounds better than <i>Normalized Scaling Factor</i>.


<h2><a name="the-process">THE PROCESS</a></h2>

Each map cell is processed individually. First, the working
color is set to the color of the corresponding cell in the
map layer chosen to represent <em>HUE</em>.  Second, this
color is multiplied by the <em>red</em> intensity of that
cell in the <em>INTENSITY</em> map layer.  This map layer
should have an appropriate gray-scale color table
associated with it. You can ensure this by using the color
manipulation capabilities of
<em><a href="r.colors.html">r.colors</a></em>.

Finally, the color is made somewhat gray-based on the
<em>red</em> intensity of that cell in the
<em>SATURATION</em> map layer.  Again, this map layer
should have a gray-scale color table associated with it.

<h2><a name="notes">NOTES</a></h2>

The name is misleading. The actual conversion used is

<pre>
  <U>H</U>.i.s + <U>G</U>.(1-s)

where

  <U>H</U>   is the R,G,B color from the hue map
  i   is the red value from the intensity map
  s   is the red value from the saturation map
  <U>G</U>   is 50% gray (R = G = B = 0.5)

</pre>

<p>
Either (but not both) of the intensity or the saturation
map layers may be omitted. This means that it is possible
to produce output images that represent combinations of
<em>his, hi,</em> or <em>hs</em>.

<p>Users wishing to store the result in new raster map layers
instead of displaying it on the monitor should use the
program <em><a href="r.his.html">r.his</a></em>.


<h2><a name="example">EXAMPLE</a></h2>
<h3><a name="spearfish-dataset">Spearfish dataset</a></h3>

<div class="code"><pre>
g.region rast=elevation.dem
r.shaded.relief map=elevation.dem shad=elev.shad_relf
d.mon wx0
d.his h=elevation.dem i=elev.shad_relf brighten=50
</pre></div>

<h2><a name="see-also">SEE ALSO</a></h2>

<em>
<a href="d.colortable.html">d.colortable</a>,
<a href="d.frame.html">d.frame</a>,
<a href="d.rgb.html">d.rgb</a>,
<a href="r.colors.html">r.colors</a>,
<a href="r.his.html">r.his</a>,
<a href="i.his.rgb.html">i.his.rgb</a>,
<a href="i.rgb.his.html">i.rgb.his</a>
</em>

<h2><a name="author">AUTHOR</a></h2>

James Westervelt, U.S. Army Construction Engineering Research Laboratory

<p><i>Last changed: $Date: 2012-11-25 11:59:42 +0100 (Sun, 25 Nov 2012) $</i><hr class="header">
<p><a href="index.html">Main index</a> | <a href="display.html">Display index</a> | <a href="topics.html">Topics index</a> | <a href="keywords.html">Keywords Index</a> | <a href="full_index.html">Full index</a></p>
<p>&copy; 2003-2014 <a href="http://grass.osgeo.org">GRASS Development Team</a>, GRASS GIS 7.1.svn Reference Manual</p>
</body>
</html>
