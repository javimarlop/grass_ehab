<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title>GRASS GIS manual: i.pansharpen</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="grassdocs.css" type="text/css">
</head>
<body bgcolor="white">

<a href="index.html"><img src="grass_logo.png" alt="GRASS logo"></a>
<hr class="header">

<h2>NAME</h2>
<em><b>i.pansharpen</b></em>  - Image fusion algorithms to sharpen multispectral with high-res panchromatic channels
<h2>KEYWORDS</h2>
imagery, fusion, sharpen, Brovey, IHS, HIS, PCA
<h2>SYNOPSIS</h2>
<div id="name"><b>i.pansharpen</b><br></div>
<b>i.pansharpen help</b><br>
<div id="synopsis"><b>i.pansharpen</b> [-<b>sl</b>] <b>sharpen</b>=<em>string</em> <b>ms3</b>=<em>name</em> <b>ms2</b>=<em>name</em> <b>ms1</b>=<em>name</em> <b>pan</b>=<em>name</em> <b>output_prefix</b>=<em>string</em>  [--<b>overwrite</b>]  [--<b>help</b>]  [--<b>verbose</b>]  [--<b>quiet</b>] 
</div>

<div id="flags">
<h3>Flags:</h3>
<dl>
<dt><b>-s</b></dt>
<dd>Serial processing rather than parallel processing</dd>

<dt><b>-l</b></dt>
<dd>Rebalance blue channel for landsat maps</dd>

<dt><b>--overwrite</b></dt>
<dd>Allow output files to overwrite existing files</dd>
<dt><b>--help</b></dt>
<dd>Print usage summary</dd>
<dt><b>--verbose</b></dt>
<dd>Verbose module output</dd>
<dt><b>--quiet</b></dt>
<dd>Quiet module output</dd>
</dl>
</div>

<div id="parameters">
<h3>Parameters:</h3>
<dl>
<dt><b>sharpen</b>=<em>string</em>&nbsp;<b>[required]</b></dt>
<dd>Choose pan sharpening method</dd>
<dd>Options: <em>brovey, ihs, pca</em></dd>
<dd>Default: <em>ihs</em></dd>

<dt><b>ms3</b>=<em>name</em>&nbsp;<b>[required]</b></dt>
<dd>Input raster map for red channel</dd>

<dt><b>ms2</b>=<em>name</em>&nbsp;<b>[required]</b></dt>
<dd>Input raster map for green channel</dd>

<dt><b>ms1</b>=<em>name</em>&nbsp;<b>[required]</b></dt>
<dd>Input raster map for blue channel</dd>

<dt><b>pan</b>=<em>name</em>&nbsp;<b>[required]</b></dt>
<dd>Input raster map for high resolution panchromatic channel</dd>

<dt><b>output_prefix</b>=<em>string</em>&nbsp;<b>[required]</b></dt>
<dd>Prefix for output raster maps</dd>

</dl>
</div>
<div class="toc">
<ul class="toc">
    <li class="toc"><a href="#description" class="toc">DESCRIPTION</a></li>
    <li class="toc"><a href="#notes" class="toc">NOTES</a></li>
    <li class="toc"><a href="#examples" class="toc">EXAMPLES</a></li>
    <li class="toc"><a href="#see-also" class="toc">SEE ALSO</a></li>
    <li class="toc"><a href="#references" class="toc">REFERENCES</a></li>
    <li class="toc"><a href="#authors" class="toc">AUTHORS</a></li>
</ul>
</div>
<h2><a name="description">DESCRIPTION</a></h2>

<em><b>i.pansharpen</b></em> uses a high resolution panchromatic band from a
multispectral image to sharpen 3 lower resolution bands. The 3
lower resolution bands can then be combined into an RGB color image at a
higher (more detailed) resolution than is possible using the original 3
bands. For example, Landsat ETM has low resolution spectral bands 1 (blue),
2 (green), 3 (red), 4 (near IR), 5 (mid-IR), and 7 (mid-IR) at 30m resolution, 
and a high resolution panchromatic band 8 at 15m resolution. Pan sharpening
allows bands 3-2-1 (or other combinations of 30m resolution bands like 4-3-2 
or 5-4-2) to be combined into a 15m resolution color image.
<br><br>
i.pansharpen offers a choice of three different 'pan sharpening' 
algorithms: IHS, Brovey, and PCA.
<br><br>
For <em>IHS pan sharpening</em>, the original 3 lower resolution bands, selected 
as red, green and blue channels for creating an RGB composite image, are 
transformed into IHS (intensity, hue, and saturation) color space. The 
panchromatic band is then substituted for the intensity channel (I), combined 
with the original hue (H) and saturation (S) channels, and transformed back to 
RGB color space at the higher resolution of the panchromatic band. The 
algorithm for this can be represented as: RGB -> IHS -> [pan]HS -> RGB.
<br><br>
With a <em>Brovey pan sharpening</em>, each of the 3 lower resolution bands and 
panchromatic band are combined using the following algorithm to calculate 
3 new bands at the higher resolution (example for band 1):
<pre>
                         band1 
    new band1 = ----------------------- * panband
                 band1 + band2 + band3
</pre>
In <em>PCA pan sharpening</em>, a principal component analysis is performed on the 
original 3 lower resolution bands to create 3 principal component images
(PC1, PC2, and PC3) and their associated eigenvectors (EV), such that:
<pre>
    
     band1  band2  band3
PC1: EV1-1  EV1-2  EV1-3
PC2: EV2-1  EV2-2  EV2-3
PC3: EV3-1  EV3-2  EV3-3  

and

PC1 = EV1-1 * band1 + EV1-2 * band2 + EV1-3 * band3 - mean(bands 1,2,3)

</pre>
An inverse PCA is then performed, substituting the panchromatic band for PC1. 
To do this, the eigenvectors matrix is inverted (in this case transposed), the 
PC images are multiplied by the eigenvectors with the panchromatic band 
substituted for PC1, and mean of each band is added to each transformed image 
band using the following algorithm (example for band 1):
<pre>

band1' = pan * EV1-1 + PC2 * EV2-1 + PC3 * EV3-1 + mean(band1)
   
</pre>
The assignment of the channels depends on the satellite. Examples of satellite 
imagery with high resolution panchromatic bands, and lower resolution spectral 
bands include Landsat 7 ETM, QuickBird, and SPOT.
<br>
<h2><a name="notes">NOTES</a></h2>
The module currently only works for 8-bit images.
<br><br>
The command temporarily changes the computational region to the high 
resolution of the panchromatic band during sharpening calculations, then 
restores the previous region settings. The current region coordinates (and 
null values) are respected. The high resolution panchromatic image is 
histogram matched to the band it is replaces prior to substitution (i.e., the 
intensity channel for IHS sharpening, the low res band selected for each color 
channel with Brovey sharpening, and the PC1 image for PCA sharpening).
<br><br>
By default, the command will attempt to employ parallel processing, using 
up to 3 cores simultaneously. The -s flag will disable parallel processing, 
but does use an optimized r.mapcalc expression to reduce disk I/O.
<br><br>
The three pan-sharpened output channels may be combined with <em>d.rgb</em> or 
<em>r.composite</em>. Colors may be optionally optimized with <em>i.landsat.rgb</em>.
While the resulting color image will be at the higher resolution in all cases, 
the 3 pan sharpening algorithms differ in terms of spectral response.  

<h2><a name="examples">EXAMPLES</a></h2>

Pan sharpening of a Landsat image from Boulder, Colorado, USA:

<div class="code"><pre>
# R, G, B composite at 30m 
g.region rast=p034r032_7dt20010924_z13_10 -p
d.rgb b=p034r032_7dt20010924_z13_10 g=lp034r032_7dt20010924_z13_20 
    r=p034r032_7dt20010924_z13_30

# i.pansharpen with IHS algorithm
i.pansharpen ms3=p034r032_7dt20010924_z13_30 ms2=p034r032_7dt20010924_z13_20 
    ms1=p034r032_7dt20010924_z13_10 pan=p034r032_7dp20010924_z13_80 
    output_prefix=ihs321 sharpen=ihs

# display at 15m
g.region rast=ihs321_blue -p
d.rgb b=ihs321_blue g=ihs321_green r=ihs321_red
</pre></div>


<b><em>Results:</em></b>

<p><center>
  <table border=1>
  <tr>
    <td align=center>
      &nbsp;<img src="rgb_landsat321.jpg" alt="R, G, B composite of Landsat at 30m">
      <br>
      <font size="-1">
      <i>R, G, B composite of Landsat at 30m</i>
      </font>
    </td>
    <td align=center>
      &nbsp;<img src="rgb_brovey321.jpg" alt="R, G, B composite of Brovey sharpened image at 15m">
      <br>
      <font size="-1">
      <i>R, G, B composite of Brovey sharpened image at 15m</i>
      </font>
    </td>
  </tr>
  <tr>
    <td align=center>
      &nbsp;<img src="rgb_ihs321.jpg" alt="R, G, B composite of IHS sharpened image at 15m">
      <br>
      <font size="-1">
      <i>R, G, B composite of IHS sharpened image at 15m</i>
      </font>
    </td>
    <td align=center>
      &nbsp;<img src="rgb_pca321.jpg" alt="R, G, B composite of PCA sharpened image at 15m">
      <br>
      <font size="-1">
      <i>R, G, B composite of PCA sharpened image at 15m"</i>
      </font>
    </td>
  </tr>
  </table>
</center>
<br>


Example: LANDSAT ETM+ (Landsat 7), North Carolina sample dataset:

<div class="code"><pre>
# original at 28m
g.region rast=lsat7_2002_10 -p

d.mon wx0
d.rgb b=lsat7_2002_10 g=lsat7_2002_20 r=lsat7_2002_30

# i.pansharpen with IHS algorithm
i.pansharpen ms3=lsat7_2002_30@PERMANENT \
  ms2=lsat7_2002_20 ms1=lsat7_2002_10 \
  pan=lsat7_2002_80 sharpen=ihs \
  output_prefix=lsat7_2002_ihs

# display at 14.25m
g.region rast=lsat7_2002_ihs_red -p
d.rgb r=lsat7_2002_ihs_red g=lsat7_2002_ihs_green b=lsat7_2002_ihs_blue

# compare before/after (RGB support in "Advanced"):
g.gui.mapswipe

# optionally color balancing:
i.landsat.rgb r=lsat7_2002_ihs_red g=lsat7_2002_ihs_green b=lsat7_2002_ihs_blue
</pre></div>

<h2><a name="see-also">SEE ALSO</a></h2>

<em>
<a href="i.his.rgb.html">i.his.rgb</a>,
<a href="i.rgb.his.html">i.rgb.his</a>,
<a href="i.pca">i.pca</a>,
<a href="d.rgb.html">d.rgb</a>,
<a href="r.composite.html">r.composite</a>
</em>


<h2><a name="references">REFERENCES</a></h2>

<ul>
<li>Original Brovey formula reference unknown, probably... <br>
   Roller, N.E.G. and Cox, S., (1980). Comparison of Landsat MSS
   and merged MSS/RBV data for analysis of natural vegetation.
   Proc. of the 14th International Symposium on Remote Sensing
   of Environment, San Jose, Costa Rica, 23-30 April, pp. 1001-1007

<li>Amarsaikhan, D., & Douglas, T. (2004). Data fusion and multisource image 
   classification. International Journal of Remote Sensing, 25(17), 3529-3539.

<li>Behnia, P. (2005). Comparison between four methods for data fusion of ETM+ 
   multispectral and pan images. Geo-spatial Information Science, 8(2), 98-103.
   
<li>Du, Q., Younan, N. H., King, R., & Shah, V. P. (2007). On the Performance 
   Evaluation of Pan-Sharpening Techniques. Geoscience and Remote Sensing 
   Letters, IEEE, 4(4), 518-522.

<li>Karathanassi, V., Kolokousis, P., & Ioannidou, S. (2007). A comparison 
   study on fusion methods using evaluation indicators. International Journal 
   of Remote Sensing, 28(10), 2309-2341.

<li>Neteler, M, D. Grasso, I. Michelazzi, L. Miori, S. Merler, and C.
   Furlanello (2005). An integrated toolbox for image registration, fusion and 
   classification. International Journal of Geoinformatics, 1(1):51-61
   (<a href="http://www.grassbook.org/neteler/papers/neteler2005_IJG_051-061_draft.pdf">PDF</a>)

<li>Pohl, C, and J.L van Genderen (1998). Multisensor image fusion in remote 
    sensing: concepts, methods and application. Int. J. of Rem. Sens., 19, 823-854.
</ul>



<h2><a name="authors">AUTHORS</a></h2>

Michael Barton (Arizona State University, USA)--with contributions from Markus 
Neteler (ITC-irst, Italy); Glynn Clements; Luca Delucchi (Fondazione E. Mach, 
Italy); Markus Metz; and Hamish Bowman. 
<p><i>Last changed: $Date: 2014-06-03 10:03:51 +0200 (Tue, 03 Jun 2014) $</i><hr class="header">
<p><a href="index.html">Main index</a> | <a href="imagery.html">Imagery index</a> | <a href="topics.html">Topics index</a> | <a href="keywords.html">Keywords Index</a> | <a href="full_index.html">Full index</a></p>
<p>&copy; 2003-2014 <a href="http://grass.osgeo.org">GRASS Development Team</a>, GRASS GIS 7.1.svn Reference Manual</p>
</body>
</html>
