<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title>GRASS GIS manual: r.patch</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="grassdocs.css" type="text/css">
</head>
<body bgcolor="white">

<a href="index.html"><img src="grass_logo.png" alt="GRASS logo"></a>
<hr class="header">

<h2>NAME</h2>
<em><b>r.patch</b></em>  - Creates a composite raster map layer by using known category values from one (or more) map layer(s) to fill in areas of "no data" in another map layer.
<h2>KEYWORDS</h2>
raster, geometry
<h2>SYNOPSIS</h2>
<div id="name"><b>r.patch</b><br></div>
<b>r.patch help</b><br>
<div id="synopsis"><b>r.patch</b> [-<b>z</b>] <b>input</b>=<em>name</em>[,<i>name</i>,...] <b>output</b>=<em>name</em>  [--<b>overwrite</b>]  [--<b>help</b>]  [--<b>verbose</b>]  [--<b>quiet</b>] 
</div>

<div id="flags">
<h3>Flags:</h3>
<dl>
<dt><b>-z</b></dt>
<dd>Use zero (0) for transparency instead of NULL</dd>

<dt><b>--overwrite</b></dt>
<dd>Allow output files to overwrite existing files</dd>
<dt><b>--help</b></dt>
<dd>Print usage summary</dd>
<dt><b>--verbose</b></dt>
<dd>Verbose module output</dd>
<dt><b>--quiet</b></dt>
<dd>Quiet module output</dd>
</dl>
</div>

<div id="parameters">
<h3>Parameters:</h3>
<dl>
<dt><b>input</b>=<em>name[,<i>name</i>,...]</em>&nbsp;<b>[required]</b></dt>
<dd>Name of raster maps to be patched together</dd>

<dt><b>output</b>=<em>name</em>&nbsp;<b>[required]</b></dt>
<dd>Name for resultant raster map</dd>

</dl>
</div>
<div class="toc">
<ul class="toc">
    <li class="toc"><a href="#description" class="toc">DESCRIPTION</a></li>
    <li class="toc"><a href="#example" class="toc">EXAMPLE</a></li>
    <li class="toc"><a href="#notes" class="toc">NOTES</a></li>
    <li class="toc"><a href="#example" class="toc">EXAMPLE</a></li>
    <li class="toc"><a href="#see-also" class="toc">SEE ALSO</a></li>
    <li class="toc"><a href="#author" class="toc">AUTHOR</a></li>
</ul>
</div>
<h2><a name="description">DESCRIPTION</a></h2>

The GRASS program <em>r.patch</em> allows the user to build a new
raster map the size and resolution of the current region by assigning
known data values from input raster maps to the cells in this region.
This is done by filling in "no data" cells, those that do not yet
contain data, contain NULL data, or, optionally contain 0 data,
with the data from the first input map.
Once this is done the remaining holes are filled in by the next input map,
and so on.
This program
is useful for making a composite raster map layer from two or more adjacent
map layers, for filling in "holes" in a raster map layer's data (e.g., in
digital elevation data), or for updating an older map layer with more recent
data. The current geographic region definition and mask settings are
respected.
<p>The first <em>name</em> listed in the string
<b>input=</b><em>name</em>,<em>name</em>,<em>name</em>, ... is the name of
the first map whose data values will be used to fill in "no data" cells
in the current region. The second through last input <em>name</em>
maps will be used, in order, to supply data values for for the remaining
"no data" cells.

<h2><a name="example">EXAMPLE</a></h2>

Below, the raster map layer on the far left is <b>patched</b> 
with the middle (<em>patching</em>) raster map layer, 
to produce the <em>composite</em> raster map layer on the right. 

<pre>
  1 1 1 0 2 2 0 0    0 0 1 1 0 0 0 0    1 1 1 1 2 2 0 0
  1 1 0 2 2 2 0 0    0 0 1 1 0 0 0 0    1 1 1 2 2 2 0 0
  3 3 3 3 2 2 0 0    0 0 0 0 0 0 0 0    3 3 3 3 2 2 0 0
  3 3 3 3 0 0 0 0    4 4 4 4 4 4 4 4    3 3 3 3 4 4 4 4
  3 3 3 0 0 0 0 0    4 4 4 4 4 4 4 4    3 3 3 4 4 4 4 4
  0 0 0 0 0 0 0 0    4 4 4 4 4 4 4 4    4 4 4 4 4 4 4 4
</pre>

Switching the <em>patched</em> and the <em>patching</em> raster map layers 
produces the following results: 

<pre>
  0 0 1 1 0 0 0 0    1 1 1 0 2 2 0 0    1 1 1 1 2 2 0 0
  0 0 1 1 0 0 0 0    1 1 0 2 2 2 0 0    1 1 1 1 2 2 0 0
  0 0 0 0 0 0 0 0    3 3 3 3 2 2 0 0    3 3 3 3 2 2 0 0
  4 4 4 4 4 4 4 4    3 3 3 3 0 0 0 0    4 4 4 4 4 4 4 4
  4 4 4 4 4 4 4 4    3 3 3 0 0 0 0 0    4 4 4 4 4 4 4 4
  4 4 4 4 4 4 4 4    0 0 0 0 0 0 0 0    4 4 4 4 4 4 4 4
</pre>

<h2><a name="notes">NOTES</a></h2>

Frequently, this program is used to patch together adjacent map layers which
have been digitized separately.  The program 
<em><a href="v.mkgrid.html">v.mkgrid</a></em> can be used to make adjacent
maps align neatly.

<p>The user should check the current geographic region settings before running 
<em>r.patch</em>, to ensure that the region boundaries encompass all 
of the data desired to be included in the composite map and to ensure that the
region resolution is the resolution of the desired data. To set the
geographic region settings to one or several raster maps, the <em>g.region</em>
program can be used:

<div class="code"><pre>
g.region rast=map1[,map2[,...]]
</pre></div>

<p>
Use of <em>r.patch</em> is generally followed by use of the GRASS programs 
<em><a href="g.remove.html">g.remove</a></em> and 
<em><a href="g.rename.html">g.rename</a></em>;
<em>g.remove</em> is used to remove the original (un-patched) raster map
layers, while <em>g.rename</em> is used to then assign to the newly-created
composite (patched) raster map layer the name of the original raster map
layer.

<p><em>r.patch</em> creates support files for the patched, composite output map. 


<h2><a name="example">EXAMPLE</a></h2>

Create a list of maps matching a pattern, extend the region to include them
all, and patch them together to create a mosaic. Overlapping maps will be 
used in the order listed.

<div class="code"><pre>
MAPS=`g.mlist type=rast sep=, pat="map_*"`
g.region rast=$MAPS
r.patch in=$MAPS out=mosaic
</pre></div>
<br>


<h2><a name="see-also">SEE ALSO</a></h2>

<em><a href="g.region.html">g.region</a></em>,
<em><a href="g.remove.html">g.remove</a></em>,
<em><a href="g.rename.html">g.rename</a></em>,
<em><a href="r.mapcalc.html">r.mapcalc</a></em>,
<em><a href="r.support.html">r.support</a></em>,
<em><a href="v.mkgrid.html">v.mkgrid</a></em>

<h2><a name="author">AUTHOR</a></h2>

Michael Shapiro, 
U.S. Army Construction Engineering Research Laboratory
<br>
-z flag and performance improvement by Huidae Cho

<p><i>Last changed: $Date: 2014-06-16 15:04:12 +0200 (Mon, 16 Jun 2014) $</i><hr class="header">
<p><a href="index.html">Main index</a> | <a href="raster.html">Raster index</a> | <a href="topics.html">Topics index</a> | <a href="keywords.html">Keywords Index</a> | <a href="full_index.html">Full index</a></p>
<p>&copy; 2003-2014 <a href="http://grass.osgeo.org">GRASS Development Team</a>, GRASS GIS 7.1.svn Reference Manual</p>
</body>
</html>
