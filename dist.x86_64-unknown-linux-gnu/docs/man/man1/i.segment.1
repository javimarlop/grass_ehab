.TH i.segment 1 "" "GRASS 7.1.svn" "Grass User's Manual"
.SH NAME
\fI\fBi.segment\fR\fR  \- Identifies segments (objects) from imagery data.
.SH KEYWORDS
imagery, segmentation, classification, object recognition
.SH SYNOPSIS
\fBi.segment\fR
.br
\fBi.segment help\fR
.br
\fBi.segment\fR [\-\fBdw\fR] \fBgroup\fR=\fIname\fR \fBoutput\fR=\fIname\fR \fBthreshold\fR=\fIfloat\fR  [\fBmethod\fR=\fIstring\fR]   [\fBsimilarity\fR=\fIstring\fR]   [\fBminsize\fR=\fIinteger\fR]   [\fBmemory\fR=\fIinteger\fR]   [\fBiterations\fR=\fIinteger\fR]   [\fBseeds\fR=\fIname\fR]   [\fBbounds\fR=\fIname\fR]   [\fBgoodness\fR=\fIname\fR]   [\-\-\fBoverwrite\fR]  [\-\-\fBhelp\fR]  [\-\-\fBverbose\fR]  [\-\-\fBquiet\fR]
.SS Flags:
.IP "\fB\-d\fR" 4m
.br
Use 8 neighbors (3x3 neighborhood) instead of the default 4 neighbors for each pixel
.IP "\fB\-w\fR" 4m
.br
Weighted input, do not perform the default scaling of input raster maps
.IP "\fB\-\-overwrite\fR" 4m
.br
Allow output files to overwrite existing files
.IP "\fB\-\-help\fR" 4m
.br
Print usage summary
.IP "\fB\-\-verbose\fR" 4m
.br
Verbose module output
.IP "\fB\-\-quiet\fR" 4m
.br
Quiet module output
.SS Parameters:
.IP "\fBgroup\fR=\fIname\fR \fB[required]\fR" 4m
.br
Name of input imagery group
.IP "\fBoutput\fR=\fIname\fR \fB[required]\fR" 4m
.br
Name for output raster map
.IP "\fBthreshold\fR=\fIfloat\fR \fB[required]\fR" 4m
.br
Difference threshold between 0 and 1
.br
Threshold = 0 merges only identical segments; threshold = 1 merges all
.IP "\fBmethod\fR=\fIstring\fR" 4m
.br
Segmentation method
.br
Options: \fIregion_growing\fR
.br
Default: \fIregion_growing\fR
.IP "\fBsimilarity\fR=\fIstring\fR" 4m
.br
Similarity calculation method
.br
Options: \fIeuclidean, manhattan\fR
.br
Default: \fIeuclidean\fR
.IP "\fBminsize\fR=\fIinteger\fR" 4m
.br
Minimum number of cells in a segment
.br
The final step will merge small segments with their best neighbor
.br
Options: \fI1\-100000\fR
.br
Default: \fI1\fR
.IP "\fBmemory\fR=\fIinteger\fR" 4m
.br
Memory in MB
.br
Default: \fI300\fR
.IP "\fBiterations\fR=\fIinteger\fR" 4m
.br
Maximum number of iterations
.br
Default: \fI20\fR
.IP "\fBseeds\fR=\fIname\fR" 4m
.br
Name for input raster map with starting seeds
.IP "\fBbounds\fR=\fIname\fR" 4m
.br
Name of input bounding/constraining raster map
.br
Must be integer values, each area will be segmented independent of the others
.IP "\fBgoodness\fR=\fIname\fR" 4m
.br
Name for output goodness of fit estimate map
.SH DESCRIPTION
Image segmentation or object recognition is the process of grouping
similar pixels into unique segments, also refered to as objects.
Boundary and region based algorithms are described in the literature,
currently a region growing and merging algorithm is implemented. Each
object found during the segmentation process is given a unique ID and
is a collection of contiguous pixels meeting some criteria. Note the
contrast with image classification where all pixels similar to each
other are assigned to the same class and do not need to be contiguous.
The image segmentation results can be useful on their own, or used as a
preprocessing step for image classification. The segmentation
preprocessing step can reduce noise and speed up the classification.
.SH NOTES
.SS Region Growing and Merging
This segmentation algorithm sequentially examines all current segments
in the raster map. The similarity between the current segment and each
of its neighbors is calculated according to the given distance
formula. Segments will be merged if they meet a number of criteria,
including:
.IP
.IP \fB1\fR
The pair is mutually most similar to each other (the similarity
distance will be smaller than to any other neighbor), and
.IP \fB2\fR
The similarity must be lower than the input threshold. The
process is repeated until no merges are made during a complete pass.
.PP
.SS Similarity and Threshold
The similarity between segments and unmerged objects is used to
determine which objects are merged. Smaller distance values indicate a
closer match, with a similarity score of zero for identical pixels.
.PP
During normal processing, merges are only allowed when the
similarity between two segments is lower than the given
threshold value. During the final pass, however, if a minimum
segment size of 2 or larger is given with the \fBminsize\fR
parameter, segments with a smaller pixel count will be merged with
their most similar neighbor even if the similarity is greater than
the threshold.
.PP
The \fBthreshold\fR must be larger than 0.0 and smaller than 1.0. A threshold
of 0 would allow only identical valued pixels to be merged, while a
threshold of 1 would allow everything to be merged. Initial empirical
tests indicate threshold values of 0.01 to 0.05 are reasonable values
to start. It is recommended to start with a low value, e.g. 0.01, and
then perform hierachical segmentation by using the output of the last
run as \fBseeds\fR for the next run.
.SS Calculation Formulas
Both Euclidean and Manhattan distances use the normal definition,
considering each raster in the image group as a dimension.
In future, the distance calculation will also take into account the
shape characteristics of the segments. The normal distances are then
multiplied by the input radiometric weight. Next an additional
contribution is added: (1\-radioweight) * {smoothness * smoothness
weight + compactness * (1\-smoothness weight)},
where compactness = Perimeter Length / sqrt( Area )
and smoothness = Perimeter Length / Bounding Box. The
perimeter length is estimated as the number of pixel sides the segment
has.
.SS Seeds
The seeds map can be used to provide either seed pixels (random or
selected points from which to start the segmentation process) or
seed segments. If the seeds are the results of a previous segmentation
with lower threshold, hierarchical segmentation can be performed. The
different approaches are automatically detected by the program: any
pixels that have identical seed values and are contiguous will be
assigned a unique segment ID.
.PP
It is expected that the \fBminsize\fR will be set to 1 if a seed
map is used, but the program will allow other values to be used. If
both options are used, the final iteration that ignores the
threshold will also ignore the seed map and force merges for all
pixels (not just segments that have grown/merged from the seeds).
.SS Maximum number of starting segments
For the region growing algorithm without starting seeds, each pixel is
sequentially numbered.  The current limit with CELL storage is 2
billion starting segment IDs.  If the initial map has a larger number
of non\-null pixels, there are two workarounds:
.IP
.IP \fB1\fR
Use starting seed pixels. (Maximum 2 billion pixels can be
labeled with positive integers.)
.IP \fB2\fR
Use starting seed segments. (By initial classification or other
methods.)
.PP
.SS Boundary Constraints
Boundary constraints limit the adjacency of pixels and segments.
Each unique value present in the \fBbounds\fR raster are
considered as a MASK. Thus no segments in the final segmentated map
will cross a boundary, even if their spectral data is very similar.
.SS Minimum Segment Size
To reduce the salt and pepper affect, a \fBminsize\fR greater
than 1 will add one additional pass to the processing. During the
final pass, the threshold is ignored for any segments smaller then
the set size, thus forcing very small segments to merge with their
most similar neighbor.
.SH EXAMPLE
This example uses the ortho photograph included in the NC Sample
Dataset. Set up an imagery group:
.br
.nf
\fC
i.group group=ortho_group input=ortho_2001_t792_1m@PERMANENT
\fR
.fi
.PP
Set the region to a smaller test region (resolution taken from
input ortho photograph).
.br
.nf
\fC
g.region \-p rast=ortho_2001_t792_1m n=220446 s=220075 e=639151 w=638592
\fR
.fi
Try out a low threshold and check the results.
.br
.nf
\fC
i.segment group=ortho_group output=ortho_segs_l1 threshold=0.02
\fR
.fi
.PP
From a visual inspection, it seems this results in too many segments.
Increasing the threshold, using the previous results as seeds,
and setting a minimum size of 2:
.br
.nf
\fC
i.segment group=ortho_group output=ortho_segs_l2 threshold=0.05 seeds=ortho_segs_l1 min=2
i.segment group=ortho_group output=ortho_segs_l3 threshold=0.1 seeds=ortho_segs_l2
i.segment group=ortho_group output=ortho_segs_l4 threshold=0.2 seeds=ortho_segs_l3
i.segment group=ortho_group output=ortho_segs_l5 threshold=0.3 seeds=ortho_segs_l4
\fR
.fi
.PP
The output ortho_segs_l4 with \fBthreshold\fR=0.2 still has
too many segments, but the output with \fBthreshold\fR=0.3 has too few
segments. A threshold value of 0.25 seems to be a good choice. There
is also some noise in the image, lets next force all segments smaller
than 10 pixels to be merged into their most similar neighbor (even if
they are less similar than required by our threshold):
.PP
Set the region to match the entire map(s) in the group.
.br
.nf
\fC
g.region \-p rast=ortho_2001_t792_1m@PERMANENT
\fR
.fi
.PP
Run \fIi.segment\fR on the full map:
.br
.nf
\fC
i.segment group=ortho_group output=ortho_segs_final threshold=0.25 min=10
\fR
.fi
.PP
Processing the entire ortho image with nearly 10 million pixels took
about 450 times more then for the final run.
.SH TODO
.SS Functionality
.RS 4n
.IP \(bu 4n
Further testing of the shape characteristics (smoothness,
compactness), if it looks good it should be added.
(\fBin progress\fR)
.IP \(bu 4n
Malahanobis distance for the similarity calculation.
.RE
.SS Use of Segmentation Results
.RS 4n
.IP \(bu 4n
Improve the optional output from this module, or better yet, add a
module for \fIi.segment.metrics\fR.
.IP \(bu 4n
Providing updates to \fIi.maxlik\fR
to ensure the segmentation output can be used as input for the
existing classification functionality.
.IP \(bu 4n
Integration/workflow for \fIr.fuzzy\fR (Addon).
.RE
.SS Speed
.RS 4n
.IP \(bu 4n
See create_isegs.c
.RE
.SH REFERENCES
This project was first developed during GSoC 2012. Project documentation,
Image Segmentation references, and other information is at the
project wiki.
.PP
Information about
classification in GRASS
is at available on the wiki.
.SH SEE ALSO
\fI
g.gui.iclass,
i.group,
i.maxlik,
i.smap,
r.kappa
\fR
.SH AUTHORS
Eric Momsen \- North Dakota State University
.br
Markus Metz (GSoC Mentor)
.PP
\fILast changed: $Date: 2013\-12\-17 22:29:07 +0100 (Tue, 17 Dec 2013) $\fR
.PP
Main index | Imagery index | Topics index | Keywords Index | Full index
.PP
� 2003\-2014 GRASS Development Team, GRASS GIS 7.1.svn Reference Manual
