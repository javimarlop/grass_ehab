.TH r.his 1 "" "GRASS 7.1.svn" "Grass User's Manual"
.SH NAME
\fI\fBr.his\fR\fR  \- Generates red, green and blue raster map layers combining hue, intensity and saturation (HIS) values from user\-specified input raster map layers.
.SH KEYWORDS
raster, color transformation, RGB, HIS, IHS
.SH SYNOPSIS
\fBr.his\fR
.br
\fBr.his help\fR
.br
\fBr.his\fR [\-\fBn\fR] \fBh_map\fR=\fIstring\fR  [\fBi_map\fR=\fIstring\fR]   [\fBs_map\fR=\fIstring\fR]  \fBr_map\fR=\fIstring\fR \fBg_map\fR=\fIstring\fR \fBb_map\fR=\fIstring\fR  [\-\-\fBoverwrite\fR]  [\-\-\fBhelp\fR]  [\-\-\fBverbose\fR]  [\-\-\fBquiet\fR]
.SS Flags:
.IP "\fB\-n\fR" 4m
.br
Respect NULL values while drawing
.IP "\fB\-\-overwrite\fR" 4m
.br
Allow output files to overwrite existing files
.IP "\fB\-\-help\fR" 4m
.br
Print usage summary
.IP "\fB\-\-verbose\fR" 4m
.br
Verbose module output
.IP "\fB\-\-quiet\fR" 4m
.br
Quiet module output
.SS Parameters:
.IP "\fBh_map\fR=\fIstring\fR \fB[required]\fR" 4m
.br
Name of layer to be used for HUE
.IP "\fBi_map\fR=\fIstring\fR" 4m
.br
Name of layer to be used for INTENSITY
.IP "\fBs_map\fR=\fIstring\fR" 4m
.br
Name of layer to be used for SATURATION
.IP "\fBr_map\fR=\fIstring\fR \fB[required]\fR" 4m
.br
Name of output layer to be used for RED
.IP "\fBg_map\fR=\fIstring\fR \fB[required]\fR" 4m
.br
Name of output layer to be used for GREEN
.IP "\fBb_map\fR=\fIstring\fR \fB[required]\fR" 4m
.br
Name of output layer to be used for BLUE
.SH DESCRIPTION
\fIHIS\fR stands for hue, intensity, and saturation.
This program produces red, green and blue raster map layers
providing a visually pleasing combination of hue,
intensity, and saturation values from two or three
user\-specified raster map layers.
.PP
The human brain automatically interprets the vast amount of
visual information available according to basic rules.
Color, or \fIhue\fR, is used to categorize objects.
Shading, or \fIintensity\fR, is interpreted as
three\-dimensional texturing. Finally, the degree of
haziness, or \fIsaturation\fR, is associated with
distance or depth. This program allows data from up to
three raster map layers to be combined into a color image
(in the form of separate red, green and blue raster map
layers) which retains the original information in terms of
\fIhue\fR, \fIintensity\fR, and \fIsaturation\fR.
.PP
While any raster map layer can be used to represent the hue
information, map layers with a few very distinct colors
work best.  Only raster map layers representing
continuously varying data like elevation, aspect, weights,
intensities, or amounts can suitably be used to provide
intensity and saturation information.
.PP
For example, a visually pleasing image can be made by using
a watershed map for the \fIhue\fR factor, an aspect map
for the \fIintensity\fR factor, and an elevation map for
\fIsaturation\fR. (The user may wish to leave out the
elevation information for a first try.) Ideally, the
resulting image should resemble the view from an aircraft
looking at a terrain on a sunny day with a bit of haze in
the valleys.
.SS The Process
Each map cell is processed individually. First, the working
color is set to the color of the corresponding cell in the
map layer chosen to represent \fIHUE\fR.  Second, this
color is multiplied by the \fIred\fR intensity of that
cell in the \fIINTENSITY\fR map layer.  This map layer
should have an appropriate gray\-scale color table
associated with it. You can ensure this by using the color
manipulation capabilities of
\fId.colors\fR or
\fIr.colors\fR.
Finally, the color is made somewhat gray\-based on the
\fIred\fR intensity of that cell in the
\fISATURATION\fR map layer.  Again, this map layer
should have a gray\-scale color table associated with it.
.SH NOTES
The name is misleading. The actual conversion used is
.br
.nf
\fC
  H.i.s + G.(1\-s)
where
  H   is the R,G,B color from the hue map
  i   is the red value from the intensity map
  s   is the red value from the saturation map
  G   is 50% gray (R = G = B = 0.5)
\fR
.fi
.PP
Either (but not both) of the intensity or the saturation
map layers may be omitted. This means that it is possible
to produce output images that represent combinations of
\fIhis, hi,\fR or \fIhs\fR.
The separate \fIred\fR, \fIgreen\fR and \fIblue\fR
maps can be displayed on the graphics monitor using
\fId.rgb\fR, or combined into
a composite RGB layer using
\fIr.composite\fR.
Users wishing to simply display an \fIhis\fR composite
image without actually generating any layers should use the
program \fId.his\fR.
.SH EXAMPLES
Recreate the following \fId.his\fR command using \fIr.his\fR:
.br
.nf
\fC
g.regiopn rast=elevation
r.shaded.relief map=elevation shad=elev.shad_relf
d.his h=elevation i=elev.shad_relf brighten=50
\fR
.fi
.br
.nf
\fC
r.mapcalc \(dqelev.shad_relf_bright50 = #elev.shad_relf * 1.5\(dq
r.colors elev.shad_relf_bright50 color=grey255
r.his h_map=elevation i_map=elev.shad_relf_bright50 r_map=esr.r g_map=esr.g bmap=esr.b
d.rgb red=esr.r green=esr.g blue=esr.b
\fR
.fi
.SH SEE ALSO
\fI
d.his,
d.colors,
d.colortable,
d.rgb,
r.blend,
r.colors,
r.composite,
r.mapcalc,
i.his.rgb,
i.rgb.his
\fR
.SH AUTHOR
Glynn Clements (based upon \fId.his\fR)
.PP
\fILast changed: $Date: 2012\-11\-25 11:59:42 +0100 (Sun, 25 Nov 2012) $\fR
.PP
Main index | Raster index | Topics index | Keywords Index | Full index
.PP
� 2003\-2014 GRASS Development Team, GRASS GIS 7.1.svn Reference Manual
