.TH v.buffer 1 "" "GRASS 7.1.svn" "Grass User's Manual"
.SH NAME
\fI\fBv.buffer\fR\fR  \- Creates a buffer around vector features of given type.
.SH KEYWORDS
vector, buffer, geometry
.SH SYNOPSIS
\fBv.buffer\fR
.br
\fBv.buffer help\fR
.br
\fBv.buffer\fR [\-\fBsct\fR] \fBinput\fR=\fIname\fR  [\fBlayer\fR=\fIstring\fR]   [\fBcats\fR=\fIrange\fR]   [\fBwhere\fR=\fIsql_query\fR]   [\fBtype\fR=\fIstring\fR[,\fIstring\fR,...]]  \fBoutput\fR=\fIname\fR  [\fBdistance\fR=\fIfloat\fR]   [\fBminordistance\fR=\fIfloat\fR]   [\fBangle\fR=\fIfloat\fR]   [\fBbufcolumn\fR=\fIname\fR]   [\fBscale\fR=\fIfloat\fR]   [\fBtolerance\fR=\fIfloat\fR]   [\-\-\fBoverwrite\fR]  [\-\-\fBhelp\fR]  [\-\-\fBverbose\fR]  [\-\-\fBquiet\fR]
.SS Flags:
.IP "\fB\-s\fR" 4m
.br
Make outside corners straight
.IP "\fB\-c\fR" 4m
.br
Don\(cqt make caps at the ends of polylines
.IP "\fB\-t\fR" 4m
.br
Transfer categories and attributes
.IP "\fB\-\-overwrite\fR" 4m
.br
Allow output files to overwrite existing files
.IP "\fB\-\-help\fR" 4m
.br
Print usage summary
.IP "\fB\-\-verbose\fR" 4m
.br
Verbose module output
.IP "\fB\-\-quiet\fR" 4m
.br
Quiet module output
.SS Parameters:
.IP "\fBinput\fR=\fIname\fR \fB[required]\fR" 4m
.br
Name of input vector map
.br
Or data source for direct OGR access
.IP "\fBlayer\fR=\fIstring\fR" 4m
.br
Layer number or name (\(cq\-1\(cq for all layers)
.br
A single vector map can be connected to multiple database tables. This number determines which table to use. When used with direct OGR access this is the layer name.
.br
Default: \fI\-1\fR
.IP "\fBcats\fR=\fIrange\fR" 4m
.br
Category values
.br
Example: 1,3,7\-9,13
.IP "\fBwhere\fR=\fIsql_query\fR" 4m
.br
WHERE conditions of SQL statement without \(cqwhere\(cq keyword
.br
Example: income < 1000 and inhab >= 10000
.IP "\fBtype\fR=\fIstring[,\fIstring\fR,...]\fR" 4m
.br
Input feature type
.br
Options: \fIpoint, line, boundary, centroid, area\fR
.br
Default: \fIpoint,line,area\fR
.IP "\fBoutput\fR=\fIname\fR \fB[required]\fR" 4m
.br
Name for output vector map
.IP "\fBdistance\fR=\fIfloat\fR" 4m
.br
Buffer distance along major axis in map units
.IP "\fBminordistance\fR=\fIfloat\fR" 4m
.br
Buffer distance along minor axis in map units
.IP "\fBangle\fR=\fIfloat\fR" 4m
.br
Angle of major axis in degrees
.br
Default: \fI0\fR
.IP "\fBbufcolumn\fR=\fIname\fR" 4m
.br
Name of column to use for buffer distances
.IP "\fBscale\fR=\fIfloat\fR" 4m
.br
Scaling factor for attribute column values
.br
Default: \fI1.0\fR
.IP "\fBtolerance\fR=\fIfloat\fR" 4m
.br
Maximum distance between theoretical arc and polygon segments as multiple of buffer
.br
Default: \fI0.01\fR
.SH DESCRIPTION
\fIv.buffer\fR creates a buffer around features of
given \fBtype\fR, which have a category in the
given \fBlayer\fR. The \fBtolerance\fR controls the number of vector
segments being generated (the smaller the value, the more vector
segments are generated).
.SH NOTES
Internal buffers for areas can be generated with negative distance
values (\(dqinward buffer\(dq or \(dqnegative buffer\(dq).
.PP
Categories and attributes can be transferred with the \fBt\fR flag.
The resulting buffer areas can have multiple categories, and multiple
buffer areas can have the same category. The buffer for the input
feature with category X can thus be retrieved by selecting all buffer
areas with category X (see example below).
.PP
Buffers for lines and areas are generated using the algorithms from
the GEOS library.
.SH EXAMPLES
All examples are based on the North Carolina sample dataset.
.SS Buffer around input lines
.br
.nf
\fC
v.buffer input=roadsmajor output=roadsmajor_buffer type=line distance=100
\fR
.fi
.br
Buffer of 100m along the \(dqroadsmajor\(dq lines (map subset, original center line
shown in black)
.SS Circles around input points
.br
.nf
\fC
v.buffer input=hospitals output=hospitals_circled type=point distance=2000
\fR
.fi
.br
Buffer of 2000m around the \(dqhospitals\(dq points (map subset, original points
shown in black, new area centroids in red)
.SS Circles around input points with attribute transfer
.br
.nf
\fC
v.buffer input=hospitals output=hospitals_circled type=point distance=1000 \-t
# display buffer around hospital with category 36,
# this buffer is composed of several areas:
d.vect map=hospitals_circled type=area layer=1 cats=36
# extract this buffer, dissolving boundaries
v.extract in=hospitals_circled output=hospital_36_circled layer=1 cats=36 \-d
\fR
.fi
.SS Buffer around input areas
.br
.nf
\fC
v.buffer input=lakes output=lakes_buffer type=area distance=100
\fR
.fi
.br
Buffer of 100m around the \(dqlakes\(dq polygons (map subset, original areas
shown in black)
.SH REFERENCE
.RS 4n
.IP \(bu 4n
GEOS Library
.RE
.SH SEE ALSO
\fI
r.buffer,
v.parallel,
v.extract,
v.type,
v.patch,
v.db.connect
\fR
.SH AUTHORS
Radim Blazek
.br
Rewritten by Rosen Matev (with support through the
Google Summer of Code program 2008)
.br
Rewritten by Markus Metz (2011, 2012)
.PP
\fILast changed: $Date: 2014\-04\-30 17:38:17 +0200 (Wed, 30 Apr 2014) $\fR
.PP
Main index | Vector index | Topics index | Keywords Index | Full index
.PP
� 2003\-2014 GRASS Development Team, GRASS GIS 7.1.svn Reference Manual
