.TH d.rast.arrow 1 "" "GRASS 7.1.svn" "Grass User's Manual"
.SH NAME
\fI\fBd.rast.arrow\fR\fR  \- Draws arrows representing cell aspect direction for a raster map containing aspect data.
.SH KEYWORDS
display, map annotations, raster
.SH SYNOPSIS
\fBd.rast.arrow\fR
.br
\fBd.rast.arrow help\fR
.br
\fBd.rast.arrow\fR [\-\fBa\fR] \fBmap\fR=\fIname\fR  [\fBtype\fR=\fIstring\fR]   [\fBarrow_color\fR=\fIstring\fR]   [\fBgrid_color\fR=\fIstring\fR]   [\fBx_color\fR=\fIstring\fR]   [\fBunknown_color\fR=\fIstring\fR]   [\fBskip\fR=\fIinteger\fR]   [\fBmagnitude_map\fR=\fIstring\fR]   [\fBscale\fR=\fIfloat\fR]   [\-\-\fBhelp\fR]  [\-\-\fBverbose\fR]  [\-\-\fBquiet\fR]
.SS Flags:
.IP "\fB\-a\fR" 4m
.br
Align grids with raster cells
.IP "\fB\-\-help\fR" 4m
.br
Print usage summary
.IP "\fB\-\-verbose\fR" 4m
.br
Verbose module output
.IP "\fB\-\-quiet\fR" 4m
.br
Quiet module output
.SS Parameters:
.IP "\fBmap\fR=\fIname\fR \fB[required]\fR" 4m
.br
Name of raster aspect map to be displayed
.IP "\fBtype\fR=\fIstring\fR" 4m
.br
Type of existing raster aspect map
.br
Options: \fIgrass, compass, agnps, answers\fR
.br
Default: \fIgrass\fR
.IP "\fBarrow_color\fR=\fIstring\fR" 4m
.br
Color for drawing arrows
.br
Default: \fIgreen\fR
.IP "\fBgrid_color\fR=\fIstring\fR" 4m
.br
Color for drawing drawing grid
.br
Color in GRASS format for drawing grid or \(dqnone\(dq not drawing
.br
Default: \fIgray\fR
.IP "\fBx_color\fR=\fIstring\fR" 4m
.br
Color for drawing null values
.br
Color in GRASS format for drawing null values (X symbol) or \(dqnone\(dq not drawing
.br
Default: \fIblack\fR
.IP "\fBunknown_color\fR=\fIstring\fR" 4m
.br
Color for showing unknown information
.br
Color in GRASS format for showing unknown information (? symbol) or \(dqnone\(dq not showing
.br
Default: \fIred\fR
.IP "\fBskip\fR=\fIinteger\fR" 4m
.br
Draw arrow every Nth grid cell
.br
Default: \fI1\fR
.IP "\fBmagnitude_map\fR=\fIstring\fR" 4m
.br
Raster map containing values used for arrow length
.IP "\fBscale\fR=\fIfloat\fR" 4m
.br
Scale factor for arrows (magnitude map)
.br
Default: \fI1.0\fR
.SH DESCRIPTION
\fId.rast.arrow\fR
is designed to help users better visualize surface water flow direction,
as indicated in an aspect raster map layer.  There are two ways to specify
the aspect layer the program is to use.  The first is to display the aspect
map layer on the graphics monitor before running \fId.rast.arrow\fR.
The second method involves setting the \fImap\fR parameter
to the name of the desired aspect map.
This allows the arrows to be drawn over any other maps already displayed
on the graphics monitor.
.PP
\fId.rast.arrow\fR will draw an arrow over each displayed cell
to indicate in which direction the cell slopes. If the aspect
layer has a category value denoting locations of \(dqunknown\(dq aspect,
\fId.rast.arrow\fR draws a question mark over the displayed cells
of that category.
Cells containing null data will be marked with an \(dqX\(dq.
You can disable drawing of null data and unknown aspect values by
setting its color to \(dqnone\(dq.
.PP
If you specify the \fImagnitude_map\fR option, arrow lengths
denoting magnitude will be extracted from the cell values of the specified
map. In this case the tail of the arrow will be centered on the source cell.
You may adjust the overall scale using the \fIscale\fR option.
\fId.rast.arrow\fR will ignore NULL and negative magnitudes, and will
warn you if the debug level is set at 5 or higher. Be aware. If your application
uses negative values for magnitude, you can use \fIr.mapcalc\fR to prepare
the magnitude map to suit your needs (absolute value, inverted direction and
so on).
.SH NOTES
By default, arrows are drawn at the size of a cell and cannot be seen if
the raster map is relatively close in scale. You can use the \fIskip\fR
option to draw arrows every n\-th cell in both directions if you are working
with relatively high resolutions. It may be useful to disable the grid in
this case, which is accomplished by setting its color to \(dqnone\(dq.
.PP
For GRASS and Compass type aspect maps, the cell values of the aspect map
will determine the corresponding direction in 360 degrees. ANSWERS type
aspect maps will be plotted in multiples of 15 degrees, and AGNPS type
aspect maps will be displayed in D8 representation, i.e. the eight multiples
of 45 degrees.
.PP
GRASS aspect maps are measured using Cartesian conventions, i.e. in degrees
counterclockwise from east. e.g.:
.br
.nf
\fC
90  North
180 West
270 South
0,360 East
\fR
.fi
They can be created from a raster elevation map with \fIr.slope.aspect\fR.
.PP
Compass type aspect maps are measured in degrees clockwise from north.
.PP
This module uses oceanographic conventions, i.e. arrows point downslope or
direction \(dqto\(dq, as opposed to atmospheric conventions (direction \(dqfrom\(dq).
.SH EXAMPLE
Convert U,V velocity component maps into magnitide,direction maps for use
with \fId.rast.arrow\fR:
.br
.nf
\fC
r.mapcalc \(dqmagnitude = sqrt(U_map^2 + V_map^2)\(dq
r.mapcalc \(dqdirection = atan(U_map, V_map)\(dq
d.rast.arrow map=direction type=grass magnitude_map=magnitude skip=3 grid=none
\fR
.fi
.SH SEE ALSO
\fId.frame\fR
.br
\fId.rast\fR
.br
\fId.rast.edit\fR
.br
\fId.rast.num\fR
.br
\fIg.region\fR
.br
\fIr.slope.aspect\fR
.br
.br
.SH AUTHORS
Original author
.br
Chris Rewerts
.br
\fIAgricultural Engineering,
.br
Purdue University\fR
.br
.br
Magnitude and 360 arrow code
.br
Hamish Bowman
.br
\fIDepartment of Marine Science,
.br
University of Otago, New Zealand\fR
.PP
\fILast changed: $Date: 2014\-05\-21 04:38:04 +0200 (Wed, 21 May 2014) $\fR
.PP
Main index | Display index | Topics index | Keywords Index | Full index
.PP
� 2003\-2014 GRASS Development Team, GRASS GIS 7.1.svn Reference Manual
