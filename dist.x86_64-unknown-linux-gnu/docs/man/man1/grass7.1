.TH grass7 1 "" "GRASS 7.1.svn" "Grass User's Manual"
.SH GRASS startup program
.SH SYNOPSIS
\fBgrass71\fR [\fB\-h\fR | \fB\-help\fR | \fB\-\-help\fR] [\fB\-v\fR | \fB\-\-version\fR]
[\fB\-c\fR | \fB\-c geofile\fR | \fB\-c EPSG:code\fR]
[\fB\-text\fR | \fB\-gtext\fR | \fB\-gui\fR]
[[[\fB<GISDBASE>/\fR]\fB<LOCATION_NAME>/\fR]
\fB<MAPSET>\fR]
.SS Flags:
.IP "\fB\-h\fR | \fB\-help\fR | \fB\-\-help\fR " 4m
.br
Prints a brief usage message and exits
.IP "\fB\-v\fR | \fB\-\-verbose\fR " 4m
.br
Prints the version of GRASS and exits
.IP "\fB\-c\fR " 4m
.br
Creates new GRASS unprojected location in specified GISDBASE
.IP "\fB\-c geofile\fR " 4m
.br
Creates new GRASS projected location in specified GISDBASE based on georeferenced file
.IP "\fB\-c EPSG:code\fR " 4m
.br
Creates new GRASS projected location in specified GISDBASE with given EPSG code
.IP "\fB\-text\fR " 4m
.br
Indicates that Text\-based User Interface should be used (skip welcome screen)
.IP "\fB\-gtext\fR " 4m
.br
Indicates that Text\-based User Interface should be used (show welcome screen)
.IP "\fB\-gui\fR " 4m
.br
Indicates that Graphical User Interface
(\fIwxGUI\fR) should be used
.SS Parameters:
.IP "\fBGISDBASE\fR " 4m
.br
Initial database directory which should be a fully qualified path
(e.g., /usr/local/share/grassdata)
.IP "\fBLOCATION_NAME\fR " 4m
.br
Initial location directory which is a subdirectory of GISDBASE
.IP "\fBMAPSET\fR " 4m
.br
Initial mapset directory which is a subdirectory of LOCATION_NAME
\fINote\fR: These parameters must be specified in one of the
following ways:
.br
.nf
\fC
    MAPSET
    LOCATION_NAME/MAPSET
    GISDBASE/LOCATION_NAME/MAPSET
\fR
.fi
.SH DESCRIPTION
.PP
This command is used to launch GRASS GIS. It will parse the command
line arguments and then initialize GRASS for the user. Since GRASS
modules require a specific environment, this program must be called
before any other GRASS module can run. The command line arguments are
optional and provide the user with a method to indicate the desired
user interface, as well as the desired mapset to work on.
.PP
The startup program will remember both the desired user interface
and mapset. Thus, the next time the user runs GRASS, typing
\fIgrass71\fR (without any options) will start GRASS with the
previous settings for the user interface and mapset selected.
.PP
If you specify a graphical user interface (\fB\-gui\fR)
the \fIgrass71\fR program will try to verify that the system you
specified exists and that you can access it successfully. If any of
these checks fail then \fIgrass71\fR will automatically switch back
to the text user interface mode.
.SH ENVIRONMENT VARIABLES
A number of environment variables are available at GRASS startup to
assist with automation and customization. Most users will not need to
bother with these.
.PP
In addition to these shell environment variables GRASS maintains a
number of GIS environment variables in the $HOME/.grass7/rc
file. User changes to this file will be read during the next startup
of GRASS. If this file becomes corrupted the user may edit it by hand
or remove it to start afresh.  See the list
of \fIimplemented GRASS variables\fR
for more information.  The rest of this help page will only consider
shell environment variables.
.PP
Note that you will need to set these variables using the
appropriate method required for the UNIX shell that you use (e.g. in a
Bash shell you must export the variables for them to
propogate).
.SS User Interface Environment Variable
.PP
The \fIgrass71\fR program will check for the existence of an
environment variable called GRASS_GUI which indicates the type of user
interface for GRASS to use. If this variable is not set
when \fIgrass71\fR is run, then it will be created and then saved
in the $HOME/.grass7/rc file for the next time GRASS is
run. It can be set to text, gtext or gui.
.PP
There is an order of precedence in the way \fIgrass71\fR
determines the user interface to use. The following is the hierarchy
from highest precedence to lowest.
.IP
.IP \fB1\fR
Command line argument
.IP \fB2\fR
Environment variable GRASS_GUI
.IP \fB3\fR
Value set in $HOME/.grass7/rc (GUI)
.IP \fB4\fR
Default value \- gui
.PP
.SS Python Environment Variables
.PP
If you choose to use \fIwxGUI\fR
interface, then the GRASS_PYTHON environment variable can be used to
override your system default python command.
.PP
Suppose for example your system has Python 2.4 installed and you
install a personal version of the Python 2.5 binaries
under $HOME/bin. You can use the above variables to have
GRASS use the Python 2.5 binaries instead.
.br
.nf
\fC
   GRASS_PYTHON=python2.5
\fR
.fi
.SS Addon Path to Extra User Scripts
This environment variable allows the user to extend the GRASS program
search paths to include locally developed/installed GRASS modules or
user scripts.
.br
.nf
\fC
   GRASS_ADDON_PATH=/usr/mytools
   GRASS_ADDON_PATH=/usr/mytools:/usr/local/othertools
\fR
.fi
.PP
In this example above path(s) would be added to the standard GRASS path
environment.
.SS Addon Base for Extra Local GRASS Addon Modules
This environment variable allows the user to extend the GRASS program
search paths to include locally installed
(see \fIg.extension\fR for details)
GRASS Addon
modules which are not distributed with the standard GRASS release.
.br
.nf
\fC
   GRASS_ADDON_BASE=/usr/grass\-addons
\fR
.fi
.PP
In this example above path would be added to the standard GRASS
path environment.
.PP
If not defined by user, this variable is set by GRASS startup program
to $HOME/.grass7/addons on GNU/Linux
and $APPDATA\(rsGRASS7\(rsaddons on MS Windows.
.SS HTML Browser Variable
The GRASS_HTML_BROWSER environment variable allows the user to set the
HTML web browser to use for displaying help pages.
.SS Location Environment Variables
.PP
The \fBSynopsis\fR section above describes options that can be used
to set the location and mapset that GRASS will use. These values can
also be set with environment variables. However, specifying the
location and mapset variables on the command line will override these
environment variables. The available variables are as follows:
.IP "LOCATION " 4m
.br
A fully qualified path to a mapset
(eg /usr/local/share/grassdata/spearfish60/PERMANENT). This
environment variable overrides the GISDBASE, LOCATION_NAME, and
MAPSET variables.
.IP "GISDBASE " 4m
.br
Initial database directory which should be a fully qualified path
(eg /usr/local/share/grassdata)
.IP "LOCATION_NAME " 4m
.br
Initial location directory which is a subdirectory of GISDBASE
.IP "MAPSET " 4m
.br
Initial mapset directory which is a subdirectory of LOCATION_NAME
.SS Running non\-interactive batch jobs
If the GRASS_BATCH_JOB environment variable is set to the \fIfull\fR
path and filename of a shell script then GRASS will be launched in a
non\-interactive way and the script will be run. The script itself can
be interactive if that is what the user requires. When it is finished
GRASS will automatically exit using the exit\-success code given by the
script. The script file must have its executable bit set.
.SH EXAMPLES
.PP
The following are some examples of how you could start GRASS
.IP "\fBgrass71\fR " 4m
.br
Start GRASS using the default user interface. The user will be
prompted to choose the appropriate location and mapset.
.IP "\fBgrass71 \-gui\fR " 4m
.br
Start GRASS using the graphical user interface. The user will be
prompted to choose the appropriate location and mapset.
.IP "\fBgrass71 \-text\fR " 4m
.br
Start GRASS using the text\-based user interface. Appropriate
location and mapset must be set by environmental variables (see
examples bellow) otherwise taken from the last GRASS session.
.IP "\fBgrass71 \-gtext\fR " 4m
.br
Start GRASS using the text\-based user interface. The user will be
prompted to choose the appropriate location and mapset.
.IP "\fBgrass71 $HOME/grassdata/spearfish60/user1\fR " 4m
.br
Start GRASS using the default user interface and automatically
launch into the given mapset, bypassing the mapset selection menu.
.IP "\fBgrass71 \-gui \-\fR " 4m
.br
Start GRASS using the graphical user interface and try to
obtain the location and mapset from environment variables.
.IP "\fBgrass71 \-c EPSG:4326 $HOME/grassdata/mylocation\fR " 4m
.br
Creates new GRASS location with EPSG code 4326 (latitude\-longitude, WGS84)
in the specified GISDBASE
.IP "\fBgrass71 \-c myvector.shp $HOME/grassdata/mylocation\fR " 4m
.br
Creates new GRASS location based on georeferenced Shapefile
.IP "\fBgrass71 \-c myraster.tif $HOME/grassdata/mylocation\fR " 4m
.br
Creates new GRASS location based on georeferenced GeoTIFF file
.SS Other examples
There are a variety of ways in which the \fIlocation environment
variables\fR (see above) can be used to specify the mapset to use.
The following are some possible examples.
.SS Example 1
The environment variables are defined as follows:
.br
.nf
\fC
LOCATION = /usr/local/share/grassdata/spearfish60/PERMANENT
GISDBASE = /usr/local/share/grassdata
LOCATION_NAME = spearfish60
MAPSET = PERMANENT
\fR
.fi
Start GRASS with the following command:
.br
.nf
\fC
grass71 \-
\fR
.fi
GRASS will start with the mapset defined by LOCATION since the LOCATION
variable overrides the other variables.
.SS Example 2
The environment variables are defined as follows:
.br
.nf
\fC
GISDBASE = /usr/local/share/grassdata
LOCATION_NAME = spearfish60
MAPSET = PERMANENT
\fR
.fi
Start GRASS with the following command:
.br
.nf
\fC
grass71 \-
\fR
.fi
GRASS will start with the mapset defined by
GISDBASE/LOCATION_NAME/MAPSET.
.br
.nf
\fC
grass71 /usr/home/grass/data/thailand/forests
\fR
.fi
GRASS will start with the
mapset /home/grass/data/thailand/forests which overrides the
environment variables.
.br
.nf
\fC
grass71 swamps
\fR
.fi
GRASS will start with the mapset defined by
GISDBASE/LOCATION_NAME/swamps since the command line argument for the
mapset overrides the environment variable MAPSET.
.br
.nf
\fC
grass71 thailand/forests
\fR
.fi
GRASS will start with the mapset defined by GISDBASE/thailand/forests
since the command line arguments for the location and mapset overrides
the environment variables LOCATION_NAME and MAPSET.
.SH CAVEAT
If you start GRASS using the \fIwxGUI\fR
interface you must have a python command in your $PATH
variable. That is, the command must be named
python and not something like python2.5. Rarely some
Python installations do not create a python command. In these
cases you can override python by GRASS_PYTHON environmental
variable.
.PP
Furthermore, if you have more than one version of Python installed,
make sure that the version you want to use with GRASS is set by
GRASS_PYTHON environmental variable.
.SH SEE ALSO
List of GRASS environment variables
.PP
GRASS GIS Web site
.br
GRASS GIS User Wiki
.br
GRASS GIS Bug Tracker
.br
GRASS GIS 7 Programmer\(cqs
Manual
.SH AUTHORS (of this page)
Justin Hickey
.br
Markus Neteler
.br
Hamish Bowman
.br
Martin Landa, Czech Technical University in Prague, Czech Republic
.PP
\fILast changed: $Date: 2014\-04\-11 15:03:39 +0200 (Fri, 11 Apr 2014) $\fR
.PP
Main index | Topics index | Keywords Index | Full index
.PP
� 2003\-2014 GRASS Development Team, GRASS GIS 7.1.svn Reference Manual
