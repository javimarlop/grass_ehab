.TH r.compress 1 "" "GRASS 7.1.svn" "Grass User's Manual"
.SH NAME
\fI\fBr.compress\fR\fR  \- Compresses and decompresses raster maps.
.SH KEYWORDS
raster, map management
.SH SYNOPSIS
\fBr.compress\fR
.br
\fBr.compress help\fR
.br
\fBr.compress\fR [\-\fBu\fR] \fBmap\fR=\fIstring\fR[,\fIstring\fR,...]  [\-\-\fBhelp\fR]  [\-\-\fBverbose\fR]  [\-\-\fBquiet\fR]
.SS Flags:
.IP "\fB\-u\fR" 4m
.br
Uncompress the map
.IP "\fB\-\-help\fR" 4m
.br
Print usage summary
.IP "\fB\-\-verbose\fR" 4m
.br
Verbose module output
.IP "\fB\-\-quiet\fR" 4m
.br
Quiet module output
.SS Parameters:
.IP "\fBmap\fR=\fIstring[,\fIstring\fR,...]\fR \fB[required]\fR" 4m
.br
Name of existing raster map(s)
.SH DESCRIPTION
\fIr.compress\fR can be used to compress and decompress
raster map layers.
.PP
During compression, this program reformats raster maps using a run\-length\-encoding
(RLE) or ZLIB\(cqs \(dqdeflate\(dq (LZ77\-based) algorithm. Raster map
layers which contain very little information (such as
boundary, geology, soils and land use maps) can be greatly
reduced in size. Some raster map layers are shrunk to
roughly 1% of their original sizes.  Raster map layers
containing complex images such as elevation and photo or
satellite images may increase slightly in size.
All newly generated raster maps are automatically stored in compressed
form (see FORMATS below).  Other modules can read both compressed
and regular (uncompressed) file formats.
.PP
As an example, the Spearfish data base raster map layer
\fIowner\fR was originally a size of 26600 bytes.  After
it was RLE compressed, the raster map became only 1249 bytes
(25351 bytes smaller).
.PP
Raster files may be decompressed manually to return them to their
original format, using the \fB\-u\fR flag of
\fIr.compress\fR. If \fIr.compress\fR is asked to
compress a raster map which is already compressed (or to
decompress an already decompressed raster map), it simply informs
the user the map is already (de)compressed and exits.
.SS TERMINOLOGY
.RS 4n
.IP \(bu 4n
INTEGER map (CELL data type): a raster map from INTEGER type (whole
numbers only)
.IP \(bu 4n
FLOAT map (FCELL data type): a raster map from FLOAT type (4 bytes,
7\-9 digits precision)
.IP \(bu 4n
DOUBLE map (DCELL data type): a raster map from DOUBLE type (8 bytes,
15\-17 digits precision)
.IP \(bu 4n
NULL: represents \(dqno data\(dq in raster maps, to be distinguished from
0 (zero) data value
.RE
.SS USED COMPRESSION ALGORITHMS
Floating point (FCELL, DCELL) raster maps never use RLE compression;
they are either compressed with ZLIB or uncompressed.
.PP
Integer (CELL) raster maps by default RLE compressed or may remain
uncompressed. If the environment variable GRASS_INT_ZLIB
exists, newly generated compressed integer (CELL type) raster maps will
be compressed using ZLIB instead of RLE compression. In the internal
cellhd file, the value for \(dqcompressed\(dq is 1 for RLE and 2 for ZLIB.
.PP
Obviously, decompression is controlled by the raster map\(cqs compression,
not the environment variable.
.SH NOTES
\fIr.compress\fR can be run either non\-interactively or
interactively.  In non\-interactive use, the user must
specify the name(s) of the raster map layer(s) to be
compressed (or decompressed) on the command line, using the
form \fBmap=\fR\fIname\fR[,\fIname\fR,...] (where
each \fIname\fR is the name of a raster map layer to be
compressed or decompressed). The default behavior is to
compress the named map(s).
.SS FORMATS
Conceptually, a raster data file consists of rows of cells,
with each row containing the same number of cells.  A cell
consists of one or more bytes.  The number of bytes per
cell depends on the category values stored in the cell.
Category values in the range 0\-255 require 1 byte per cell,
while category values in the range 256\-65535 require 2
bytes, and category values in the range above 65535 require
3 (or more) bytes per cell.
.PP
The \fBdecompressed\fR raster map format matches the
conceptual format.  For example, a raster map with 1 byte
cells that is 100 rows with 200 cells per row, consists of
20,000 bytes.  Running the UNIX command \fIls \-l\fR on
this file will show a size of 20,000.  If the cells were 2
byte cells, the file would require 40,000 bytes.  The map
layer category values start with the upper left corner cell
followed by the other cells along the northern boundary.
The byte following the last byte of that first row is the
first cell of the second row of category values (moving
from left to right).  There are no end\-of\-row markers or
other syncing codes in the raster map.  A cell header file
(\fIcellhd\fR) is used to define how this string of bytes
is broken up into rows of category values.
.PP
The \fBcompressed\fR RLE format is not so simple, but is quite
elegant in its design. It not only requires less disk space
to store the raster data, but often can result in faster
execution of graphic and analysis programs since there is
less disk I/O. There are two compressed RLE formats: the
pre\-version 3.0 format (which GRASS programs can read but
no longer produce), and the version 3.0 format (which is
automatically used when new raster map layers are
created).
.SS PRE\-3.0 FORMAT:
First 3 bytes (chars) \- These are a special code that identifies
the raster data as compressed.
.PP
Address array (long) \- array (size of the number of rows +
1) of addresses pointing to the internal start of each row.
Because each row may be a different size, this array is
necessary to provide a mapping of the data.
.PP
Row by row, beginning at the northern edge of the data, a
series of byte groups describes the data.  The number of
bytes in each group is the number of bytes per cell plus
one.  The first byte of each group gives a count (up to
255) of the number of cells that contain the category
values given by the remaining bytes of the group.
.SS POST\-3.0 FORMAT:
The 3 byte code is not used.
Instead, a field in the cell header is used to indicate compressed format.
.PP
The address array is the same.
.PP
The RLE format is the same as the pre\-3.0 RLE, except that
each row of data is preceded by a single byte containing
the number of bytes per cell for the row, and if
run\-length\-encoding the row would not require less space
than non\-run\-length\-encoding, then the row is not encoded.
.PP
These improvements give better compression than the pre\-3.0
format in 99% of the raster data layers.  The kinds of
raster data layers which get bigger are those in which each
row would be larger if compressed (e.g., imagery band
files).  But even in this case the raster data layer would
only be larger by the size of the address array and the
single byte preceding each row.
.SH SEE ALSO
\fIr.support\fR
.SH AUTHORS
James Westervelt,
.br
Michael Shapiro,
.br
U.S. Army Construction Engineering Research Laboratory
.PP
\fILast changed: $Date: 2014\-06\-13 08:55:54 +0200 (Fri, 13 Jun 2014) $\fR
.PP
Main index | Raster index | Topics index | Keywords Index | Full index
.PP
� 2003\-2014 GRASS Development Team, GRASS GIS 7.1.svn Reference Manual
