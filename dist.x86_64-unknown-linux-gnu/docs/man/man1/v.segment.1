.TH v.segment 1 "" "GRASS 7.1.svn" "Grass User's Manual"
.SH NAME
\fI\fBv.segment\fR\fR  \- Creates points/segments from input vector lines and positions.
.SH KEYWORDS
vector, geometry
.SH SYNOPSIS
\fBv.segment\fR
.br
\fBv.segment help\fR
.br
\fBv.segment\fR \fBinput\fR=\fIname\fR  [\fBlayer\fR=\fIstring\fR]  \fBoutput\fR=\fIname\fR  [\fBfile\fR=\fIname\fR]   [\-\-\fBoverwrite\fR]  [\-\-\fBhelp\fR]  [\-\-\fBverbose\fR]  [\-\-\fBquiet\fR]
.SS Flags:
.IP "\fB\-\-overwrite\fR" 4m
.br
Allow output files to overwrite existing files
.IP "\fB\-\-help\fR" 4m
.br
Print usage summary
.IP "\fB\-\-verbose\fR" 4m
.br
Verbose module output
.IP "\fB\-\-quiet\fR" 4m
.br
Quiet module output
.SS Parameters:
.IP "\fBinput\fR=\fIname\fR \fB[required]\fR" 4m
.br
Name of input vector lines map
.br
Or data source for direct OGR access
.IP "\fBlayer\fR=\fIstring\fR" 4m
.br
Layer number or name
.br
Vector features can have category values in different layers. This number determines which layer to use. When used with direct OGR access this is the layer name.
.br
Default: \fI1\fR
.IP "\fBoutput\fR=\fIname\fR \fB[required]\fR" 4m
.br
Name for output vector map
.IP "\fBfile\fR=\fIname\fR" 4m
.br
Name of file containing segment rules
.br
\(cq\-\(cq for standard input
.SH DESCRIPTION
\fIv.segment\fR generates segments or points from input lines and from
positions read from a text file or \(cqstdin\(cq. It includes the creation
of parallel lines or points in given destination from the line.
.PP
The format is:
.br
.nf
\fC
P <point id>   <line cat> <offset> [<side offset>]
L <segment id> <line cat> <start offset> <end offset> [<side offset>]
\fR
.fi
The offsets can be percent values of the line length. If the offsets are
negative, they start from the end node of the line. \-0 means the end of the
line.
.SS EXAMPLE
The user could send to stdin something like:
.br
.nf
\fC
P 1 356 24.56
P 2 495 12.31
P 3 500 \-12.31
P 4 510 \-20%
\&...
\fR
.fi
(pipe or redirect from file into the command).
.br
.SH NOTES
A segment is only created for the first line found of the specified category.
.PP
Points are generated along the lines at the given distance(s) or percent(s)
of the line length from the beginning or end, if offsets are negative, of the
vector line.
.PP
The side offset is the orthogonal distance from the line. Positive side
offsets are to the right side of the line going forward, negative offsets
are to the left (\fId.vect\fR with \fIdisplay=shape,dir\fR shows
the direction of vector lines). As the segment distance is measured along the
original line, side\-offset lines will be longer than the start\-end segment distance
for outside corners of curving lines, and shorter for inside corners.
.PP
All offsets are measured in map units (see \(dq\fIg.proj \-p\fR\(dq) or percents
of the line length, if followed by a % character.
.PP
To place a point in the middle of a line, 50% offset can be used or the
\fIv.to.db\fR module may be used to find the line\(cqs length. Then half of
that distance can be used as the along\-line offset.
.SH EXAMPLES
The examples may be used in the North Carolina sample location.
.PP
Example: Extract line segment from 400m to 5000m from beginning
of line 1:
.br
.nf
\fC
# extract lines from railroad map:
v.extract railroads out=myrr cats=1
# join segments into polyline and reassign category numbers
v.build.polylines myrr out=myrr_pol
v.category myrr_pol out=myrailroads option=add
# zoom to an area of interest
g.region vector=myrailroads \-p
# show line, category, direction (to find the beginning)
d.mon wx0
d.vect myrailroads disp=shape,cat,dir lsize=12
# extract line segment from 400m to 5000m from beginning of line 1
echo \(dqL 1 1 400 5000\(dq | v.segment myrailroads out=myrailroads_segl
d.erase
d.vect myrailroads
d.vect myrailroads_segl col=green width=2
# set node at 5000m from beginning of line 1
echo \(dqP 1 1 5000\(dq | v.segment myrailroads out=myrailroads_segp
d.vect myrailroads_segp icon=basic/circle color=red fcolor=red size=5
\fR
.fi
.br
Extract line segment from 400m to 5000m from beginning
of line 1
.PP
Example: Create parallel 1km long line segments along first 8km of
track, offset 500m to the left of the tracks.
.br
.nf
\fC
v.segment myrailroads out=myrailroads_segl_side << EOF
L 1 1 1000 2000 \-500
L 2 1 3000 4000 \-500
L 3 1 5000 6000 \-500
L 4 1 7000 8000 \-500
EOF
d.erase
d.vect myrailroads disp=shape,dir
d.vect \-c myrailroads_segl_side width=2
\fR
.fi
.PP
Example: A series of points, spaced every 2km along the tracks
.br
.nf
\fC
v.segment myrailroads out=myrailroads_pt2km << EOF
P 1 1 1000
P 2 1 3000
P 3 1 5000
P 4 1 7000
EOF
d.erase
d.vect myrailroads disp=shape,dir
d.vect myrailroads_pt2km icon=basic/circle color=blue fcolor=blue size=5
\fR
.fi
.br
A series of points, spaced every 2km along the tracks
.PP
Example: A series of points, spaced every 2km along the tracks, offset 500m to the right
.br
.nf
\fC
v.segment myrailroads out=myrailroads_pt2kmO500m << EOF
P 1 1 1000 500
P 2 1 3000 500
P 3 1 5000 500
P 4 1 7000 500
EOF
d.erase
d.vect myrailroads disp=shape,dir
d.vect myrailroads_pt2kmO500m icon=basic/circle color=aqua fcolor=aqua size=5
\fR
.fi
.br
A series of points, spaced every 2km along the tracks, offset 500m to the right
.PP
Example: A series of points, spaced every 10% of the line\(cqs length along the
tracks from the end of the line up to the middle point, offset 500m to the right
.br
.nf
\fC
v.segment myrailroads out=myrailroads_pt10pctO500m << EOF
P 1 1  \-0% 500
P 2 1 \-10% 500
P 3 1 \-20% 500
P 4 1 \-30% 500
P 5 1 \-40% 500
P 6 1 \-50% 500
EOF
d.erase
d.vect myrailroads disp=shape,dir
d.vect myrailroads_pt10pctO500m icon=basic/circle color=red fcolor=black size=5
\fR
.fi
.br
A series of points, spaced every 10% of the line\(cqs length along the
tracks from the end of the line up to the middle point, offset 500m to the right
.SH BUGS
There is a problem with side\-offset parallel line generation for inside corners.
.SH SEE ALSO
\fI
LRS tutorial (Linear Referencing System),
.br
d.vect,
v.build.polylines,
v.lrs.segment,
v.parallel,
v.split,
v.to.db,
v.to.points
\fR
.SH AUTHOR
Radim Blazek, ITC\-Irst, Trento, Italy
.PP
\fILast changed: $Date: 2014\-03\-26 12:41:40 +0100 (Wed, 26 Mar 2014) $\fR
.PP
Main index | Vector index | Topics index | Keywords Index | Full index
.PP
� 2003\-2014 GRASS Development Team, GRASS GIS 7.1.svn Reference Manual
