.TH r.mapcalc 1 "" "GRASS 7.1.svn" "Grass User's Manual"
.SH NAME
\fI\fBr.mapcalc\fR\fR  \- Raster map calculator.
.SH KEYWORDS
raster, algebra
.SH SYNOPSIS
\fBr.mapcalc\fR
.br
\fBr.mapcalc help\fR
.br
\fBr.mapcalc\fR  [\fBexpression\fR=\fIstring\fR]   [\fBfile\fR=\fIname\fR]   [\-\-\fBoverwrite\fR]  [\-\-\fBhelp\fR]  [\-\-\fBverbose\fR]  [\-\-\fBquiet\fR]
.SS Flags:
.IP "\fB\-\-overwrite\fR" 4m
.br
Allow output files to overwrite existing files
.IP "\fB\-\-help\fR" 4m
.br
Print usage summary
.IP "\fB\-\-verbose\fR" 4m
.br
Verbose module output
.IP "\fB\-\-quiet\fR" 4m
.br
Quiet module output
.SS Parameters:
.IP "\fBexpression\fR=\fIstring\fR" 4m
.br
Expression to evaluate
.IP "\fBfile\fR=\fIname\fR" 4m
.br
File containing expression(s) to evaluate
.SH DESCRIPTION
\fIr.mapcalc\fR performs arithmetic on raster map layers.
New raster map layers can be created which are arithmetic expressions
involving existing raster map layers, integer or floating point constants,
and functions.
.SS Program use
\fIr.mapcalc\fR expression have the form:
.PP
\fBresult =\fR\fI expression\fR
.PP
where \fIresult\fR is the name of a raster map layer
to contain the result of the calculation and
\fIexpression\fR is any legal arithmetic expression involving existing
raster map layers, integer or floating point constants,
and functions known to the calculator.
Parentheses are allowed in the expression and may be nested to any depth.
\fIresult\fR will be created in the user\(cqs current mapset.
.PP
As \fIexpression=\fR is the first option, it is the default. This
means that passing an expression on the command line is possible
as long as the expression is quoted and a space is included before the
first \fI=\fR sign.
Example (\(cqfoo\(cq is the resulting map):
.br
.nf
\fC
r.mapcalc \(dqfoo = 1\(dq
\fR
.fi
or:
.br
.nf
\fC
r.mapcalc \(cqfoo = 1\(cq
\fR
.fi
An unquoted expression (i.e. split over multiple arguments) won\(cqt
work, nor will omitting the space before the = sign:
.br
.nf
\fC
r.mapcalc \(cqfoo=1\(cq
Sorry, <foo> is not a valid parameter
\fR
.fi
To read command from the file, use file=
explicitly, e.g.:
.br
.nf
\fC
r.mapcalc file=file
\fR
.fi
or:
.br
.nf
\fC
r.mapcalc file=\- < file
\fR
.fi
or:
.br
.nf
\fC
r.mapcalc file=\- <<EOF
foo = 1
EOF
\fR
.fi
.PP
The formula entered to \fIr.mapcalc\fR by the user is recorded both in the
\fIresult\fR map title (which appears in the category file for \fIresult\fR)
and in the history file for \fIresult\fR.
.PP
Some characters have special meaning to the command shell. If the user
is entering input to \fIr.mapcalc\fR on the command line, expressions
should be enclosed within single quotes.  See NOTES, below.
.SS Operators and order of precedence
The following operators are supported:
.br
.nf
\fC
     Operator   Meaning                    Type        Precedence
     \-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-
     \-          negation                   Arithmetic  12
     ~          one\(cqs complement           Bitwise     12
     !          not                        Logical     12
     ^          exponentiation             Arithmetic  11
     %          modulus                    Arithmetic  10
     /          division                   Arithmetic  10
     *          multiplication             Arithmetic  10
     +          addition                   Arithmetic   9
     \-          subtraction                Arithmetic   9
     <<         left shift                 Bitwise      8
     >>         right shift                Bitwise      8
     >>>        right shift (unsigned)     Bitwise      8
     >          greater than               Logical      7
     >=         greater than or equal      Logical      7
     <          less than                  Logical      7
     <=         less than or equal         Logical      7
     ==         equal                      Logical      6
     !=         not equal                  Logical      6
     &          bitwise and                Bitwise      5
     |          bitwise or                 Bitwise      4
     &&         logical and                Logical      3
     &&&        logical and[1]             Logical      3
     ||         logical or                 Logical      2
     |||        logical or[1]              Logical      2
     ?:         conditional                Logical      1
\fR
.fi
(modulus is the remainder upon division)
.PP
[1] The &&& and ||| operators handle null values differently to other
operators. See the section entitled \fBNULL support\fR below for more
details.
.PP
The operators are applied from left to right, with those of higher precedence
applied before those with lower precedence.
Division by 0 and modulus by 0 are acceptable and give a NULL result.
The logical operators give a 1 result if the comparison is true, 0 otherwise.
.PP
.PP
.SS Raster map layer names
Anything in the expression which is not a number, operator, or function name
is taken to be a raster map layer name.
Examples:
.PP
.br
.nf
\fC
elevation
x3
3d.his
\fR
.fi
.PP
Most GRASS raster map layers meet this naming convention.
However, if a raster map layer has a name which conflicts with the
above rule, it should be quoted.  For example, the expression
.PP
.br
.nf
\fC
x = a\-b
\fR
.fi
.PP
would be interpreted as:  x equals a minus b, whereas
.PP
.br
.nf
\fC
x = \(dqa\-b\(dq
\fR
.fi
.PP
would be interpreted as:  x equals the raster map layer named \fIa\-b\fR
.PP
Also
.PP
.br
.nf
\fC
x = 3107
\fR
.fi
.PP
would create \fIx\fR filled with the number 3107, while
.PP
.br
.nf
\fC
x = \(dq3107\(dq
\fR
.fi
.PP
would copy the raster map layer \fI3107\fR to the raster map layer \fIx\fR.
.PP
Quotes are not required unless the raster map layer names
look like numbers or contain operators, OR unless the program
is run non\-interactively.  Examples given here assume the
program is run interactively.  See NOTES, below.
.PP
\fIr.mapcalc\fR will look for the raster map layers according to the
user\(cqs current mapset search path.
It is possible to override the search path and specify the mapset
from which to select the raster map layer.
This is done by specifying the raster map layer name in the form:
.PP
.br
.nf
\fC
name@mapset
\fR
.fi
.PP
For example, the following is a legal expression:
.PP
.br
.nf
\fC
result = x@PERMANENT / y@SOILS
\fR
.fi
.PP
The mapset specified does not have to be in the mapset search path.
(This method of overriding the mapset search path is common to all
GRASS commands, not just \fIr.mapcalc\fR.)
.PP
.SS The neighborhood modifier
Maps and images are data base files stored in raster format, i.e.,
two\-dimensional matrices of integer values.
In \fIr.mapcalc\fR, maps may be followed by a \fIneighborhood\fR modifier that
specifies a relative offset from the current cell being evaluated.  The format is
\fImap[r,c]\fR, where \fIr\fR is the row offset and \fIc\fR is the column offset.
For example, \fImap[1,2]\fR refers to the cell one row below and two columns
to the right of the current cell, \fImap[\-2,\-1]\fR refers to the cell
two rows above and one column to the left of the current cell,
and \fImap[0,1]\fR refers to the cell one column to the right of the current cell.
This syntax permits the development of neighborhood\-type filters within a single
map or across multiple maps.
.PP
.SS Raster map layer values from the category file
Sometimes it is desirable to use a value associated with a category\(cqs
\fIlabel\fR instead of the category value itself.  If a raster
map layer name is preceded by the \fB@\fR
operator, then the labels in the category file for the raster map layer
are used in the expression instead of the category value.
.PP
For example, suppose that the raster map layer \fIsoil.ph\fR
(representing soil pH values) has a category file with labels as follows:
.PP
.br
.nf
\fC
cat     label
\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-
0       no data
1       1.4
2       2.4
3       3.5
4       5.8
5       7.2
6       8.8
7       9.4
\fR
.fi
.PP
Then the expression:
.PP
.br
.nf
\fC
result = @soils.ph
\fR
.fi
.PP
would produce a result with category values
0, 1.4, 2.4, 3.5, 5.8, 7.2, 8.8 and 9.4.
.PP
Note that this operator may only be applied to raster map layers
and produces a floating point value in the expression.
Therefore, the category label must start with a valid number.
If the category label is integer, it will be represented by
a floating point number. I the category label does not start
with a number or is missing, it will be represented by NULL
(no data) in the resulting raster map.
.SS Grey scale equivalents and color separates
It is often helpful to manipulate the colors assigned to map categories.
This is particularly useful when the spectral properties of cells have meaning
(as with imagery data), or when the map category values represent real
quantities (as when category values reflect true elevation values).
Map color manipulation can also aid visual recognition, and map printing.
.PP
The # operator can be used to either convert map category values to their
grey scale equivalents or to extract the red, green, or blue components
of a raster map layer into separate raster map layers.
.PP
.br
.nf
\fC
result = #map
\fR
.fi
.PP
converts each category value in \fImap\fR to a value in the range 0\-255 which
represents the grey scale level implied by the color for the category.
If the map has a grey scale color table, then the grey level is what
#map evaluates to.  Otherwise, it is computed as:
.PP
.br
.nf
\fC
 0.10 * red + 0.81 * green + 0.01 * blue
\fR
.fi
.PP
Alternatively, you can use:
.PP
.br
.nf
\fC
result = y#map
\fR
.fi
.PP
to use the NTSC weightings:
.PP
.br
.nf
\fC
 0.30 * red + 0.59 * green + 0.11 * blue
\fR
.fi
.PP
Or, you can use:
.PP
.br
.nf
\fC
result = i#map
\fR
.fi
.PP
to use equal weightings:
.PP
.br
.nf
\fC
 0.33 * red + 0.33 * green + 0.33 * blue
\fR
.fi
.PP
The # operator has three other forms:  r#map, g#map, b#map.
These extract the red, green, or blue components in the named raster map,
respectively.  The GRASS shell script \fIr.blend\fR extracts each of these
components from two raster map layers, and combines them by a user\-specified
percentage.
These forms allow color separates to be made.  For example, to
extract the red component from \fImap\fR
and store it in the new 0\-255 map layer \fIred\fR,
the user could type:
.PP
.br
.nf
\fC
red = r#map
\fR
.fi
.PP
To assign this map grey colors type:
.PP
.br
.nf
\fC
r.colors map=red color=rules
black
white
\fR
.fi
.PP
To assign this map red colors type:
.PP
.br
.nf
\fC
r.colors map=red color=rules
black
red
\fR
.fi
.PP
.SS Functions
The functions currently supported are listed in the table below.
The type of the result is indicated in the last column.
\fIF\fR
means that the functions always results in a floating point value,
\fII\fR
means that the function gives an integer result, and
\fI*\fR
indicates that the result is float if any of the arguments to the function
are floating point values and integer if all arguments are integer.
.PP
.br
.nf
\fC
function                description                                     type
\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-
abs(x)                  return absolute value of x                      *
acos(x)                 inverse cosine of x (result is in degrees)      F
asin(x)                 inverse sine of x (result is in degrees)        F
atan(x)                 inverse tangent of x (result is in degrees)     F
atan(x,y)               inverse tangent of y/x (result is in degrees)   F
cos(x)                  cosine of x (x is in degrees)                   F
double(x)               convert x to double\-precision floating point    F
eval([x,y,...,]z)       evaluate values of listed expr, pass results to z
exp(x)                  exponential function of x                       F
exp(x,y)                x to the power y                                F
float(x)                convert x to single\-precision floating point    F
graph(x,x1,y1[x2,y2..]) convert the x to a y based on points in a graph F
graph2(x,x1[,x2,..],y1[,y2..])
                        alternative form of graph()                     F
if                      decision options:                               *
if(x)                   1 if x not zero, 0 otherwise
if(x,a)                 a if x not zero, 0 otherwise
if(x,a,b)               a if x not zero, b otherwise
if(x,a,b,c)             a if x > 0, b if x is zero, c if x < 0
int(x)                  convert x to integer [ truncates ]              I
isnull(x)               check if x = NULL
log(x)                  natural log of x                                F
log(x,b)                log of x base b                                 F
max(x,y[,z...])         largest value of those listed                   *
median(x,y[,z...])      median value of those listed                    *
min(x,y[,z...])         smallest value of those listed                  *
mode(x,y[,z...])        mode value of those listed                      *
nmax(x,y[,z...])        largest value of those listed, excluding NULLs  *
nmedian(x,y[,z...])     median value of those listed, excluding NULLs   *
nmin(x,y[,z...])        smallest value of those listed, excluding NULLs *
nmode(x,y[,z...])       mode value of those listed, excluding NULLs     *
not(x)                  1 if x is zero, 0 otherwise
pow(x,y)                x to the power y                                *
rand(a,b)               random value x : a <= x < b
round(x)                round x to nearest integer                      I
round(x,y)              round x to nearest multiple of y
round(x,y,z)            round x to nearest y*i+z for some integer i
sin(x)                  sine of x (x is in degrees)                     F
sqrt(x)                 square root of x                                F
tan(x)                  tangent of x (x is in degrees)                  F
xor(x,y)                exclusive\-or (XOR) of x and y                   I
\fR
.fi
.br
.nf
\fC
Internal variables:
 row()                  current row of moving window
 col()                  current col of moving window
 x()                    current x\-coordinate of moving window
 y()                    current y\-coordinate of moving window
 ewres()                current east\-west resolution
 nsres()                current north\-south resolution
 null()                 NULL value
\fR
.fi
Note, that the row() and col() indexing starts with 1.
.SS Floating point values in the expression
Floating point numbers are allowed in the expression. A floating point
number is a number which contains a decimal point:
.br
.nf
\fC
    2.3   12.0   12.   .81
\fR
.fi
Floating point values in the expression are handled in a special way.
With arithmetic and logical operators, if either operand is float,
the other is converted to float and the result of the operation is float.
This means, in particular that division of integers results in a
(truncated) integer, while division of floats results in an accurate
floating point value.  With functions of type * (see table above),
the result is float if any argument is float, integer otherwise.
.PP
Note: If you calculate with integer numbers, the resulting map will
be integer. If you want to get a float result, add the decimal point
to integer number(s).
.PP
If you want floating point division, at least one of the arguments has
to be a floating point value. Multiplying one of them by 1.0 will
produce a floating\-point result, as will using float():
.br
.nf
\fC
      r.mapcalc \(dqndvi = float(lsat.4 \- lsat.3) / (lsat.4 + lsat.3)\(dq
\fR
.fi
.SS NULL support
.RS 4n
.IP \(bu 4n
Division by zero should result in NULL.
.IP \(bu 4n
Modulus by zero should result in NULL.
.IP \(bu 4n
NULL\-values in any arithmetic or logical operation should result
in NULL. (however, &&& and ||| are treated specially, as described below).
.IP \(bu 4n
The &&& and ||| operators observe the following axioms even when x is NULL:
.br
.nf
\fC
	x &&& false == false
	false &&& x == false
	x ||| true == true
	true ||| x == true
\fR
.fi
.IP \(bu 4n
NULL\-values in function arguments should result in NULL (however,
if(), eval() and isnull() are treated specially, as described below).
.IP \(bu 4n
The eval() function always returns its last argument
.IP \(bu 4n
The situation for if() is:
.br
.nf
\fC
if(x)
	NULL if x is NULL; 0 if x is zero; 1 otherwise
if(x,a)
	NULL if x is NULL; a if x is non\-zero; 0 otherwise
if(x,a,b)
	NULL if x is NULL; a if x is non\-zero; b otherwise
if(x,n,z,p)
	NULL if x is NULL; n if x is negative;
z if x is zero; p if x is positive
\fR
.fi
.IP \(bu 4n
The (new) function isnull(x) returns: 1 if x is NULL;
0 otherwise. The (new) function null()
(which has no arguments) returns an integer NULL.
.IP \(bu 4n
Non\-NULL, but invalid, arguments to functions should result in NULL.
.br
.nf
\fC
Examples:
log(\-2)
sqrt(\-2)
pow(a,b) where a is negative and b is not an integer
\fR
.fi
.RE
.PP
NULL support: Please note that any math performed with NULL cells always
results in a NULL value for these cells. If you want to replace a NULL cell
on\-the\-fly, use the isnull() test function in a if\-statement.
.PP
Example: The users wants the NULL\-valued cells to be treated like zeros. To
add maps A and B (where B contains NULLs) to get a map C the user can use a
construction like:
.PP
.br
.nf
\fC
C = A + if(isnull(B),0,B)
\fR
.fi
.PP
\fBNULL and conditions:\fR
.PP
For the one argument form:
.br
.nf
\fC
if(x) = NULL		if x is NULL
if(x) = 0		if x = 0
if(x) = 1		otherwise (i.e. x is neither NULL nor 0).
\fR
.fi
.PP
For the two argument form:
.br
.nf
\fC
if(x,a) = NULL		if x is NULL
if(x,a) = 0		if x = 0
if(x,a) = a		otherwise (i.e. x is neither NULL nor 0).
\fR
.fi
.PP
For the three argument form:
.br
.nf
\fC
if(x,a,b) = NULL	if x is NULL
if(x,a,b) = b		if x = 0
if(x,a,b) = a		otherwise (i.e. x is neither NULL nor 0).
\fR
.fi
.PP
For the four argument form:
.br
.nf
\fC
if(x,a,b,c) = NULL	if x is NULL
if(x,a,b,c) = a		if x > 0
if(x,a,b,c) = b		if x = 0
if(x,a,b,c) = c		if x < 0
\fR
.fi
More generally, all operators and most functions return NULL if *any*
of their arguments are NULL.
.br
The functions if(), isnull() and eval() are exceptions.
.br
The function isnull() returns 1 if its argument is NULL and 0 otherwise.
If the user wants the opposite, the ! operator, e.g. \(dq!isnull(x)\(dq must be
used.
.PP
All forms of if() return NULL if the first argument is NULL. The 2, 3
and 4 argument forms of if() return NULL if the \(dqselected\(dq argument is
NULL, e.g.:
.br
.nf
\fC
if(0,a,b) = b	regardless of whether a is NULL
if(1,a,b) = a	regardless of whether b is NULL
\fR
.fi
eval() always returns its last argument, so it only returns NULL if
the last argument is NULL.
.PP
\fBNote\fR: The user cannot test for NULL using the == operator, as that
returns NULL if either or both arguments are NULL, i.e. if x and y are
both NULL, then \(dqx == y\(dq and \(dqx != y\(dq are both NULL rather than 1 and
0 respectively.
.br
The behaviour makes sense if the user considers NULL as representing an
unknown quantity. E.g. if x and y are both unknown, then the values of
\(dqx == y\(dq and \(dqx != y\(dq are also unknown; if they both have unknown
values, the user doesn\(cqt know whether or not they both have the same value.
.SH NOTES
.SS Usage from command line
Extra care must be taken if the expression is given on the command line.
Some characters have special meaning to the UNIX shell.
These include, among others:
.br
.nf
\fC
* ( ) > & |
\fR
.fi
.PP
It is advisable to put single quotes around the expression; e.g.:
.br
.nf
\fC
\(cqresult = elevation * 2\(cq
\fR
.fi
Without the quotes, the *, which has special meaning to the UNIX shell,
would be altered and \fIr.mapcalc\fR would see something other than the *.
.SS Multiple computations
.PP
In general, it\(cqs preferable to do as much as possible in each
r.mapcalc command. E.g. rather than:
.br
.nf
\fC
        r.mapcalc \(dq$GIS_OPT_OUTPUT.r = r#$GIS_OPT_FIRST * .$GIS_OPT_PERCENT + (1.0 \- .$GIS_OPT_PERCENT) * r#$GIS_OPT_SECOND\(dq
        r.mapcalc \(dq$GIS_OPT_OUTPUT.g = g#$GIS_OPT_FIRST * .$GIS_OPT_PERCENT + (1.0 \- .$GIS_OPT_PERCENT) * g#$GIS_OPT_SECOND\(dq
        r.mapcalc \(dq$GIS_OPT_OUTPUT.b = b#$GIS_OPT_FIRST * .$GIS_OPT_PERCENT + (1.0 \- .$GIS_OPT_PERCENT) * b#$GIS_OPT_SECOND\(dq
\fR
.fi
.PP
use:
.br
.nf
\fC
	r.mapcalc <<EOF
        $GIS_OPT_OUTPUT.r = r#$GIS_OPT_FIRST * .$GIS_OPT_PERCENT + (1.0 \- .$GIS_OPT_PERCENT) * r#$GIS_OPT_SECOND
        $GIS_OPT_OUTPUT.g = g#$GIS_OPT_FIRST * .$GIS_OPT_PERCENT + (1.0 \- .$GIS_OPT_PERCENT) * g#$GIS_OPT_SECOND
        $GIS_OPT_OUTPUT.b = b#$GIS_OPT_FIRST * .$GIS_OPT_PERCENT + (1.0 \- .$GIS_OPT_PERCENT) * b#$GIS_OPT_SECOND
        EOF
\fR
.fi
.PP
as the latter will read each input map only once.
.SS Backwards compatibility
For the backwards compatibility with GRASS 6,
if no options are given, it manufactures file=\- (which reads from
stdin), so you can continue to use e.g.:
.br
.nf
\fC
r.mapcalc < file
\fR
.fi
or:
.br
.nf
\fC
r.mapcalc <<EOF
foo = 1
EOF
\fR
.fi
But unless you need compatibility with previous GRASS GIS versions, use file=
explicitly, as stated above.
.PP
When the map name contains uppercase letter(s) or a dot which are not
allowed to be in module option names, the \fIr.mapcalc\fR command will
be valid also without quotes:
.br
.nf
\fC
r.mapcalc elevation_A=1
r.mapcalc elevation.1=1
\fR
.fi
However, this syntax is not recommended as quotes as stated above more safe.
Using quotes is both backwards compatible and valid in future.
.SS Interactive input in command line
For formulas that the user enters from standard input
(rather than from the command line), a line continuation feature now exists.
If the user adds a backslash to the end of an input line, \fIr.mapcalc\fR assumes that
the formula being entered by the user continues on to the next input line.
There is no limit to the possible number of input lines
or to the length of a formula.
.PP
If the \fIr.mapcalc\fR formula entered by the user is very long,
the map title will contain only some of it, but most (if not all) of
the formula will be placed into the history file for the \fIresult\fR map.
.PP
When the user enters input to \fIr.mapcalc\fR non\-interactively on
the command line, the program will not warn the user not to overwrite
existing map layers.  Users should therefore take care to assign program
outputs raster map names that do not yet exist in their current mapsets.
.SS Raster MASK handling
.PP
\fIr.mapcalc\fR follows the common GRASS behavior of raster MASK handling,
so the MASK is only applied when reading an existing GRASS raster map.
This implies that, for example, the command:
.br
.nf
\fC
r.mapcalc \(dqelevation_exaggerated = elevation * 3\(dq
\fR
.fi
create a map respecting the masked pixels if MASK is active.
.PP
However, when creating a map which is not based on any map,
e.g. a map from a constant:
.br
.nf
\fC
r.mapcalc \(dqbase_height = 200.0\(dq
\fR
.fi
the created raster map is limited only by a computation region
but it is not affected by an active MASK.
This is expected because, as mentioned above, MASK is only applied when reading,
not when writing a raster map.
.PP
If also in this case the MASK should be applied, an if() statement including the
MASK should be used, e.g.:
.br
.nf
\fC
r.mapcalc \(dqbase_height = if(MASK, 200.0, null())\(dq
\fR
.fi
When testing MASK related expressions keep in mind that when MASK is active
you don\(cqt see data in masked areas even if they are not NULL.
See \fIr.mask\fR for details.
.SS eval function
If the output of the computation should be only one map
but the expression is so complex that it is better to split it
to several expressions, the eval function can be used:
.br
.nf
\fC
r.mapcalc << EOF
eval(elev_200 = elevation \- 200, \(rs
     elev_5 = 5 * elevation, \(rs
     elev_p = pow(elev_5, 2))
elevation_result = (0.5 * elev_200) + 0.8 * elev_p
EOF
\fR
.fi
This example uses unix\-like << EOF syntax to provide
input to \fIr.mapcalc\fR.
.PP
Note that the temporary variables (maps) are not created
and thus it does not matter whether they exists or not.
In the example above, if map elev_200 exists it will not be
overwritten and no error will be generated.
The reason is that the name elev_200 now denotes the temporary
variable (map) and not the existing map.
The following parts of the expression will use the temporary elev_200
and the existing elev_200 will be left intact and will not be used.
If a user want to use the existing map, the name of the temporary variable
(map) must be changed.
.SS Random number generator initialization
.PP
The environment variable GRASS_RND_SEED is read to initialize the
random number generator.
.SH EXAMPLES
To compute the average of two raster map layers
\fIa\fR and \fIb\fR:
.br
.nf
\fC
ave = (a + b)/2
\fR
.fi
To form a weighted average:
.br
.nf
\fC
ave = (5*a + 3*b)/8.0
\fR
.fi
To produce a binary representation of the raster map layer
\fIa\fR so that category 0 remains 0 and all other categories become 1:
.br
.nf
\fC
mask = a != 0
\fR
.fi
This could also be accomplished by:
.br
.nf
\fC
mask = if(a)
\fR
.fi
To mask raster map layer \fIb\fR by raster map layer \fIa\fR:
.br
.nf
\fC
result = if(a,b)
\fR
.fi
To change all values below 5 to NULL:
.br
.nf
\fC
newmap = if(map<5, null(), 5)
\fR
.fi
The graph() function allows users to specify a x\-y conversion using
pairs of x,y coordinates.
In some situations a transformation from one value to another is not
easily established mathematically, but can be represented by a 2\-D
graph and then linearly interpolated. The graph() function provides
the opportunity to accomplish this.
An x\-axis value is provided to the graph function along with
the associated graph represented by a series of x,y pairs.  The x
values must be monotonically increasing (each larger than or equal to
the previous).  The graph function linearly interpolates between
pairs.  Any x value lower the lowest x value (i.e. first) will have
the associated y value returned.  Any x value higher than the last
will similarly have the associated y value returned.  Consider the
request:
.br
.nf
\fC
newmap = graph(map, 1,10, 2,25, 3,50)
\fR
.fi
X (map) values supplied and y (newmap) values returned:
.br
.nf
\fC
0, 10
1, 10
1.5, 17.5
2.9, 47.5
4, 50
100, 50
\fR
.fi
.SH KNOWN ISSUES
Continuation lines must end with a \(rs and have \fIno\fR trailing
white space (blanks or tabs).
If the user does leave white space at the end of
continuation lines, the error messages produced by \fIr.mapcalc\fR will
be meaningless and the equation will not work as the user intended.
This is particularly important for the eval() function.
.PP
Currently, there is no comment mechanism in \fIr.mapcalc\fR.
Perhaps adding a capability that would cause the entire line to be
ignored when the user inserted a # at the start of a line
as if it were not present, would do the trick.
.PP
The function should require the user to type \(dqend\(dq or \(dqexit\(dq instead
of simply a blank line. This would make separation of multiple scripts
separable by white space.
.PP
\fIr.mapcalc\fR does not print a warning in case of operations on
NULL cells. It is left to the user to utilize the isnull() function.
.SH SEE ALSO
.PP
\fI
g.region,
r.bitpattern,
r.blend,
r.colors,
r.fillnulls
\fR
.SH REFERENCES
\fBr.mapcalc: An Algebra for GIS and Image
Processing\fR, by Michael Shapiro and Jim Westervelt, U.S. Army
Construction Engineering Research Laboratory (March/1991).
.PP
\fBPerforming Map Calculations on GRASS Data:
r.mapcalc Program Tutorial\fR, by Marji Larson, Michael Shapiro and Scott
Tweddale, U.S. Army Construction Engineering Research Laboratory (December
1991)
.PP
Grey scale conversion is based on the C.I.E. x,y,z system where y represents
luminance.  See \(dqFundamentals of Digital Image Processing,\(dq
by Anil K. Jain (Prentice Hall, NJ, 1989; p 67).
.SH AUTHORS
Michael Shapiro, U.S.Army Construction Engineering
Research Laboratory
.PP
Glynn Clements
.PP
\fILast changed: $Date: 2014\-03\-26 15:31:42 +0100 (Wed, 26 Mar 2014) $\fR
.PP
Main index | Raster index | Topics index | Keywords Index | Full index
.PP
� 2003\-2014 GRASS Development Team, GRASS GIS 7.1.svn Reference Manual
