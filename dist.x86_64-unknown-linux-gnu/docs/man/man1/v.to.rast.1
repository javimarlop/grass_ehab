.TH v.to.rast 1 "" "GRASS 7.1.svn" "Grass User's Manual"
.SH NAME
\fI\fBv.to.rast\fR\fR  \- Converts (rasterize) a vector map into a raster map.
.SH KEYWORDS
vector, conversion, raster, rasterization
.SH SYNOPSIS
\fBv.to.rast\fR
.br
\fBv.to.rast help\fR
.br
\fBv.to.rast\fR [\-\fBd\fR] \fBinput\fR=\fIname\fR  [\fBlayer\fR=\fIstring\fR]   [\fBtype\fR=\fIstring\fR[,\fIstring\fR,...]]   [\fBcats\fR=\fIrange\fR]   [\fBwhere\fR=\fIsql_query\fR]  \fBoutput\fR=\fIname\fR \fBuse\fR=\fIstring\fR  [\fBattrcolumn\fR=\fIname\fR]   [\fBrgbcolumn\fR=\fIname\fR]   [\fBlabelcolumn\fR=\fIname\fR]   [\fBvalue\fR=\fIfloat\fR]   [\fBrows\fR=\fIinteger\fR]   [\-\-\fBoverwrite\fR]  [\-\-\fBhelp\fR]  [\-\-\fBverbose\fR]  [\-\-\fBquiet\fR]
.SS Flags:
.IP "\fB\-d\fR" 4m
.br
Create densified lines (default: thin lines)
.br
All cells touched by the line will be set, not only those on the render path
.IP "\fB\-\-overwrite\fR" 4m
.br
Allow output files to overwrite existing files
.IP "\fB\-\-help\fR" 4m
.br
Print usage summary
.IP "\fB\-\-verbose\fR" 4m
.br
Verbose module output
.IP "\fB\-\-quiet\fR" 4m
.br
Quiet module output
.SS Parameters:
.IP "\fBinput\fR=\fIname\fR \fB[required]\fR" 4m
.br
Name of input vector map
.br
Or data source for direct OGR access
.IP "\fBlayer\fR=\fIstring\fR" 4m
.br
Layer number or name
.br
Vector features can have category values in different layers. This number determines which layer to use. When used with direct OGR access this is the layer name.
.br
Default: \fI1\fR
.IP "\fBtype\fR=\fIstring[,\fIstring\fR,...]\fR" 4m
.br
Input feature type
.br
Options: \fIpoint, line, area\fR
.br
Default: \fIpoint,line,area\fR
.IP "\fBcats\fR=\fIrange\fR" 4m
.br
Category values
.br
Example: 1,3,7\-9,13
.IP "\fBwhere\fR=\fIsql_query\fR" 4m
.br
WHERE conditions of SQL statement without \(cqwhere\(cq keyword
.br
Example: income < 1000 and inhab >= 10000
.IP "\fBoutput\fR=\fIname\fR \fB[required]\fR" 4m
.br
Name for output raster map
.IP "\fBuse\fR=\fIstring\fR \fB[required]\fR" 4m
.br
Source of raster values
.br
Options: \fIattr, cat, val, z, dir\fR
.br
\fBattr\fR: read values from attribute table
.br
\fBcat\fR: use category values
.br
\fBval\fR: use value specified by value option
.br
\fBz\fR: use z coordinate (points or contours only)
.br
\fBdir\fR: output as flow direction (lines only)
.IP "\fBattrcolumn\fR=\fIname\fR" 4m
.br
Name of column for \(cqattr\(cq parameter (data type must be numeric)
.IP "\fBrgbcolumn\fR=\fIname\fR" 4m
.br
Name of color definition column (with RRR:GGG:BBB entries)
.IP "\fBlabelcolumn\fR=\fIname\fR" 4m
.br
Name of column used as raster category labels
.IP "\fBvalue\fR=\fIfloat\fR" 4m
.br
Raster value (for use=val)
.br
Default: \fI1\fR
.IP "\fBrows\fR=\fIinteger\fR" 4m
.br
Number of rows to hold in memory
.br
Default: \fI4096\fR
.SH DESCRIPTION
\fIv.to.rast\fR transforms GRASS vector map layers into GRASS raster map
layer format. Optionally, attributes can be converted into raster category
labels.
.SH NOTES
\fIv.to.rast\fR will only affect data in areas lying
inside the boundaries of the current geographic region.
Before running \fIv.to.rast\fR, the user should
therefore ensure that the current geographic region is
correctly set and that the region resolution is at the
desired level.
.PP
Either the \fI\fBcolumn\fR\fR parameter or the \fI\fBvalue\fR\fR
parameter must be specified.  The \fI\fBuse\fR\fR option may be
specified alone when using the \fIdir\fR option.
.PP
\fI\fBuse\fR\fR options are:
.RS 4n
.IP \(bu 4n
\fIattr\fR \- read values from attribute table (default)
.IP \(bu 4n
\fIcat\fR  \- read values from category
.IP \(bu 4n
\fIval\fR  \- use value specified by \fI\fBvalue\fR\fR option
.IP \(bu 4n
\fIz\fR    \- use z coordinate (points or contours only)
.IP \(bu 4n
\fIdir\fR  \- output as flow direction in degrees (lines only)
.RE
.PP
The \fI\fBcolumn\fR\fR parameter uses an existing column from the vector map
database table as the category value in the output raster map. Existing table
columns can be shown by using \fIdb.describe\fR.
.PP
An empty raster map layer will be created if the vector map layer has not
been assigned category/attribute labels (e.g., through use of
v.category option=add).
.PP
Otherwise:
.RS 4n
.IP \(bu 4n
Labeled areas and/or centroids will produce filled raster coverages with edges
that straddle the original area boundary \fBas long as the boundary is NOT
labeled\fR.
.br
(Use v.category option=del type=boundary to remove.)
.IP \(bu 4n
Labeled lines and boundaries will produce lines of raster cells which touch the
original vector line. This tends to be more aggressive than area\-only conversions.
.IP \(bu 4n
Points and orphaned centroids will be converted into single cells on the
resultant raster map.
.RE
.PP
.PP
\fBFlow directions\fR are given in degrees counterclockwise from east.
.PP
.PP
Raster category labels are supported for all of \fIuse=\fR except \fIuse=z\fR.
.SH EXAMPLES
\fI1. Convert a vector map and use column SPEED from attribute table\fR
.br
.br
.nf
\fC
db.describe \-c table=vect_map
ncols:3
Column 1: CAT
Column 2: SPEED
Column 3: WIDTH
\fR
.fi
.br
.nf
\fC
v.to.rast in=vect_map out=raster_map col=SPEED
\fR
.fi
.PP
.PP
\fI2. Calculate stream directions from a river vector map (Spearfish)\fR
.br
.br
.nf
\fC
v.to.rast in=streams out=streamsdir use=dir
\fR
.fi
.PP
.PP
\fI3. Convert a vector polygon map to raster including descriptive labels (Spearfish)\fR
.br
.br
.nf
\fC
v.to.rast in=fields out=myfields use=attr col=cat labelcol=label
r.category myfields
\fR
.fi
.SH SEE ALSO
\fI
db.describe,
v.category
\fR
.br
.SH AUTHORS
Original code: Michael Shapiro, U.S. Army Construction Engineering Research Laboratory
.br
GRASS 6.0 updates: Radim Blazek, ITC\-irst, Trento, Italy
.br
Stream directions: Jaro Hofierka and Helena Mitasova
.br
GRASS 6.3 code cleanup and label support: Brad Douglas
.PP
.PP
\fILast changed: $Date: 2011\-11\-08 22:24:20 +0100 (Tue, 08 Nov 2011) $\fR
.PP
Main index | Vector index | Topics index | Keywords Index | Full index
.PP
� 2003\-2014 GRASS Development Team, GRASS GIS 7.1.svn Reference Manual
