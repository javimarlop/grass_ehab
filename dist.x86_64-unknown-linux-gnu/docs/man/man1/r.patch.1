.TH r.patch 1 "" "GRASS 7.1.svn" "Grass User's Manual"
.SH NAME
\fI\fBr.patch\fR\fR  \- Creates a composite raster map layer by using known category values from one (or more) map layer(s) to fill in areas of \(dqno data\(dq in another map layer.
.SH KEYWORDS
raster, geometry
.SH SYNOPSIS
\fBr.patch\fR
.br
\fBr.patch help\fR
.br
\fBr.patch\fR [\-\fBz\fR] \fBinput\fR=\fIname\fR[,\fIname\fR,...] \fBoutput\fR=\fIname\fR  [\-\-\fBoverwrite\fR]  [\-\-\fBhelp\fR]  [\-\-\fBverbose\fR]  [\-\-\fBquiet\fR]
.SS Flags:
.IP "\fB\-z\fR" 4m
.br
Use zero (0) for transparency instead of NULL
.IP "\fB\-\-overwrite\fR" 4m
.br
Allow output files to overwrite existing files
.IP "\fB\-\-help\fR" 4m
.br
Print usage summary
.IP "\fB\-\-verbose\fR" 4m
.br
Verbose module output
.IP "\fB\-\-quiet\fR" 4m
.br
Quiet module output
.SS Parameters:
.IP "\fBinput\fR=\fIname[,\fIname\fR,...]\fR \fB[required]\fR" 4m
.br
Name of raster maps to be patched together
.IP "\fBoutput\fR=\fIname\fR \fB[required]\fR" 4m
.br
Name for resultant raster map
.SH DESCRIPTION
The GRASS program \fIr.patch\fR allows the user to build a new
raster map the size and resolution of the current region by assigning
known data values from input raster maps to the cells in this region.
This is done by filling in \(dqno data\(dq cells, those that do not yet
contain data, contain NULL data, or, optionally contain 0 data,
with the data from the first input map.
Once this is done the remaining holes are filled in by the next input map,
and so on.
This program
is useful for making a composite raster map layer from two or more adjacent
map layers, for filling in \(dqholes\(dq in a raster map layer\(cqs data (e.g., in
digital elevation data), or for updating an older map layer with more recent
data. The current geographic region definition and mask settings are
respected.
.PP
The first \fIname\fR listed in the string
\fBinput=\fR\fIname\fR,\fIname\fR,\fIname\fR, ... is the name of
the first map whose data values will be used to fill in \(dqno data\(dq cells
in the current region. The second through last input \fIname\fR
maps will be used, in order, to supply data values for for the remaining
\(dqno data\(dq cells.
.SH EXAMPLE
Below, the raster map layer on the far left is \fBpatched\fR
with the middle (\fIpatching\fR) raster map layer,
to produce the \fIcomposite\fR raster map layer on the right.
.br
.nf
\fC
  1 1 1 0 2 2 0 0    0 0 1 1 0 0 0 0    1 1 1 1 2 2 0 0
  1 1 0 2 2 2 0 0    0 0 1 1 0 0 0 0    1 1 1 2 2 2 0 0
  3 3 3 3 2 2 0 0    0 0 0 0 0 0 0 0    3 3 3 3 2 2 0 0
  3 3 3 3 0 0 0 0    4 4 4 4 4 4 4 4    3 3 3 3 4 4 4 4
  3 3 3 0 0 0 0 0    4 4 4 4 4 4 4 4    3 3 3 4 4 4 4 4
  0 0 0 0 0 0 0 0    4 4 4 4 4 4 4 4    4 4 4 4 4 4 4 4
\fR
.fi
Switching the \fIpatched\fR and the \fIpatching\fR raster map layers
produces the following results:
.br
.nf
\fC
  0 0 1 1 0 0 0 0    1 1 1 0 2 2 0 0    1 1 1 1 2 2 0 0
  0 0 1 1 0 0 0 0    1 1 0 2 2 2 0 0    1 1 1 1 2 2 0 0
  0 0 0 0 0 0 0 0    3 3 3 3 2 2 0 0    3 3 3 3 2 2 0 0
  4 4 4 4 4 4 4 4    3 3 3 3 0 0 0 0    4 4 4 4 4 4 4 4
  4 4 4 4 4 4 4 4    3 3 3 0 0 0 0 0    4 4 4 4 4 4 4 4
  4 4 4 4 4 4 4 4    0 0 0 0 0 0 0 0    4 4 4 4 4 4 4 4
\fR
.fi
.SH NOTES
Frequently, this program is used to patch together adjacent map layers which
have been digitized separately.  The program
\fIv.mkgrid\fR can be used to make adjacent
maps align neatly.
.PP
The user should check the current geographic region settings before running
\fIr.patch\fR, to ensure that the region boundaries encompass all
of the data desired to be included in the composite map and to ensure that the
region resolution is the resolution of the desired data. To set the
geographic region settings to one or several raster maps, the \fIg.region\fR
program can be used:
.br
.nf
\fC
g.region rast=map1[,map2[,...]]
\fR
.fi
.PP
Use of \fIr.patch\fR is generally followed by use of the GRASS programs
\fIg.remove\fR and
\fIg.rename\fR;
\fIg.remove\fR is used to remove the original (un\-patched) raster map
layers, while \fIg.rename\fR is used to then assign to the newly\-created
composite (patched) raster map layer the name of the original raster map
layer.
.PP
\fIr.patch\fR creates support files for the patched, composite output map.
.SH EXAMPLE
Create a list of maps matching a pattern, extend the region to include them
all, and patch them together to create a mosaic. Overlapping maps will be
used in the order listed.
.br
.nf
\fC
MAPS=\(gag.mlist type=rast sep=, pat=\(dqmap_*\(dq\(ga
g.region rast=$MAPS
r.patch in=$MAPS out=mosaic
\fR
.fi
.br
.SH SEE ALSO
\fIg.region\fR,
\fIg.remove\fR,
\fIg.rename\fR,
\fIr.mapcalc\fR,
\fIr.support\fR,
\fIv.mkgrid\fR
.SH AUTHOR
Michael Shapiro,
U.S. Army Construction Engineering Research Laboratory
.br
\-z flag and performance improvement by Huidae Cho
.PP
\fILast changed: $Date: 2014\-06\-16 15:04:12 +0200 (Mon, 16 Jun 2014) $\fR
.PP
Main index | Raster index | Topics index | Keywords Index | Full index
.PP
� 2003\-2014 GRASS Development Team, GRASS GIS 7.1.svn Reference Manual
