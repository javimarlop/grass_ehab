.TH t.select 1 "" "GRASS 7.1.svn" "Grass User's Manual"
.SH NAME
\fI\fBt.select\fR\fR  \- Select maps from space time datasets by topological relationships to other space time datasets using temporal algebra.
.SH KEYWORDS
temporal, metadata
.SH SYNOPSIS
\fBt.select\fR
.br
\fBt.select help\fR
.br
\fBt.select\fR [\-\fBs\fR]  [\fBtype\fR=\fIname\fR]  \fBexpression\fR=\fIexpression\fR  [\-\-\fBhelp\fR]  [\-\-\fBverbose\fR]  [\-\-\fBquiet\fR]
.SS Flags:
.IP "\fB\-s\fR" 4m
.br
Activate spatial topology
.IP "\fB\-\-help\fR" 4m
.br
Print usage summary
.IP "\fB\-\-verbose\fR" 4m
.br
Verbose module output
.IP "\fB\-\-quiet\fR" 4m
.br
Quiet module output
.SS Parameters:
.IP "\fBtype\fR=\fIname\fR" 4m
.br
Type of the input space time dataset
.br
Options: \fIstrds, stvds, str3ds\fR
.br
Default: \fIstrds\fR
.IP "\fBexpression\fR=\fIexpression\fR \fB[required]\fR" 4m
.br
The temporal mapcalc expression
.SH DESCRIPTION
t.select performs selection of maps that are registered in space time datasets using temporal algebra.
.SS PROGRAM USE
The module expects an \fBexpression\fR as input parameter in the following form:
.br
.br
\fB \(dqresult = expression\(dq \fR
.br
.br
The statement structure is similar to r.mapcalc, see r.mapcalc.
Where \fBresult\fR represents the name of a space time dataset (STDS)that will
contain the result of the calculation that is given as \fBexpression\fR
on the right side of the equality sign.
These expression can be any valid or nested combination of temporal
operations and functions that are provided by the temporal algebra.
.br
The temporal algebra works with space time datasets of any type (STRDS, STR3DS and STVDS).
The algebra provides methods for map selection from STDS based on their temporal relations.
It is also possible to temporally shift maps, to create temporal buffer and to snap time
instances to create a valid temporal topology. Furthermore expressions can be nested and
evaluated in conditional statements (if, else statements). Within if\-statements the algebra
provides temporal variables like start time, end time, day of year, time differences or
number of maps per time interval to build up conditions. These operations can be assigned
to space time datasets or to the results of operations between space time datasets.
.br
.br
The type of the input space time datasets must be defined with the input
parameter \fBtype\fR. Possible options are STRDS, STVDS or STR3DS.
The default is set to space time raster datasets (STRDS).
.br
.br
As default, topological relationships between space time datasets will be
evaluated only temporal. Use the \fBs\fR flag to activate the
additionally spatial topology evaluation.
.br
.br
The expression option must be passed as \fBquoted\fR
expression, for example:
.br
.br
.nf
\fC
t.select expression=\(dqC = A : B\(dq
\fR
.fi
Where \fBC\fR is the new space time raster dataset that will contain maps
from \fBA\fR that are selected by equal temporal relationships
to the existing dataset \fBB\fR in this case.
.br
.br
.SH TEMPORAL ALGEBRA
The temporal algebra provides a wide range of temporal operators and
functions that will be presented in the following section.
.br
.br
.SS TEMPORAL RELATIONS
Several temporal topology relations between registered maps of space
time datasets are supported:
.br
.br
.nf
\fC
equals            A \-\-\-\-\-\-
                  B \-\-\-\-\-\-
during            A  \-\-\-\-
                  B \-\-\-\-\-\-
contains          A \-\-\-\-\-\-
                  B  \-\-\-\-
starts            A \-\-\-\-
                  B \-\-\-\-\-\-
started           A \-\-\-\-\-\-
                  B \-\-\-\-
finishs           A   \-\-\-\-
                  B \-\-\-\-\-\-
finished          A \-\-\-\-\-\-
                  B   \-\-\-\-
precedes          A \-\-\-\-
                  B     \-\-\-\-
follows           A     \-\-\-\-
                  B \-\-\-\-
overlapped        A   \-\-\-\-\-\-
                  B \-\-\-\-\-\-
overlaps          A \-\-\-\-\-\-
                  B   \-\-\-\-\-\-
over              booth overlaps and overlapped
\fR
.fi
The relations must be read as: A is related to B, like \- A equals B \- A is
during B \- A contains B
.br
.br
Topological relations must be specified in {} parentheses.
.br
.SS TEMPORAL SELECTION
The temporal selection simply selects parts of a space time dataset without
processing raster or vector data.
The algebra provides a selection operator \fB:\fR that selects parts
of a space time dataset that are temporally equal to parts of a second one
by default. The following expression
.br
.nf
\fC
C = A : B
\fR
.fi
means: Select all parts of space time dataset A that are equal to B and store
it in space time dataset C. The parts are time stamped maps.
.br
.br
In addition the inverse selection operator \fB!:\fR is defined as the complement of
the selection operator, hence the following expression
.br
.nf
\fC
C = A !: B
\fR
.fi
means: select all parts of space time time dataset A that are not equal to B
and store it in space time dataset (STDS) C.
.br
.br
To select parts of a STDS by different topological relations to other STDS,
the temporal topology selection operator can be used. The operator consists of
topological relations, that must be separated by the logical OR operator
\fB||\fR and the temporal selection operator. Both parts are separated by
comma and surrounded by curly braces:
{\(dqtopological relations\(dq, \(dqtemporal selection operator\(dq}
.br
.br
Examples:
.br
.nf
\fC
C = A {equals,:} B
C = A {equals,!:} B
\fR
.fi
We can now define arbitrary topological relations using the OR operator \(dq|\(dq
to connect them:
.br
.nf
\fC
C = A {equals|during|overlaps,:} B
\fR
.fi
Select all parts of A that are equal to B, during B or overlaps B.
.br
.br
The selection operator is implicitly contained in the temporal topology
selection operator, so that the following statements are exactly the same:
.br
.nf
\fC
C = A : B
C = A {:} B
C = A {equal,:} B
\fR
.fi
Same for the complementary selection:
.br
.nf
\fC
C = A !: B
C = A {!:} B
C = A {equal,!:} B
\fR
.fi
.SS CONDITIONAL STATEMENTS
Selection operations can be evaluated within conditional statements.
.br
.br
.nf
\fC
Note A and B can either be space time datasets or expressions.
if statement                         decision option                        temporal relations
  if(if, then, else)
  if(conditions, A)                    A if conditions are True;              temporal topological relation between if and then is equal.
  if(conditions, A, B)                 A if conditions are True, B otherwise; temporal topological relation between if, then and else is equal.
  if(topologies, conditions, A)        A if conditions are True;              temporal topological relation between if and then is explicit specified by topologies.
  if(topologies, conditions, A, B)     A if conditions are True, B otherwise; temporal topological relation between if, then and else is explicit specified by topologies.
\fR
.fi
The conditions are comparison expressions that are used to evaluate
space time datasets. Specific values of temporal variables are
compared by logical operators and evaluated for each map of the STDS.
.br
.br
The supported logical operators:
.br
.nf
\fC
Symbol  description
  ==    equal
  !=    not equal
  >     greater than
  >=    greater than or equal
  <     less than
  <=    less than or equal
  &&    and
  ||    or
\fR
.fi
Temporal functions:
.br
.br
.nf
\fC
td(A)                   Returns a list of time intervals of STDS A
start_time()            Start time as HH::MM:SS
start_date()            Start date as yyyy\-mm\-DD
start_datetime()        Start datetime as yyyy\-mm\-DD HH:MM:SS
end_time()              End time as HH:MM:SS
end_date()              End date as yyyy\-mm\-DD
end_datetime()          End datetime as  yyyy\-mm\-DD HH:MM
start_doy()             Day of year (doy) from the start time [1 \- 366]
start_dow()             Day of week (dow) from the start time [1 \- 7], the start of the week is Monday == 1
start_year()            The year of the start time [0 \- 9999]
start_month()           The month of the start time [1 \- 12]
start_week()            Week of year of the start time [1 \- 54]
start_day()             Day of month from the start time [1 \- 31]
start_hour()            The hour of the start time [0 \- 23]
start_minute()          The minute of the start time [0 \- 59]
start_second()          The second of the start time [0 \- 59]
end_doy()               Day of year (doy) from the end time [1 \- 366]
end_dow()               Day of week (dow) from the end time [1 \- 7], the start of the week is Monday == 1
end_year()              The year of the end time [0 \- 9999]
end_month()             The month of the end time [1 \- 12]
end_week()              Week of year of the end time [1 \- 54]
end_day()               Day of month from the start time [1 \- 31]
end_hour()              The hour of the end time [0 \- 23]
end_minute()            The minute of the end time [0 \- 59]
end_second()            The second of the end time [0 \- 59]
\fR
.fi
Additionally the number of maps in intervals can be computed and
used in conditional statements.
.br
The operator to count the number of maps
is the hash \fB#\fR.
.br
.nf
\fC
A{contains,#}B
\fR
.fi
This expression computes the number of maps from space
time dataset B which are during the time intervals of maps from
space time dataset A.
.br
A list of integers (scalars) corresponding to the maps of A
that contain maps from B will be returned.
.br
.br
Furthermore the temporal algebra allows temporal buffering, shifting
and snapping with the functions buff_t(), tshift() and tsnap()
respectively.
.br
.nf
\fC
buff_t(A, size)         Buffer STDS A with granule (\(dq1 month\(dq or 5)
tshift(A, size)         Shift STDS A with granule (\(dq1 month\(dq or 5)
tsnap(A)                Snap time instances and intervals of STDS A
\fR
.fi
.SS Examples:
Select all maps from space time dataset A which have equal time stamps
with space time dataset B and C and are ealier that Jan. 1. 2005 and
store them in space time dataset D.
.br
.nf
\fC
D = if(start_date() < \(dq2005\-01\-01\(dq, A : B : C)
\fR
.fi
Select all maps from space time dataset A which contains more than three
maps of space time dataset B, else select maps from C with time
stamps that are not equal to A and store them in space time dataset D.
.br
.nf
\fC
D = if(A {contain, #} B > 3, A {contain, :} B, C)
\fR
.fi
Select all maps from space time dataset B which are during the temporal
buffered space time dataset A with a map interval of three days, else
select maps from C and store them in space time dataset D.
.br
.nf
\fC
D = if(contain, td(buff_t(A, \(dq1 days\(dq)) == 3, B, C)
\fR
.fi
.SH REFERENCES
PLY(Python\-Lex\-Yacc)
.SH SEE ALSO
\fI
r.mapcalc
\fR
.SH AUTHORS
Thomas Leppelt, Soeren Gebbert, Thuenen Institut, Germany
.br
.PP
\fILast changed: $Date: 2014\-03\-26 16:21:32 +0100 (Wed, 26 Mar 2014) $\fR
.PP
Main index | Temporal index | Topics index | Keywords Index | Full index
.PP
� 2003\-2014 GRASS Development Team, GRASS GIS 7.1.svn Reference Manual
