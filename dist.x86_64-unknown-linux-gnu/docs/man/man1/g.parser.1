.TH g.parser 1 "" "GRASS 7.1.svn" "Grass User's Manual"
.SH NAME
\fI\fBg.parser\fR\fR \- Provides full parser support for GRASS
scripts.
.SH SYNOPSIS
\fBg.parser help\fR
.br
\fBg.parser\fR [\-\fBs\fR] [\-\fBt\fR] \fIfilename\fR [\fIargument\fR,...]
.SS Flags:
.IP "\fB\-t\fR" 4m
.br
Print strings for translation
.IP "\fB\-s\fR" 4m
.br
Write option values to standard output instead of reinvoking script
.IP "\fB\-n\fR" 4m
.br
Write option values to standard output separated by null character
.SH DESCRIPTION
The \fIg.parser\fR module provides full parser support for GRASS
scripts, including an auto\-generated GUI interface, help page
template, and command line option checking. In this way a simple
script can very quickly be made into a full\-fledged GRASS module.
.SH OPTIONS
Unless the \fB\-s\fR or \fB\-n\fR switch is used, the arguments are stored in
environment variables for use in your scripts. These variables are
named \(dqGIS_FLAG_<NAME>\(dq for flags and \(dqGIS_OPT_<NAME>\(dq for
options. The names of variables are converted to upper case. For
example if an option with key \fBinput\fR was defined in the script
header, the value will be available in variable \fBGIS_OPT_INPUT\fR
and the value of flag with key \fBf\fR will be available in variable
\fBGIS_FLAG_F\fR.
.PP
For flags, the value will be \(dq1\(dq if the flag was given, and \(dq0\(dq otherwise.
.PP
If the \fB\-s\fR or \fB\-n\fR switch is used, the options and flags are written to
standard output in the form \fIopt_<name>=<value>\fR and
\fIflag_<name>=<value>\fR, preceded by the string
\fB@ARGS_PARSED@\fR. If this string doesn\(cqt appear as the first line
of standard output, it indicates that the script was invoked with a switch such
as \fB\-\-html\-description\fR. In this case, the data written by
\fIg.parser\fR to standard output should be copied to the script\(cqs standard output
verbatim.
If the \fB\-s\fR switch is used, the options and flags are separated
by newlines. If the \fB\-n\fR switch is used, the options and flags
are separated by null characters.
.PP
Typical header definitions are as follows:
.br
.nf
\fC
#%module
#% description: g.parser test script
#%end
#%flag
#% key: f
#% description: A flag
#%end
#%option
#% key: raster
#% type: string
#% gisprompt: old,cell,raster
#% description: Raster input map
#% required : yes
#%end
\fR
.fi
With {NULL} it is possible to suppress a predefined description
or label.
.PP
The parsers allows to use predefined \fIstandardized options and
flags\fR, see the list
of options and flags
in the programmer manual. Eg. the option
.br
.nf
\fC
#%option
#% key: raster
#% type: string
#% gisprompt: old,cell,raster
#% description: Raster input map
#% required : yes
#%end
\fR
.fi
can be easily defined as
.br
.nf
\fC
#%option G_OPT_R_MAP
#% key: raster
#%end
\fR
.fi
.SH NOTES
An option can be instructed to allow multiple inputs by adding the
following line:
.br
.nf
\fC
#% multiple : yes
\fR
.fi
While this will only directly change the \fIUsage\fR section of the help
screen, the option\(cqs environmental string may be easily parsed from within
a script. For example, individual comma separated identities for an option
named \(dqinput\(dq can be parsed with the following Bash shell code:
.br
.nf
\fC
IFS=,
for opt in $GIS_OPT_INPUT ; do
    ... \(dq$opt\(dq
done
\fR
.fi
.PP
A \(dqguisection\(dq field may be added to each option and flag to
specify that the options should appear in multiple tabs in the
auto\-generated GUI.  Any options without a guisection field
go into the \(dqRequired\(dq or \(dqOptions\(dq tab.  For example:
.br
.nf
\fC
#% guisection: tabname
\fR
.fi
would put that option in a tab named \fItabname\fR.
.PP
A \(dqkey_desc\(dq field may be added to each option to specify the text that
appears in the module\(cqs usage help section. For example:
.br
.nf
\fC
#% key_desc: filename
\fR
.fi
added to an \fBinput\fR option would create the usage summary
[input=filename].
.PP
If a script is run with \fB\-\-o\fR, the parser will
set GRASS_OVERWRITE=1, which has the same effect as passing
\fB\-\-o\fR to every module which is run from the script. Similarly, passing
\fB\-\-q\fR or \fB\-\-v\fR will set GRASS_VERBOSE to 0 or 3 respectively,
which has the same effect as passing \fB\-\-q\fR or \fB\-\-v\fR to every module which
is run from the script.  Rather than checking whether \fB\-\-o\fR, \fB\-\-q\fR or \fB\-\-v\fR
were used, you should be checking GRASS_OVERWRITE and/or
GRASS_VERBOSE instead. If those variables are set, the script
should behave the same way regardless of whether they were set
by \fB\-\-o\fR, \fB\-\-q\fR or \fB\-\-v\fR being passed to the script or
set by other means.
.SH AUTOMATED SCRIPT CREATION
The flag \fB\-\-script\fR added to a GRASS command, generates shell
output. To write out a \fIg.parser\fR boilerplate for easy
prototyping of shell scripts, the flag \fB\-\-script\fR can be added
to any GRASS command. Example:
.br
.nf
\fC
v.in.db \-\-script
\fR
.fi
.SH Help page template (HTML)
The flag \fB\-\-html\-description\fR added to a GRASS command
generates a related help page template in HTML. Example:
.br
.nf
\fC
v.in.db \-\-html\-description
\fR
.fi
.SH GUI window parser (XML)
The flag \fB\-\-interface\-description\fR added to a GRASS command
generates a related help page template in XML. Example:
.br
.nf
\fC
v.in.db \-\-interface\-description
\fR
.fi
.SH Web Processing Service (WPS)
The flag \fB\-\-wps\-process\-description\fR added to a GRASS command
generates a Web Processing Service process description. Example:
.br
.nf
\fC
v.in.db \-\-wps\-process\-description
\fR
.fi
.SH reStructuredText (REST)
The flag \fB\-\-rest\-description\fR added to a GRASS command
generates a related help page template in reStructuredText. Example:
.br
.nf
\fC
v.in.db \-\-rest\-description
\fR
.fi
.SH TRANSLATION
\fIg.parser\fR provides some support for translating the options of
scripts. If called with the \-t switch before the script filename like
this
.br
.nf
\fC
g.parser \-t somescriptfile
\fR
.fi
\fIg.parser\fR will print the text of the translatable options to
standard output, one per line, and exit. This is for internal use within
the build system to prepare GRASS scripts for translation.
.SH EXAMPLES
All examples below autogenerate the graphical user interface when invoked
without parameters of flags:
.PP
.PP
To run properly, the script needs to be copied into a directory listed
in $GRASS_ADDON_PATH environmental variable with the
executable flag being set.
.PP
The script will provide a GUI (as above) and the following usage help
text:
.br
.nf
\fC
test.py|sh|pl \-\-help
Description:
 g.parser test script (python)
Usage:
 test.sh [\-f] raster=string vector=string [option1=string]
   [\-\-verbose] [\-\-quiet]
Flags:
  \-f   A flag
 \-\-v   Verbose module output
 \-\-q   Quiet module output
Parameters:
   raster   Raster input map
   vector   Vector input map
  option1   An option
\fR
.fi
.SS Example code for Python
.br
.nf
\fC
#!/usr/bin/env python
# g.parser demo script for python programing
#%module
#%  description: g.parser test script (python)
#%end
#%flag
#%  key: f
#%  description: A flag
#%end
#%option G_OPT_R_MAP
#% key: raster
#%end
#%option G_OPT_V_MAP
#% key: vector
#%end
#%option
#%  key: option1
#%  type: string
#%  description: An option
#%  required : no
#%end
import os
import sys
import grass.script as grass
def main():
    flag_f = flags[\(cqf\(cq]
    option1 = options[\(cqoption1\(cq]
    raster = options[\(cqraster\(cq]
    vector = options[\(cqvector\(cq]
    #### add your code here ####
    if flag_f:
        print \(dqFlag \-f set\(dq
    else:
        print \(dqFlag \-f not set\(dq
    # test if parameter present:
    if option1:
        print \(dqValue of option1 option: \(cq%s\(cq\(dq % option1
    print \(dqValue of raster option: \(cq%s\(cq\(dq % raster
    print \(dqValue of vector option: \(cq%s\(cq\(dq % vector
    #### end of your code ####
    return 0
if __name__ == \(dq__main__\(dq:
    options, flags = grass.parser()
    sys.exit(main())
\fR
.fi
.SS Example code for SHELL
.br
.nf
\fC
#!/bin/sh
# g.parser demo script for shell programing
#%module
#%  description: g.parser test script
#%end
#%flag
#%  key: f
#%  description: A flag
#%end
#%option G_OPT_R_MAP
#% key: raster
#%end
#%option G_OPT_V_MAP
#% key: vector
#%end
#%option
#% key: option1
#% type: string
#% description: An option
#% required : no
#%end
if [ \-z \(dq$GISBASE\(dq ] ; then
    echo \(dqYou must be in GRASS GIS to run this program.\(dq 1>&2
    exit 1
fi
if [ \(dq$1\(dq != \(dq@ARGS_PARSED@\(dq ] ; then
    exec g.parser \(dq$0\(dq \(dq$@\(dq
fi
#### add your code below ####
echo \(dq\(dq
if [ $GIS_FLAG_F \-eq 1 ] ; then
    echo \(dqFlag \-f set\(dq
else
    echo \(dqFlag \-f not set\(dq
fi
# test if parameter present:
if [ \-n \(dq$GIS_OPT_OPTION1\(dq ] ; then
    echo \(dqValue of GIS_OPT_OPTION1: \(cq$GIS_OPT_OPTION1\(cq\(dq
fi
echo \(dqValue of GIS_OPT_RASTER: \(cq$GIS_OPT_RASTER\(cq\(dq
echo \(dqValue of GIS_OPT_VECTOR: \(cq$GIS_OPT_VECTOR\(cq\(dq
\fR
.fi
.SS Example code for Perl
.br
.nf
\fC
#!/usr/bin/perl \-w
use strict;
# g.parser demo script
#%module
#%  description: g.parser test script (perl)
#%  keywords: keyword1, keyword2
#%end
#%flag
#%  key: f
#%  description: A flag
#%end
#%option G_OPT_R_MAP
#% key: raster
#%end
#%option G_OPT_V_MAP
#% key: vector
#%end
#%option
#% key: option1
#% type: string
#% description: An option
#% required : no
#%end
if ( !$ENV{\(cqGISBASE\(cq} ) {
    printf(STDERR  \(dqYou must be in GRASS GIS to run this program.\(rsn\(dq);
    exit 1;
}
if( $ARGV[0] ne \(cq@ARGS_PARSED@\(cq ){
    my $arg = \(dq\(dq;
    for (my $i=0; $i < @ARGV;$i++) {
        $arg .= \(dq $ARGV[$i] \(dq;
    }
    system(\(dq$ENV{GISBASE}/bin/g.parser $0 $arg\(dq);
    exit;
}
#### add your code here ####
print  \(dq\(rsn\(dq;
if ( $ENV{\(cqGIS_FLAG_F\(cq} eq \(dq1\(dq ){
   print \(dqFlag \-f set\(rsn\(dq
}
else {
   print \(dqFlag \-f not set\(rsn\(dq
}
printf (\(dqValue of GIS_OPT_option1: \(cq%s\(cq\(rsn\(dq, $ENV{\(cqGIS_OPT_OPTION1\(cq});
printf (\(dqValue of GIS_OPT_raster: \(cq%s\(cq\(rsn\(dq, $ENV{\(cqGIS_OPT_RASTER\(cq});
printf (\(dqValue of GIS_OPT_vect: \(cq%s\(cq\(rsn\(dq, $ENV{\(cqGIS_OPT_VECTOR\(cq});
#### end of your code ####
\fR
.fi
.SH SEE ALSO
\fI
g.filename,
g.findfile,
g.tempfile
\fR
and SUBMITTING_PYTHON
file in the GRASS source code.
.PP
Related Wiki pages:
Using GRASS with other programming languages
.SH AUTHOR
Glynn Clements
.PP
\fILast changed: $Date: 2014\-04\-20 12:09:22 +0200 (Sun, 20 Apr 2014) $\fR
.PP
Main index | Full index
.PP
� 2003\-2014 GRASS Development Team, GRASS GIS 7.0.svn Reference Manual
