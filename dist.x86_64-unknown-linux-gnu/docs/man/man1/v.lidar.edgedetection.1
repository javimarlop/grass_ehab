.TH v.lidar.edgedetection 1 "" "GRASS 7.1.svn" "Grass User's Manual"
.SH NAME
\fI\fBv.lidar.edgedetection\fR\fR  \- Detects the object\(cqs edges from a LIDAR data set.
.SH KEYWORDS
vector, LIDAR, edges
.SH SYNOPSIS
\fBv.lidar.edgedetection\fR
.br
\fBv.lidar.edgedetection help\fR
.br
\fBv.lidar.edgedetection\fR [\-\fBe\fR] \fBinput\fR=\fIname\fR \fBoutput\fR=\fIname\fR  [\fBsee\fR=\fIfloat\fR]   [\fBsen\fR=\fIfloat\fR]   [\fBlambda_g\fR=\fIfloat\fR]   [\fBtgh\fR=\fIfloat\fR]   [\fBtgl\fR=\fIfloat\fR]   [\fBtheta_g\fR=\fIfloat\fR]   [\fBlambda_r\fR=\fIfloat\fR]   [\-\-\fBoverwrite\fR]  [\-\-\fBhelp\fR]  [\-\-\fBverbose\fR]  [\-\-\fBquiet\fR]
.SS Flags:
.IP "\fB\-e\fR" 4m
.br
Estimate point density and distance
.br
Estimate point density and distance for the input vector points within the current region extends and quit
.IP "\fB\-\-overwrite\fR" 4m
.br
Allow output files to overwrite existing files
.IP "\fB\-\-help\fR" 4m
.br
Print usage summary
.IP "\fB\-\-verbose\fR" 4m
.br
Verbose module output
.IP "\fB\-\-quiet\fR" 4m
.br
Quiet module output
.SS Parameters:
.IP "\fBinput\fR=\fIname\fR \fB[required]\fR" 4m
.br
Name of input vector map
.br
Or data source for direct OGR access
.IP "\fBoutput\fR=\fIname\fR \fB[required]\fR" 4m
.br
Name for output vector map
.IP "\fBsee\fR=\fIfloat\fR" 4m
.br
Interpolation spline step value in east direction
.br
Default: \fI4\fR
.IP "\fBsen\fR=\fIfloat\fR" 4m
.br
Interpolation spline step value in north direction
.br
Default: \fI4\fR
.IP "\fBlambda_g\fR=\fIfloat\fR" 4m
.br
Regularization weight in gradient evaluation
.br
Default: \fI0.01\fR
.IP "\fBtgh\fR=\fIfloat\fR" 4m
.br
High gradient threshold for edge classification
.br
Default: \fI6\fR
.IP "\fBtgl\fR=\fIfloat\fR" 4m
.br
Low gradient threshold for edge classification
.br
Default: \fI3\fR
.IP "\fBtheta_g\fR=\fIfloat\fR" 4m
.br
Angle range for same direction detection
.br
Default: \fI0.26\fR
.IP "\fBlambda_r\fR=\fIfloat\fR" 4m
.br
Regularization weight in residual evaluation
.br
Default: \fI2\fR
.SH DESCRIPTION
\fIv.lidar.edgedetection\fR is the first of three steps to filter
LiDAR data. The filter aims to recognize and extract attached and
detached object (such as buildings, bridges, power lines,  trees, etc.)
in order to create a Digital Terrain Model.
.br
.br
In particular, this module detects the edge of each single feature over
the terrain surface of a LIDAR point surface. First of all, a bilinear
spline interpolation with a Tychonov regularization parameter is
performed. The gradient is minimized and the low Tychonov regularization
parameter brings the interpolated functions as close as possible to the
observations. Bicubic spline interpolation with Tychonov regularization
is then performed. However, now the curvature is minimized and the
regularization parameter is set to a high value. For each point, an
interpolated value is computed from the bicubic surface and an interpolated
gradient is computed from the bilinear surface. At each point the gradient
magnitude and the direction of the edge vector are calculated, and the
residual between interpolated and observed values is computed. Two thresholds
are defined on the gradient, a high threshold \fBtgh\fR and a low one
\fBtgl\fR. For each point, if the gradient magnitude is greater than or
equal to the high threshold and its residual is greater than or equal to
zero, it is labeled as an EDGE point. Similarly a point is labeled as
being an EDGE point if the gradient magnitude is greater than or equal to
the low threshold, its residual is greater than or equal to zero, and the
gradient to two of eight neighboring points is greater than the high
threshold. Other points are classified as TERRAIN.
.br
.br
The output will be a vector map in which points has been classified as
TERRAIN, EDGE or UNKNOWN. This vector map should be the input of
\fIv.lidar.growing\fR module.
.SH NOTES
In this module, an external table will be created which will be useful for
the next module of the procedure of LiDAR data filtering. In this table
the interpolated height values of each point will be recorded. Also points
in the output vector map will be classified as:
.br
.br
TERRAIN (cat = 1, layer = 1)
.br
EDGE (cat = 2, layer = 1)
.br
UNKNOWN (cat = 3, layer = 1)
.br
The final result of the whole procedure (v.lidar.edgedetection,
v.lidar.growing, v.lidar.correction) will be a point classification in
four categories:
.br
.br
TERRAIN SINGLE PULSE (cat = 1, layer = 2)
.br
TERRAIN DOUBLE PULSE (cat = 2, layer = 2)
.br
OBJECT SINGLE PULSE (cat = 3, layer = 2)
.br
OBJECT DOUBLE PULSE (cat = 4, layer = 2)
.SH EXAMPLES
.SS Basic edge detection
.br
.nf
\fC
v.lidar.edgedetection input=vector_last output=edge see=8 sen=8 lambda_g=0.5
\fR
.fi
.SH SEE ALSO
\fI
v.lidar.growing,
v.lidar.correction,
v.surf.bspline
\fR
.SH AUTHORS
Original version of program in GRASS 5.4:
.br
Maria Antonia Brovelli, Massimiliano Cannata, Ulisse Longoni and Mirko Reguzzoni
.br
.br
Update for GRASS 6.X:
.br
Roberto Antolin and Gonzalo Moreno
.SH REFERENCES
Antolin, R. et al., 2006. Digital terrain models determination by LiDAR
technology: Po basin experimentation. Bolletino di Geodesia e Scienze
Affini, anno LXV, n. 2, pp. 69\-89.
.br
.br
Brovelli M. A., Cannata M., Longoni U.M., 2004. LIDAR Data Filtering and
DTM Interpolation Within GRASS, Transactions in GIS, April 2004,  vol. 8,
iss. 2, pp. 155\-174(20), Blackwell Publishing Ltd.
.br
.br
Brovelli M. A., Cannata M., 2004. Digital Terrain model reconstruction in
urban areas from airborne laser scanning data: the method and an  example
for Pavia (Northern Italy). Computers and Geosciences 30 (2004) pp.325\-331
.br
.br
Brovelli M. A. and Longoni U.M., 2003. Software per il filtraggio di dati
LIDAR, Rivista dell?Agenzia del Territorio, n. 3\-2003, pp. 11\-22 (ISSN 1593\-2192).
.br
.br
Brovelli M. A., Cannata M. and Longoni U.M., 2002. DTM LIDAR in area urbana,
Bollettino SIFET N.2, pp. 7\-26.
.br
.br
Performances of the filter can be seen in the
ISPRS WG III/3 Comparison of Filters
report by Sithole, G. and Vosselman, G., 2003.
.br
.PP
\fILast changed: $Date: 2011\-11\-08 22:24:20 +0100 (Tue, 08 Nov 2011) $\fR
.PP
Main index | Vector index | Topics index | Keywords Index | Full index
.PP
� 2003\-2014 GRASS Development Team, GRASS GIS 7.1.svn Reference Manual
