.TH sql 1 "" "GRASS 7.1.svn" "Grass User's Manual"
.SH SQL support in GRASS GIS
GRASS can use various RDBMS
(Relational
database management system) and embedded databases. SQL
(Structured Query
Language) queries are directly passed to the underlying database
system. The set of supported SQL commands depends on the RDMBS and
database driver selected.
.SH Database drivers
The list of available database drivers can vary in various binary
distributions of GRASS:
.br
.br
.TS
expand;
lw60 lw1 lw60 lw1 lw60.
T{
dbf
T}	 	T{
DBF files. Data are stored in DBF files.
T}	 	T{
http://shapelib.maptools.org/dbf_api.html
T}
.sp 1
T{
sqlite
T}	 	T{
SQLite embedded database (GRASS 7 default DB backend).
T}	 	T{
http://sqlite.org/
T}
.sp 1
T{
pg
T}	 	T{
PostgreSQL RDBMS.
T}	 	T{
http://postgresql.org/
T}
.sp 1
T{
mysql
T}	 	T{
MySQL RDBMS.
T}	 	T{
http://mysql.org/
T}
.sp 1
T{
odbc
T}	 	T{
UnixODBC. (PostgreSQL, Oracle, etc.)
T}	 	T{
http://www.unixodbc.org/
T}
.sp 1
T{
ogr
T}	 	T{
OGR files.
T}	 	T{
http://gdal.org/ogr
T}
.sp 1
.TE
.SH NOTES
.RS 4n
.IP \(bu 4n
SQL does not support \(cq.\(cq (dots) in table names.
.IP \(bu 4n
Supported table name characters are only:
.br
.br
.nf
\fC
[A\-Za\-z][A\-Za\-z0\-9_]*
\fR
.fi
.IP \(bu 4n
A table name must start with a character, not a number.
.IP \(bu 4n
Text\-string matching requires the text part to be \(cqsingle quoted\(cq.
When run from the command line multiple queries should be contained
in \(dqdouble quotes\(dq. e.g.
.br
.br
.nf
\fC
d.vect map where=\(dqindividual=\(cqjuvenile\(cq and area=\(cqbeach\(cq\(dq
\fR
.fi
.IP \(bu 4n
An error message such as \(dqdbmi: Protocol
error\(dq either indicates an invalid column name or an
unsupported column type (then the GRASS SQL parser needs to be
extended).
.IP \(bu 4n
DBF column names are limited to 10 characters (DBF API definition).
.IP \(bu 4n
Attempts to use a reserved SQL word (depends on database backend) as
column or table name will cause a \(dqSQL syntax error\(dq.
.RE
.SH EXAMPLES
Display all vector points except for \fILAMAR\fR valley
and \fIextensive trapping\fR (brackets are superfluous in this
example):
.br
.nf
\fC
d.vect trapping_sites_points fcol=black icon=basic/diamond col=white size=13 \(rs
    where=\(dqvalley <> \(cqLAMAR\(cq OR (valley = \(cqLAMAR\(cq AND description = \(cqextensive trapping\(cq)\(dq
\fR
.fi
.PP
Select all attributes from table where \fIstr1\fR column values are not \(cqNo
Name\(cq:
.br
.nf
\fC
echo \(dqSELECT * FROM archsites WHERE str1 <> \(cqNo Name\(cq\(dq | db.select
\fR
.fi
.PP
.PP
Example of subquery expressions from a list (does not work
for DBF driver):
.br
.nf
\fC
v.db.select mysites where=\(dqid IN (\(cqP04\(cq, \(cqP05\(cq)\(dq
\fR
.fi
.PP
Example of pattern matching:
.br
.nf
\fC
# field contains string:
#  for DBF driver:
v.extract rivers out=rivers_noce where=\(dqDES LIKE \(cqNOCE\(cq\(dq
#  for SQLite driver:
v.extract rivers out=rivers_noce where=\(dqDES LIKE \(cq%NOCE%\(cq\(dq
# match exactly number of characters (here: 2), does not work for DBF driver:
v.db.select mysites where=\(dqid LIKE \(cqP__\(cq\(dq
#define wildcard:
v.db.select mysites where=\(dqid LIKE \(cqP%\(cq\(dq
\fR
.fi
.PP
Example of null handling:
.br
.nf
\fC
v.db.addcolumn map=roads col=\(dqnulltest int\(dq
v.db.update map=roads col=nulltest value=1 where=\(dqcat > 2\(dq
d.vect roads where=\(dqnulltest is null\(dq
v.db.update map=roads col=nulltest value=2 where=\(dqcat <= 2\(dq
\fR
.fi
.PP
Examples of complex expressions in updates (using v.db.*
modules):
.br
.nf
\fC
v.db.addcolumn map=roads col=\(dqexprtest double precision\(dq
v.db.update map=roads col=exprtest value=cat/nulltest
v.db.update map=roads col=exprtest value=cat/nulltest+cat where=cat=1
\fR
.fi
.PP
Examples of complex expressions in updates (using db.*
modules):
.br
.nf
\fC
echo \(dqUPDATE roads SET exprtest=null\(dq
echo \(dqUPDATE roads SET exprtest=cat/2\(dq | db.execute
echo \(dqUPDATE roads SET exprtest=cat/2+cat/3\(dq | db.execute
echo \(dqUPDATE roads SET exprtest=NULL WHERE cat>2\(dq | db.execute
echo \(dqUPDATE roads SET exprtest=cat/3*(cat+1) WHERE exprtest IS NULL\(dq | db.execute\(dq
\fR
.fi
.PP
Instead of creating and updating new columns with an expression, you
can use the expression directly in a command:
.br
.nf
\fC
d.vect roads where=\(dq(cat/3*(cat+1))>8\(dq
d.vect roads where=\(dqcat>exprtest\(dq
\fR
.fi
.PP
Example of changing a SQL type (type casting, does not work
for DBF driver):
.br
.nf
\fC
# North Carolina data set: convert string column to double precision
#  copy map into current mapset
g.copy vect=geodetic_pts,mygeodetic_pts
v.db.addcolumn mygeodetic_pts col=\(dqzval double precision\(dq
# the \(cqz_value\(cq col contains \(cqN/A\(cq strings, not to be converted
v.db.update mygeodetic_pts col=zval \(rs
            qcol=\(dqCAST(z_value AS double precision)\(dq \(rs
            where=\(dqz_value <> \(cqN/A\(cq\(dq
\fR
.fi
.PP
Example of concatenating fields (does not work for DBF
driver):
.br
.nf
\fC
v.db.update vectormap column=column3 qcolumn=\(dqcolumn1 || column2\(dq
\fR
.fi
.SH SEE ALSO
\fI
db.select,
db.execute,
v.db.select,
v.db.update
\fR
.PP
Database management in GRASS GIS,
Help pages for database modules
.PP
\fILast changed: $Date: 2014\-04\-20 12:28:15 +0200 (Sun, 20 Apr 2014) $\fR
.PP
Main index | Topics index | Keywords Index | Full index
.PP
� 2003\-2014 GRASS Development Team, GRASS GIS 7.1.svn Reference Manual
