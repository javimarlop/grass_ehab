.TH r.walk 1 "" "GRASS 7.1.svn" "Grass User's Manual"
.SH NAME
\fI\fBr.walk\fR\fR  \- Outputs a raster map showing the anisotropic cumulative cost.
.br
Computes anisotropic cumulative cost of moving between different geographic locations on an input elevation raster map whose cell category values represent elevation combined with an input raster map layer whose cell values represent friction cost.
.SH KEYWORDS
raster, cost surface, cumulative costs
.SH SYNOPSIS
\fBr.walk\fR
.br
\fBr.walk help\fR
.br
\fBr.walk\fR [\-\fBknri\fR] \fBelevation\fR=\fIname\fR \fBfriction\fR=\fIname\fR \fBoutput\fR=\fIname\fR  [\fBoutdir\fR=\fIname\fR]   [\fBstart_points\fR=\fIname\fR]   [\fBstop_points\fR=\fIname\fR]   [\fBstart_rast\fR=\fIname\fR]   [\fBstart_coordinates\fR=\fIeast,north\fR[,\fIeast,north\fR,...]]   [\fBstop_coordinates\fR=\fIeast,north\fR[,\fIeast,north\fR,...]]   [\fBmax_cost\fR=\fIinteger\fR]   [\fBnull_cost\fR=\fIfloat\fR]   [\fBpercent_memory\fR=\fIinteger\fR]   [\fBwalk_coeff\fR=\fIa,b,c,d\fR]   [\fBlambda\fR=\fIfloat\fR]   [\fBslope_factor\fR=\fIfloat\fR]   [\-\-\fBoverwrite\fR]  [\-\-\fBhelp\fR]  [\-\-\fBverbose\fR]  [\-\-\fBquiet\fR]
.SS Flags:
.IP "\fB\-k\fR" 4m
.br
Use the \(cqKnight\(cqs move\(cq; slower, but more accurate
.IP "\fB\-n\fR" 4m
.br
Keep null values in output map
.IP "\fB\-r\fR" 4m
.br
Start with values in raster map
.IP "\fB\-i\fR" 4m
.br
Only print info about disk space and memory requirements
.IP "\fB\-\-overwrite\fR" 4m
.br
Allow output files to overwrite existing files
.IP "\fB\-\-help\fR" 4m
.br
Print usage summary
.IP "\fB\-\-verbose\fR" 4m
.br
Verbose module output
.IP "\fB\-\-quiet\fR" 4m
.br
Quiet module output
.SS Parameters:
.IP "\fBelevation\fR=\fIname\fR \fB[required]\fR" 4m
.br
Name of input elevation raster map
.IP "\fBfriction\fR=\fIname\fR \fB[required]\fR" 4m
.br
Name of input raster map containing friction costs
.IP "\fBoutput\fR=\fIname\fR \fB[required]\fR" 4m
.br
Name for output raster map to contain walking costs
.IP "\fBoutdir\fR=\fIname\fR" 4m
.br
Name for output raster map to contain movement directions
.IP "\fBstart_points\fR=\fIname\fR" 4m
.br
Name of starting vector points map
.br
Or data source for direct OGR access
.IP "\fBstop_points\fR=\fIname\fR" 4m
.br
Name of stopping vector points map
.br
Or data source for direct OGR access
.IP "\fBstart_rast\fR=\fIname\fR" 4m
.br
Name of starting raster points map
.IP "\fBstart_coordinates\fR=\fIeast,north[,\fIeast,north\fR,...]\fR" 4m
.br
Coordinates of starting point(s) (E,N)
.IP "\fBstop_coordinates\fR=\fIeast,north[,\fIeast,north\fR,...]\fR" 4m
.br
Coordinates of stopping point(s) (E,N)
.IP "\fBmax_cost\fR=\fIinteger\fR" 4m
.br
Maximum cumulative cost
.br
Default: \fI0\fR
.IP "\fBnull_cost\fR=\fIfloat\fR" 4m
.br
Cost assigned to null cells. By default, null cells are excluded
.IP "\fBpercent_memory\fR=\fIinteger\fR" 4m
.br
Percent of map to keep in memory
.br
Options: \fI0\-100\fR
.br
Default: \fI100\fR
.IP "\fBwalk_coeff\fR=\fIa,b,c,d\fR" 4m
.br
Coefficients for walking energy formula parameters a,b,c,d
.br
Default: \fI0.72,6.0,1.9998,\-1.9998\fR
.IP "\fBlambda\fR=\fIfloat\fR" 4m
.br
Lambda coefficients for combining walking energy and friction cost
.br
Default: \fI1.0\fR
.IP "\fBslope_factor\fR=\fIfloat\fR" 4m
.br
Slope factor determines travel energy cost per height step
.br
Default: \fI\-0.2125\fR
.SH DESCRIPTION
\fIr.walk\fR outputs 1) a raster map showing the lowest
cumulative cost of moving between each cell and the user\-specified
starting points and 2) a second raster map showing the movement
direction to the next cell on the path back to the start point (see
Movement Direction). It uses an input elevation
raster map whose cell category values represent elevation,
combined with a second input raster map whose cell values
represent friction costs.
.PP
This function is similar to \fIr.cost\fR,
but in addiction to a friction map, it considers an anisotropic travel
time due to the different walking speed associated with downhill and
uphill movements.
.SH NOTES
.PP
The formula from Aitken 1977/Langmuir 1984 (based on Naismith\(cqs rule
for walking times) has been used to estimate the cost parameters of
specific slope intervals:
.br
.nf
\fC
T= [(a)*(Delta S)] + [(b)*(Delta H uphill)] + [(c)*(Delta H moderate downhill)] + [(d)*(Delta H steep downhill)]
\fR
.fi
where:
.RS 4n
.IP \(bu 4n
T is time of movement in seconds,
.IP \(bu 4n
Delta S is the distance covered in meters,
.IP \(bu 4n
Delta H is the altitude difference in meter.
.RE
.PP
The a, b, c, d \fBwalk_coeff\fR parameters take in account
movement speed in the different conditions and are linked to:
.RS 4n
.IP \(bu 4n
a: underfoot condition (a=1/walking_speed)
.IP \(bu 4n
b: underfoot condition and cost associated to movement uphill
.IP \(bu 4n
c: underfoot condition and cost associated to movement moderate downhill
.IP \(bu 4n
d: underfoot condition and cost associated to movement steep downhill
.RE
It has been proved that moving downhill is favourable up to a specific
slope value threshold, after that it becomes unfavourable. The default
slope value threshold (\fBslope_factor\fR) is \-0.2125, corresponding
to tan(\-12), calibrated on human behaviour (>5 and <12 degrees:
moderate downhill; >12 degrees: steep downhill). The default values
for a, b, c, d \fBwalk_coeff\fR parameters are those proposed by
Langmuir (0.72, 6.0, 1.9998, \-1.9998), based on man walking effort in
standard conditions.
.PP
The \fBlambda\fR parameter of the linear equation
combining movement and friction costs:
.br
.br
.nf
\fC
total cost = movement time cost + (lambda) * friction costs
\fR
.fi
must be set in the option section of \fIr.walk\fR.
.PP
For a more accurate result, the \(dqknight\(cqs move\(dq option can be used
(although it is more time consuming). In the diagram below, the center
location (O) represents a grid cell from which cumulative distances
are calculated. Those neighbours marked with an x are always
considered for cumulative cost updates. With the \(dqknight\(cqs move\(dq
option, the neighbours marked with a K are also considered.
.br
.nf
\fC
  K   K
K x x x K
  x O x
K x x x K
  K   K
\fR
.fi
.PP
The minimum cumulative costs are computed using Dijkstra\(cqs
algorithm, that find an optimum solution (for more details see
\fIr.cost\fR, that uses the same algorithm).
.SH Movement Direction
.PP
The movement direction surface is created to record the sequence of
movements that created the cost accumulation surface. Without it
\fIr.drain\fR would not correctly create a path from an end point
back to the start point. The direction of each cell points towards
the next cell. The directions are recorded as degrees CCW from East:
.br
.nf
\fC
       112.5      67.5         i.e. a cell with the value 135
157.5  135   90   45   22.5    means the next cell is to the north\-west
       180   x   360
202.5  225  270  315  337.5
       247.5     292.5
\fR
.fi
.PP
Once \fIr.walk\fR computes the cumulative cost map as a linear
combination of friction cost (from friction map) and the altitude and
distance covered (from the digital elevation
model), \fIr.drain\fR can be used to
find the minimum cost path. Make sure to use the \fB\-d\fR flag and
the movement direction raster map when
running \fIr.drain\fR to ensure the path
is computed according to the proper movement directions.
.SH REFERENCES
.RS 4n
.IP \(bu 4n
Aitken, R. 1977. Wilderness areas in Scotland. Unpublished Ph.D. thesis.
University of Aberdeen.
.IP \(bu 4n
Steno Fontanari, University of Trento, Italy, Ingegneria per l\(cqAmbiente e
il Territorio, 2000\-2001.
Svilluppo di metodologie GIS per la determinazione dell\(cqaccessibilità
territoriale come supporto alle decisioni nella gestione ambientale.
.IP \(bu 4n
Langmuir, E. 1984. Mountaincraft and leadership. The Scottish
Sports Council/MLTB. Cordee, Leicester.
.RE
.SH SEE ALSO
\fI
r.cost,
r.drain,
r.in.ascii,
r.mapcalc,
r.out.ascii
\fR
.SH AUTHORS
\fBBased on r.cost written by :\fR
.br
Antony Awaida, Intelligent Engineering, Systems Laboratory, M.I.T.
.br
James Westervelt, U.S.Army Construction Engineering Research Laboratory
.br
Updated for Grass 5 by Pierre de Mouveaux (pmx@audiovu.com)
.PP
\fBInitial version of r.walk:\fR
.br
Steno Fontanari, 2002
.PP
\fBCurrent version of r.walk:\fR
.br
Franceschetti Simone, Sorrentino Diego, Mussi Fabiano and Pasolli Mattia
.br
Correction by: Fontanari Steno, Napolitano Maurizio and  Flor Roberto
.br
In collaboration with: Franchi Matteo, Vaglia Beatrice, Bartucca Luisa, Fava Valentina and Tolotti Mathias, 2004
.PP
\fBUpdated for Grass 6.1:\fR
.br
Roberto Flor and Markus Neteler
.PP
\fILast changed: $Date: 2013\-01\-05 15:04:55 +0100 (Sat, 05 Jan 2013) $\fR
.PP
Main index | Raster index | Topics index | Keywords Index | Full index
.PP
© 2003\-2014 GRASS Development Team, GRASS GIS 7.1.svn Reference Manual
