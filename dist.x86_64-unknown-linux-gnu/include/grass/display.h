#ifndef GRASS_DISPLAY_H
#define GRASS_DISPLAY_H

#include <grass/gis.h>
#include <grass/raster.h>
#include <grass/symbol.h>

enum clip_mode
{
    M_NONE,
    M_CULL,
    M_CLIP,
};

#include <grass/defs/display.h>

#endif /* GRASS_DISPLAY_H */
